<?php

use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\Type;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateYearlyStaffLeavesTrackersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Type::hasType('double')) {
            Type::addType('double', FloatType::class);
        }
        Schema::table('yearly_staff_leaves_trackers', function (Blueprint $table) {
            $table->double('prev_year_bal')->default(0);
            $table->double('days_covered')->default(0)->change();
            $table->double('days_remaining')->default(0)->change();
            $table->double('max_days_allowed')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yearly_staff_leaves_trackers', function (Blueprint $table) {
            $table->dropColumn('prev_year_bal');
            $table->integer('days_covered')->default(0)->change();
            $table->integer('days_remaining')->default(0)->change();
            $table->unsignedInteger('max_days_allowed')->default(0)->change();
        });
    }
}
