<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLeaveRequestsAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_requests', function (Blueprint $table) {
            $table->dateTime('date_from')->nullable()->change();
            $table->dateTime('date_to')->nullable()->change();
            $table->dateTime('date_returned')->nullable()->change();
            $table->longText('am_approve_comment')->nullable();
            $table->longText('am_reject_reason')->nullable();
            $table->dateTime('am_action_at')->nullable();
            $table->unsignedTinyInteger('is_cse')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_requests', function (Blueprint $table) {
            $table->date('date_from')->nullable()->change();
            $table->date('date_to')->nullable()->change();
            $table->dropColumn('am_approve_comment');
            $table->dropColumn('am_reject_reason');
            $table->dropColumn('am_action_at');
            $table->dropColumn('is_cse');
        });
    }
}
