<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRequestDatesTableAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('compensation_request_dates');
        Schema::table('offduty_request_dates', function (Blueprint $table) {
            $table->unsignedTinyInteger('status')->default(0); // 0-pending, 1-approved_offduty, 2-rejected_offduty, 3-pending_compensation, 4-approved_compensation, 4-rejected_compensation
            $table->unsignedBigInteger('compensation_request_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offduty_request_dates', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('compensation_request_id');
        });
        Schema::create('compensation_request_dates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('compensation_request_id');
            $table->date('date');
            $table->timestamps();
        });
    }
}
