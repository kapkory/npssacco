<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateStaffsTableAddEmployeeNumberDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('staffs','employee_number'))
            Schema::table('staffs', function (Blueprint $table) {
                $table->dropColumn('employee_number');
            });
        Schema::table('staffs', function (Blueprint $table) {
            $table->unsignedBigInteger('dep_unique_no')->default(0);//unique per department
            $table->string('employee_number')->unique()->nullable(); //CIT~001~M20
            $table->dropColumn('unique_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staffs', function (Blueprint $table) {
            $table->dropColumn('dep_unique_no');
            $table->dropColumn('employee_number');
            $table->string('unique_id')->nullable();
            $table->string('employee_number')->nullable();
        });
    }
}
