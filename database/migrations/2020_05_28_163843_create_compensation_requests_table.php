<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompensationRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compensation_requests', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('staff_id');
			$table->longText('description')->nullable();
            $table->unsignedTinyInteger('status')->default(0); //0-pending, 1-approved, 2-rejected
            $table->unsignedTinyInteger('days')->default(0);
            $table->dateTime('approved_at')->nullable();
            $table->dateTime('rejected_at')->nullable();
			$table->unsignedBigInteger('action_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compensation_requests');
    }
}
