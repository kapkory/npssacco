<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffBeneficiariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_beneficiaries', function (Blueprint $table) {
            $table->id();
			$table->integer('staff_id');
			$table->string('name');
			$table->bigInteger('phone')->nullable();
			$table->string('email')->nullable();
			$table->string('relationship');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_beneficiaries');
    }
}
