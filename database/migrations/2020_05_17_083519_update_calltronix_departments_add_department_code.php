<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCalltronixDepartmentsAddDepartmentCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('calltronix_departments','department_code'))
            Schema::table('calltronix_departments', function (Blueprint $table) {
                $table->dropColumn('department_code');
            });
        Schema::table('calltronix_departments', function (Blueprint $table) {
            $table->string('department_code')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calltronix_departments', function (Blueprint $table) {
            $table->dropColumn('department_code');
        });
    }
}
