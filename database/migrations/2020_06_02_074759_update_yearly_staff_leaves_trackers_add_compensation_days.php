<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateYearlyStaffLeavesTrackersAddCompensationDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yearly_staff_leaves_trackers', function (Blueprint $table) {
            $table->tinyInteger('compensation_days')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yearly_staff_leaves_trackers', function (Blueprint $table) {
            $table->dropColumn('compensation_days');
        });
    }
}
