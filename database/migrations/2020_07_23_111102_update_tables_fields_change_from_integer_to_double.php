<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateTablesFieldsChangeFromIntegerToDouble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `tbl_leave_types` CHANGE `entitlement` `entitlement` DOUBLE UNSIGNED NOT NULL DEFAULT '0';");
        DB::statement("ALTER TABLE `tbl_compensation_requests` CHANGE `days` `days` DOUBLE UNSIGNED NOT NULL DEFAULT '0';");
        DB::statement("ALTER TABLE `tbl_yearly_staff_leaves_trackers` CHANGE `days_covered` `days_covered` DOUBLE UNSIGNED NOT NULL DEFAULT '0';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `tbl_leave_types` CHANGE `entitlement` `entitlement` INT UNSIGNED NOT NULL DEFAULT '0';");
        DB::statement("ALTER TABLE `tbl_compensation_requests` CHANGE `days` `days` TINYINT UNSIGNED NOT NULL DEFAULT '0';");
        DB::statement("ALTER TABLE `tbl_yearly_staff_leaves_trackers` CHANGE `days_covered` `days_covered` INT UNSIGNED NOT NULL DEFAULT '0';");
    }
}
