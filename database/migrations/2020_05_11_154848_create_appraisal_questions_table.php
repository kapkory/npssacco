<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('appraisal_question_category_id');
			$table->longText('question');
			$table->unsignedTinyInteger('status')->default(1);
			$table->unsignedInteger('position');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_questions');
    }
}
