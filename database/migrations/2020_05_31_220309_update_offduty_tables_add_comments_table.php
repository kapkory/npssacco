<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOffdutyTablesAddCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offduty_requests', function (Blueprint $table) {
            $table->longText('hod_approve_comment')->nullable();
            $table->longText('hod_reject_reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offduty_requests', function (Blueprint $table) {
            $table->dropColumn('hod_approve_comment');
            $table->dropColumn('hod_reject_reason');
        });
    }
}
