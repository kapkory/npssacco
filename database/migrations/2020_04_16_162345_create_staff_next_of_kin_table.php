<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffNextOfKinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_next_of_kin', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('staff_id');
			$table->string('nok_name');
			$table->bigInteger('nok_phone');
			$table->string('nok_email')->nullable();
			$table->string('nok_relationship');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_next_of_kin');
    }
}
