<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('calltronix_department_id')->after('department_id')->default(0);
            $table->integer('line_of_business_id')->after('department_id')->default(0);
            $table->string('middlename')->after('firstname')->nullable();
            $table->date('dob')->after('name')->nullable();
            $table->tinyInteger('is_hod')->after('calltronix_department_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['calltronix_department_id', 'line_of_business_id', 'middle_name', 'dob', 'is_hod']);
        });
    }
}
