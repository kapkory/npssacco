<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffAppraisalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_appraisals', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('staff_id');
			$table->date('review_period_from');
			$table->date('review_period_to');
			$table->longText('staff_comments')->nullable();
			$table->longText('supervisor_comments')->nullable();
			$table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_appraisals');
    }
}
