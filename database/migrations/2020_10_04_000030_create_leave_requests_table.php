<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\FloatType;
use Doctrine\DBAL\Types\Type;

class CreateLeaveRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('leave_requests');
//        if (!Type::hasType('double')) {
//            Type::addType('double', FloatType::class);
//        }
        Schema::create('leave_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('staff_user_id');
            $table->unsignedBigInteger('assuming_staff_user_id')->nullable(); //user id of the staff who'll be incharge
            $table->unsignedBigInteger('leave_type_id');
            $table->unsignedBigInteger('next_action_department_id')->nullable(); // The next department expected to act on the request
            $table->dateTime('date_from')->nullable();
            $table->dateTime('date_to')->nullable();
            $table->unsignedTinyInteger('is_halfday')->default(0);
            $table->double('days_out')->default(0);
            $table->dateTime('date_returned')->nullable(); // the exact date when the staff returned from leave
            $table->longText('reason')->nullable();
            $table->dateTime('canceled_at')->nullable();
            $table->longText('cancel_reason')->nullable();
            $table->string('contact')->nullable(); // contact while away
            $table->unsignedTinyInteger('status')->default(0); //(0-pending,1-approved, 2-declined, 5 -canceled by applicant)
            $table->unsignedTinyInteger('request_state')->default(0); //(0-pending,1-active, 2-closed)
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_requests');
    }
}
