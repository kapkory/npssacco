<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTicketColumnsRemoveCalltronix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            if (Schema::hasColumn('tickets', 'calltronix_department_id'))
                $table->dropColumn('calltronix_department_id');
            if (Schema::hasColumn('tickets', 'line_of_business_id'))
                $table->dropColumn('line_of_business_id');
            if (Schema::hasColumn('tickets', 'line_of_business_id_from'))
                $table->dropColumn('line_of_business_id_from');
            if (Schema::hasColumn('tickets', 'line_of_business_id_to'))
                $table->dropColumn('line_of_business_id_to');
            if (Schema::hasColumn('tickets', 'calltronix_department_id_from'))
                $table->renameColumn('calltronix_department_id_from', 'department_id_from');
            if (Schema::hasColumn('tickets', 'calltronix_department_id_to'))
                $table->renameColumn('calltronix_department_id_to', 'department_id_to');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            //
        });
    }
}
