<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffSpouseOrChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_spouse_or_childrens', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('staff_id');
			$table->string('name');
			$table->bigInteger('phone')->nullable();
			$table->string('relationship')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_spouse_or_childrens');
    }
}
