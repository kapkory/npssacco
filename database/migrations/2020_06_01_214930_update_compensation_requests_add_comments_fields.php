<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCompensationRequestsAddCommentsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compensation_requests', function (Blueprint $table) {
            $table->longText('hr_approve_comment')->nullable();
            $table->longText('hr_reject_reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compensation_requests', function (Blueprint $table) {
            $table->dropColumn('hr_reject_reason');
            $table->dropColumn('hr_approve_comment');
        });
    }
}
