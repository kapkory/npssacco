<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateStaffsTableAddDefaultFieldValuesAndMakeNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staffs', function (Blueprint $table) {
            if (Schema::hasColumn('staffs', 'leave_days_remaining'))
                $table->dropColumn('leave_days_remaining');
            if (Schema::hasColumn('staffs', 'leave_days_gone'))
                $table->dropColumn('leave_days_gone');

            $table->string('kra_pin')->nullable()->change();
            $table->string('medical_history')->nullable()->change();
            $table->string('residential_physical_address')->nullable()->change();
            $table->unsignedBigInteger('id_number')->nullable()->change();
        });
        DB::statement("ALTER TABLE `tbl_staffs` CHANGE `gender` `gender` TINYINT NULL DEFAULT NULL;");
        DB::statement("ALTER TABLE `tbl_staffs` CHANGE `marital_status` `marital_status` TINYINT NULL DEFAULT NULL;");
        DB::statement("ALTER TABLE `tbl_staffs` CHANGE `position_id` `position_id` TINYINT NULL DEFAULT NULL;");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staffs', function (Blueprint $table) {
            $table->string('kra_pin')->change();
            $table->string('residential_physical_address')->change();
            $table->unsignedBigInteger('id_number')->change();
            DB::statement("ALTER TABLE `tbl_staffs` CHANGE `gender` `gender` TINYINT NULL;");
            DB::statement("ALTER TABLE `tbl_staffs` CHANGE `marital_status` `marital_status` TINYINT NULL;");
            DB::statement("ALTER TABLE `tbl_staffs` CHANGE `position_id` `position_id` TINYINT NULL;");
        });
    }
}
