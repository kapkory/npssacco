<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToOnboardingChecklistItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('onboarding_checklist_items', function (Blueprint $table) {
            $table->unsignedBigInteger('item_category_id')->default(1)->after('id');
            $table->unsignedTinyInteger('is_notifiable')->default(0);
            $table->unsignedBigInteger('disposition_id')->nullable();
            $table->unsignedBigInteger('calltronix_department_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('onboarding_checklist_items', function (Blueprint $table) {
            $table->dropColumn('item_category_id');
            $table->dropColumn('is_notifiable');
            $table->dropColumn('disposition_id');
            $table->dropColumn('calltronix_department_id');
        });
    }
}
