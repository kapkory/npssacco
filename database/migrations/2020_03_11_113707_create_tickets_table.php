<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('issue_category_id');
			$table->integer('user_id');
			$table->integer('ticket_status_id');
			$table->integer('department_id');
			$table->integer('disposition_id');
			$table->integer('line_of_business_id')->nullable();
			$table->integer('assigned_to');
			$table->string('tat')->nullable();
			$table->tinyInteger('priority')->nullable();
			$table->dateTime('start_time')->nullable();
			$table->dateTime('end_time')->nullable();
			$table->string('solution')->nullable();
			$table->longText('measures')->nullable();
			$table->longText('description')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
