<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCalltronixDepartmentsAddParentDepartmentIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calltronix_departments', function (Blueprint $table) {
            $table->unsignedTinyInteger('parent_department_id')->default(0);
            $table->unsignedTinyInteger('sub_departments_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calltronix_departments', function (Blueprint $table) {
            $table->dropColumn('parent_department_id');
            $table->dropColumn('sub_departments_count');
        });
    }
}
