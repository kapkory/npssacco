<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDispositionIssueCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('disposition_issue_category'))
            Schema::create('disposition_issue_category', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('disposition_id');
                $table->integer('issue_category_id');
                $table->integer('issue_sub_category_id');
                $table->tinyInteger('is_fcr')->default(0);
                $table->tinyInteger('status')->default(1);
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disposition_issue_category');
    }
}
