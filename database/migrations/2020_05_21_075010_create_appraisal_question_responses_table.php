<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalQuestionResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_question_responses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('staff_appraisal_id');
            $table->unsignedBigInteger('appraisal_question_id');
            $table->unsignedDouble('score')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_question_responses');
    }
}
