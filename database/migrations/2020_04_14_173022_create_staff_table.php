<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('user_id');
			$table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('line_of_business_id')->default(0);
            $table->unsignedBigInteger('calltronix_department_id');
			$table->unsignedBigInteger('staff_role_id')->default(0);
			$table->unsignedTinyInteger('gender');
			$table->unsignedTinyInteger('marital_status');
			$table->unsignedBigInteger('position_id');
			$table->string('unique_id')->nullable();
			$table->unsignedBigInteger('id_number');
			$table->unsignedBigInteger('alternate_phone')->nullable();
			$table->string('postal_address')->nullable();
			$table->string('residential_physical_address');
			$table->date('employment_date');
			$table->date('end_date')->nullable();
			$table->string('bank_name')->nullable();
			$table->string('bank_branch')->nullable();
			$table->bigInteger('account_number')->nullable();
			$table->bigInteger('nssf_number')->nullable();
			$table->bigInteger('nhif_number')->nullable();
			$table->string('kra_pin');
			$table->string('employee_number')->nullable();
			$table->string('pay_roll_number')->nullable();
			$table->longText('medical_history')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
