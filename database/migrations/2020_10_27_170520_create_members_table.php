<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
			$table->integer('user_id');
			$table->integer('id_number')->nullable();
			$table->string('occupation')->nullable();
			$table->integer('role_id')->nullable();
			$table->string('employer')->nullable();
			$table->integer('county_id')->nullable();
			$table->tinyInteger('gender')->nullable();
			$table->string('location')->nullable();
			$table->string('passport_photo')->nullable();
			$table->string('id_copy')->nullable();
			$table->string('payslip')->nullable();
			$table->string('address')->nullable();
			$table->tinyInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
