<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeOnboardingChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_onboarding_checklists', function (Blueprint $table) {
            $table->id();
			$table->unsignedBigInteger('onboarding_checklist_item_id');
			$table->unsignedBigInteger('staff_id');
			$table->unsignedTinyInteger('return_status')->default(0); //(0-non_returnable,1-assigned,2-returned)
            $table->dateTime('date_returned')->nullable();
            $table->string('returned_condition')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_onboarding_checklists');
    }
}
