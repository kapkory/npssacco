<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn(['measures', 'department_id_from', 'department_id_to']);
            if (!Schema::hasColumn('tickets', 'requester_id'))
                $table->integer('requester_id')->nullable();
            if (!Schema::hasColumn('tickets', 'issue_sub_category_id'))
                $table->integer('issue_sub_category_id')->nullable();
            if (!Schema::hasColumn('tickets', 'issue_source_id'))
                $table->integer('issue_source_id')->nullable();
            if (!Schema::hasColumn('tickets', 'subject'))
                $table->string('subject')->nullable();
            if (!Schema::hasColumn('tickets', 'branch_id'))
                $table->tinyInteger('branch_id')->nullable();
            if (!Schema::hasColumn('tickets', 'department_id'))
                $table->tinyInteger('department_id')->nullable();
            if (!Schema::hasColumn('tickets', 'impact'))
                $table->tinyInteger('impact')->nullable();
            if (!Schema::hasColumn('tickets', 'closure_comment'))
                $table->longText('closure_comment')->nullable();
            if (!Schema::hasColumn('tickets', 'escalation_sent'))
                $table->tinyInteger('escalation_sent')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            //
        });
    }
}
