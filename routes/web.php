<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    \Illuminate\Support\Facades\Auth::loginUsingId(1);
    return view('auth.login');
});

Route::view('backends','layouts.backend');
Route::get('counties/list', 'GuestController@listCounties');
Route::post('register-user','Auth\CustomAuthController@registerUser');
Route::post('login-user','Auth\CustomAuthController@login');
//Auth::routes();

Auth::routes(['register' => true,'verify' => true]);
Route::group(['middleware'=>['auth','web','verified','member']],function(){
    Route::get('home', 'HomeController@index');
//    /**
//     * Admin Routes
//     */
//    Route::group(
//        ['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['role:admin']], function () {
//        Route::get('/', ['uses' => 'AdminController@dashboard']);
//        Route::group(
//            ['namespace' => 'Config', 'prefix' => 'config'], function () {
//            Route::get('departments/list', 'DepartmentsController@listDepartments');
//            Route::get('issuecategories/list', 'IssueCategoriesController@listIssueCategories');
//            Route::get('lineofbusiness/list', 'LineOfBusinessesController@listLineOfBusinesses');
//            Route::get('disposition/list', 'DispositionsController@listDispositions');
//            $status = "TicketStatusController@";
//            Route::get('ticket-status/',$status.'index');
//            Route::post('ticket-status/',$status.'storeTicketStatus');
//            Route::get('ticket-status/list/',$status.'listTicketStatuses');
//            Route::delete('ticket-status/delete/',$status.'destroyTicketStatus');
//        });
//
//        Route::group(
//            ['namespace' => 'Tickets', 'prefix' => 'tickets'], function () {
//            Route::get('/', 'TicketsController@index');
//            Route::post('/', 'TicketsController@storeTicket');
//            Route::get('/create', 'TicketsController@create');
//            Route::get('/list', 'TicketsController@listTickets');
//            Route::group(
//                ['namespace' => 'Ticket', 'prefix' => 'ticket'], function () {
//                $controller = "TicketController@";
//                Route::get('/{id}', $controller . 'view');
//                Route::get('details/{id}', $controller . 'getTicketDetails');
//                Route::get('update/{id}', $controller . 'updateTicket');
//            });
//
//        });
//
//        Route::group(
//            ['namespace' => 'Users', 'prefix' => 'users'], function () {
//            Route::get('/', 'UsersController@index');
//            Route::get('/list', 'UsersController@listUsers');
//        });
//    });
//    /**
//     * User routes
//     */
//    Route::group(['namespace' => 'Member', 'prefix' => 'member', 'as' => 'member.', 'middleware' => ['role:member']], function () {
//        Route::get('/', ['uses' => 'MemberController@index'])->name('dashboard');
//        Route::group(
//            ['namespace' => 'Tickets'], function () {
//            Route::get('users/', 'TicketsController@index');
//            Route::any('tickets/create/{phone?}', 'TicketsController@createTicket');
//        });
//    });

    $routes_path = base_path('routes');
    $route_files = File::allFiles(base_path('routes'));
    foreach ($route_files as $file) {
        $path = $file->getPath();
        if ($path != $routes_path) {
            $file_name = $file->getFileName();
            $prefix = str_replace($file_name, '', $path);
            $prefix = str_replace($routes_path, '', $prefix);
            $file_path = $file->getPathName();
            $this->route_path = $file_path;
            $arr = explode('/', $prefix);
            $len = count($arr);
            $main_file = $arr[$len - 1];
            $arr = array_map('ucwords', $arr);
            $arr = array_filter($arr);
            $ext_route = str_replace('index.route.php', '', $file_name);
            $ext_route = str_replace($main_file, '', $ext_route);
            $ext_route = str_replace('.route.php', '', $ext_route);
            $ext_route = str_replace('web', '', $ext_route);
            if ($ext_route)
                $ext_route = '/' . $ext_route;
            Route::group(['namespace' => implode('\\', $arr), 'prefix' => strtolower($prefix . $ext_route)], function () {
                require $this->route_path;
            });
        }
    }
});



