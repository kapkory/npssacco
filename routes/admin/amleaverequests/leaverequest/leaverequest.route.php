<?php
$controller = "LeaveRequestController@";
Route::get('{id}',$controller.'index')->where('id', '[0-9]+');
Route::get('details/{id}',$controller.'getLeaveRequestDetails')->where('id', '[0-9]+');
Route::post('approve',$controller.'approveLeaveRequest');
Route::post('reject',$controller.'rejectLeaveRequest');
