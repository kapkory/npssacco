<?php
$controller = "LeaveRequestsController@";
Route::get('/',$controller.'index');
Route::get('/list/{status}',$controller.'listLeaveRequestsByStatus');
Route::get('/staff/list/{staff_id}',$controller.'listStaffLeaveRequests');
Route::get('/list',$controller.'listLeaveRequests');
