<?php
$controller = "OffdutyRequestsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeOffdutyRequest');
Route::get('/list',$controller.'listOffdutyRequests');
Route::delete('/delete/{id}',$controller.'destroyOffdutyRequest');
