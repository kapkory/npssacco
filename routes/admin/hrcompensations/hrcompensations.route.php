<?php
$controller = "CompensationRequestsController@";
Route::get('/',$controller.'index');
Route::get('/get-compensation-form',$controller.'returnCompensationFormView');
Route::post('/',$controller.'storeCompensationRequest');
Route::get('/list',$controller.'listCompensationRequests');
Route::delete('/delete/{compensationrequest}',$controller.'destroyCompensationRequest');

Route::get('/list/{status}',$controller.'listCompensationRequestsByStatus');
