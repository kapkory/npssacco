<?php
$controller = "CompensationRequestController@";
Route::get('edit/{id}',$controller.'returnEditView');
Route::get('get-details/{id}',$controller.'returnRequestDetailsView');

Route::get('{id}',$controller.'index')->where('id', '[0-9]+');
Route::get('details/{id}',$controller.'getCompensationRequestDetails')->where('id', '[0-9]+');
Route::post('approve',$controller.'approveCompensationRequest');
Route::post('reject',$controller.'rejectCompensationRequest');
