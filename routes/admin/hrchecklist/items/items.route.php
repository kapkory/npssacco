<?php
$controller = "HrChecklistItemsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeOnboardingChecklistItem');
Route::post('item-section',$controller.'storeItemSection');
Route::get('/list',$controller.'listOnboardingChecklistItems');
Route::get('/item-sections/list',$controller.'listItemSections');
Route::delete('/delete/{id}',$controller.'destroyOnboardingChecklistItem');


