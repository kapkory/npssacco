<?php
$controller = "HrExitChecklistController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'store');
Route::get('/list',$controller.'list');
Route::delete('/delete/{}',$controller.'destroy');

//staff items routes
Route::get('staff-items-details/{user_id}',$controller.'getStaffItemsData');
Route::get('staff-items-details-list/{staff_id}',$controller.'listStaffAssignedReturnableItemDetails');
Route::post('staff-items-details-return/{staff_id}',$controller.'returnStaffAssignedItem');
