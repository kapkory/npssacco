<?php
$controller = "HrOnboardingChecklistController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'store');
Route::get('/list',$controller.'list');
Route::delete('/delete/{id}',$controller.'destroy');

//staff items routes
Route::get('staff-items-details/{user_id}',$controller.'getStaffItemsData');
Route::get('staff-items-details-list/{staff_id}',$controller.'listStaffAssignedItemDetails');
Route::get('staff-items-details-list-unassigned/{staff_id}',$controller.'listStaffUnAssignedItemDetails');
Route::post('staff-items-details-add/{staff_id}',$controller.'storeStaffAssignedItem');
Route::delete('/staff-items-details-remove/{id}',$controller.'removeEmployeeOnboardingChecklist');
