<?php
$controller = "ItemCategoriesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeItemCategory');
Route::get('/items/{item_category_id}',$controller.'viewItems');
Route::get('/assign-staff-items/{staff_id}',$controller.'assignStaffItems');
Route::get('/items/list/{item_category_id}',$controller.'listItemCategoryItems');
Route::get('/list',$controller.'listItemCategories');
Route::delete('/delete/{id}',$controller.'destroyItemCategory');


