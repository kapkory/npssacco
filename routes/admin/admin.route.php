<?php
$controller = "AdminController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeUser');
Route::get('/list',$controller.'listUsers');
Route::delete('/delete/{user}',$controller.'destroyUser');