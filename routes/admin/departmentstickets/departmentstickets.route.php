<?php
$controller = "DepartmentTicketsController@";
Route::get('created/{department_id}',$controller.'viewCreatedDepartmentTickets');
Route::get('assigned/{department_id}',$controller.'viewAssignedDepartmentTickets');

Route::get('created/list/{department_id}',$controller.'listCreatedTickets');
Route::get('assigned/list/{department_id}',$controller.'listAssignedTickets');
