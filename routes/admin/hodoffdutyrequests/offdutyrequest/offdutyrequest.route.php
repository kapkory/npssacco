<?php
$controller = "OffdutyRequestController@";
Route::get('edit/{id}',$controller.'returnEditView');
Route::get('get-details/{id}',$controller.'returnRequestDetailsView');

Route::get('{id}',$controller.'index')->where('id', '[0-9]+');
Route::get('details/{id}',$controller.'getOffdutyRequestDetails')->where('id', '[0-9]+');
Route::post('approve',$controller.'approveOffdutyRequest');
Route::post('reject',$controller.'rejectOffdutyRequest');
