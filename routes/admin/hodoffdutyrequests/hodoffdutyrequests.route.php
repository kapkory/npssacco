<?php
$controller = "OffdutyRequestsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeOffdutyRequest');
Route::get('/list',$controller.'listOffdutyRequests');
Route::get('/list/{status}',$controller.'listOffdutyRequestsByStatus');
Route::delete('/delete/{id}',$controller.'destroyOffdutyRequest');
