<?php
$controller = "StaffExitsController@";
Route::get('/', $controller . 'index');
Route::post('/', $controller . 'storeStaffExitRequest');
Route::get('/list', $controller . 'listDismissedStaff');
//Route::delete('/delete/{id}',$controller.'destroy');
