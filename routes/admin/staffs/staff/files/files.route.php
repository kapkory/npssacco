<?php
$controller = "FilesController@";
Route::post('/tmp',$controller.'uploadTmp');
Route::delete('/delete/{id}',$controller.'deleteFile');
Route::get('/download/{id}',$controller.'downloadFile');
Route::post('/upload',$controller.'uploadFile');
Route::get('list',$controller.'listFiles');
Route::post('client_feedback',$controller.'listFiles');
Route::get('/downloadAll/{id}', $controller.'downloadAll');
Route::post('/allow-client/{id}', $controller.'allowClient');
Route::post('/deny-client/{id}', $controller.'denyClient');
