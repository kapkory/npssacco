<?php
$controller = "StaffController@";
Route::get('/{id}', $controller . 'index');
Route::get('update-profile/{id}', $controller . 'updateProfileView');
Route::post('update-profile/{id}', $controller . 'updateProfile');
Route::post('update-contacts/{id}', $controller . 'updateContactDetails');
Route::post('update-statutorydocs/{id}', $controller . 'updateStatutoryDocs');
Route::post('update-staff-state/{id}', $controller . 'toggleStaffAvailableStatus');

//discharge staff
Route::post('discharge/{id}', $controller . 'dischargeStaff');
