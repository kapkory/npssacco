<?php
$controller = "StaffsController@";
Route::get('/',$controller.'index');
Route::get('/create',$controller.'createView');
Route::post('/',$controller.'storeStaff');
Route::get('/list',$controller.'listStaff');
Route::get('my-department/list',$controller.'listMyDepartmentStaffs');
Route::delete('/delete/{staff}',$controller.'destroyStaff');
