<?php
$controller = "TicketController@";
$controller1 = "TicketFilesController@";
Route::get('/{id}',$controller.'view');
Route::get('details/{id}',$controller.'getTicketDetails');
Route::get('update/{id}',$controller.'updateTicket');
Route::get('acknowledge/{id}',$controller.'acknowledgeTicket');
Route::get('reassign/{id}',$controller.'reassignTicket');
Route::get('update-modal/{id}',$controller.'updateTicketModal');
Route::post('cancel/{id}',$controller.'cancelTicket');
Route::get('files/list/{id}',$controller1.'listTicketFiles');
Route::get('file/download/{id}',$controller1.'downloadFile');
