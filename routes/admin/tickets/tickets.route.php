<?php
$controller = "TicketsController@";
Route::get('/',$controller.'index');
Route::get('create',$controller.'createTicket');
Route::post('/',$controller.'storeTicket');
Route::post('customers/add',$controller.'storeCustomer');
//Route::get('/list',$controller.'listTickets');
Route::get('/list',$controller.'listTickets');
Route::get('/list/status/{status_id}',$controller.'listTicketsByStatus')->where('status_id','[1-9]+');
Route::get('/export',$controller.'exportTickets');
Route::delete('/delete/{ticket}',$controller.'destroyTicket');
Route::post('tmp', $controller.'saveTmpFiles');
Route::post('tmp/delete', $controller.'deleteTmpFiles');
