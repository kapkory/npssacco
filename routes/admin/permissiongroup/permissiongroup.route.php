<?php
$controller = "PermissionGroupController@";
Route::get('/',$controller.'index');
//Route::get('/permissions/{id}',$controller.'getPermissions');
Route::get('/permissions/{id}',$controller.'getDepartmentPermissions');
Route::get('/view/{id}',$controller.'viewDepartment');
Route::post('/permissions/{id}',$controller.'updatePermissions');
Route::post('/user',$controller.'addUser');
Route::post('/user/remove/{id}',$controller.'removeUser');
Route::post('/',$controller.'storeDepartment');
Route::get('/list',$controller.'listDepartments');
Route::get('/list/users',$controller.'listUsers');
Route::get('/get/users',$controller.'getUsers');
