<?php
$controller = "LeaveRequestsController@";
Route::get('/',$controller.'index');
Route::get('/list/{status}',$controller.'listLeaveRequestsByStatus');
Route::get('/staff/list/{staff_id}',$controller.'listStaffLeaveRequests');
Route::get('/list',$controller.'listLeaveRequests');

Route::get('/hrleavetracker',$controller.'hrLeaveTracker');
Route::get('/viewleavetracker',$controller.'viewLeaveTracker');
Route::post('/update-leaves/{id}',$controller.'updateLeaveTracker');
Route::get('/staffleavehistory/{staff_id}',$controller.'viewStaffLeaveHistory');

//leave requests dashboard
Route::get('dashboard',$controller.'viewLeaveRequestsDashboard');
Route::get('/dashboard/list/returningtoday',$controller.'listStaffLeavesReturningToday');
Route::get('/dashboard/list/notreturned',$controller.'listStaffLeavesNotYestReturned');
Route::get('/dashboard/list/count',$controller.'getReturningLeavesTabCounts');
