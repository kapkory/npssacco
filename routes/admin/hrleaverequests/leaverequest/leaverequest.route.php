<?php
$controller = "LeaveRequestController@";
Route::get('{id}',$controller.'index')->where('id', '[0-9]+');
Route::get('details/{id}',$controller.'getLeaveRequestDetails')->where('id', '[0-9]+');
Route::post('approve',$controller.'approveLeaveRequest');
Route::post('reject',$controller.'rejectLeaveRequest');
Route::post('returned',$controller.'setStaffLeaveRequestReturnDetails');
Route::get('days-covered/{id}',$controller.'calculateDaysCovered');
Route::get('record/{staff_id}',$controller.'addLeaveRecord');
Route::post('record',$controller.'postLeaveRecord');
