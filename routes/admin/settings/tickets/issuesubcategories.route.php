<?php
//issue sub categories
$issue_sub_catogory ="IssueSubCategoriesController@";
Route::get('/',$issue_sub_catogory.'index');
Route::post('/',$issue_sub_catogory.'storeIssueSubCategory');
Route::get('/list',$issue_sub_catogory.'listIssueSubCategories');
Route::get('/dispositions/{id}',$issue_sub_catogory.'manageDispositionView');
Route::post('/dispositions/save',$issue_sub_catogory.'saveDisposition');
Route::get('/list/dispositions',$issue_sub_catogory.'listIssueSubCategoriesDispositions');
//dispositions
