<?php
$status = "TicketStatusController@";
Route::get('/', $status . 'index');
Route::post('/', $status . 'storeTicketStatus');
Route::get('/list', $status . 'listTicketStatuses');
Route::delete('/delete/', $status . 'destroyTicketStatus');
