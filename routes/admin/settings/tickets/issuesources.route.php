<?php
$issue_source = "IssueSourcesController@";
Route::get('/',$issue_source.'index');
Route::post('/',$issue_source.'storeIssueSource');
Route::get('/list/',$issue_source.'listIssueSources');
Route::delete('/delete/',$issue_source.'destroyIssueSource');

