<?php
$departments ="CalltronixDepartmentsController@";
Route::get('departments/',$departments.'index');
Route::get('departments/roles/{id}',$departments.'departmentRolesView');
Route::post('departments/roles',$departments.'saveDepartmentRole');
Route::get('departments/heads/list',$departments.'listDepartmentHeads');
Route::post('departments/heads',$departments.'saveDepartmentHeads');
Route::post('departments/heads/remove/{id}',$departments.'removeDepartmentHead');
Route::get('departments/roles/list/{id}',$departments.'listDepartmentRoles');
Route::post('departments/',$departments.'storeDepartment');
Route::get('departments/list/',$departments.'listDepartments');
Route::delete('departments/delete/',$departments.'destroyDepartment');
