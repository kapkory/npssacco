<?php
$controller = "BranchesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeBranch');
Route::get('/list',$controller.'listBranches');
Route::delete('/delete/{branch}',$controller.'destroyBranch');