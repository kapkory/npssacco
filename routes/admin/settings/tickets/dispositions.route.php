<?php
$disposition = "DispositionsController@";
Route::get('/',$disposition.'index');
Route::post('/',$disposition.'storeDisposition');
Route::get('list/',$disposition.'listDispositions');
Route::delete('delete/',$disposition.'destroyDisposition');
Route::get('search/',$disposition.'SearchDispositions');
