<?php
//issue categories
$issue_catogory ="IssueCategoriesController@";
Route::get('/',$issue_catogory.'index');
Route::post('/',$issue_catogory.'storeIssueCategory');
Route::delete('delete/{id}',$issue_catogory.'destroyIssueCategory');
Route::get('/list',$issue_catogory.'listIssueCategories');

Route::get('issuesubcategories/{id}',$issue_catogory.'manageIssueSubCategoryView');
Route::post('issuesubcategories',$issue_catogory.'saveIssueSubCategory');
Route::get('dispositions/{id}',$issue_catogory.'manageDispositionView');
Route::post('dispositions',$issue_catogory.'saveDisposition');
Route::get('dispositions/{id}',$issue_catogory.'manageDispositionView');
Route::get('dispositions/ajax/get',$issue_catogory.'getDispositions');
Route::post('dispositions',$issue_catogory.'saveDisposition');
Route::get('list/dispositions',$issue_catogory.'listIssueCategoriesDispositions');
Route::get('list/issuesubcategories',$issue_catogory.'listIssueSubCategories');
