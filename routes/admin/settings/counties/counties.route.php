<?php
$controller = "CountiesController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeCounty');
Route::get('/list',$controller.'listCounties');
Route::delete('/delete/{county}',$controller.'destroyCounty');