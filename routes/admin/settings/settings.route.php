<?php
$controller = "SettingsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'store');
Route::get('/list',$controller.'list');
Route::delete('/delete/{id}',$controller.'destroy');
