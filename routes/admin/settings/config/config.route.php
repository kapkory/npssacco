<?php
$controller = "ConfigController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'store');
Route::get('/list',$controller.'list');
Route::delete('/delete/{id}',$controller.'destroy');
//issue categories
$issue_catogory ="IssueCategoriesController@";
Route::get('issuecategories',$issue_catogory.'index');
Route::post('issuecategories',$issue_catogory.'storeIssueCategory');
Route::delete('issuecategories/delete/{id}',$issue_catogory.'destroyIssueCategory');
Route::get('/issuecategories/list',$issue_catogory.'listIssueCategories');
Route::get('issuecategories/dispositions/{id}',$issue_catogory.'manageDispositionView');
Route::get('issuecategories/dispositions/ajax/get',$issue_catogory.'getDispositions');
Route::post('issuecategories/dispositions/save',$issue_catogory.'saveDisposition');
Route::get('issuecategories/list/dispositions',$issue_catogory.'listIssueCategoriesDispositions');
Route::get('issuecategories/list/issuesubcategories',$issue_catogory.'listIssueSubCategories');

//issue sub categories
$issue_sub_catogory ="IssueSubCategoriesController@";
Route::get('issuesubcategories',$issue_sub_catogory.'index');
Route::post('issuesubcategories',$issue_sub_catogory.'storeIssueSubCategory');
Route::get('/issuesubcategories/list',$issue_sub_catogory.'listIssueSubCategories');
Route::get('issuesubcategories/dispositions/{id}',$issue_sub_catogory.'manageDispositionView');
Route::post('issuesubcategories/dispositions/save',$issue_sub_catogory.'saveDisposition');
Route::get('issuesubcategories/list/dispositions',$issue_sub_catogory.'listIssueSubCategoriesDispositions');
//dispositions
$disposition = "DispositionsController@";
Route::get('dispositions/',$disposition.'index');
Route::post('dispositions/',$disposition.'storeDisposition');
Route::get('dispositions/list/',$disposition.'listDispositions');
Route::delete('dispositions/delete/',$disposition.'destroyDisposition');
Route::get('dispositions/search/',$disposition.'SearchDispositions');

$shopType = "ShopTypesController@";
Route::get('shop-types/',$shopType.'index');
Route::post('shop-types/',$shopType.'storeShopType');
Route::get('shop-types/list/',$shopType.'listShopTypes');
Route::delete('shop-types/delete/',$shopType.'destroyShopType');

$shops = "ShopsController@";
Route::get('shops/',$shops.'index');
Route::post('shops/',$shops.'storeShop');
Route::get('shops/list/',$shops.'listShops');
Route::delete('shops/delete/',$shops.'destroyShop');

$issue_source = "IssueSourceController@";
Route::get('issue-source/',$issue_source.'index');
Route::post('issue-source/',$issue_source.'storeIssueSource');
Route::get('issue-source/list/',$issue_source.'listIssueSources');
Route::delete('issue-source/delete/',$issue_source.'destroyIssueSource');


$status = "TicketStatusController@";
Route::get('ticket-status/',$status.'index');
Route::post('ticket-status/',$status.'storeTicketStatus');
Route::get('ticket-status/list/',$status.'listTicketStatuses');
Route::delete('ticket-status/delete/',$status.'destroyTicketStatus');

$departments ="DepartmentsController@";
Route::get('departments/',$departments.'index');
Route::get('departments/roles/{id}',$departments.'departmentRolesView');
Route::post('departments/roles',$departments.'saveDepartmentRole');
Route::get('departments/heads/list',$departments.'listDepartmentHeads');
Route::post('departments/heads',$departments.'saveDepartmentHeads');
Route::post('departments/heads/remove/{id}',$departments.'removeDepartmentHead');
Route::get('departments/roles/list/{id}',$departments.'listDepartmentRoles');
Route::post('departments/',$departments.'storeDepartment');
Route::get('departments/list/',$departments.'listDepartments');
Route::delete('departments/delete/',$departments.'destroyDepartment');


$lob ="LineOfBusinessesController@";
Route::get('lineofbusiness/',$lob.'index');
Route::post('lineofbusiness/',$lob.'storeLineOfBusiness');
Route::get('lineofbusiness/list/',$lob.'listLineOfBusinesses');
Route::delete('lineofbusiness/delete/',$lob.'destroyLineOfBusiness');

$leaveType = "LeaveTypesController@";
Route::get('leavetypes/',$leaveType.'index');
Route::post('leavetypes/',$leaveType.'storeLeaveType');
Route::get('leavetypes/list/',$leaveType.'listLeaveTypes');
Route::delete('leavetypes/delete/',$leaveType.'destroyLeaveType');

$appraisalQuestionCategory = "AppraisalQuestionCategoriesController@";
Route::get('appraisalquestioncategories/',$appraisalQuestionCategory.'index');
Route::get('appraisalquestioncategories/view/{category_id}',$appraisalQuestionCategory.'viewQuestionCategory');
Route::post('appraisalquestioncategories/',$appraisalQuestionCategory.'storeQuestionCategory');
Route::get('appraisalquestioncategories/list/',$appraisalQuestionCategory.'listQuestionCategories');
Route::delete('appraisalquestioncategories/delete/',$appraisalQuestionCategory.'destroyQuestionCategory');

$appraisalQuestions = "AppraisalQuestionsController@";
Route::get('appraisalquestions/',$appraisalQuestions.'index');
Route::get('appraisalquestions/view/{question_id}',$appraisalQuestions.'viewQuestion');
Route::post('appraisalquestions/',$appraisalQuestions.'storeQuestion');
Route::get('appraisalquestions/list/',$appraisalQuestions.'listQuestions');
Route::delete('appraisalquestions/delete/',$appraisalQuestions.'destroyQuestion');
