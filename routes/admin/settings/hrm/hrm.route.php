<?php
$controller = "HrmConfigController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'store');
Route::get('/list',$controller.'list');
Route::delete('/delete/{id}',$controller.'destroy');


$positions = "PositionsController@";
Route::get('/positions',$positions.'index');
Route::post('/positions',$positions.'storePosition');
Route::get('positions/list',$positions.'listPositions');
Route::delete('positions/delete/{position}',$positions.'destroyPosition');
