<?php
$controller = "UsersController@";
Route::get('/',$controller.'index');
Route::get('list',$controller.'listUsers');
Route::get('/user/{id}',$controller.'viewUser');
Route::get('auth-by-user-id/{id}',$controller.'authenticateByUserId');
Route::post('/user/update',$controller.'userProfile');
Route::get('/user/token/{id}',$controller.'resendToken');
Route::post('/',$controller.'saveUser');
Route::post('/user/update-password',$controller.'updatePassword');

Route::delete('/delete/{user}',$controller.'destroyUser');
Route::post('/import',$controller.'importUsers');
