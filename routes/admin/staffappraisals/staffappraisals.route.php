<?php
$controller = "StaffAppraisalsController@";
Route::get('/',$controller.'index');
Route::post('/',$controller.'storeStaffAppraisal');
Route::get('/staff/get-data/{staff_id}',$controller.'getStaffItemsData');
Route::get('/staff/list/{staff_id}',$controller.'listStaffAppraisals');
Route::get('/list',$controller.'listAllStaffAppraisals');
Route::delete('/delete/{staff_appraisal}',$controller.'destroyStaffAppraisal');
