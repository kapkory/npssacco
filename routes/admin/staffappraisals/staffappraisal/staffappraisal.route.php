<?php
$controller = "StaffAppraisalController@";
Route::get('/questions/{staff_appraisal_id}/question/{position?}',$controller.'viewQuestion')->where('staff_appraisal_id', '[0-9]+');;
Route::get('/questions/{staff_appraisal_id}/results',$controller.'viewStaffAppraisalResults')->where('staff_appraisal_id', '[0-9]+');;
Route::get('/get-next-question/{question_position}',$controller.'getNextQuestion')->where('question_position', '[0-9]+');;
Route::post('/',$controller.'storeStaffAppraisal');
Route::get('/staff/get-data/{staff_id}',$controller.'getStaffItemsData');
Route::get('/staff/list/{staff_id}',$controller.'listStaffAppraisals');
Route::get('/list',$controller.'listAllStaffAppraisals');
Route::delete('/delete/{staff_appraisal}',$controller.'destroyStaffAppraisal');
Route::post('question',$controller.'storeQuestionResponse');
