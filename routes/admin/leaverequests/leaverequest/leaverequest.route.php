<?php
$controller = "LeaveRequestController@";
Route::get('{id}',$controller.'index')->where('id', '[0-9]+');
Route::get('details/{id}',$controller.'getLeaveRequestDetails');
Route::get('update/{id}',$controller.'updateLeaveRequest');
Route::post('cancel/{id}',$controller.'cancelLeaveRequest');
