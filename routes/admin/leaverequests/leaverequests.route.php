<?php
$controller = "LeaveRequestsController@";
Route::get('/',$controller.'index');
Route::get('create',$controller.'createLeaveRequest');
Route::post('/',$controller.'storeLeaveRequest');
Route::get('/list',$controller.'listMyLeaveRequests');
Route::get('/export',$controller.'exportLeaveRequests');
Route::delete('/delete/{leave}',$controller.'destroyLeaveRequest');
