<?php
$controller = "VacanciesController@";
Route::get('/',$controller.'index');
Route::get('create',$controller.'newVacancy');
Route::post('/',$controller.'storeVacancy');
Route::get('/list',$controller.'listVacancies');
Route::delete('/delete/{vacancy}',$controller.'destroyVacancy');
