<?php
$controller = "UserController@";
Route::get('profile',$controller.'profile');
Route::post('profile',$controller.'updateInitialUser');
Route::post('changecampaign',$controller.'changeCampaign');
Route::get('state/{state}',$controller.'changeUserState');
Route::put('profile/{id}',$controller.'updateProfile');
Route::post('profile-picture',$controller.'updateProfilePicture');
Route::post('password',$controller.'updatePassword');

Route::get('leaves',$controller.'myLeaves');
Route::get('leaves-details/list',$controller.'listUserLeavesDetails');
