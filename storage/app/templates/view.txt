@extends('layouts.admin')

@section('title') {umodels} @endsection

@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <a href="#{model}_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD {cmodel}</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["id",{model_fields},"action"],
                    'data_url'=>'{route_url}/list',
                    'base_tbl'=>'{models}'
                    ])
                 </div>

               </div>

            </div>
        </div>
@endsection

@section('sidebar')
  @include('common.auto_modal',[
        'modal_id'=>'{model}_modal',
        'modal_title'=>'{cmodel} FORM',
        'modal_content'=>autoForm(\App\{model_namespace}::class,"{route_url}")
    ])
@endsection

