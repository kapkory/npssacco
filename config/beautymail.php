<?php

return [

    // These CSS rules will be applied after the regular template CSS

    /*
        'css' => [
            '.button-content .button { background: red }',
        ],
    */

    'colors' => [

        'highlight' => '#e6eaed;',
        'button' => '#ffb833;',
//        'button'    => '#004cad',

    ],

    'view' => [
        'senderName'  => null,
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => null,

        'logo'        => [
            'path'   => env('APP_URL').'/theme/assets/images/Calltronix-Animation .gif',
            'width'  => '',
            'height' => '40',
        ],

        'twitter'  => null,
        'facebook' => null,
        'flickr'   => null,
    ],

];
