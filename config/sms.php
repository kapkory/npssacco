<?php


return [

    'default' => env('SMS_GATEWAY', 'at'),

    'connections' => [
        'at' => [
            'username' =>  env('AT_SMS_USERNAME','twiga'),
            'from' => env('AT_SMS_FROM',NULL),
            'apiKey' => env('AT_SMS_APIKEY', '7ccebf4f2a90eb73529a7bd985a9cc606e8d8e19314471f7aa7ef912db84287f'),
            'environment' => env('AT_SMS_ENVIRONMENT','production'),
        ],
    ]
];
