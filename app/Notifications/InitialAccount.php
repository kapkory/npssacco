<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class InitialAccount extends Notification implements ShouldQueue
{
    use Queueable;
    protected $token;
    protected $user_name;
    protected $email;
    protected $password_reset;
    protected $url;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token, $url, $user_name, $password_reset, $email)
    {

        $this->token = $token;
        $this->email = $email;
        $this->user_name = $user_name;
        $this->password_reset=$password_reset;
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if ($this->password_reset==0){
            return (new MailMessage)
                ->greeting('Dear ' . $this->user_name . '!')
                ->subject('Initial Account Password Reset')
                ->line('You are receiving this email because a new account has been created for you, click the button below to set your new password.')
                ->action('Set New Password', url($this->url))
                ->line('This set new password link will expire in 60 minutes..')
                ->line('If you did not request an account creation. Please ignore this message.');
        }else{
            return (new MailMessage)
                ->greeting('Dear ' . $this->user_name . ' !')
                ->subject('Reset Password Email')
                ->line('You are receiving this email because we received a password reset request for your account.')
                ->action('Reset Password', url($this->url))
                ->line('This password reset link will expire in 60 minutes.')
                ->line('If you did not request a password reset, no further action is required.');
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
