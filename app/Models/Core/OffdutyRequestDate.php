<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class OffdutyRequestDate extends Model
{
    protected $fillable = ['offduty_request_id','date','status'];
    public function offdutyRequest(){
        return $this->belongsTo(OffdutyRequest::class);
    }
    public function compensationRequest(){
        return $this->belongsTo(CompensationRequest::class);
    }
}
