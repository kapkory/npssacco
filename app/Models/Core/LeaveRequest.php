<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveRequest extends Model
{

    use HasFactory;
    protected $fillable = ['deleted_at', 'staff_user_id', 'assuming_staff_user_id', 'leave_type_id', 'next_action_department_id', 'is_halfday', 'date_from', 'date_to', 'date_returned', 'reason', 'cancel_reason', 'canceled_at','contact', 'status', 'request_state','days_out'];

    public function leaveType()
    {
        return $this->belongsTo(LeaveType::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
