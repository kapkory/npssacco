<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

	protected $fillable = ["name","description","slug", "permissions", "user_id"];

	public function users(){
	    return $this->hasMany(User::class);
    }

}
