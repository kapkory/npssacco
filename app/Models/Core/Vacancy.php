<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{

	protected $fillable = ["name","description","department_id","position_id","application_deadline","status","user_id"];

}
