<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class CalltronixDepartmentHead extends Model
{

	protected $fillable = ["user_id","department_id","status"];

}
