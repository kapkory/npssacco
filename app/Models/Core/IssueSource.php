<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class IssueSource extends Model
{

	protected $fillable = ["user_id","name"];

}
