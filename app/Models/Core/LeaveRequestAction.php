<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveRequestAction extends Model
{
    use HasFactory;
    protected $fillable = ['leave_request_id','action_by_user_id','action','comment','action_at'];
}
