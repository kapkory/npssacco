<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class StaffNextOfKin extends Model
{
    protected $table="staff_next_of_kin";

	protected $fillable = ["staff_id","nok_name","nok_phone","nok_email","nok_relationship"];

}
