<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class CalltronixDepartment extends Model
{

	protected $fillable = ["name","department_code","description","staff_count","user_id","parent_department_id","sub_departments_count"];

}
