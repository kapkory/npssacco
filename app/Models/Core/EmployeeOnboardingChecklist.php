<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class EmployeeOnboardingChecklist extends Model
{
    
	protected $fillable = ["onboarding_checklist_item_id","staff_id","date_returned","return_status","returned_condition","user_id"];

}
