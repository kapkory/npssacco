<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class TicketFile extends Model
{
    protected $fillable = ["ticket_id","name","description","size","path","user_id"];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getFileNameAttribute()
    {
        return Str::limit($this->name, 30);

    }
}
