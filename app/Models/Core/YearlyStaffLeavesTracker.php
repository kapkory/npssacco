<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class YearlyStaffLeavesTracker extends Model
{
    protected $fillable = ['staff_id','leave_type_id','year','days_covered','days_remaining','max_days_allowed', 'prev_year_bal'];
}
