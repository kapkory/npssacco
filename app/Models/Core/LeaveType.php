<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    protected $fillable = ['name','description','entitlement','is_paid'];

    public function leaveRequests()
    {
        return $this->hasMany(LeaveRequest::class);
    }
}
