<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
 {
	protected $fillable = ["user_id","id_number","occupation","role_id","employer","county_id","gender","location","passport_photo","id_copy","payslip","address","created_by"];

    use HasFactory;
}
