<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class IssueSubCategory extends Model
{

	protected $fillable = ["name","user_id","status"];

	public function category()
    {
        return $this->belongsTo(IssueCategory::class);
    }

}
