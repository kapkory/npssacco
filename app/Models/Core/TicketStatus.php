<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class TicketStatus extends Model
{
    
	protected $fillable = ["user_id","name"];

}
