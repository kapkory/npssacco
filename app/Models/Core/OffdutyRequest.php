<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class OffdutyRequest extends Model
{

    protected $fillable = ["staff_id","description","status","days","approved_at","rejected_at","action_by"];
    public function offdutyRequestDates()
    {
        return $this->hasMany(OffdutyRequestDate::class);
    }
}
