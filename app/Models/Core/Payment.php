<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

	protected $fillable = ["subscriber_id","subscriber_packages","amount","payment_method","requested_amount","transaction_reference","status"];


}
