<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class StaffRole extends Model
{

	protected $fillable = ["department_id","name"];

}
