<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class County extends Model
 {
	protected $fillable = ["name","code","capital"];

    use HasFactory;
}
