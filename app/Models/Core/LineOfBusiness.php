<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;

class LineOfBusiness extends Model
{

    protected $fillable = ["name", "description", "user_id"];

    public function accountManager()
    {
        return $this->hasOne(AccountManager::class);
    }

    public function getAccountManager()
    {
        return User::join('account_managers', 'account_managers.user_id', 'users.id')
            ->where('account_managers.line_of_business_id', $this->id)
            ->first();
    }

    /**
     * update Account manager
     */
    public function updateAccountManager($account_manager_id = null)
    {
        if($account_manager_id){
            AccountManager::updateOrCreate(['user_id'=>$account_manager_id,'line_of_business_id'=>$this->id],[
                'user_id'=>$account_manager_id,
                'line_of_business_id'=>$this->id
            ]);
        }
        else
            AccountManager::where('line_of_business_id',$this->id)->delete();
        return;
    }
}
