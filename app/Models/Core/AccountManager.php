<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class AccountManager extends Model
{
    protected $fillable = ['line_of_business_id','user_id','status'];
    public function lineOfBusiness()
    {
        return $this->belongsTo(LineOfBusiness::class);
    }
}
