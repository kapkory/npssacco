<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class File extends Model
 {
	protected $fillable = ["name","size","path","mime_type","model_id","model_slug_id","user_id"];

    use HasFactory;
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getFileNameAttribute()
    {
        return Str::limit($this->name, 30);

    }
}
