<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveApprovalLevel extends Model
{
    use HasFactory;
    protected $fillable = ['department_id','approving_department_id','position','user_id','status'];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
