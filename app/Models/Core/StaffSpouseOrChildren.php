<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class StaffSpouseOrChildren extends Model
{

	protected $fillable = ["staff_id","name","phone","relationship"];

}
