<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class DispositionIssueCategory extends Model
{
    protected $table="disposition_issue_category";
	protected $fillable = ["disposition_id","department_id","issue_category_id", "call_source", "call_type", "issue_sub_category_id", "is_fcr"];

    public function issueCategory()
    {
        return $this->belongsTo(IssueCategory::class,'issue_category_id','id');
    }

}
