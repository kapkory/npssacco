<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class AppraisalQuestion extends Model
{
    
	protected $fillable = ["question","user_id","status"];

}
