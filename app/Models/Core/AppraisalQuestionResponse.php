<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class AppraisalQuestionResponse extends Model
{
    protected $fillable = ['staff_appraisal_id','appraisal_question_id','score'];
}
