<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionGroup extends Model
{
    use HasFactory;

    protected $fillable = ["name","description","slug", "permissions", "user_id"];

    public function users(){
        return $this->hasMany(User::class);
    }
}
