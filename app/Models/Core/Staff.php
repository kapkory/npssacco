<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = "staffs";
//    protected $casts = ['employment_date'];
    protected $fillable = ["created_by", "employee_number", "department_id", "staff_role_id", "user_id", "marital_status", "gender",
        "position_id", "unique_id", "id_number", "alternate_phone", "postal_address", "residential_physical_address",
        "employment_date", "employment_status", "dismissal_date", "bank_name", "bank_branch", "account_number", "nssf_number", "nhif_number", "line_of_business_id",
        "kra_pin", "pay_roll_number", "medical_history","annual_leave_days_last_sync_date"];


public static function validateEmployeeData($request)
    {
        $request->validate([
            'firstname' => 'required',
            'middlename' => 'nullable',
            'lastname' => 'required',
            'dob' => 'required',
            'id_number' => 'required',
            'email' => 'required',
            'marital_status' => 'nullable',
            'employment_date' => 'required',
            'position_id' => 'required',
            'department_id' => 'required',
            'staff_role_id' => 'required',
            'phone' => ['nullable', 'unique:users', 'regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
            'residential_physical_address' => 'nullable',
            'bank_name' => 'nullable',
            'bank_branch' => 'nullable',
            'account_number' => 'nullable',
            'kra_pin' => 'nullable',
            'nhif_number' => 'nullable',
            'nssf_number' => 'nullable',
            'gender' => 'required',
        ]);
    }

    public static function validateNokData($request)
    {

    }

    public static function emptyElementExists($arr){
        return array_search(null, $arr) !== false;
    }

    public function nextOfKins(){
        return $this->hasMany(StaffNextOfKin::class);
    }

    public function beneficiaries(){
        return $this->hasMany(StaffBeneficiary::class);
    }

    public function staffSpouses(){
        return $this->hasMany(StaffSpouseOrChildren::class);
    }

    public function files(){
        return $this->hasMany(File::class);
    }

    public function getStaffLeaveRequests(){
        return LeaveRequest::where('user_id',$this->user_id)->get();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * return user's calltronix
     * department details
     */
    public function getCalltronixDepartment($staff_id = null)
    {
        $department_id = $this->department_id;
        return CalltronixDepartment::whereId($department_id)->first();
    }
}
