<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class OnboardingChecklistItem extends Model
{

    protected $fillable = ["item_category_id","name","description","is_returnable","disposition_id","is_notifiable"];

    public function itemCategory()
    {
        return $this->belongsTo(ItemCategory::class);
    }
}
