<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{

	protected $fillable = ["name","description","status","user_id"];

}
