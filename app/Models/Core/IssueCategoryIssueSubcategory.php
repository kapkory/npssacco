<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class IssueCategoryIssueSubcategory extends Model
{

    protected $table="issue_category_issue_sub_category";
    protected $fillable = ["issue_category_id","issue_sub_category_id", "status"];

}
