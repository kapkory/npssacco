<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class StaffAppraisal extends Model
{

	protected $fillable = ["staff_id","review_period_from","review_period_to","staff_comments","supervisor_comments","status","user_id"];

}
