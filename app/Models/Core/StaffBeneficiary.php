<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class StaffBeneficiary extends Model
{

	protected $fillable = ["staff_id","name","phone","email","relationship"];

}
