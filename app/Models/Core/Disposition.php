<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Disposition extends Model
{

	protected $fillable = ["department_id","tat","name", "description"];

	public function issueCategories(){
	    return $this->belongsToMany(IssueCategory::class)->withTimestamps();;
    }

}
