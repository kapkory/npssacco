<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    protected $fillable = ['name','description'];

    public function onboardingChecklistItems()
    {
        return $this->hasMany(OnboardingChecklistItem::class);
    }
}
