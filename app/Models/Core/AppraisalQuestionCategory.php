<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class AppraisalQuestionCategory extends Model
{
    
	protected $fillable = ["name","description","user_id"];

}
