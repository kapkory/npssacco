<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    
	protected $fillable = ["name","description","status","user_id"];

}
