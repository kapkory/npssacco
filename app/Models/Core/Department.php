<?php

namespace App\Models\Core;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    protected $fillable = ["name","department_code","description","staff_count","user_id","parent_department_id","sub_departments_count"];

    public function leaveApprovalLevels()
    {
        return $this->hasMany(LeaveApprovalLevel::class);
    }
}
