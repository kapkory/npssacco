<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class CompensationRequest extends Model
{

    protected $fillable = ["staff_id", "description", "status", "days", "approved_at", "rejected_at", "action_by"];

    public function requestDates()
    {
        return $this->hasMany(OffdutyRequestDate::class, 'compensation_request_id');
    }
}
