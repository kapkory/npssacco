<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class IssueCategory extends Model
{

    protected $fillable = ["name"];

    public function dispositions()
    {
        return $this->belongsToMany(Disposition::class)->withTimestamps()->withPivot('issue_sub_category_id');
    }

    public function issueSubCategories()
    {
        return $this->belongsToMany(IssueSubCategory::class)->withTimestamps();
    }
}
