<?php

namespace App\Models\Core;

use Illuminate\Database\Eloquent\Model;

class StaffExit extends Model
{
    protected $fillable = ['user_id','staff_user_id','discharge_reason','discharged_at','status'];
}
