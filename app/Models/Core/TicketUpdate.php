<?php

namespace App\Models\Core;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TicketUpdate extends Model
{

    protected $fillable = ["user_id", "ticket_id", "issue_category_id", "issue_source_id", "assigned_to", "disposition_id", "ticket_status_id", "tat", "comment", "status"];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function Assigned()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    public function disposition()
    {
        return $this->belongsTo(Disposition::class);
    }

    public function TicketStatus()
    {
        return $this->belongsTo(TicketStatus::class);
    }


    public static function getAllTickets($count = false)
    {
        $can_view_all_tickets = false;
        if (userCan('view_all_system_tickets'))
            $can_view_all_tickets = true;
        $user = auth()->user();
        $is_hod = DepartmentHead::where('user_id', $user->id)->first();

        if ($can_view_all_tickets)
            $tickets = Ticket::query()
                ->leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
                ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
                ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
                ->leftjoin('users', 'users.id', 'tickets.user_id')
                ->leftjoin('users as assigned_user', 'assigned_user.id', 'tickets.assigned_to')
                ->leftjoin('departments', 'departments.id', 'tickets.department_id_from')
//
                ->where('tickets.status', 1);
        elseif ($is_hod)
            $tickets = Ticket::query()
                ->leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
                ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
                ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
                ->leftjoin('users', 'users.id', 'tickets.user_id')
                ->leftjoin('users as assigned_user', 'assigned_user.id', 'tickets.assigned_to')
                ->leftjoin('departments', 'departments.id', 'tickets.department_id_from')
//
                ->where([
                    ['tickets.department_id_from', $is_hod->department_id],
                    ['tickets.status', 1]
                ])
                ->orWhere([
                    ['tickets.department_id_to', $is_hod->department_id],
                    ['tickets.status', 1]
                ])
                ->orWhere([
                    ['tickets.user_id', $user->id],
                    ['tickets.status', 1]
                ])
                ->orWhere([
                    ['tickets.assigned_to', $user->id],
                    ['tickets.status', 1]
                ]);
        else
            $tickets = Ticket::query()
                ->leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
                ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
                ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
                ->leftjoin('users', 'users.id', 'tickets.user_id')
                ->leftjoin('users as assigned_user', 'assigned_user.id', 'tickets.assigned_to')
                ->leftjoin('departments', 'departments.id', 'tickets.department_id_from')
//
                ->where([
                    ['tickets.status', 1],
                    ['tickets.user_id', $user->id]
                ])
                ->orWhere([
                    ['tickets.status', 1],
                    ['tickets.assigned_to', $user->id]
                ]);
//            ->whereDate('tickets.updated_at', Carbon::today())

        if ($count)
            return $tickets->count();

        return $tickets->select('tickets.*', 'departments.name as department',  'tickets.ticket_status_id',
            'tickets.disposition_id', 'tickets.id as ticket_id', 'tickets.id as id', 'tickets.user_id as user_id', 'users.name as created_by',
            'assigned_user.name as assigned_to', 'issue_categories.name as issue_category', 'dispositions.name as disposition', 'tickets.ticket_status_id as ticket_status_id ',
            'ticket_statuses.name as status')
            ->orderBy('tickets.id', 'desc');
    }

    public static function getAllTicketsByStatus($status_id, $count = false)
    {
        $can_view_all_tickets = false;
        if (userCan('view_all_system_tickets'))
            $can_view_all_tickets = true;
        $user = auth()->user();
        $is_hod = DepartmentHead::where('user_id', $user->id)->first();

        if ($can_view_all_tickets)
            $tickets = Ticket::query()
                ->leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
                ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
                ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
                ->leftjoin('users', 'users.id', 'tickets.user_id')
                ->leftjoin('users as assigned_user', 'assigned_user.id', 'tickets.assigned_to')
                ->leftjoin('departments', 'departments.id', 'tickets.department_id_from')
                ->where([
                    ['ticket_statuses.id', $status_id],
                    ['tickets.status', 1]
                ]);
        elseif ($is_hod)
            $tickets = Ticket::query()
                ->leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
                ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
                ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
                ->leftjoin('users', 'users.id', 'tickets.user_id')
                ->leftjoin('users as assigned_user', 'assigned_user.id', 'tickets.assigned_to')
                ->leftjoin('departments', 'departments.id', 'tickets.department_id_from')

                ->where([
                    ['tickets.department_id_from', $is_hod->department_id],
                    ['ticket_statuses.id', $status_id],
                    ['tickets.status', 1]
                ])
                ->orWhere([
                    ['tickets.department_id_to', $is_hod->department_id],
                    ['ticket_statuses.id', $status_id],
                    ['tickets.status', 1]
                ])
                ->orWhere([
                    ['tickets.user_id', $user->id],
                    ['ticket_statuses.id', $status_id],
                    ['tickets.status', 1]
                ])
                ->orWhere([
                    ['tickets.assigned_to', $user->id],
                    ['ticket_statuses.id', $status_id],
                    ['tickets.status', 1]
                ]);
        else
            $tickets = Ticket::query()
                ->leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
                ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
                ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
                ->leftjoin('users', 'users.id', 'tickets.user_id')
                ->leftjoin('users as assigned_user', 'assigned_user.id', 'tickets.assigned_to')
                ->leftjoin('departments', 'departments.id', 'tickets.department_id_from')
                ->where([
                    ['tickets.user_id', $user->id],
                    ['ticket_statuses.id', $status_id],
                    ['tickets.status', 1]
                ])
                ->orWhere([
                    ['tickets.assigned_to', $user->id],
                    ['ticket_statuses.id', $status_id],
                    ['tickets.status', 1]
                ]);

        if ($count)
            return $tickets->count();

        return $tickets->select('tickets.*', 'departments.name as department',  'tickets.ticket_status_id',
            'tickets.disposition_id', 'tickets.id as ticket_id', 'tickets.id as id', 'tickets.user_id as user_id', 'users.name as created_by',
            'assigned_user.name as assigned_to', 'issue_categories.name as issue_category', 'dispositions.name as disposition', 'tickets.ticket_status_id as ticket_status_id ',
            'ticket_statuses.name as status')
            ->orderBy('tickets.id', 'desc');
    }
}
