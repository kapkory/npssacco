<?php

namespace App\Jobs;

use App\Models\Core\LeaveType;
use App\Models\Core\Staff;
use App\Models\Core\YearlyStaffLeavesTracker;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SyncStaffAvailableAnnualLeaveDays implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $all_staff;
    protected $staff_user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($all_staff = false, $staff_user_id = null)
    {
        $this->all_staff = $all_staff;
        $this->staff_user_id = $staff_user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $all_staff = $this->all_staff;
        $staff_user_id = $this->staff_user_id;
        if ($all_staff) {
            $staff = User::join('staffs', 'staffs.user_id', 'users.id')->where('staffs.employment_status', '<>', getStaffEmploymentStatus('dismissed'))->select('users.id as user_id')->get();
            foreach ($staff as $staff_user) {
                $this->syncStaffAvailableAnnualLeaveDays($staff_user->user_id);
            }
        } else {
            if ($staff_user_id)
                $this->syncStaffAvailableAnnualLeaveDays($staff_user_id);
        }
        return;
    }

    /**
     * calculate and increment
     * staff annual leave days based on the
     * month they joined ->each month = 1.75 days.
     */
    public function syncStaffAvailableAnnualLeaveDays($user_id)
    {
        $staff = @Staff::where('user_id', $user_id)->first();
        $leave_type_id = getAnnualLeaveTypeId();
        $annual_leave_type = @LeaveType::whereId($leave_type_id)->first();
        if (!$annual_leave_type)
            return redirect()->back()->with('notice', ['type' => 'error', 'message' => "Couldn't sync available leave days. Please report this to the IT team."]);

        $days_count = 0;
//        $last_synced_at = $staff->annual_leave_days_last_sync_date;
//        $last_synced_at = null;
//        $months_date_from = ($last_synced_at) ? $last_synced_at : $staff->employment_date;
        $last_synced_at = $staff->annual_leave_days_last_sync_date;
        if ($last_synced_at == null && date('Y', strtotime($staff->employment_date)) < date('Y')){
            $last_synced_at = Carbon::today()->copy()->startOfYear();
        } elseif ($last_synced_at == null && date('Y', strtotime($staff->employment_date)) == date('Y')) {
            $last_synced_at = $staff->employment_date;
        } else $last_synced_at = $last_synced_at;
        $months_date_from = $last_synced_at;
        $months_date_to = Carbon::today();

        if ($months_date_from) {
            $months_date_from = Carbon::parse($months_date_from);
            $months_count = $months_date_to->diffInMonths($months_date_from);
            if ($months_count > 0) {
//                $new_date = $months_date_from->addMonths($months_count);
                $new_date = Carbon::now();
                $staff->annual_leave_days_last_sync_date = $new_date;
                $staff->save();
                $days_count = $months_count * 1.75;
            }
        }

        $data = [
            'staff_id' => $staff->id,
            'leave_type_id' => $leave_type_id,
            'year' => date('Y'),
        ];

        $data1 = $data;
        $staff_yearly_data = YearlyStaffLeavesTracker::where([
            'staff_id' => $staff->id,
            'leave_type_id' => $leave_type_id,
            'year' => date('Y'),
        ])->first();
        $entitled_days = $staff_yearly_data ? $staff_yearly_data->max_days_allowed : 0;

        if (@$staff->annual_leave_days_last_sync_date)
            $entitled_days += $days_count;
        else
            $entitled_days = $days_count;

        $data1['max_days_allowed'] = $entitled_days;

        if ($staff_yearly_data) {
            $staff_yearly_data->update($data1);
        } else {
            $prev_staff_yearly_data = YearlyStaffLeavesTracker::where([
                'staff_id' => $staff->id,
                'leave_type_id' => $leave_type_id,
                'year' => date('Y') - 1,
            ])->first();
            $days_remaining = $prev_staff_yearly_data ? $prev_staff_yearly_data->days_remaining : 0;
            $staff_yearly_data = YearlyStaffLeavesTracker::create([
                'staff_id' => $data1['staff_id'],
                'leave_type_id' => $data1['leave_type_id'],
                'year' => $data1['year'],
                'max_days_allowed' => $data1['max_days_allowed'],
                'days_remaining' => $days_remaining,
                'prev_year_bal' => $days_remaining,
            ]);
        }
//
        if ($staff_yearly_data) {
            //Update days remaining : ($maximum days allowed + $previous year balance) - $days covered
            $remaining_days = ($staff_yearly_data->max_days_allowed + $staff_yearly_data->prev_year_bal) - $staff_yearly_data->days_covered;
            $staff_yearly_data->days_remaining = $remaining_days;
            $staff_yearly_data->save();
        }
        return 0;
    }
}
