<?php

namespace App\Jobs;

use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\Ticket;
use App\Models\Core\TicketUpdate;
use Carbon\CarbonInterval;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Snowfire\Beautymail\Beautymail;

class TicketMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $ticket_id;
    protected $is_initial;
    protected $is_acknowledgement;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($ticket_id, $is_initial, $is_acknowledgement=0)
    {
        $this->ticket_id = $ticket_id;
        $this->is_initial = $is_initial;
        $this->is_acknowledgement = $is_acknowledgement;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $title_type = "Ticket Updates";
        $ticket = Ticket::leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
            ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
            ->leftjoin('users as users_1', 'users_1.id', 'tickets.user_id')
            ->leftjoin('users as users_2', 'users_2.id', 'tickets.assigned_to')
            ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
            ->leftjoin('departments', 'departments.id', 'tickets.department_id')
            ->leftjoin('line_of_businesses', 'tickets.line_of_business_id', 'line_of_businesses.id')
            ->where([
                ['tickets.id', $this->ticket_id],
            ])->select('tickets.*', 'departments.name as department', 'line_of_businesses.name as LOB', 'users_2.id as assigned_id', 'users_1.name as created_by', 'users_2.name as assigned_to', 'issue_categories.name as issue_category',
                'users_1.email as creator_email', 'users_1.name as creator_name','users_2.email as assignee_email','users_2.email as assignee_name', 'dispositions.name as disposition', 'ticket_statuses.name as status')->first();
        if ($this->is_initial) {
            $message = "Kindly note the enclosed ticket has just been created and assigned to you. ";
            $time=0;
            $title_type = "Ticket Creation";
        } else {
            $message = "The Enclosed ticket is updated as below :-";

            if($ticket->tat){
                $time = CarbonInterval::minute($ticket->tat)->cascade()->forHumans();
                $time = "<b>" . $time . "</b>";
            }


        }

        if($this->is_acknowledgement){
            $message = "We acknowledge the receipt of your ticket and is updated as below:-";
            $title_type = "Ticket Acknowledgement";
        }
        $subject = "Calltronix Intranet >> Ticket Number CC" . $ticket->id." | ".$ticket->issue_category;
        //get ticket description
        $description = @TicketUpdate::where('ticket_id', $ticket->id)->first()->comment;
        //get hod email
        $hod = CalltronixDepartmentHead::join('users', 'users.id', 'department_heads.user_id')
            ->where([
                ['department_heads.department_id', '=', $ticket->department_id]
            ])->select('users.email', 'users.name')->first();

        //if ticket is being acknowledge send email to the creator and copy the assignee and hod
        if($this->is_acknowledgement){
            $to_email = $ticket->creator_email;
            $cc_emails= [$ticket->assignee_email];
            $name=($ticket->creator_name) ? $ticket->creator_name : "System";
            $reply_to = $ticket->assignee_email;
            if($hod)
                if($hod->email !=$ticket->assignee_email)
                    array_push($cc_emails, $hod->email);

        }else{
            //check if current user is creator
            if(request()->user()->email==@$ticket->creator_email){
                $to_email = $ticket->assignee_email;
                $cc_emails = [$ticket->creator_email];
                $reply_to=$ticket->creator_email;
                $name=  $ticket->assigned_to;
                if($hod)
                    if($hod->email !=$ticket->assignee_email)
                        array_push($cc_emails, $hod->email);


            }else{
                $cc_emails = [$ticket->assignee_email];
                $to_email = ($ticket->creator_email) ? $ticket->creator_email : $ticket->assignee_email;
                $reply_to=$ticket->assignee_email;
                $name= ($ticket->creator_name) ? $ticket->creator_name : "System";
                if($hod)
                    if($hod->email !=$ticket->assignee_email)
                        array_push($cc_emails, $hod->email);
            }
        }
        $data = [
            'subject' => $subject,
            'to_email'=>$to_email,
            'cc_emails'=>$cc_emails,
            'assigned_to'=> $ticket->assigned_to,
            'is_acknowledgement'=>$this->is_acknowledgement,
            'name' => $name,
            'reply_to'=>$reply_to,
            'time'=>@$time,
            'message' => @$message,
            'title_type' => @$title_type,
            'description'=>@$description,
            'link' => url('admin/tickets/ticket/' . $ticket->id)
        ];

        $mail = app()->make(Beautymail::class);
        $files = $ticket->ticketFiles;

        $mail->send('emails.ticket', ['data' => $data, 'ticket' => $ticket], function ($message) use ($data, $ticket, $cc_emails,$files) {
            $message->to($data['to_email'],  $data['name'])
                ->cc($cc_emails)
                ->replyTo($data['reply_to'])
                ->subject($data['subject']);

            if(count($files) > 0) {
                foreach($files as $file) {
                    $message->attach(storage_path('app/'.$file->path));
                }
            }
        });

        return 0;

    }
}
