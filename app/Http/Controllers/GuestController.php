<?php

namespace App\Http\Controllers;

use App\Models\Core\CalltronixDepartment;
use App\Models\Core\County;
use App\Repositories\SearchRepo;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * return department values
     */
    public function listDepartments()
    {
        $departments = CalltronixDepartment::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $departments->get();
        return SearchRepo::of($departments)
            ->addColumn('action', function ($department) {
                $str = '';
                $json = json_encode($department);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'department_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str .= '&nbsp;| &nbsp;<a href="' . url('admin/settings/config/departments/roles/' . $department->id) . '"  class="btn badge btn-dark btn-sm load-page"><i class="fa fa-user-secret"></i> Roles</a>';
                $str .= '&nbsp;|&nbsp;<a href="' . url('admin/settings/config/issuecategories/dispositions/' . $department->id) . '"  class="btn badge btn-success btn-sm load-page text-uppercase"><i class="fas fa-cogs"></i> Ticket Dispositions</a>';


                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/departments/delete').'\',\''.$department->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }
public function listCounties(){
    $counties = County::where([
        ['id', '>', 0]
    ]);
    if (\request('all'))
        return $counties->get();
}
}
