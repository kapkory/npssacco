<?php

namespace App\Http\Controllers\Admin\Users;

use App\Mail\SendPassword;
use App\Models\Core\CalltronixDepartment;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\LeaveRequest;
use App\Models\Core\LineOfBusiness;
use App\Models\Core\OffdutyRequest;
use App\Models\Core\Position;
use App\Models\Core\Ticket;
use App\Models\Core\TicketUpdate;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Core\Staff;
use App\Notifications\InitialAccount;
use App\Notifications\SendPasswordNotification;
use App\Repositories\SearchRepo;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        return view($this->folder . '.index');
    }

    public function saveUser()
    {
        //TODO::check for duplicates
        \request()->merge(['phone' => $this->formatPhone(\request()->phone)]);


        request()->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['nullable', 'unique:users', 'regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'department_id' => 'required',
            'permission_group_id' => 'required'
        ]);


        $user_data = [
            'firstname' => request()->first_name,
            'lastname' => request()->last_name,
            'name' => request()->first_name . ' ' . request()->last_name,
            'email' => request()->email,
            'phone' => request()->phone,
            'department_id' => request()->permission_group_id,
            'password' => request()->password,
            'role' => 'admin',
            'form_model' => User::class,
            'email_verified_at' => date("Y-m-d H:i:s")
        ];
        $user = $this->autoSaveModel($user_data);
        event(new Registered($user));
        $token = app('auth.password.broker')->createToken($user);
        $url = "password/reset/" . $token;
        try {
            $notification = new InitialAccount($token, $url, $user->name, 0, $user->email);
            $user->notify($notification);
        } catch (\Exception $e) {
            $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return back()->with('notice', $notice);
        }

        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'User saved successfully']);
    }

    protected function formatPhone($phone)
    {
        $len = strlen($phone);
        if ($len == 10) {
            $phone = "repl" . $phone;
            $phone = str_replace('repl07', '+2547', $phone);
        }
        if ($len == 12) {
            $phone = '+' . $phone;
        }

        return $phone;
    }

    public function resendToken($id)
    {
        $user = User::findOrFail($id);
        $token = app('auth.password.broker')->createToken($user);
        $url = "password/reset/" . $token;

        try {
            $notification = new InitialAccount($token, $url, $user->name, 1, $user->email);
            $user->notify($notification);
        } catch (\Exception $e) {
            $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return back()->with('notice', $notice);
        }
        return back()->with('notice', ['type' => 'success', 'message' => 'Password reset link has been emailed successfully']);
    }

    public static function resendPasswordToken($id)
    {
        $user = User::find($id);
        $token = app('auth.password.broker')->createToken($user);
        $url = "password/reset/" . $token;

        try {
            $notification = new InitialAccount($token, $url, $user->name, 1, $user->email);
            $user->notify($notification);
        } catch (\Exception $e) {
            $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return back()->with('notice', $notice);
        }
        return back()->with('notice', ['type' => 'success', 'message' => 'Password reset link has been emailed successfully']);
    }

    public function listUsers()
    {
        if (\request()->department_id) {
            return User::where('id', '>', 0)->get();

        } elseif (\request()->requester) {
            return User::where('id', '>', 0)->get();
        } else {
            $users = User::leftJoin('permission_groups', 'permission_groups.id', 'users.permission_group_id')
                ->leftJoin('departments', 'departments.id', 'users.department_id')
//                ->where('users.is_available',1)
                ->select('users.*', 'permission_groups.name as permission_group', 'departments.name as department')
                ->groupBy('users.id');
        }


        if (\request('all'))
            return $users->get();

        return SearchRepo::of($users)
            ->addColumn('is_staff', function ($user) {
                if ($user->is_staff == 1)
                    return "Yes <span data-toggle='tooltip' title='Is a Staff'  class='text-success'><i class='fa fa-check-circle'></i></span>";
                return "No <span data-toggle='tooltip' title='Not a Staff' class='text-warning'><i class='fa fa-exclamation-circle'></i></span>";
            })
            ->addColumn('permission_group', function ($user) {
//                if ($user->department)
//                    return $user->department->name;
                return "NOT ASSIGNED";
            })
            ->addColumn('action', function ($user) {

                $json = json_encode($user);
                $str = '<div class="btn-group">';
                $str .= '<a href="' . url("admin/users/user/$user->id") . '"   class="btn badge btn-success btn-sm load-page text-uppercase"><i class="fa fa-eye"></i> View</a> &nbsp;';
                $str .= '&nbsp;<a href="' . url("admin/users/user/token/$user->id") . '"   class="btn badge btn-dark btn-sm  text-uppercase"><i class="fa fa-lock"></i> Send Password Token</a>&nbsp;';
//                if (auth()->user()->email == "daniel.tarus@calltronix.com" || auth()->user()->email == "allan.kirui@calltronix.com"){
                $str .= '&nbsp;<a href="' . url('admin/users/auth-by-user-id/' . $user->id) . '" class="btn badge btn-outline-warning btn-sm"><i class="fa fa-unlock"></i> Login</a>';
//                    $str .= '&nbsp;<a href="#" onclick="deleteItem(\'' . url('admin/users/delete') . '\',\'' . $user->id . '\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';

//                }

                $str .= '</div>';
                return $str;
            })->make();


    }

    public function viewUser($id)
    {
        $user = User::leftJoin('departments', 'departments.id', 'users.department_id')
            ->where('users.id', $id)
            ->select('users.*', 'departments.name as department')
            ->first();

        return view($this->folder . 'profile', compact('user'));

    }

    public function userProfile()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'nullable',
        ]);
        if (\request()->phone)
            request()->validate([
                'phone' => ['unique:users,phone,' . \request('user_id'), 'regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/']
            ]);
        $user = User::find(\request('user_id'));
        $user->name = \request('name');
        $user->email = \request('email');
        $user->phone = \request('phone');
        $user->department_id = \request('department_id');

        $user->save();
        return back()->with('notice', ['type' => 'success', 'message' => 'User Profile Updated Successfully']);
    }

    public function updatePassword()
    {
        request()->validate([
            'password' => 'required|confirmed',
        ]);

        $user = User::find(\request('user_id'));

        $user->password = Hash::make(request('password'));
        $user->update();
        $password = request('password');

        $data = [
            'subject' => 'New Password For Calltronix CRM ',
            'message' => 'Your Calltronix CRM new password is a below',
            'password' => $password,
            'instruction' => 'Please use the password as it appears.',
            'user_name' => $user->name,
            'user_email' => $user->email,
        ];
        try {
            Mail::to($user->email)->send(new SendPassword($data));
        } catch (\Exception $e) {
            $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
            return redirect()->back()->with('notice', ['type' => 'error', 'message' => $mail_error_message]);
        }
//        $user->notify(new SendPasswordNotification($password));
        return back()->with('notice', ['type' => 'success', 'message' => 'Password updated Successfully']);
    }

    /**
     * delete vacancy
     */
    public function destroyUser($user_id)
    {
        $user = User::findOrFail($user_id);
        $user->delete();
        $staff = Staff::where('user_id', $user_id)->first();
        if ($staff) {
            $staff->delete();
            LeaveRequest::where('staff_id', $staff->id)->delete();
            Ticket::where('user_id', $user_id)->delete();
            TicketUpdate::where('user_id', $user_id)->delete();
            OffdutyRequest::where('staff_id', $staff->id)->delete();
        }
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'User record deleted successfully']);
    }

    /**
     * import users
     * from the excel file
     */
    public function importUsers()
    {
        request()->validate([
            'file' => 'required'
        ]);
        $extensions = ["xls", "xlsx", "xlm", "xla", "xlc", "xlt", "xlw"];
        $ext = [request()->file('file')->getClientOriginalExtension()];
        if (!in_array($ext[0], $extensions))
            return response(['errors' => ['file' => ['Invalid file format. The file must be a file of type: xls, xlm, xla, xlc, xlt, xlw, xlsx.']]], 422);


        if (request()->hasFile('file')) {
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', 3000);
            $filedata = Excel::toArray(true, request()->file('file'));

            $dataarray = @$filedata[0];
//            dd($dataarray,$filedata);
            if (count($dataarray) > 0) {
                $rows_count = 0;

                foreach ($dataarray as $index => $row) {
                    $fields_count = count(array_filter($row));

                    if ($index > 1 && $fields_count > 10) {
                        $number = $row[0];
                        $surname = $row[1];
                        $other_names = $row[2];
                        $gender = $row[3];
                        $title = $row[4];
                        $department = $row[5];
                        $unit = $row[6];
                        $id_num = $row[7];
                        $employment_date = $row[8];
                        $kra_pin = $row[9];
                        $nssf = $row[10];
                        $nhif = $row[11];
                        $phone = $row[12];
                        $email = $row[13];
                        $department_id = @$this->getCalltronixDepartment($department);
//                        $this->formatDateField($employment_date);
//dd("gender: ".$gender," Title: ".$title,"Department: ".$department," Unit: ".$unit," ID Num: ".$id_num," Employment Date: ".$employment_date," KRA Pin: ".$kra_pin," NSSF: ".$nssf," NHIF: ".$nhif," Phone: ".$phone," Email: ".$email);
                        $data = [];
//                            //User
                        $data['firstname'] = $surname;
                        $data['lastname'] = $other_names;
                        $data['department_id'] = @$department_id;
                        $data['name'] = $surname . " " . $other_names;
                        $data['email'] = @$email;
                        $data['department_id'] = 7;
                        $data['phone'] = ($phone) ? trim($phone) : null;
                        $user = $this->syncUserData($data);

                        if ($user) {
                            // staff details
                            $staff_data = [];
                            $staff_data['user_id'] = $user->id;
                            $staff_data['line_of_business_id'] = $this->getStaffLineOfBusiness($unit);
                            $staff_data['created_by'] = auth()->id();
                            $staff_data['gender'] = $this->getGenderInt($gender);
                            $staff_data['employment_date'] = $this->formatDateField($employment_date);
                            $staff_data['phone'] = ($phone) ? trim($phone) : null;
                            $staff_data['id_number'] = @$id_num;
                            $staff_data['department_id'] = @$department_id;
                            $staff_data['kra_pin'] = @$kra_pin;
                            $staff_data['nhif_number'] = @$nhif;
                            $staff_data['pay_roll_number'] = @$number;
                            $staff_data['nssf_number'] = @$nssf;
                            $staff_data['position_id'] = @$this->getStaffPosition($title);;
//                            $staff_data['staff_role_id'] = 0;
                            $staff = $this->syncStaffData($staff_data);
                            $user->update([
                                'department_id' => $staff->department_id,
                                'line_of_business_id' => $staff->line_of_business_id
                            ]);
                            if ($user->department_id == 1)
                                $user->update(['department_id' => 7]);
                            $rows_count += 1;
                        }

                    }
                }
                $message = $rows_count . " Uers added";
            }
        }
        return back()->with('notice', ['type' => 'success', 'message' => $message]);
    }

    public function getGenderInt($gender_str)
    {
        $gender_str = ucfirst(trim($gender_str));
        return getGender($gender_str);
    }

    public function formatDateField($date)
    {
        if (!$date)
            return null;
        $new_date = \DateTime::createFromFormat('d/m/y', $date);
        if (!$new_date) {
            $new_date = @Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date))->format('Y-m-d');
        } else {
            $new_date = @$new_date->format('Y-m-d');
        }
        if ($new_date == "1970-01-01")
            $new_date = \DateTime::createFromFormat('d/m/Y', $date);
        if (!$new_date)
            dd($new_date, $date);

//        $new_date = Carbon::createFromFormat('d/m/y', $date)->format('Y-m-d');
//        dd($new_date,$carbon_date,$date);


        return $new_date;
    }

    public function getStaffPosition($position)
    {
        $position = Position::firstOrCreate(['name' => $position], ['description' => $position]);
        if ($position)
            return $position->id;
        return 0;
    }

    public function getStaffLineOfBusiness($lob)
    {
        $lob_model = LineOfBusiness::firstOrCreate(['name' => $lob], ['user_id' => auth()->id()]);
        if ($lob_model)
            return $lob_model->id;
        return 0;
    }

    public function getCalltronixDepartment($name)
    {
        $department = CalltronixDepartment::firstOrCreate(['name' => $name], ['user_id' => auth()->id(), 'description' => $name]);
        if ($department)
            return $department->id;
        return 8888;
    }

    public function syncUserData($data)
    {
        $user = null;
        if (isset($data['email']))
            $user = User::where('email', $data['email'])->first();

        if ($user) {
            $user->update([
                'name' => $data['name'],
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'phone' => $data['phone'],
                'import_status' => 1 //record updated
            ]);
        } else {
            $user = new User();
            $user->email = @$data['email'];
            $user->name = @$data['name'];
            $user->firstname = @$data['firstname'];
            $user->lastname = @$data['lastname'];
            $user->phone = @$data['phone'];
            $user->role = 'admin';
            $user->import_status = 2; // new record imported
            $user->password = bcrypt('June2020'); //
            $user->save();
        }
        return @$user;
    }

    /**
     * save or update
     * staff details
     */
    public function syncStaffData($data)
    {
        $staff = Staff::where('user_id', $data['user_id'])->first();
        if ($staff) {
            $staff->created_by = $data['created_by'];
            $staff->department_id = $data['department_id'];
            $staff->employment_date = $data['employment_date'];
            $staff->position_id = $data['position_id'];
            $staff->line_of_business_id = $data['line_of_business_id'];
            $staff->gender = $data['gender'];
            $staff->marital_status = null;
            $staff->residential_physical_address = null;
            $staff->bank_name = null;
            $staff->bank_branch = null;
            $staff->dismissal_date = null;
            $staff->medical_history = null;
            $staff->postal_address = null;
            $staff->alternate_phone = null;
            $staff->account_number = null;
            $staff->user_id = $data['user_id'];
            $staff->id_number = $data['id_number'];
            $staff->save();
        } else {
            $staff = new Staff();
            $staff->created_by = $data['created_by'];
            $staff->department_id = $data['department_id'];
            $staff->employment_date = $data['employment_date'];
            $staff->position_id = $data['position_id'];
            $staff->line_of_business_id = $data['line_of_business_id'];
            $staff->gender = $data['gender'];
            $staff->marital_status = null;
            $staff->residential_physical_address = null;
            $staff->bank_name = null;
            $staff->bank_branch = null;
            $staff->dismissal_date = null;
            $staff->medical_history = null;
            $staff->postal_address = null;
            $staff->alternate_phone = null;
            $staff->account_number = null;
            $staff->user_id = $data['user_id'];
            $staff->id_number = $data['id_number'];
            $staff->save();
        }
        return $staff;
    }

    /**
     * format all users names
     * change to case to only upper case for
     * first character
     */
    public function sanitizeAllUsersNames()
    {
        $users = User::all();
        $firstname = null;
        $middlename = null;
        $lastname = null;
        foreach ($users as $user) {

            if ($user->firstname && $user->firstname !== 0)
                $firstname = $this->capitalizeFirstChars($user->firstname);

            if ($user->lastname && $user->lastname !== 0)
                $lastname = $this->capitalizeFirstChars($user->lastname);
            if ($user->middlename)
                $middlename = $this->capitalizeFirstChars($user->middlename);

            $user->firstname = ($firstname) ? $firstname : $user->firstname;
            $user->lastname = ($lastname) ? $lastname : $user->lastname;
            $user->middlename = ($middlename) ? $middlename : $user->middlename;
            $user->name = $user->firstname . " " . $user->lastname;
            $user->save();

        }
    }

    public function capitalizeFirstChars($str = null)
    {
        if (!$str || $str === 0)
            return null;
        $str = trim($str);
        $str_fields = explode(" ", $str);
        $new_str = "";
        foreach ($str_fields as $str_field) {
            $str_field = strtolower($str_field);
            $str_field = ucfirst($str_field);
            $new_str .= " " . $str_field;
        }

        return trim($new_str);
    }

    public function authenticateByUserId($user_id)
    {
        $user = User::whereId($user_id)->first();
        if ($user)
            Auth::login($user, true);

        return redirect('admin')->with('notice', ['type' => 'success', 'message' => 'Now logged in as ' . $user->name]);

    }
}
