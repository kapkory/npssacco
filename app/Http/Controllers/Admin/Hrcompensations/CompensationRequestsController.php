<?php

namespace App\Http\Controllers\Admin\Hrcompensations;

use App\Http\Controllers\Controller;
use App\Models\Core\OffdutyRequestDate;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\CompensationRequest;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class CompensationRequestsController extends Controller
{
    /**
     * return compensationrequest's index view
     */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }
    /**
     * return new compensationrequest form view
     */
    public function returnCompensationFormView(){
        $compensation = null;
        $existing = [];
        $compensation_id = request()->compensation_id;
        if ($compensation_id)
            $compensation = CompensationRequest::whereId($compensation_id)->first();
        if($compensation){
            $existing = $compensation->requestDates()->pluck('id')->toArray();
            $available_dates = OffdutyRequestDate::join('offduty_requests','offduty_request_dates.offduty_request_id','offduty_requests.id')
                ->where([
                    ['offduty_requests.staff_id',$this->getAuthStaffId()],
                    ['offduty_requests.status',getOffdutyRequestStatus('approved')],
                    ['offduty_request_dates.date','<',Carbon::today()] //request compensation for past dates only
                ])
                ->when($compensation ,function ($available_dates,$compensation){
                    return $available_dates->where([
                        ['offduty_request_dates.status',getOffdutyRequestDateStatus('approved_offduty')]
                    ])->orWhere([
                        ['offduty_request_dates.status',getOffdutyRequestDateStatus('pending_compensation')],
                        ['offduty_request_dates.compensation_request_id',$compensation->id]
                    ]);
                })
//            ->whereIn('offduty_request_dates.status',getOffdutyRequestDateStatus(['approved_offduty','rejected_compensation']))
                ->select('offduty_request_dates.*','offduty_requests.description')
                ->get();
        }else
            $available_dates = OffdutyRequestDate::join('offduty_requests','offduty_request_dates.offduty_request_id','offduty_requests.id')
                ->where([
                    ['offduty_requests.staff_id',$this->getAuthStaffId()],
                    ['offduty_requests.status',getOffdutyRequestStatus('approved')],
                    ['offduty_request_dates.date','<',Carbon::today()] //request compensation for past dates only
                ])
//            ->whereIn('offduty_request_dates.status',getOffdutyRequestDateStatus(['approved_offduty','rejected_compensation']))
                ->select('offduty_request_dates.*','offduty_requests.description')
                ->get();
        return view($this->folder.'compensation_form',compact('available_dates','existing','compensation'));
    }

    /**
     * store compensationrequest
     */
    public function storeCompensationRequest(){
        request()->validate([
            'description'=>'required'
        ]);

        $dates = (request()->dates) ? request()->dates : [];

        if(count($dates) == 0)
            return response(['errors'=>['dates_field'=>['Please pick a date to proceed.']]],422);
        $data['form_model'] = CompensationRequest::class;
        $data['staff_id'] = $this->getAuthStaffId();
        $data['description'] = request()->description;
        $data['days'] = @count($dates);
        $compensation = $this->autoSaveModel($data);
        if(isset($dates) && count($dates) > 0){
            foreach($dates as $date_id){
                $date = OffdutyRequestDate::whereId($date_id)->first();
                $date->compensation_request_id = $compensation->id;
                $date->status = getOffdutyRequestDateStatus('pending_compensation');
                $date->save();
            }
        }
        $action="saved";
        if(\request()->id)
            $action="updated";

        return redirect()->back()->with('notice',['type'=>'success','message'=>'Compensation Request '.$action.' successfully']);
    }

    /**
     * return compensationrequest values
     */
    public function listCompensationRequests1()
    {
        $compensationrequests = CompensationRequest::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $compensationrequests->get();
        return SearchRepo::of($compensationrequests)
            ->addColumn('Created_at', function ($compensationrequest) {
                return Carbon::parse($compensationrequest->created_at)->toDayDateTimeString();
            })
            ->addColumn('Status', function ($compensationrequest) {
                return getCompensationStatusButton($compensationrequest->status);
            })
            ->addColumn('action', function ($compensationrequest) {
                $str = '';
                $json = json_encode($compensationrequest);
                $str .= '<a href="#more_info_modal" onclick="getMoreDetails(' . $compensationrequest->id . ')" data-toggle="modal" class="btn btn-dark badge"><i class="fa fa-info-circle"></i> More </a>';
                if(getCompensationStatus($compensationrequest->status) == 'pending'){
                    $str.='&nbsp;&nbsp;<a href="#compensationrequest_modal" onclick="loadCompensationForm('.$compensationrequest->id.')" class="btn badge btn-info btn-sm" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url('admin/compensationrequests/delete').'\',\''.$compensationrequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>';
                }
                return $str;
            })->make();
    }



    /**
     * return compensationrequest values
     */
    public function listCompensationRequests(){

        $compensation_request = CompensationRequest::leftJoin('staffs', 'compensation_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
//                ['staffs.department_id',@$staff->department_id]
            ])
            ->select('compensation_requests.*','users.name as staff','users.email');
        if(\request('all'))
            return $compensation_request->get();
        if(request('count'))
            return $this->getTabsCount();
        return SearchRepo::of($compensation_request)
            ->addColumn('Created_at', function ($compensationrequest) {
                return Carbon::parse($compensationrequest->created_at)->toDayDateTimeString();
            })
            ->addColumn('Status', function ($compensationrequest) {
                return getCompensationStatusButton($compensationrequest->status);
            })
            ->addColumn('Description', function ($compensationrequest) {
                return limit_string_words($compensationrequest->description,15);
            })
            ->addColumn('action',function($compensationrequest){
                $str = '';
                $json = json_encode($compensationrequest);
                $str.='<a href="#more_compensation_request_info_modal" onclick="getMoreDetails(\''.$compensationrequest->id.'\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if(getCompensationStatus($compensationrequest->status) == "pending"){
                    $str.='&nbsp;&nbsp;<a href="#" onclick="approveRequest(\''.$compensationrequest->id.'\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\''.$compensationrequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
//                $str .= '&nbsp;&nbsp;<a href="' . url(  'admin/hrcompensations/compensation/'.$compensationrequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }

    /**
     * return compensationrequest values
     */
    public function listCompensationRequestsByStatus($status){

        $compensationrequests = CompensationRequest::leftJoin('staffs', 'compensation_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['compensation_requests.status',getCompensationStatus($status)]
            ])
            ->select('compensation_requests.*','users.name as staff','users.email');
        if(\request('all'))
            return $compensationrequests->get();
        return SearchRepo::of($compensationrequests)
            ->addColumn('Created_at', function ($compensationrequest) {
                return Carbon::parse($compensationrequest->created_at)->toDayDateTimeString();
            })
            ->addColumn('Status', function ($compensationrequest) {
                return getCompensationStatusButton($compensationrequest->status);
            })
            ->addColumn('Description', function ($compensationrequest) {
                return limit_string_words($compensationrequest->description,15);
            })
            ->addColumn('action',function($compensationrequest){
                $str = '';
                $json = json_encode($compensationrequest);
                $str.='<a href="#more_compensation_request_info_modal" onclick="getMoreDetails(\''.$compensationrequest->id.'\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if(getCompensationStatus($compensationrequest->status) == "pending"){
                    $str.='&nbsp;&nbsp;<a href="#" onclick="approveRequest(\''.$compensationrequest->id.'\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\''.$compensationrequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
//                $str .= '&nbsp;&nbsp;<a href="' . url(  'admin/hodcompensationrequests/compensationrequest/'.$compensationrequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }


    /**
     * count tabs
     */
    public function getTabsCount(){
        return [
            'approved'=>$this->countCompensationRequests('approved'),
            'pending'=>$this->countCompensationRequests('pending'),
            'rejected'=>$this->countCompensationRequests('rejected'),
            'all_requests'=>$this->countCompensationRequests(),
        ];
    }

    public function countCompensationRequests($status=null){

        if (!$status)
            return CompensationRequest::join('staffs', 'compensation_requests.staff_id', 'staffs.id')
                ->leftJoin('users', 'staffs.user_id', 'users.id')
                ->select('compensation_requests.id')
                ->count();
        return CompensationRequest::join('staffs', 'compensation_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where('compensation_requests.status',getCompensationStatus($status))
            ->select('compensation_requests.id')
            ->count();
    }

}
