<?php

namespace App\Http\Controllers\Admin\Hrcompensations\Compensation;

use App\Http\Controllers\Controller;
use App\Models\Core\CompensationRequest;
use App\Models\Core\LeaveType;
use App\Models\Core\OffdutyRequestDate;
use App\Models\Core\Staff;
use App\Models\Core\YearlyStaffLeavesTracker;
use App\Notifications\ApprovedCompensationNotification;
use App\Notifications\ApprovedOffDutyNotification;
use App\Notifications\RejectedCompensationNotification;
use App\Notifications\RejectedOffDutyNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\OffdutyRequest;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class CompensationRequestController extends Controller
{
    /**
     * return offdutyrequest's edit view
     */
    public function returnEditView($compensation_id){
        $offduty = CompensationRequest::whereId($compensation_id)->firstOrFail();

        $f_date = OffdutyRequestDate::where('offduty_request_id',$offduty->id)->first();
        $dates = OffdutyRequestDate::where([
                ['id','<>',@$f_date->id],
                ['offduty_request_id',$offduty->id]
            ])
            ->get();
        return view('core.admin.offdutyrequests.edit_form',compact('offduty','f_date','dates'));
    }
    /**
     * return offdutyrequest's edit view
     */
    public function returnRequestDetailsView($compensation_id){
        $compensation = CompensationRequest::whereId($compensation_id)->firstOrFail();
        $dates = OffdutyRequestDate::where([
                ['compensation_request_id',$compensation->id]
            ])
            ->get();

        return view('core.admin.compensationrequests.edit_form',compact('compensation','f_date','dates'));
    }



    public function index($compensation_request_id)
    {
        $compensation_request = CompensationRequest ::findOrFail($compensation_request_id);
        $staff = $this->getStaffDetails($compensation_request->staff_id);

        return view($this->folder."index",compact('compensation_request','staff','assuming_staff'));
    }

    public function getCompensationRequestDetails($compensation_request_id)
    {
        $compensation_request = CompensationRequest ::findOrFail($compensation_request_id);
        $staff = $this->getStaffDetails($compensation_request->staff_id);

        return view($this->folder."more_details",compact('compensation_request','staff'));
    }

    /**
     * approve leave request
     */
    public function approveCompensationRequest()
    {
        request()->validate([
            'approve_comment'=>'required|string'
        ]);
        $compensation_request = CompensationRequest ::findOrFail(request()->id);
        $compensation_request->hr_reject_reason = null;
        $compensation_request->hr_approve_comment = request()->approve_comment;
        $compensation_request->status = getCompensationStatus('approved');
        $compensation_request->approved_at = Carbon::now();
        $compensation_request->action_by = auth()->id();
        $compensation_request->save();
        $compensation_request->requestDates()->update(['status'=>getOffdutyRequestDateStatus('approved_compensation')]);
        $this->updateStaffAvailableLeaveDays($compensation_request->staff_id,$compensation_request->days);
        $user = User::join('staffs','staffs.user_id','users.id')
            ->where('staffs.id',$compensation_request->staff_id)
            ->select('users.id','users.name','users.email')
            ->first();

        if($user){
            $staff_message = "Your pending compensation request has been approved, click the button below to check.";
            $staff_action_url = url('admin/compensations/compensation/'.$compensation_request->id);
            try {
                $user->notify(new ApprovedCompensationNotification($staff_action_url,$staff_message));
            }catch (\Exception $e) {
                $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
                $notice = ['type' => 'error', 'message' => $mail_error_message];
                return redirect()->back()->with('notice', $notice);
            }
        }

        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Off-duty request approved successfully']);
    }

    /**
     * reject leave request
     */
    public function rejectCompensationRequest()
    {
        request()->validate([
            'reject_reason'=>'required|string'
        ]);
        $compensation_request = CompensationRequest ::findOrFail(request()->id);
        $compensation_request->hr_approve_comment = null;
        $compensation_request->hr_reject_reason = request()->reject_reason;
        $compensation_request->status = getCompensationStatus('rejected');
        $compensation_request->rejected_at = Carbon::now();
        $compensation_request->action_by = auth()->id();
        $compensation_request->save();
        $compensation_request->requestDates()->update(['status'=>getOffdutyRequestDateStatus('rejected_compensation')]);


        $staff = User::join('staffs','staffs.user_id','users.id')
            ->where('staffs.id',$compensation_request->staff_id)
            ->select('users.id','users.name','users.email')
            ->first();
        if($staff){
            $staff_message = "Your pending compensation request has been rejected, click the button below to check.";
            $staff_action_url = url('admin/compensations/compensation/'.$compensation_request->id);
            try {
                $staff->notify(new RejectedCompensationNotification($staff_action_url,$staff_message));
            }catch (\Exception $e) {
                $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
                $notice = ['type' => 'error', 'message' => $mail_error_message];
                return redirect()->back()->with('notice', $notice);
            }
        }
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Compensation request rejected successfully']);
    }

    /**
     * update staff annual leave
     * days - ie increment approved compensation
     * days
     */
    public function updateStaffAvailableLeaveDays($staff_id,$days)
    {
        if(!getAnnualLeaveTypeId())
            return ;
        $staff_yearly_data = YearlyStaffLeavesTracker::where([
            ['staff_id',$staff_id],
            ['leave_type_id',getAnnualLeaveTypeId()],
            ['year',date('Y')]
        ])->first();
        if($staff_yearly_data){
            $staff_yearly_data->compensation_days += $days;
            $staff_yearly_data->days_remaining += $days;
        }else{
            $days_covered = 0;
            $leavetype = LeaveType::whereId(getAnnualLeaveTypeId())->first();
            $max_days = $leavetype->entitlement;
            $staff_yearly_data = new YearlyStaffLeavesTracker();
            $staff_yearly_data->staff_id = $staff_id;
            $staff_yearly_data->leave_type_id = $leavetype->id;
            $staff_yearly_data->year = date('Y');
            $staff_yearly_data->days_covered = $days_covered;
            $staff_yearly_data->days_remaining = ($max_days - $days_covered) + $days;
            $staff_yearly_data->max_days_allowed = $max_days;
            $staff_yearly_data->compensation_days = (integer)$days;
        }
        $staff_yearly_data->save();
    }
}
