<?php

namespace App\Http\Controllers\Admin\Staffs\Staff\Files;

use App\Models\Core\File;
use App\Models\Core\InspectionEstimate;
use App\Models\Core\InspectionEstimateFile;
use App\Models\Core\Staff;
use App\Repositories\FileRepository;
use App\Repositories\SearchRepo;
use App\Repositories\StatusRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use ZipArchive;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class FilesController extends Controller
{
    //
    public function listFiles()
    {
        $files = File::join('users', 'users.id', '=', 'files.user_id')
            ->join('staffs', 'staffs.id', 'files.staff_id')
            ->where([['files.staff_id', '=', \request()->staff_id]])
            ->select('files.*', 'users.name as uploaded_by', 'files.created_at as uploaded_at');

        return SearchRepo::of($files)
            ->addColumn('size', function ($file) {
                return $file->getSize();
            })
            ->addColumn('name', function ($file) {
                return '<a href="' . url("admin/staffs/staff/files/download/$file->id") . '" download="' . $file->name . '" ><span title="' . $file->name . '">' . $file->fileName . '</span></a>';
            })
            ->addColumn('action', function ($file) {
                $str = '<div class="btn-group">';
                $str .= '<a href="' . url("admin/staffs/staff/files/download/$file->id") . '"   class="btn btn-sm btn-info"><i class="fa fa-download"></i> Download</a>&nbsp;';
//                $str.='<a onclick="deleteItem(\''.url("admin/orders/files/delete/$file->id").'\')" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>  Delete</a>';
                $str .= '</div>';
                return $str;
            })
            ->make(true);
    }

    public function uploadFile()
    {
        request()->validate([
            'file' => ['required'],
            'description' => ['required'],
            'type' => ['required'],
        ]);
        $staff = Staff::findOrFail(\request()->staff_id);
        $file = \request()->file('file');
        if ($file) {
            $size = $file->getSize();
            if ($size === false) {
                return redirect()->back()->with('notice', ['type' => 'error', 'message' => 'Failed! Uploaded file exceeded ' . ini_get('upload_max_filesize') . 'B']);
            }
            $uploaded = FileRepository::move($file, $staff->id);
            if ($uploaded['uploaded']) {
                $file = $staff->files()->create([
                    'name' => $file->getClientOriginalName(),
                    'type' => \request('type') ? \request('type') : 'Staff File',
                    'staff_id' => $staff->id,
                    'size' => $size,
                    'user_id' => \request()->user()->id,
                    'description' => \request()->description,
                    'url' => $uploaded['path'],
                ]);
                return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'File uploaded']);
            } else {
                return redirect()->back()->with('notice', ['type' => 'error', 'message' => 'Failed! ' . $uploaded['error']]);
            }
        }

        return redirect()->back()->with('notice', ['type' => 'error', 'message' => 'An error occurred while uploading file']);
    }

    public function deleteFile($id)
    {
        $file = InspectionEstimateFile::findOrFail($id);
        unlink(storage_path("app/" . $file->path));
        $file->delete();
        return redirect()->back();
    }

    public function downloadFile($id)
    {
        $file = File::findOrFail($id);
        try {
            return Storage::download($file->url, $file->name);
        } catch (\Exception $e) {
            return Storage::disk('local')->download($file->url, $file->name);
        }
        return redirect()->back(404);
    }

    public function downloadAll($staff_id)
    {
        $files = File::where('staff_id', '=', $staff_id)->get();

        $user = Staff::find($staff_id)->user;

        $zipFileName = $user->name . '_files.zip';
        $public_dir = storage_path("app/tmp/");
        $zip = new \ZipArchive();
        if ($zip->open($public_dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {
            foreach ($files as $file) {
                $zip->addFile(storage_path('app/' . $file->url), $file->name);
            }

            $zip->close();
        }

        $headers = array(
            'Content-Type' => 'application/octet-stream',
        );
        $filetopath = $public_dir . '/' . $zipFileName;
        if (file_exists($filetopath)) {
            return response()->download($filetopath, $zipFileName, $headers)->deleteFileAfterSend(true);
        }
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Files downloaded successfully']);
    }
}
