<?php

namespace App\Http\Controllers\Admin\Staffs\Staff;

use App\Http\Controllers\Controller;
use App\Models\Core\Staff;
use App\Models\Core\StaffExit;
use App\Notifications\DischargedStaffNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class StaffController extends Controller
{
    /**
     * return staff's index view
     */
    public function index($staff_id)
    {
//        $this->truncateTables();
        $staff = $this->getStaffDetails($staff_id);
        return view($this->folder . 'view_staff', compact('staff'));
    }

    /**
     * update staff
     * profile
     */
    public function updateProfileView($staff_id)
    {
        $staff = $this->getStaffDetails($staff_id);
        return view($this->folder . 'update_staff_profile', compact('staff'));
    }

    public function updateProfile($staff_id)
    {
        request()->validate([
//            'email'    => 'required|email|max:255|unique:users,email,"'.request()->user_id.'"|regex:/^[A-Za-z0-9\.]*@(calltronix)[.](co.ke)$/',
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'employment_date' => 'required|date|before:tomorrow',
            'dob' => 'required|date',
            'position_id' => 'required',
            'line_of_business_id' => 'required',
            'department_id' => 'required',
            'staff_role_id' => 'nullable',
            'marital_status' => 'nullable',
            'id_number' => 'required|unique:staffs,id_number,' . request()->id,
//            'phone' => ['unique:users,phone,'.request()->id,'regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
//            'alternate_phone' => ['nullable','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
        ]);
        $data = request()->all();
        unset($data['user_id']);
        unset($data['dob']);
        unset($data['firstname']);
        unset($data['middlename']);
        unset($data['lastname']);
        $staff = $this->autoSaveModel($data);
        User::join('staffs', 'staffs.user_id', 'users.id')
            ->where('staffs.id', request()->id)->update([
                'users.firstname' => request()->firstname,
                'users.middlename' => request()->middlename,
                'users.dob' => request()->dob,
                'users.lastname' => request()->lastname,
                'users.department_id' => request()->department_id,
                'users.line_of_business_id' => request()->line_of_business_id,
                'users.name' => request()->firstname . " " . request()->lastname,
            ]);
        return back()->with('notice', ['type' => 'success', 'message' => 'Staff details updated successfully']);

    }

    public function updateContactDetails($staff_id)
    {
        request()->validate([
            'email' => 'required|email|max:255|unique:users,email,"' . request()->user_id . '"|regex:/^[A-Za-z0-9\.]*@(calltronix)[.](co.ke)$/',
            'residential_physical_address' => 'nullable',
            'postal_address' => 'nullable',
            'phone' => ['unique:users,phone,' . request()->user_id, 'regex:/^([7]{1}[0-9]{8})$/'],
            'alternate_phone' => ['nullable', 'regex:/^([7]{1}[0-9]{8})$/'],
        ]);
        User::join('staffs', 'staffs.user_id', 'users.id')
            ->where('staffs.id', request()->id)->update([
                'users.email' => request()->email,
                'users.alternate_email' => request()->alternate_email,
                'users.phone' => @$this->formatPhone(\request()->phone),
                'staffs.alternate_phone' => @$this->formatPhone(\request()->alternate_phone),
                'staffs.postal_address' => request()->postal_address,
                'staffs.residential_physical_address' => request()->residential_physical_address,
            ]);
        return back()->with('notice', ['type' => 'success', 'message' => 'Staff details updated successfully']);
    }

    public function updateStatutoryDocs($staff_id)
    {
        request()->validate([
            'bank_name' => 'nullable',
            'bank_branch' => 'nullable',
            'account_number' => ['nullable', 'unique:staffs,account_number,' . request()->id],
            'kra_pin' => ['nullable', 'unique:staffs,kra_pin,' . request()->id],
            'nhif_number' => ['nullable', 'unique:staffs,nhif_number,' . request()->id],
            'nssf_number' => ['nullable', 'unique:staffs,nssf_number,' . request()->id]
        ]);
        $data = request()->all();
        unset($data['user_id']);
        $staff = $this->autoSaveModel($data);

        return back()->with('notice', ['type' => 'success', 'message' => 'Staff details updated successfully']);
    }

    protected function formatPhone($phone)
    {
        if (!$phone)
            return null;
        $phone = "repl" . $phone;
        $phone = str_replace('repl07', '7', $phone);
        $phone = str_replace('repl2547', '7', $phone);
        $phone = str_replace('repl+2547', '7', $phone);
        $phone = str_replace('repl', '', $phone);

        return $phone;
    }

    /**
     * toggle staff as active or not
     * active ie on leave/available
     */
    public function toggleStaffAvailableStatus($staff_id)
    {
        $staff = Staff::whereId($staff_id)->first();
        if ($staff) {
            $user = User::whereId($staff->user_id)->first();
            if ($user->is_available == 0)
                $user->is_available = 1;
            else
                $user->is_available = 0;
            $user->save();
        }
        return back()->with('notice', ['type' => 'success', 'message' => 'Staff state updated successfully']);
    }

    /**
     * discharge staff
     * de-activate user account
     */
    public function dischargeStaff($staff_id)
    {
        request()->validate([
            'date_discharged' => 'required|date|before:tomorrow',
            'reason' => 'required'
        ]);

        $staff = Staff::join('users', 'staffs.user_id', 'users.id')->where([
            ['staffs.id', $staff_id],
            ['staffs.employment_status', '<>', getStaffEmploymentStatus('dismissed')]
        ])
            ->select('staffs.*', 'users.name as staff_name')
            ->first();

        if ($staff) {
            StaffExit::create([
                'user_id' => auth()->id(),
                'staff_user_id' => $staff->user_id,
                'discharge_reason' => $staff->user_id,
                'discharged_at' => request()->date_discharged
            ]);
            $staff->employment_status = getStaffEmploymentStatus('dismissed');
            $staff->dismissal_date = request()->date_discharged;
            $staff->save();
            $target_emails = ['daniel.tarus@calltronix.com', 'ian.madara@calltronix.com', 'john.muhia@calltronix.com', 'salma.moraa@calltronix.com', 'ann.kimani@calltronix.com'];
            $target_users = User::whereIn('email', $target_emails)->get();
            $message = "This is to inform you that " . $staff->staff_name . " has been discharged by Calltronix Contact Center Ltd as of " . Carbon::parse(request()->date_discharged)->isoFormat('Do MMM Y');
            Notification::send($target_users, new DischargedStaffNotification($message));
        } else {
            return back()->with('notice', ['type' => 'error', 'message' => 'Staff details not found']);
        }
        return back()->with('notice', ['type' => 'success', 'message' => 'Staff status set as discharged']);
    }
}
