<?php

namespace App\Http\Controllers\Admin\Staffs;

use App\Http\Controllers\Controller;
use App\Models\Core\CalltronixDepartment;
use App\Models\Core\File;
use App\Models\Core\StaffBeneficiary;
use App\Models\Core\StaffNextOfKin;
use App\Models\Core\StaffSpouseOrChildren;
use App\Repositories\FileRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\Staff;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class StaffsController extends Controller
{
    /**
     * return staff's index view
     */
    public function index()
    {
        $this->sanitizeStaffCalltronixDepartments();

//        $this->sanitizeStaffEmployeeNumbers();
        return view($this->folder . 'index', [

        ]);
    }

    public function createView()
    {
        return view($this->folder . 'create');
    }

    /**
     * store staffStatus
     */
    public function storeStaff()
    {
        //validate data
        Staff::validateEmployeeData(\request());
        //validate dob
        if (\request()->dob > Carbon::now()->subYears(10)->format('Y-m-d')) {
            return \Response::json(["errors" => [
                "dob" => ["Invalid Date of Birth, Please rectify. "],
            ]], 422);
        }
        //validate nok arrays as required
        if (Staff::emptyElementExists(\request()->nok_name)) {
//            return \Response::json(["errors" => [
//                "nok_name[]" => ["You must fill all Next of Kin Details. "],
//            ]], 422);
        }
//        if (Staff::emptyElementExists(\request()->nok_phone)) {
//            return \Response::json(["errors" => [
//                "nok_phone[]" => ["You must fill all Next of Kin Details. "]
//            ]], 422);
//        }
//        if (Staff::emptyElementExists(\request()->nok_relationship)) {
//            return \Response::json(["errors" => [
//                "nok_relationship[]" => ["You must fill all Next of Kin Details. "]
//            ]], 422);
//        }

        if (empty(\request()->nok_name) || empty(\request()->nok_phone) || empty(\request()->nok_relationship)) {
            return \Response::json(["errors" => [
                "nok_relationship[]" => ["You must fill all Next of Kin Details. "],
                "nok_phone[]" => ["You must fill all Next of Kin Details. "],
                "nok_name[]" => ["You must fill all Next of Kin Details. "],
            ]], 422);
        }

        //store data in user table
        $user = $this->createUser();
        //store data on staff table
        $data = \request()->all();
        $data['created_by'] = request()->user()->id;
        $data['user_id'] = $user->id;
        //store staff
        //unset data
        unset($data['nok_name']);
        unset($data['nok_phone']);
        unset($data['nok_relationship']);
        unset($data['s_or_c_name']);
        unset($data['s_or_c_phone']);
        unset($data['s_or_c_relationship']);
        unset($data['beneficiary_name']);
        unset($data['beneficiary_phone']);
        unset($data['beneficiary_phone']);
        unset($data['beneficiary_relationship']);
        unset($data['firstname']);
        unset($data['middlename']);
        unset($data['lastname']);
        unset($data['dob']);
        unset($data['phone']);
        unset($data['email']);
        $staff = Staff::where('user_id', $user->id)->first();
        if (!$staff) {
            if (!\request()->id) {
                $staff = $this->autoSaveModel($data);
                //store staff next of kin
                $this->creatNok($staff);
                $this->createSpouseOrChildren($staff);
                //store staff beneficiary
                $this->createBeneficiary($staff);
                $this->setEmployeeNumber($staff->id);
            }
        }
        if ($staff) {
            $user->is_staff = 1;
            $user->save();
        }
        //store staff file
        $file = \request()->file('passport');


        if ($file) {
            $size = $file->getSize();
            if ($size === false) {
                return \Response::json(["errors" => [
                    "passport" => ["Failed! Uploaded file exceeded " . ini_get('upload_max_filesize') . "B. You should remove the photo and upload letter"]
                ]], 422);
//            return redirect()->back()->with('notice', ['type' => 'error', 'message' => 'Failed! Uploaded file exceeded ' . ini_get('upload_max_filesize') . 'B']);
            }
            $uploaded = FileRepository::saveFile(request()->passport, $staff->id);
            if ($uploaded['uploaded']) {
                $file = new File();
                $file->name = $file['file_name'];
                $file->type = \request('file_type') ? \request('file_type') : 'passport';
                $file->url = $uploaded['path'];
                $file->size = $file->getSize();
                $file->staff_id = $staff->id;
                $file->user_id = auth()->id();
                $file->save();
            }
        }

        return redirect()->to('admin/staffs/staff/' . $staff->id)->with('notice', ['type' => 'success', 'message' => 'Staff saved successfully']);

    }

    /**
     * Insert staff record into users table
     */
    private function createUser()
    {
        $user = User::where('email', \request()->email)->first();

        if (!$user) {
            $user = new User();
            $user->firstname = \request()->firstname;
            $user->middlename = \request()->middlename;
            $user->lastname = \request()->lastname;
            $user->name = \request()->firstname . ' ' . \request()->lastname;
            $user->dob = Carbon::parse(\request()->dob)->format('Y-m-d');
            $user->email = str_replace('calltronix.co.ke', 'calltronix.com', \request()->email);
            $user->password = bcrypt(Carbon::now()->monthName . Carbon::now()->year);
            $user->phone = \request()->phone;
            $user->role = 'admin';
            $user->line_of_business_id = \request()->line_of_business_id;
            $user->department_id = \request()->department_id;
            $user->email_verified_at = Carbon::now()->format('Y-m-d H:i:s');
            $user->save();
        }

        return $user;

    }

    private function creatNok($staff)
    {

        $nok_array = [];
        $nok_names = \request()->nok_name;
        $nok_names = array_filter($nok_names);
        if (isset($nok_names) && count($nok_names) > 0) {
            foreach (\request()->nok_name as $key => $nok) {
                $nok_array[] = StaffNextOfKin::create(
                    [
                        'staff_id' => $staff->id,
                        'nok_name' => $nok,
                        'nok_phone' => \request()->nok_phone[$key],
                        'nok_relationship' => \request()->nok_relationship[$key],
                    ]
                );

            }
        }

        return $nok_array;
    }

    private function createSpouseOrChildren($staff)
    {
        $s_or_c_name = request()->s_or_c_name;
        $s_or_c_name = array_filter($s_or_c_name);
        if (count($s_or_c_name) > 0) {
            foreach (\request()->s_or_c_name as $key => $spouse) {
                if ($spouse != null) {
                    $s_or_record = StaffSpouseOrChildren::create(
                        [
                            'staff_id' => $staff->id,
                            'name' => $spouse,
                            'phone' => \request()->s_or_c_phone[$key],
                            'relationship' => \request()->s_or_c_relationship[$key],
                        ]
                    );
                }
            }
        }

        return 0;
    }

    private function createBeneficiary($staff)
    {

        foreach (\request()->beneficiary_name as $key => $beneficiary) {
            if ($beneficiary != null) {
                $beneficiary_record = StaffBeneficiary::create(
                    [
                        'staff_id' => $staff->id,
                        'name' => $beneficiary,
                        'phone' => \request()->beneficiary_phone[$key],
                        'relationship' => \request()->beneficiary_relationship[$key],
                    ]);
            }

        }

        return 0;
    }

    /**
     * return staff values
     */
    public function listStaff()
    {

        $staff = Staff::join('users', 'users.id', 'staffs.user_id')
            ->leftJoin('line_of_businesses', 'line_of_businesses.id', 'staffs.line_of_business_id')
            ->leftJoin('departments', 'departments.id', 'staffs.department_id')
            ->leftJoin('staff_roles', 'staff_roles.id', 'staffs.staff_role_id')
            ->leftJoin('positions', 'positions.id', 'staffs.position_id')
            ->where([
                ['staffs.employment_status', '<>', getStaffEmploymentStatus('dismissed')]
            ])->select("staffs.*", "users.name as name", "departments.name as department", "positions.name as position", "staff_roles.name as role",
                "users.phone", "users.email");
        if (\request('all'))
            return $staff->get();
        return SearchRepo::of($staff)
            ->addColumn('Employee_no', function ($staff) {
                return "<a onMouseOver=\"this.style.color='#ffb833'\" onMouseOut=\"this.style.color='#31b0d5'\" style='color: #31b0d5' class='load-page font-weight-bolder' href='" . url('admin/staffs/staff/' . $staff->id) . "'>" . $staff->employee_number . "</a>";
            })
            ->addColumn('role', function ($staff) {
                if (!$staff->role)
                    return "N/A";
                return $staff->role;
            })
            ->addColumn('Status', function ($staff) {
                return getStaffEmploymentStatusButton($staff->employment_status);
            })
            ->addColumn('action', function ($staff) {
                $str = '';
                $json = json_encode($staff);
                $str .= '<a href="' . url('admin/staffs/staff/' . $staff->id) . '"  class="btn load-page badge btn-success btn-sm"><i class="fa fa-eye"></i> View</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/staff/delete').'\',\''.$staff->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * return staff values
     */
    public function listMyDepartmentStaffs()
    {
        $department_id = @Staff::where('user_id', auth()->id())->first()->department_id;
        $staff = Staff::join('users', 'users.id', 'staffs.user_id')
            ->join('line_of_businesses', 'line_of_businesses.id', 'staffs.line_of_business_id')
            ->join('departments', 'departments.id', 'staffs.department_id')
            ->where([
                ['staffs.department_id', $department_id],
                ['users.is_available', 1],
                ['users.id', '<>', auth()->id()]
            ])->select("staffs.id", "staffs.employee_number", "users.name as name", "departments.name as department",
                "users.phone", "users.email");
        return $staff->get();
    }

    /**
     * delete staff
     */
    public function destroyStaff($staff_id)
    {
        $staff = Staff::findOrFail($staff_id);
        $staff->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Staff deleted successfully']);
    }

    public function sanitizeStaffEmployeeNumbers()
    {
//        Staff::where('id','>',0)->update(['employee_number'=>null,'dep_unique_no'=>0]);
//        CalltronixDepartment::where('id','>',0)->update(['staff_count'=>0]);
        $staffs = Staff::join('users', 'users.id', 'staffs.user_id')
            ->whereNull('staffs.employee_number')
            ->select('staffs.id', 'staffs.user_id', 'staffs.employee_number')
            ->orderBy('staffs.employment_date', 'ASC')
            ->get();

        foreach ($staffs as $staff) {
//            if(!$staff->employee_number)  //uncomment this once resolved
//                $this->setEmployeeNumber($staff->id);
            $user = User::whereId($staff->user_id)->first();
            if ($user) {
                $user->is_staff = 1;
                $user->save();
                if ($user->department_id == 1 && $user->is_staff = 1) {
                    $user->department_id = 7;
                    $user->save();
                }
            }
        }
    }

    public function setEmployeeNumber($staff_id)
    {
        $staff = Staff::whereId($staff_id)->first();
        if ($staff->employee_number)
            return;
        $department = CalltronixDepartment::whereId($staff->department_id)->first();
        $staff_count_num = 0;
        $min_num = ($department->staff_count < 1) ? 0 : $department->staff_count;
        for ($i = $min_num; $i >= 0; $i++) {  //CIT-001-M20
            $exists = Staff::where([
                ['department_id', $staff->department_id],
                ['dep_unique_no', $i]
            ])->first();
            if (!$exists) {
                $staff_count_num = $i;
                break;
            }
        }
        $employee_num = "C" . strtoupper($department->department_code) . "-" . str_pad($staff_count_num, 3, '0', STR_PAD_LEFT) . strtoupper(getStaffGenderInitial($staff->gender)) . "/" . Carbon::parse($staff->employment_date)->format('y');
        $department->staff_count += 1;
        $staff->dep_unique_no = $staff_count_num;
        $staff->employee_number = $employee_num;
        $department->save();
        $staff->save();
//        dd($department,$staff,$employee_num,$staff_count_num);
        return;
    }

    public function sanitizeStaffCalltronixDepartments()
    {
        $staffs = Staff::join('users', 'users.id', 'staffs.user_id')
            ->select('staffs.id', 'staffs.user_id', 'staffs.employee_number')
            ->orderBy('staffs.employment_date', 'ASC')
            ->get();

        foreach ($staffs as $staff) {
            $staff->update([
                'users.department_id' => $staff->department_id,
                'users.line_of_business_id' => $staff->line_of_business_id,
            ]);
        }

    }
}
