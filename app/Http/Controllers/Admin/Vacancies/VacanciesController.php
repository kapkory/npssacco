<?php

namespace App\Http\Controllers\Admin\Vacancies;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\Vacancy;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class VacanciesController extends Controller
{
    /**
     * return vacancy's index view
     */
    public function index(){
        return view($this->folder.'index');
    }
    /**
     * return vacancy's index view
     */
    public function newVacancy(){
        return view($this->folder.'create');
    }

    /**
     * store vacancy
     */
    public function storeVacancy(){
        request()->validate([
            ''
        ]);
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('vacancies', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if(\request()->id){
            $action="updated";
        }else{
            $action="saved";
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Vacancy '.$action.' successfully']);
    }

    /**
     * return vacancy values
     */
    public function listVacancies(){
        $vacancies = Vacancy::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $vacancies->get();
        return SearchRepo::of($vacancies)
            ->addColumn('action',function($vacancy){
                $str = '';
                $json = json_encode($vacancy);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'vacancy_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/vacancies/delete').'\',\''.$vacancy->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete vacancy
     */
    public function destroyVacancy($vacancy_id)
    {
        $vacancy = Vacancy::findOrFail($vacancy_id);
        $vacancy->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Vacancy deleted successfully']);
    }

}
