<?php

namespace App\Http\Controllers\Admin\Hodleaverequests\Leaverequest;

use App\Http\Controllers\Controller;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\LeaveRequest;
use App\Models\Core\Staff;
use App\Notifications\AssumingStaffLeaveNotification;
use App\Notifications\LeaveRequestNotification;
use App\Notifications\RejectedLeaveRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeaveRequestController extends Controller
{
    public function index($leave_request_id)
    {
        $leave_request = LeaveRequest ::findOrFail($leave_request_id);
        $staff = $this->getStaffDetails($leave_request->staff_id);
        $assuming_staff = Staff::join('users','users.id','staffs.user_id')
            ->where([
                ['staffs.id',$leave_request->assuming_staff_id]
            ])
            ->select('staffs.*','users.name')
            ->first();
        return view($this->folder."index",compact('leave_request','staff','assuming_staff'));
    }
    public function getLeaveRequestDetails($leave_request_id)
    {
        $leave_request = LeaveRequest ::findOrFail($leave_request_id);
        $staff = $this->getStaffDetails($leave_request->staff_id);
        $assuming_staff = Staff::join('users','users.id','staffs.user_id')
            ->where([
                ['staffs.id',$leave_request->assuming_staff_id]
            ])
            ->select('staffs.*','users.name')
            ->first();
        return view($this->folder."more_details",compact('leave_request','staff','assuming_staff'));
    }

    /**
     * approve leave request
     */
    public function approveLeaveRequest()
    {
        request()->validate([
            'approve_comment'=>'required|string'
        ]);
        $leave = LeaveRequest ::findOrFail(request()->id);
        $leave->hod_reject_reason = null;
        $leave->hod_approve_comment = request()->approve_comment;
        $leave->status = getLeaveRequestStatus('hod_approved');
        $leave->hod_action_at = Carbon::now();
        $leave->save();

        $assuming_staff = User::join('staffs','users.id','staffs.user_id')
            ->where([
                ['staffs.id',$leave->assuming_staff_id]
            ])
            ->select('staffs.id as staff_id','users.*')
            ->first();
        $staff = User::join('staffs','users.id','staffs.user_id')
            ->where([
                ['staffs.id',$leave->staff_id]
            ])
            ->select('staffs.id as staff_id','users.*')
            ->first();
        $hr_users = User::join('departments','departments.id','users.department_id')
            ->where('departments.is_hr',1)
            ->select('users.*')
            ->get();
        $action_url = url('admin/hrleaverequests/leaverequest/'.$leave->id);


        $hods_ids = CalltronixDepartmentHead::all()->pluck('user_id')->toArray();
        //check if is CSE ie check if LOB has an Account Manager
        $am_users = User::join('account_managers', 'account_managers.user_id', 'users.id')
            ->where([
                ['account_managers.line_of_business_id', $staff->line_of_business_id],
                ['users.id', '<>', $staff->user_id]
            ])
            ->whereNotIn('users.id', $hods_ids)
            ->select('users.*')
            ->get();
        if ($am_users->count() > 0) {
            //notify A.Ms of a leave request by CSE
            $am_action_url = url('admin/amleaverequests/leaverequest/' . $leave->id);
        }
            try {
            if($assuming_staff){
                $message = "You've been selected as the assuming staff by ".$staff->name." to take over his/her role as from date ".$leave->date_from." to ".$leave->date_to.".\nIf this is without your consent please contact your H.O.D.";
                $assuming_staff->notify(new AssumingStaffLeaveNotification($message));
            }

            foreach($hr_users as $user){
                $user->notify(new LeaveRequestNotification($staff->name,$action_url));
            }
        } catch (\Exception $e) {
            $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return back()->with('notice', $notice);
        }
        return back()->with('notice', ['type' => 'success', 'message' => 'Leave request approved successfully']);
    }

    /**
     * reject leave request
     */
    public function rejectLeaveRequest()
    {
        request()->validate([
            'reject_reason'=>'required|string'
        ]);
        $leave = LeaveRequest ::findOrFail(request()->id);
        $leave->status = getLeaveRequestStatus('hod_rejected');
        $leave->hod_approve_comment = null;
        $leave->hod_reject_reason = request()->reject_reason;
        $leave->hod_action_at = Carbon::now();
        $leave->save();
        $staff = User::join('staffs','users.id','staffs.user_id')
            ->where('staffs.id',$leave->staff_id)
            ->select('users.*')
            ->first();

        $staff_message = "Your pending leave request has been rejected, click the button below to check.";
        $staff_action_url = url('admin/leaverequests/leaverequest/'.$leave->id);
        try {
            $staff->notify(new RejectedLeaveRequest($staff_action_url,$staff_message));
        }catch (\Exception $e) {
            $mail_error_message = 'Opps! Email alert failed to send, You may need to escalate this manually.';
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return redirect()->back()->with('notice', $notice);
        }

        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave request rejected successfully']);
    }
}
