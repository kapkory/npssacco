<?php

namespace App\Http\Controllers\Admin\Amleaverequests;

use App\Http\Controllers\Controller;
use App\Models\Core\AccountManager;
use App\Models\Core\CalltronixDepartment;
use App\Models\Core\LeaveType;
use App\Models\Core\LeaveRequest;
use App\Models\Core\Staff;
use App\Repositories\SearchRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeaveRequestsController extends Controller
{
    public function index(){
        $staff = @Staff::where('user_id',auth()->id())->first();
        $calltronix_department = CalltronixDepartment::whereId($staff->department_id)->first();
        return view($this->folder.'index',compact('calltronix_department'));
    }

    /**
     * return leave requests values
     */
    public function listLeaveRequestsByStatus($status)
    {
        $staff = @Staff::where('user_id',auth()->id())->first();
        $am_lobs = AccountManager::where('user_id', auth()->id())->pluck('line_of_business_id')->toArray();
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['leave_requests.status',getLeaveRequestStatus($status)],
                ['staffs.line_of_business_id',@$staff->line_of_business_id],
                ['staffs.department_id',@$staff->department_id]
            ])
            ->whereIn('staffs.line_of_business_id',$am_lobs)
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid','users.name as staff','users.email');
        if (\request('all'))
            return $leaverequests->get();
        return SearchRepo::of($leaverequests)
            ->addColumn('Request_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Return_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Days', function ($leaverequest) {
                $date_to = Carbon::parse($leaverequest->date_to);
                $date_from = Carbon::parse($leaverequest->date_from);
//                $date_diff=$date_to->diffInDays($date_from);
                $date_diff = $date_to->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isWeekend();
                }, $date_from);
                return $date_diff;
            })
            ->addColumn('Comments', function ($leaverequest) {
                if($leaverequest->reporting_omments)
                    return limit_string_words($leaverequest->reporting_omments,15);
                return "";
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason,15);
            })
            ->addColumn('Status', function ($leaverequest) {
                return getLeaveRequestStatusButton($leaverequest->status);
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str.='<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\''.$leaverequest->id.'\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if(getLeaveRequestStatus($leaverequest->status) == "pending"){
                    $str.='&nbsp;&nbsp;<a href="#" onclick="approveRequest(\''.$leaverequest->id.'\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\''.$leaverequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
                $str .= '&nbsp;&nbsp;<a href="' . url(  'admin/hodleaverequests/leaverequest/'.$leaverequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }
    /**
     * return my department leave requests
     */
    public function listLeaveRequests()
    {
        $staff = @Staff::where('user_id',auth()->id())->first();
        $am_lobs = AccountManager::where('user_id', auth()->id())->pluck('line_of_business_id')->toArray();
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['leave_requests.status','<>',getLeaveRequestStatus('canceled')],
                ['staffs.line_of_business_id',@$staff->line_of_business_id],
                ['staffs.department_id',@$staff->department_id]
            ])
            ->whereIn('staffs.line_of_business_id',$am_lobs)
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid','users.name as staff','users.email');

        if (\request('all'))
            return $leaverequests->get();
        if(request('count'))
            return $this->getTabsCount();
        return SearchRepo::of($leaverequests)
            ->addColumn('Request_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Return_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Days', function ($leaverequest) {
                $date_to = Carbon::parse($leaverequest->date_to);
                $date_from = Carbon::parse($leaverequest->date_from);
//                $date_diff=$date_to->diffInDays($date_from);
                $date_diff = $date_to->diffInDaysFiltered(function(Carbon $date) {
                    return !$date->isWeekend();
                }, $date_from);
                return $date_diff;
            })
            ->addColumn('Comments', function ($leaverequest) {
                if($leaverequest->reporting_omments)
                    return limit_string_words($leaverequest->reporting_omments,15);
                return "";
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason,15);
            })
            ->addColumn('Status', function ($leaverequest) {
                return getLeaveRequestStatusButton($leaverequest->status);
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str.='<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\''.$leaverequest->id.'\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if(getLeaveRequestStatus($leaverequest->status) == "pending"){
                    $str.='<a href="#" onclick="approveRequest(\''.$leaverequest->id.'\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\''.$leaverequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Decline</a>';
                }
                $str .= '&nbsp;&nbsp;<a href="' . url(  'admin/amleaverequests/leaverequest/'.$leaverequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }

    /**
     * return staff leave requests
     */
    public function listStaffLeaveRequests($staff_id)
    {
        $staff = @Staff::where('user_id',auth()->id())->first();
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['leave_requests.status','<>',getLeaveRequestStatus('canceled')],
                ['staffs.id',$staff_id],
                ['staffs.department_id',@$staff->department_id]
            ])
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid','users.name as staff','users.email');
        if (\request('all'))
            return $leaverequests->get();
        return SearchRepo::of($leaverequests)
            ->addColumn('Date_from', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Date_to', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason,15);
            })
            ->addColumn('Status', function ($leaverequest) {
                return ucfirst(getLeaveRequestStatus($leaverequest->status));
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                    $str.='<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\''.$leaverequest->id.'\');" data-toggle="modal" class="btn badge btn-outline-dark btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';
                return $str;
            })->make();
    }


    /**
     * count tabs
     */
    public function getTabsCount(){
        return [
            'approved'=>$this->countLeaveRequests('am_approved'),
            'pending'=>$this->countLeaveRequests('pending'),
            'rejected'=>$this->countLeaveRequests('am_rejected'),
            'all_requests'=>$this->countLeaveRequests(),
        ];
    }

    public function countLeaveRequests($status=null){
        $staff = @Staff::where('user_id',auth()->id())->first();
        $am_lobs = AccountManager::where('user_id', auth()->id())->pluck('line_of_business_id')->toArray();

        if (!$status)
            return LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
                ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
                ->leftJoin('users', 'staffs.user_id', 'users.id')
                ->where([
                    ['leave_requests.status','<>',getLeaveRequestStatus('canceled')],
                    ['staffs.line_of_business_id',@$staff->line_of_business_id],
                    ['staffs.department_id',@$staff->department_id]
                ])
                ->whereIn('staffs.line_of_business_id',$am_lobs)
                ->count();
        return LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['leave_requests.status',getLeaveRequestStatus($status)],
                ['staffs.line_of_business_id',@$staff->line_of_business_id],
                ['staffs.department_id',@$staff->department_id]
            ])
            ->whereIn('staffs.line_of_business_id',$am_lobs)
            ->count();
    }

}
