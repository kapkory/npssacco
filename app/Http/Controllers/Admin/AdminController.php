<?php

namespace App\Http\Controllers\Admin;

use App\Models\Core\CalltronixDepartment;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{

    /**
     * return user's index view
     */
    protected $main_menu;
    protected $folder;
    protected $inner_menu;

    public function __construct($inner_menu = null, $main_menu = "home")
    {
        parent::__construct();
        $this->main_menu = $main_menu;
        $this->inner_menu = $inner_menu;
    }


    public function index()
    {

        $this->inner_menu = 'dashboard';
//$res = $this->solution1([2,6,8,5]);
//dd($res); //should yield 2
        return view($this->folder . 'index', [
            'main_menu' => $this->main_menu,
            'inner_menu' => $this->inner_menu,
            'tickets_assigned' => $this->getDepartmentTickets(true),
            'tickets_created' => $this->getDepartmentTickets(),
            'tickets_count' => $this->getSystemTicketsCount()
        ]);
    }

    function solution($N, $K)
    {
        // write your code in PHP7.0
        $remaining_amount = $K;
        $count = 0;
        $glasses = [];
        for ($i = $N; $i >= 1; $i--) {
            if ($remaining_amount >= $i) {
                if ($remaining_amount % $i == 0) {
                    $count += 1;
                    $glasses[] = $i;
                    $remaining_amount = $remaining_amount - $i;
                    if ($remaining_amount == 0)
                        break;
                } else {
                    $count += 1;
                    $glasses[] = $i;
                    $remaining_amount = $remaining_amount - $i;
                    if ($remaining_amount == 0)
                        break;

                }
            }

        }
        if ($remaining_amount > 0)
            return -1;
        return $count;
        dd($remaining_amount, $count, $glasses);

    }

    function solution1($blocks)
    {
        // write your code in PHP7.0
        $max_dist = 0;
        $length = count($blocks);
        foreach ($blocks as $key => $block) {

            if ($key < $length - 1) {
                $block1 = $block;
                $block2 = $blocks[$key + 1];
                $distance = $block1 - $block2 + 1;
                if ($distance > $max_dist) {
                    $max_dist = $distance;
                }

            }

        }
        return $max_dist;
    }

    public function checkForNextGlass($remaining_amount, $max_glass_num)
    {
//        dd($remaining_amount);

    }

    public function getDepartmentTickets($assigned = false)
    {
        $records = DB::table('departments');
        if ($assigned)
            $records = $records->join('tickets', 'tickets.department_id_to', 'departments.id');
        else
            $records = $records->join('tickets', 'tickets.department_id_from', 'departments.id');

        $records = $records->where('tickets.status', 1)
            ->select(DB::raw('count(ticket_status_id) as status_count, tbl_tickets.ticket_status_id'), 'departments.*')
            ->groupBy('departments.id', 'tickets.ticket_status_id')
            ->get();
        return $this->formatDepartmentsTicketsCount($records);

    }

    public function formatDepartmentsTicketsCount($records)
    {
        $open = getTicketStatus('open');
        $resolved = getTicketStatus('resolved');
        $closed = getTicketStatus('closed');
        $in_progress = getTicketStatus('in_progress');
        $waiting_for_response = getTicketStatus('waiting_for_response');
        $cancelled = getTicketStatus('cancelled');
        $status_conv = [
            $open => 'open',
            $in_progress => 'in_progress',
            $resolved => 'resolved',
            $closed => 'closed',
            $cancelled  => 'cancelled',
            $waiting_for_response => 'waiting_for_response',
        ];
        $departments = [];
        foreach ($records as $record) {
            $dep = @$departments[$record->id];
        try{

            if (!$dep)
                $departments[$record->id] = [
                    'id' => $record->id,
                    'department_code' => $record->department_code,
                    'name' => $record->name,
                    'open' => 0,
                    'in_progress' => 0,
                    'resolved' => 0,
                    'closed' => 0,
                    'cancelled' => 0,
                ];
//            dd($status_conv[$record->ticket_status_id]);

            $status_index = $status_conv[$record->ticket_status_id];
            $departments[$record->id][$status_index] = $record->status_count;
        }catch(\Exception $e){
            dd($e->getMessage(),$status_conv,$status_index,$record->ticket_status_id,$record);
        }

        }
        return $departments;
    }

    /**
     * format tickets count
     */
    public function getSystemTicketsCount()
    {
        $tickets_count = DB::table('tickets')
            ->where('tickets.status',1)
            ->select(DB::raw('count(ticket_status_id) as status_count, tbl_tickets.ticket_status_id'))
            ->groupBy( 'tickets.ticket_status_id')
            ->get();
         $open = getTicketStatus('open');
        $resolved = getTicketStatus('resolved');
        $closed = getTicketStatus('closed');
        $in_progress = getTicketStatus('in_progress');
        $cancelled = getTicketStatus('cancelled');
        $waiting_for_response = getTicketStatus('waiting_for_response');
        $status_conv = [
            $open => 'open',
            $in_progress => 'in_progress',
            $resolved => 'resolved',
            $closed => 'closed',
            $cancelled => 'cancelled',
            $waiting_for_response => 'waiting_for_response',
        ];
        $records_count = [];
        $net_count = 0;
        foreach($tickets_count as $record){
            $count = $record->status_count;
            $status = $status_conv[$record->ticket_status_id];
            $records_count[$status] = $count;
            $net_count += $count;
        }
        $records_count['all'] = $net_count;
        return $records_count;

    }
}
