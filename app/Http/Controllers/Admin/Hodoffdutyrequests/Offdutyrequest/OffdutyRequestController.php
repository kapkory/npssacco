<?php

namespace App\Http\Controllers\Admin\Hodoffdutyrequests\Offdutyrequest;

use App\Http\Controllers\Controller;
use App\Models\Core\OffdutyRequestDate;
use App\Models\Core\Staff;
use App\Notifications\ApprovedOffDutyNotification;
use App\Notifications\RejectedOffDutyNotification;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\OffdutyRequest;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class OffdutyRequestController extends Controller
{
    /**
     * return offdutyrequest's edit view
     */
    public function returnEditView($offduty_id){
        $offduty = OffdutyRequest::whereId($offduty_id)->firstOrFail();

        $f_date = OffdutyRequestDate::where('offduty_request_id',$offduty->id)->first();
        $dates = OffdutyRequestDate::where([
                ['id','<>',@$f_date->id],
                ['offduty_request_id',$offduty->id]
            ])
            ->get();
        return view('core.admin.offdutyrequests.edit_form',compact('offduty','f_date','dates'));
    }
    /**
     * return offdutyrequest's edit view
     */
    public function returnRequestDetailsView($offduty_id){
        $offduty = OffdutyRequest::whereId($offduty_id)->firstOrFail();
        $dates = OffdutyRequestDate::where([
                ['offduty_request_id',$offduty->id]
            ])
            ->get();
        return view('core.admin.offdutyrequests.edit_form',compact('offduty','f_date','dates'));
    }



    public function index($offduty_request_id)
    {
        $offduty_request = OffdutyRequest ::findOrFail($offduty_request_id);
        $staff = $this->getStaffDetails($offduty_request->staff_id);

        return view($this->folder."index",compact('offduty_request','staff','assuming_staff'));
    }
    public function getOffdutyRequestDetails($offduty_request_id)
    {
        $offduty_request = OffdutyRequest ::findOrFail($offduty_request_id);
        $staff = $this->getStaffDetails($offduty_request->staff_id);

        return view($this->folder."more_details",compact('offduty_request','staff'));
    }

    /**
     * approve leave request
     */
    public function approveOffdutyRequest()
    {
        request()->validate([
            'approve_comment'=>'required|string'
        ]);
        $offduty_request = OffdutyRequest ::findOrFail(request()->id);
        $offduty_request->hod_reject_reason = null;
        $offduty_request->hod_approve_comment = request()->approve_comment;
        $offduty_request->status = getOffdutyRequestStatus('approved');
        $offduty_request->approved_at = Carbon::now();
        $offduty_request->action_by = auth()->id();
        $offduty_request->save();
        $offduty_request->offdutyRequestDates()->update(['status'=>getOffdutyRequestDateStatus('approved_offduty')]);

        $user = User::join('staffs','staffs.user_id','users.id')
            ->where('staffs.id',$offduty_request->staff_id)
            ->select('users.id','users.name','users.email')
            ->first();
        if($user){
            $staff_message = "Your pending off-duty request has been approved, click the button below to check.";
            $staff_action_url = url('admin/offdutyrequests/offdutyrequest/'.$offduty_request->id);
            $user->notify(new ApprovedOffDutyNotification($staff_action_url,$staff_message));
        }

        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Off-duty request approved successfully']);
    }

    /**
     * reject leave request
     */
    public function rejectOffdutyRequest()
    {
        request()->validate([
            'reject_reason'=>'required|string'
        ]);
        $offduty_request = OffdutyRequest ::findOrFail(request()->id);
        $offduty_request->hod_approve_comment = null;
        $offduty_request->hod_reject_reason = request()->reject_reason;
        $offduty_request->status = getOffdutyRequestStatus('rejected');
        $offduty_request->rejected_at = Carbon::now();
        $offduty_request->action_by = auth()->id();
        $offduty_request->save();
        $offduty_request->offdutyRequestDates()->update(['status'=>getOffdutyRequestDateStatus('rejected_offduty')]);


        $staff = User::join('staffs','staffs.user_id','users.id')
            ->where('staffs.id',$offduty_request->staff_id)
            ->select('users.id','users.name','users.email')
            ->first();
        if($staff){
            $staff_message = "Your pending off-duty request has been rejected, click the button below to check.";
            $staff_action_url = url('admin/offdutyrequests/offdutyrequest/'.$offduty_request->id);
            $staff->notify(new RejectedOffDutyNotification($staff_action_url,$staff_message));
        }
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave request rejected successfully']);
    }
}
