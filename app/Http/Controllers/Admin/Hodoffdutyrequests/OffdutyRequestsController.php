<?php

namespace App\Http\Controllers\Admin\Hodoffdutyrequests;

use App\Http\Controllers\Controller;
use App\Models\Core\CalltronixDepartment;
use App\Models\Core\OffdutyRequestDate;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\OffdutyRequest;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class OffdutyRequestsController extends Controller
{
    /**
     * return offdutyrequest's index view
     */
    public function index(){
        $staff = @Staff::where('user_id',auth()->id())->first();
        $calltronix_department = CalltronixDepartment::whereId($staff->department_id)->first();
        return view($this->folder.'index',compact('calltronix_department'));
    }

    /**
     * store offdutyrequest
     */
    public function storeOffdutyRequest(){
        request()->validate([
            'dates.*' => 'required|distinct|date|after:yesterday',
//            'dates.*' => 'required|distinct|unique:units,name,NULL,id,unit_group_id,' . request()->unit_group_id,
            'description' => 'required'
        ]);

        $data = [];
        $data['form_model'] = OffdutyRequest::class;
        $data['id'] = request()->id;
        $data['description'] = request()->description;
        $data['days'] = count(request()->dates);
        $data['staff_id'] = $this->getAuthStaffId();
        $offduty = $this->autoSaveModel($data);
        $dates_data = request()->dates;
        if(count($dates_data) > 0)
            foreach ($dates_data as $key=>$date) {
                $data = [];
                $data['offduty_request_id'] = $offduty->id;
                $data['date'] = $date;
                $cdata = $data;
                $cdata['id'] = $key;
                OffdutyRequestDate::updateOrCreate($cdata,$data);
            }

        return back()->with('notice',['type'=>'success','message'=>'Request saved successfully']);
    }

    /**
     * return offdutyrequest values
     */
    public function listOffdutyRequests(){
        $staff = @Staff::where('user_id',auth()->id())->first();
        $offdutyrequests = OffdutyRequest::leftJoin('staffs', 'offduty_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['staffs.department_id',@$staff->department_id]
            ])
            ->select('offduty_requests.*','users.name as staff','users.email');
        if(\request('all'))
            return $offdutyrequests->get();
        if(request('count'))
            return $this->getTabsCount();
        return SearchRepo::of($offdutyrequests)
            ->addColumn('Created_at', function ($offdutyrequest) {
                return Carbon::parse($offdutyrequest->created_at)->toDayDateTimeString();
            })
            ->addColumn('Status', function ($offdutyrequest) {
                return getOffdutyRequestStatusButton($offdutyrequest->status);
            })
            ->addColumn('Description', function ($offdutyrequest) {
                return limit_string_words($offdutyrequest->description,15);
            })
            ->addColumn('action',function($offdutyrequest){
                $str = '';
                $json = json_encode($offdutyrequest);
                $str.='<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\''.$offdutyrequest->id.'\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if(getOffdutyRequestStatus($offdutyrequest->status) == "pending"){
                    $str.='&nbsp;&nbsp;<a href="#" onclick="approveRequest(\''.$offdutyrequest->id.'\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\''.$offdutyrequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
//                $str .= '&nbsp;&nbsp;<a href="' . url(  'admin/hodoffdutyrequests/offdutyrequest/'.$offdutyrequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }

    /**
     * return offdutyrequest values
     */
    public function listOffdutyRequestsByStatus($status){
        $staff = @Staff::where('user_id',auth()->id())->first();
        $offdutyrequests = OffdutyRequest::leftJoin('staffs', 'offduty_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['staffs.department_id',@$staff->department_id],
                ['offduty_requests.status',getOffdutyRequestStatus($status)]
            ])
            ->select('offduty_requests.*','users.name as staff','users.email');
        if(\request('all'))
            return $offdutyrequests->get();
        return SearchRepo::of($offdutyrequests)
            ->addColumn('Created_at', function ($offdutyrequest) {
                return Carbon::parse($offdutyrequest->created_at)->toDayDateTimeString();
            })
            ->addColumn('Status', function ($offdutyrequest) {
                return getOffdutyRequestStatusButton($offdutyrequest->status);
            })
            ->addColumn('Description', function ($offdutyrequest) {
                return limit_string_words($offdutyrequest->description,15);
            })
            ->addColumn('action',function($offdutyrequest){
                $str = '';
                $json = json_encode($offdutyrequest);
                $str.='<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\''.$offdutyrequest->id.'\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if(getOffdutyRequestStatus($offdutyrequest->status) == "pending"){
                    $str.='&nbsp;&nbsp;<a href="#" onclick="approveRequest(\''.$offdutyrequest->id.'\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\''.$offdutyrequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
//                $str .= '&nbsp;&nbsp;<a href="' . url(  'admin/hodoffdutyrequests/offdutyrequest/'.$offdutyrequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }

    /**
     * delete offdutyrequest
     */
    public function destroyOffdutyRequest($offdutyrequest_id)
    {
        $offdutyrequest = OffdutyRequest::findOrFail($offdutyrequest_id);
        if($offdutyrequest){
            $offdutyrequest->offdutyRequestDates()->delete();
            $offdutyrequest->delete();
        }
        return redirect()->back()->with('notice',['type'=>'success','message'=>'OffdutyRequest deleted successfully']);
    }

    /**
     * count tabs
     */
    public function getTabsCount(){
        return [
            'approved'=>$this->countOffdutyRequests('approved'),
            'pending'=>$this->countOffdutyRequests('pending'),
            'rejected'=>$this->countOffdutyRequests('rejected'),
            'all_requests'=>$this->countOffdutyRequests(),
        ];
    }

    public function countOffdutyRequests($status=null){
        $staff = @Staff::where('user_id',auth()->id())->first();

        if (!$status)
            return OffdutyRequest::join('staffs', 'offduty_requests.staff_id', 'staffs.id')
                ->leftJoin('users', 'staffs.user_id', 'users.id')
                ->where('staffs.department_id',@$staff->department_id)
                ->select('offduty_requests.id')
                ->count();
        return OffdutyRequest::join('staffs', 'offduty_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where('staffs.department_id',@$staff->department_id)
            ->where('offduty_requests.status',getOffdutyRequestStatus($status))
            ->select('offduty_requests.id')
            ->count();
    }
}
