<?php

namespace App\Http\Controllers\Admin\Offdutyrequests;

use App\Http\Controllers\Controller;
use App\Models\Core\OffdutyRequestDate;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\OffdutyRequest;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class OffdutyRequestsController extends Controller
{
    /**
     * return offdutyrequest's index view
     */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store offdutyrequest
     */
    public function storeOffdutyRequest(){
        request()->validate([
            'dates.*' => 'required|distinct|date|after:yesterday',
//            'dates.*' => 'required|distinct|unique:units,name,NULL,id,unit_group_id,' . request()->unit_group_id,
            'description' => 'required'
        ]);

        $data = [];
        $data['form_model'] = OffdutyRequest::class;
        $data['id'] = request()->id;
        $data['description'] = request()->description;
        $data['days'] = count(request()->dates);
        $data['staff_id'] = $this->getAuthStaffId();
        $offduty = $this->autoSaveModel($data);
        $dates_data = request()->dates;
        if(count($dates_data) > 0)
            foreach ($dates_data as $key=>$date) {
                $data = [];
                $data['offduty_request_id'] = $offduty->id;
                $data['date'] = $date;
                $cdata = $data;
                $cdata['id'] = $key;
                OffdutyRequestDate::updateOrCreate($cdata,$data);
            }

        return back()->with('notice',['type'=>'success','message'=>'Request saved successfully']);
    }

    /**
     * return offdutyrequest values
     */
    public function listOffdutyRequests(){
        $staff = Staff::where('user_id',auth()->id())->first();
        $offdutyrequests = OffdutyRequest::where([
            ['staff_id',$staff->id]
        ]);
        if(\request('all'))
            return $offdutyrequests->get();
        return SearchRepo::of($offdutyrequests)
            ->addColumn('Created_at', function ($offdutyrequest) {
                return Carbon::parse($offdutyrequest->created_at)->toDayDateTimeString();
            })
            ->addColumn('Status', function ($offdutyrequest) {
                return getOffdutyRequestStatusButton($offdutyrequest->status);
            })
            ->addColumn('action',function($offdutyrequest){
                $str = '';
                $json = json_encode($offdutyrequest);
                                $str .= '<a href="#more_info_modal" onclick="getMoreDetails(' . $offdutyrequest->id . ')" data-toggle="modal" class="btn btn-dark badge"><i class="fa fa-info-circle"></i> More </a>';
                if(getOffdutyRequestStatus($offdutyrequest->status) == 'pending'){
                    $str.='&nbsp;&nbsp;<a href="#edit_offdutyrequest_modal" onclick="loadEditOffdutyForm('.$offdutyrequest->id.')" class="btn badge btn-info btn-sm" data-toggle="modal"><i class="fa fa-edit"></i> Edit</a>';
                    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url('admin/offdutyrequests/delete').'\',\''.$offdutyrequest->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>';
                }
                return $str;
            })->make();
    }

    /**
     * delete offdutyrequest
     */
    public function destroyOffdutyRequest($offdutyrequest_id)
    {
        $offdutyrequest = OffdutyRequest::findOrFail($offdutyrequest_id);
        if($offdutyrequest){
            $offdutyrequest->offdutyRequestDates()->delete();
            $offdutyrequest->delete();
        }
        return redirect()->back()->with('notice',['type'=>'success','message'=>'OffdutyRequest deleted successfully']);
    }

}
