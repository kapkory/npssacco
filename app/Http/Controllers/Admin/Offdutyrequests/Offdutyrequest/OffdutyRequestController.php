<?php

namespace App\Http\Controllers\Admin\Offdutyrequests\Offdutyrequest;

use App\Http\Controllers\Controller;
use App\Models\Core\OffdutyRequestDate;
use Illuminate\Http\Request;

use App\Models\Core\OffdutyRequest;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class OffdutyRequestController extends Controller
{
    /**
     * return offdutyrequest's edit view
     */
    public function returnEditView($offduty_id){
        $offduty = OffdutyRequest::whereId($offduty_id)->firstOrFail();

        $f_date = OffdutyRequestDate::where('offduty_request_id',$offduty->id)->first();
        $dates = OffdutyRequestDate::where([
                ['id','<>',@$f_date->id],
                ['offduty_request_id',$offduty->id]
            ])
            ->get();
        return view('core.admin.offdutyrequests.edit_form',compact('offduty','f_date','dates'));
    }
    /**
     * return offdutyrequest's edit view
     */
    public function returnRequestDetailsView($offduty_id){
        $offduty = OffdutyRequest::whereId($offduty_id)->firstOrFail();
        $dates = OffdutyRequestDate::where([
                ['offduty_request_id',$offduty->id]
            ])
            ->get();
        return view('core.admin.offdutyrequests.edit_form',compact('offduty','f_date','dates'));
    }
}
