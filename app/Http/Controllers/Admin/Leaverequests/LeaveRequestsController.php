<?php

namespace App\Http\Controllers\Admin\Leaverequests;

use App\Http\Controllers\Controller;
use App\Models\Core\AccountManager;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\LeaveType;
use App\Models\Core\LeaveRequest;
use App\Models\Core\Staff;
use App\Models\Core\YearlyStaffLeavesTracker;
use App\Notifications\LeaveRequestNotification;
use App\Repositories\SearchRepo;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class LeaveRequestsController extends Controller
{
    public function index()
    {
        return view($this->folder . 'index');
    }

    public function createLeaveRequest()
    {
        return view($this->folder . "create");
    }

    /**
     * store leave type
     */
    public function storeLeaveType()
    {
        request()->validate([
            'name' => 'required|unique:leave_types,name,' . \request('id'),
            'is_paid' => 'required|numeric',
            'entitlement' => 'required|numeric',
        ]);
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        $this->autoSaveModel($data);
        $action = "added";
        if (\request()->id)
            $action = "updated";
        return back()->with('notice', ['type' => 'success', 'message' => 'Leave type ' . $action . ' successfully']);
    }

    /**
     * store leave type
     */
    public function storeLeaveRequest()
    {
        request()->validate([
            'leave_type_id' => 'required',
            'contact' => 'required',
            'assuming_staff_id' => 'required',
            'date_from' => 'required|date',
            'date_to' => 'required|date|after:' . request()->date_from,
            'reason' => 'required',
        ]);

        $staff = @Staff::where('user_id', auth()->id())->first();
        /**
         * check the no.of months
         * one has been with the org.
         */
        $this->syncStaffAvailableAnnualLeaveDays($staff->user_id);
       $leave_request_id = request()->id;
            $pending_status = getLeaveRequestStatus(['pending', 'hod_approved', 'am_approved']);
            $has_pending_request = LeaveRequest::where([
                ['staff_id', $staff->id]
            ])
                ->whereIn('status', $pending_status)
                ->first();

            if ($has_pending_request && !isset(request()->id)) //staff can only apply for leave when he/she doeasn't have a pending leave request
                return back()->with('notice', ['type' => 'error', 'message' => "You can't apply for another leave at the moment. You still have a pending request"])->withInput();


        $hods_ids = CalltronixDepartmentHead::all()->pluck('user_id')->toArray();
        $date_from = Carbon::parse(request()->date_from);
        $date_to = Carbon::parse(request()->date_to);
        if ($date_to < $date_from)
            return \Response::json(["errors" => ["date_to" => ["Please select a date later or same as date to field."]]], 422);

        if($date_to->isSunday())
            return \Response::json(["errors" => ["date_to" => ["You've selected \"Sunday\" please select a working day. "]]], 422);
        if($date_from->isSunday())
            return \Response::json(["errors" => ["date_from" => ["You've selected \"Sunday\" please select a working day. "]]], 422);
        $is_account_manager = AccountManager::where('user_id', auth()->id())->first();

        $diff = $date_from->diffInDays($date_to);
        $max_days = $this->staffAvailableLeaveTypeDays($staff->id, request()->leave_type_id);

        if ($diff > $max_days)
            return \Response::json(["errors" => ["date_to" => ["Max. allowed days (" . $max_days . ") exceeded "]]], 422);
        $data = \request()->all();
        $data['form_model'] = LeaveRequest::class;
        $data['staff_id'] = $staff->id;
        $leave_request = $this->autoSaveModel($data);

        //check if is CSE ie check if LOB has an Account Manager
        $target_users = User::join('account_managers', 'account_managers.user_id', 'users.id')
            ->where([
                ['account_managers.line_of_business_id', $staff->line_of_business_id],
                ['users.id', '<>', $staff->user_id]
            ])
            ->whereNotIn('users.id', $hods_ids)
            ->select('users.*')
            ->get();

        if ($target_users->count() > 0 && !$is_account_manager) {
            $leave_request->is_cse = 1;
            $leave_request->save();
            //notify A.Ms of a leave request by CSE
            $action_url = url('admin/amleaverequests/leaverequest/' . $leave_request->id);

        } else {
            $target_users = User::join('department_heads', 'department_heads.user_id', 'users.id')
                ->where('department_heads.department_id', $staff->department_id)
                ->select('users.*')
                ->get();
            $action_url = url('admin/hodleaverequests/leaverequest/' . $leave_request->id);
        }

        try {
            Notification::send($target_users, new LeaveRequestNotification(request()->user()->name, $action_url));

        } catch (\Exception $e) {
            $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return redirect('admin/leaverequests')->with('notice', $notice);
        }
        $action = (\request()->id) ? "added" : "updated";
        return redirect('admin/leaverequests')->with('notice', ['type' => 'success', 'message' => 'Leave request ' . $action . ' successfully']);
    }

    /**
     * return leave requests values
     */
    public function listMyLeaveRequests()
    {
        $staff_id = @Staff::where('user_id', auth()->id())->first()->id;
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->where('leave_requests.staff_id', @$staff_id)
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid');
        if (\request('all'))
            return $leaverequests->get();
        return SearchRepo::of($leaverequests)
            ->addColumn('Request_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Return_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Days', function ($leaverequest) {
                $date_to = Carbon::parse($leaverequest->date_to);
                $date_from = Carbon::parse($leaverequest->date_from);
//                $date_diff=$date_to->diffInDays($date_from);
                $date_diff = $date_to->diffInDaysFiltered(function (Carbon $date) {
                    return !$date->isSunday();
                }, $date_from);
                return $date_diff;
            })
            ->addColumn('Comments', function ($leaverequest) {
                if ($leaverequest->reporting_omments)
                    return limit_string_words($leaverequest->reporting_omments, 15);
                return "";
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason, 15);
            })
            ->addColumn('Status', function ($leaverequest) {
                return getStaffLeaveRequestStatusButton($leaverequest->status);
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str .= '<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\'' . $leaverequest->id . '\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';
                if (getLeaveRequestStatus($leaverequest->status) == "pending") {
                    $str .= '&nbsp;&nbsp;<a href="' . url('admin/leaverequests/leaverequest/update/' . $leaverequest->id) . '"  class="btn badge btn-info btn-sm load-page"><i class="fa fa-edit"></i> Edit</a>';
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="runPlainRequest(\'' . url(request()->user()->role . '/leaverequests/leaverequest/cancel/' . $leaverequest->id) . '\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Cancel</a>';
                }
                return $str;
            })->make();
    }

    /**
     * delete leave
     */
    public function destroyLeave($leave_id)
    {
        $leave = LeaveRequest::findOrFail($leave_id);
        $leave->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave type deleted successfully']);
    }

}
