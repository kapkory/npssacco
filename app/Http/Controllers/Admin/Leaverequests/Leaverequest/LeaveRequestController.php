<?php

namespace App\Http\Controllers\Admin\Leaverequests\Leaverequest;

use App\Http\Controllers\Controller;
use App\Models\Core\LeaveRequest;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeaveRequestController extends Controller
{
    public function index($leave_request_id)
    {
        return view($this->folder."index");
    }
    public function updateLeaveRequest($leave_request_id)
    {
        $leave_request = LeaveRequest::whereId($leave_request_id)->firstOrFail();
        return view($this->folder."update",compact('leave_request'));
    }

    /**
     * cancel leave request
     */
    public function cancelLeaveRequest($leave_id)
    {
        $leave = LeaveRequest ::findOrFail($leave_id);
        $leave->status = getLeaveRequestStatus('canceled');
        $leave->canceled_at = Carbon::now();
        $leave->save();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave request canceled successfully']);
    }
    public function getLeaveRequestDetails($leave_request_id)
    {
        $leave_request = LeaveRequest ::findOrFail($leave_request_id);
        $staff = $this->getStaffDetails($leave_request->staff_id);
        $assuming_staff = Staff::join('users','users.id','staffs.user_id')
            ->where([
                ['staffs.id',$leave_request->assuming_staff_id]
            ])
            ->select('staffs.*','users.name')
            ->first();
        return view($this->folder."more_details",compact('leave_request','staff','assuming_staff'));
    }
}
