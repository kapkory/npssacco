<?php

namespace App\Http\Controllers\Admin\Permissiongroup;

use App\Http\Controllers\Controller;
use App\Models\Core\Department;
use App\Models\Core\PermissionGroup;
use App\Repositories\SearchRepo;
use App\User;
use Illuminate\Http\Request;

class PermissionGroupController extends Controller
{
    public function index()
    {

        return view($this->folder . 'index', [

        ]);
    }


    public function storeDepartment()
    {
        request()->validate([
            'name' => 'required|unique:permission_groups,name,' . \request('id'),
            'description' => 'required'

        ]);

        $data = \request()->all();
        $data['slug'] = str_slug($data['name']);
        $data['user_id'] = request()->user()->id;
        if (\request()->id) {
            $action = "updated";
        } else {
            $action = "saved";
            $data['permissions'] = '[]';
        }


        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Permission Group ' . $action . ' successfully']);
    }

    public function getDepartmentPermissions($id)
    {
        $department = PermissionGroup::findOrFail($id);

        $permissions = json_decode(file_get_contents(storage_path("app/system/roles.json")));
        $existing = json_decode($department->permissions);
        return view($this->folder . 'view', [
            'department' => $department,
            'permissions' => $permissions,
            'existing' => $existing
        ]);
    }

    public function viewDepartment($id)
    {
        $department = PermissionGroup::findOrFail($id);


        $permissions = json_decode(file_get_contents(storage_path("app/system/roles.json")));
        $existing = json_decode($department->permissions);
//        dd($this->folder);
        return view($this->folder . 'view', [
            'department' => $department,
            'permissions' => $permissions,
            'existing' => $existing
        ]);
    }

    public function getPermissions($id)
    {
        $department = PermissionGroup::findOrFail($id);
        $permissions = json_decode(file_get_contents(storage_path("app/system/roles.json")));
        $existing = json_decode($department->permissions);
        return view($this->folder . 'permissions', [
            'department' => $department,
            'permissions' => $permissions,
            'existing' => $existing
        ]);
    }

    public function updatePermissions($id)
    {
        $department = PermissionGroup::findOrFail($id);
        $permissions = \request('permissions');

        if ($permissions == null) {

            $permissions = '[]';
        } else {

            $permissions = json_encode($permissions);
        }
//       if($department->is_default){
//           return redirect()->back()->with('warning', ['type' => 'success', 'message' => 'User added successfully']);
//       }

        $department->permissions = $permissions;
        $department->update();
        return redirect("admin/permissiongroup/view/" . $department->id . '?tab=permissions')->with('notice', ['type' => 'success', 'message' => 'Permissions Updated successfully']);
    }

    public function addUser()
    {
        $permissionGroup = PermissionGroup::findOrFail(\request('department_id'));
        $user = User::findOrFail(\request('user_id'));
        $user->permission_group_id = $permissionGroup->id;
        $user->save();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'User added successfully']);
    }

    public function listDepartments()
    {
        $departments = PermissionGroup::where([
            ['id', '>', 0]

        ]);
        if (\request('all'))
            return $departments->get();
        return SearchRepo::of($departments)
            ->addColumn('system_users', function ($department) {
                $str = '<ul>';
                $users = $department->users;
                foreach ($users as $user) {
                    $str .= '<li>' . $user->name . ' <span onclick="runPlainRequest(\'' . url("admin/permissiongroup/user/remove/$user->id") . '\')" style="color: #f44336; cursor: pointer"><i class="fa fa-times"></i></span></li>';
                }
                $str .= '</ul>';
                $str .= '<a onclick="setDepartmentId(' . $department->id . ')" href="#add_user_modal" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Add</a>';
                return $str;
            })
            ->addColumn('permissions', function ($department) {
                $str = '<ul>';
                $permissions = json_decode($department->permissions);
                if (!$permissions)
                    $permissions = [];
                foreach ($permissions as $permission) {
                    $str .= '<li>' . ucwords(str_replace('_', ' ', $permission)) . '</li>';
                }
                $str .= '</ul>';
                $str .= '<a onclick="getDepartmentPermission(' . $department->id . ');" href="#permissions_modal" data-toggle="modal" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })
            ->addColumn('action', function ($department) {
                $str = '';
                $json = json_encode($department);
                $str .= '<a href="' . url('admin/permissiongroup/view/' . $department->id) . '"  class="btn badge btn-dark btn-sm load-page"><i class="fas fa-unlock-alt"></i> Permissions</a>&nbsp;|';
                $str .= '&nbsp;<a href="' . url('admin/permissiongroup/view/' . $department->id . '?tab=users') . '"  class="btn badge btn-primary btn-sm load-page"><i class="fa fa-users"></i> Users</a>&nbsp;|';
                $str .= '&nbsp;<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'department_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                return $str;
            })
            ->make();
    }

    public function getUsers()
    {
        $users = User::where([
            ['id', '<>', \request()->user()->id],
            ['role', 'admin'],
            ['permission_group_id', 0]
        ])->get();
        return $users;
    }

    public function listUsers()
    {
//        dd('hey');
        $users = User::join('permission_groups','permission_groups.id','users.permission_group_id')
            ->where([
                ['role', 'admin'],
                ['permission_group_id', \request('department_id')]
            ])->select('users.*','permission_groups.name as permission_group');
        if (\request('all'))
            return $users->get();
        return SearchRepo::of($users)
            ->addColumn('action', function ($user) {
                $str = '';
                $json = json_encode($user);
                $str .= '<a href="' . url("admin/users/user/$user->id") . '"   class="btn badge btn-success btn-sm load-page text-uppercase"><i class="fa fa-eye"></i> View </a>&nbsp; |';
                $str .= '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="runPlainRequest(\'' . url("admin/permissiongroup/user/remove/$user->id") . '\')" class="btn badge btn-danger btn-sm  text-uppercase"><i class="fa fa-times"></i> Remove ';
                return $str;
            })->make();


    }

    public function removeUser($id)
    {
        $user = User::findOrFail($id);

        if ($user->id == \request()->user()->id) {
            return redirect()->back()->with('notice', ['type' => 'warning', 'message' => 'You cannot remove yourself from a department']);
        } else {
            $user->permission_group_id = 0;
            $user->save();
            return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Staff removed successfully']);
        }

    }
}
