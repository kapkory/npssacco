<?php

namespace App\Http\Controllers\Admin\Hrleaverequests\Leaverequest;

use App\Http\Controllers\Controller;
use App\Models\Core\AccountManager;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\LeaveRequest;
use App\Models\Core\LeaveType;
use App\Models\Core\Staff;
use App\Models\Core\YearlyStaffLeavesTracker;
use App\Notifications\ApprovedLeaveRequest;
use App\Notifications\RejectedLeaveRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeaveRequestController extends Controller
{
    public function index($leave_request_id)
    {
        $leave_request = LeaveRequest::findOrFail($leave_request_id);
        $staff = $this->getStaffDetails($leave_request->staff_id);
        $assuming_staff = Staff::join('users', 'users.id', 'staffs.user_id')
            ->where([
                ['staffs.id', $leave_request->assuming_staff_id]
            ])
            ->select('staffs.*', 'users.name')
            ->first();
        return view($this->folder . "index", compact('leave_request', 'staff', 'assuming_staff'));
    }

    public function getLeaveRequestDetails($leave_request_id)
    {
        $leave_request = LeaveRequest::findOrFail($leave_request_id);
        $staff = $this->getStaffDetails($leave_request->staff_id);
        $assuming_staff = Staff::join('users', 'users.id', 'staffs.user_id')
            ->where([
                ['staffs.id', $leave_request->assuming_staff_id]
            ])
            ->select('staffs.*', 'users.name')
            ->first();
        return view($this->folder . "more_details", compact('leave_request', 'staff', 'assuming_staff'));
    }

    /**
     * approve leave request
     */
    public function approveLeaveRequest()
    {
        request()->validate([
            'approve_comment' => 'required|string'
        ]);
        $leave = LeaveRequest::findOrFail(request()->id);
        $leave->hr_reject_reason = null;
        $leave->hr_approve_comment = request()->approve_comment;
        $leave->status = getLeaveRequestStatus('hr_approved');
        $leave->hr_action_at = Carbon::now();
        $leave->save();
        $staff = User::join('staffs', 'users.id', 'staffs.user_id')
            ->where('staffs.id', $leave->staff_id)
            ->select('users.*')
            ->first();
        $staff_name = ucfirst($staff->name) . "'s";
        $staff_message = "Your pending leave request has been approved, click the button below to check.";
        $hod_message = $staff_name . " pending leave request has been approved by " . auth()->user()->name . "(H.R), click the button below to check.";

        $staff_action_url = url('admin/leaverequests/leaverequest/' . $leave->id);
        $hod_action_url = url('admin/hodleaverequests/leaverequest/' . $leave->id);

        $hods = User::join('department_heads', 'department_heads.user_id', 'users.id')
            ->where('department_heads.department_id', $staff->department_id)
            ->select('users.*')
            ->get();
        try {
            foreach ($hods as $user) {
                $user->notify(new ApprovedLeaveRequest($hod_action_url, $hod_message));
            }
            $staff->notify(new ApprovedLeaveRequest($staff_action_url, $staff_message));
        } catch (\Exception $e) {
            $mail_error_message = 'Opps! Email alert failed to send, You may need to escalate this manually.';
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return redirect()->back()->with('notice', $notice);
        }


        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave request approved successfully']);
    }

    /**
     * reject leave request
     */
    public function rejectLeaveRequest()
    {
        request()->validate([
            'reject_reason' => 'required|string'
        ]);
        $leave = LeaveRequest::findOrFail(request()->id);
        $leave->status = getLeaveRequestStatus('hr_rejected');
        $leave->hr_approve_comment = null;
        $leave->hr_reject_reason = request()->reject_reason;
        $leave->hr_action_at = Carbon::now();
        $leave->save();

        $staff = User::join('staffs', 'users.id', 'staffs.user_id')
            ->where('staffs.id', $leave->staff_id)
            ->select('users.*')
            ->first();
        $staff_name = ucfirst($staff->name) . "'s";
        $staff_message = "Your pending leave request has been rejected, click the button below to check.";
        $hod_message = $staff_name . " pending leave request has been rejected by " . auth()->user()->name . "(H.R), click the button below to check.";

        $staff_action_url = url('admin/leaverequests/leaverequest/' . $leave->id);
        $hod_action_url = url('admin/hodleaverequests/leaverequest/' . $leave->id);

        $hods = User::join('department_heads', 'department_heads.user_id', 'users.id')
            ->where('department_heads.department_id', $staff->department_id)
            ->select('users.*')
            ->get();

        try {
            foreach ($hods as $user) {
                $user->notify(new RejectedLeaveRequest($hod_action_url, $hod_message));
            }
            $staff->notify(new RejectedLeaveRequest($staff_action_url, $staff_message));
        } catch (\Exception $e) {
            $mail_error_message = 'Opps! Email alert failed to send, You may need to escalate this manually.';
            $notice = ['type' => 'error', 'message' => $mail_error_message];
            return redirect()->back()->with('notice', $notice);
        }
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave request rejected successfully']);
    }

    /**
     * mark staff leave
     * request as returned
     */
    public function setStaffLeaveRequestReturnDetails()
    {
        request()->validate([
            'date_returned' => 'required|date|before:tomorrow',
            'days_out' => 'required|numeric',
            'comment' => 'required|string'
        ]);
        $leaverequest = LeaveRequest::findOrFail(request()->id);
        $leaverequest->date_returned = request()->date_returned;
        $leaverequest->reporting_comments = request()->comment;
        $leaverequest->days_out = request()->days_out;
        $leaverequest->save();
        $this->syncStaffYearlyLeaveRequests($leaverequest);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Staff leave updated and set as returned']);

    }

    /**
     * update staff yearly leave
     * requests
     */
    public function syncStaffYearlyLeaveRequests($leaverequest)
    {
        $date_returned = Carbon::parse($leaverequest->date_returned);
        $date_from = Carbon::parse($leaverequest->date_from);
        $calc_days_covered = $date_returned->diffInDaysFiltered(function (Carbon $date) {
            return !$date->isSunday();
        }, $date_from);
        $days_covered = ($leaverequest->days_out > 0) ? $leaverequest->days_out : $calc_days_covered;

        $staff_yearly_data = YearlyStaffLeavesTracker::where([
            ['staff_id', $leaverequest->staff_id],
            ['leave_type_id', $leaverequest->leave_type_id],
            ['year', Carbon::parse($leaverequest->created_at)->format('Y')]
        ])->first();
        if ($staff_yearly_data) {
            $staff_yearly_data->days_covered += $days_covered;
            $staff_yearly_data->days_remaining -= $days_covered;
        } else {
            $leavetype = LeaveType::whereId($leaverequest->leave_type_id)->first();
            $max_days = $leavetype->entitlement;
            $staff_yearly_data = new YearlyStaffLeavesTracker();
            $staff_yearly_data->staff_id = $leaverequest->staff_id;
            $staff_yearly_data->leave_type_id = $leaverequest->leave_type_id;
            $staff_yearly_data->year = Carbon::parse($leaverequest->created_at)->format('Y');
            $staff_yearly_data->days_covered = $days_covered;
            $staff_yearly_data->days_remaining = $max_days - $days_covered;
            $staff_yearly_data->max_days_allowed = $max_days;
        }

        $staff_yearly_data->save();
        return;
    }

    /**
     * @param $leave_request_id
     * calculate staff days out
     */
    public function calculateDaysCovered($leave_request_id)
    {
        $leaverequest = LeaveRequest::findOrFail($leave_request_id);
        $date_returned = request()->date_returned;
        $date_returned = Carbon::parse($date_returned);
        $date_from = Carbon::parse($leaverequest->date_from);
        $calc_days_covered = $date_returned->diffInDaysFiltered(function (Carbon $date) {
            return !$date->isSunday();
        }, $date_from);
        $days_covered = ($calc_days_covered < 1) ? 0.5 : $calc_days_covered;

        return $days_covered;
    }

    public function addLeaveRecord($staff_id)
    {
        $staff = Staff::join('users', 'users.id', 'staffs.user_id')->select('staffs.*', 'users.phone as phone')->findOrFail($staff_id);

        return view($this->folder . 'includes.leave_record', compact('staff'));
    }

    public function postLeaveRecord()
    {
        \request()->validate([
            'leave_type_id'=>'required',
            'date_from'=>'required',
            'date_to'=>'required',
            'days_out'=>'required',
            'reason'=>'required'
        ]);

        $staff = @Staff::findOrFail(\request()->id);
        $this->syncStaffAvailableAnnualLeaveDays($staff->user_id);

        $hods_ids = CalltronixDepartmentHead::all()->pluck('user_id')->toArray();
        $date_from = Carbon::parse(request()->date_from);
        $date_to = Carbon::parse(request()->date_to);
        if ($date_to < $date_from)
            return \Response::json(["errors" => ["date_to" => ["Please select a date later or same as date to field."]]], 422);

        if($date_to->isSunday())
            return \Response::json(["errors" => ["date_to" => ["You've selected \"Sunday\" please select a working day. "]]], 422);
        if($date_from->isSunday())
            return \Response::json(["errors" => ["date_from" => ["You've selected \"Sunday\" please select a working day. "]]], 422);
        $is_account_manager = AccountManager::where('user_id', auth()->id())->first();

        $diff = $date_from->diffInDays($date_to);
        $max_days = $this->staffAvailableLeaveTypeDays($staff->id, request()->leave_type_id);

        if ($diff > $max_days)
            return \Response::json(["errors" => ["date_to" => ["Max. allowed days (" . $max_days . ") exceeded "]]], 422);
        $data = \request()->all();
        $data['form_model'] = LeaveRequest::class;
        $data['staff_id'] = $staff->id;
        $leave_request = $this->autoSaveModel($data);

        //check if is CSE ie check if LOB has an Account Manager
        $target_users = User::join('account_managers', 'account_managers.user_id', 'users.id')
            ->where([
                ['account_managers.line_of_business_id', $staff->line_of_business_id],
                ['users.id', '<>', $staff->user_id]
            ])
            ->whereNotIn('users.id', $hods_ids)
            ->select('users.*')
            ->get();

        if ($target_users->count() > 0 && !$is_account_manager) {
            $leave_request->is_cse = 1;
           }

        $leave_request->status = 2;
        $leave_request->date_returned = $date_to;
        $leave_request->days_out = request()->days_out;
        $leave_request->save();

        $this->syncStaffYearlyLeaveRequests($leave_request);

        $action = (\request()->id) ? "added" : "updated";
        return back()->with('notice', ['type' => 'success', 'message' => 'Leave request ' . $action . ' successfully']);


    }
}
