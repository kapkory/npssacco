<?php

namespace App\Http\Controllers\Admin\Hrleaverequests;

use App\Http\Controllers\Controller;
use App\Models\Core\CalltronixDepartment;
use App\Models\Core\LeaveType;
use App\Models\Core\LeaveRequest;
use App\Models\Core\Staff;
use App\Models\Core\YearlyStaffLeavesTracker;
use App\Repositories\SearchRepo;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeaveRequestsController extends Controller
{
    public function index()
    {
        $staff = @Staff::where('user_id', auth()->id())->first();
        $calltronix_department = CalltronixDepartment::whereId($staff->department_id)->first();
        return view($this->folder . 'index', compact('calltronix_department'));
    }

    public function viewLeaveRequestsDashboard()
    {

        return view($this->folder . 'dashboard');
    }

    /**
     * return leave requests values
     */
    public function listLeaveRequestsByStatus($status)
    {
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where('leave_requests.status', getLeaveRequestStatus($status))
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid', 'users.name as staff', 'users.email');
        if (\request('all'))
            return $leaverequests->get();
        return SearchRepo::of($leaverequests)
            ->addColumn('Request_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Return_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Days', function ($leaverequest) {
                $date_to = Carbon::parse($leaverequest->date_to);
                $date_from = Carbon::parse($leaverequest->date_from);
                $date_diff = $date_to->diffInDaysFiltered(function (Carbon $date) {
                    return !$date->isWeekend();
                }, $date_from);
                return $date_diff;
            })
            ->addColumn('Comments', function ($leaverequest) {
                if ($leaverequest->reporting_omments)
                    return limit_string_words($leaverequest->reporting_omments, 15);
                return "";
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason, 15);
            })
            ->addColumn('Status', function ($leaverequest) {
                return getLeaveRequestStatusButton($leaverequest->status);
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str .= '<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\'' . $leaverequest->id . '\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if (getLeaveRequestStatus($leaverequest->status) == "hod_approved") {
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="approveRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
                $str .= '&nbsp;&nbsp;<a href="' . url('admin/hrleaverequests/leaverequest/' . $leaverequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }

    /**
     * return staffs - leave requests values -returning today
     */
    public function listStaffLeavesReturningToday($count = null)
    {
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['leave_requests.date_returned', null],
                ['leave_requests.status', getLeaveRequestStatus('hr_approved')]
            ])
            ->whereDate('leave_requests.date_to', Carbon::today())
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid', 'users.name as staff', 'users.email');
        if (\request('all'))
            return $leaverequests->get();
        if ($count)
            return $leaverequests->count();
        return SearchRepo::of($leaverequests)
            ->addColumn('Request_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Return_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Days', function ($leaverequest) {
                $date_to = Carbon::parse($leaverequest->date_to);
                $date_from = Carbon::parse($leaverequest->date_from);
                $date_diff = $date_to->diffInDaysFiltered(function (Carbon $date) {
                    return !$date->isWeekend();
                }, $date_from);
                return $date_diff;
            })
            ->addColumn('Comments', function ($leaverequest) {
                if ($leaverequest->reporting_omments)
                    return limit_string_words($leaverequest->reporting_omments, 15);
                return "";
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason, 15);
            })
            ->addColumn('Status', function ($leaverequest) {
                return getLeaveRequestStatusButton($leaverequest->status);
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str .= '<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\'' . $leaverequest->id . '\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if (getLeaveRequestStatus($leaverequest->status) == "hod_approved") {
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="approveRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
                $str .= '&nbsp;&nbsp;<a href="' . url('admin/hrleaverequests/leaverequest/' . $leaverequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }

    /**
     * return staffs - leave requests values -not yet returned
     */
    public function listStaffLeavesNotYestReturned($count = null)
    {
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['leave_requests.status', getLeaveRequestStatus('hr_approved')],
                ['leave_requests.date_returned', null]
            ])
            ->whereDate('leave_requests.date_to', '<', Carbon::today())
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid', 'users.name as staff', 'users.email');
        if (\request('all'))
            return $leaverequests->get();
        if ($count)
            return $leaverequests->count();
        return SearchRepo::of($leaverequests)
            ->addColumn('Request_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Expected_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Days', function ($leaverequest) {
                $date_to = Carbon::parse($leaverequest->date_to);
                $date_from = Carbon::parse($leaverequest->date_from);
                $date_diff = $date_to->diffInDaysFiltered(function (Carbon $date) {
                    return !$date->isWeekend();
                }, $date_from);
                return number_format($date_diff);
            })
            ->addColumn('Days_exceeded', function ($leaverequest) {
                $date_today = Carbon::today();
                $target_date = Carbon::parse($leaverequest->date_to);
                $days = $target_date->diffInDaysFiltered(function (Carbon $date) {
                    return !$date->isWeekend();
                }, $date_today);
//                return $date_today." | ".$target_date." | ".$days;
                return "<span class='text-danger'>" . number_format($days) . "</span>";
            })
            ->addColumn('Comments', function ($leaverequest) {
                if ($leaverequest->reporting_omments)
                    return limit_string_words($leaverequest->reporting_omments, 15);
                return "";
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Status', function ($leaverequest) {
                return getLeaveRequestStatusButton($leaverequest->status);
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str .= '<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\'' . $leaverequest->id . '\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if (getLeaveRequestStatus($leaverequest->status) == "hod_approved") {
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="approveRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
                $str .= '&nbsp;&nbsp;<a href="' . url('admin/hrleaverequests/leaverequest/' . $leaverequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                $str .= '&nbsp;&nbsp;<a href="#" onclick="setAsReturned(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-primary btn-sm"><i class="fa fa-check-circle"></i> Mark returned</a>';

                return $str;
            })->make();
    }

    public function getReturningLeavesTabCounts()
    {
        return [
            'today' => $this->listStaffLeavesReturningToday(1),
            'all' => $this->listStaffLeavesNotYestReturned(1),
        ];
    }

    /**
     * return my department leave requests
     */
    public function listLeaveRequests()
    {
//        $statuses = getLeaveRequestStatus(['hod_approved','hod_rejected','hr_approved','hr_rejected','am_rejected','am_approved']);
        $statuses = getLeaveRequestStatus(['pending', 'canceled']);
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->whereNotIn('leave_requests.status', $statuses)
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid', 'users.name as staff', 'users.email');
        if (\request('all'))
            return $leaverequests->get();
        if (request('count'))
            return $this->getTabsCount();
        return SearchRepo::of($leaverequests)
            ->addColumn('Request_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Return_date', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Days', function ($leaverequest) {
                $date_to = Carbon::parse($leaverequest->date_to);
                $date_from = Carbon::parse($leaverequest->date_from);
//                $date_diff=$date_to->diffInDays($date_from);
                $date_diff = $date_to->diffInDaysFiltered(function (Carbon $date) {
                    return !$date->isWeekend();
                }, $date_from);
                return $date_diff;
            })
            ->addColumn('Comments', function ($leaverequest) {
                if ($leaverequest->reporting_omments)
                    return limit_string_words($leaverequest->reporting_omments, 15);
                return "";
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason, 15);
            })
            ->addColumn('Status', function ($leaverequest) {
                $status_str = getLeaveRequestStatusButton($leaverequest->status);
//                $status_str = getLeaveRequestStatus($leaverequest->status);
//                if($status_str == "hod_approved")
//                    return "<span class='text-primary'><i class='fa fa-spinner'></i> <b>Pending</b></span>";
//                else if($status_str == "hr_approved")
//                    return "<span class='text-success'><i class='fa fa-thumbs-up'></i> <b>Approved</b></span>";
//                else if($status_str == "hr_rejected")
//                    return "<span class='text-danger'><i class='fa fa-thumbs-down'></i> <b>Rejected</b></span>";
                return $status_str;
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str .= '<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\'' . $leaverequest->id . '\');" data-toggle="modal" class="btn badge btn-primary btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';

                if (getLeaveRequestStatus($leaverequest->status) == "hod_approved") {
                    $str .= '<a href="#" onclick="approveRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check-circle"></i> Approve</a>';
                    $str .= '&nbsp;&nbsp;<a href="#" onclick="rejectRequest(\'' . $leaverequest->id . '\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Reject</a>';
                }
                $str .= '&nbsp;&nbsp;<a href="' . url('admin/hrleaverequests/leaverequest/' . $leaverequest->id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-eye"></i> view</a>';
                return $str;
            })->make();
    }

    /**
     * return staff leave requests
     */
    public function listStaffLeaveRequests($staff_id)
    {
//        $staff = @Staff::where('user_id',auth()->id())->first();
        $leaverequests = LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where([
                ['leave_requests.status', '<>', getLeaveRequestStatus('canceled')],
                ['staffs.id', $staff_id],
//                ['staffs.department_id',@$staff->department_id]
            ])
            ->select('leave_requests.*', 'leave_types.name as leave_type', 'leave_types.is_paid', 'users.name as staff', 'users.email');
        if (\request('all'))
            return $leaverequests->get();
        return SearchRepo::of($leaverequests)
            ->addColumn('Date_from', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_from)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Date_to', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->date_to)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Created_at', function ($leaverequest) {
                $date_str = Carbon::parse($leaverequest->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('Reason', function ($leaverequest) {
                return limit_string_words($leaverequest->reason, 15);
            })
            ->addColumn('Status', function ($leaverequest) {
                return getLeaveRequestStatusButton($leaverequest->status);
            })
            ->addColumn('Is_paid', function ($leaverequest) {
                return ($leaverequest->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leaverequest) {
                $str = '';
                $str .= '<a href="#more_leave_request_info_modal" onclick="getMoreDetails(\'' . $leaverequest->id . '\');" data-toggle="modal" class="btn badge btn-outline-dark btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';
                return $str;
            })->make();
    }

    /**
     * count tabs
     */
    public function getTabsCount()
    {
        return [
            'approved' => $this->countLeaveRequests('hr_approved'),
            'pending' => $this->countLeaveRequests('hod_approved'),
            'rejected' => $this->countLeaveRequests('hr_rejected'),
            'all_requests' => $this->countLeaveRequests(),
        ];
    }

    public function countLeaveRequests($status = null)
    {
//        $statuses = getLeaveRequestStatus(['hod_approved','hr_approved','hr_rejected']);
        $statuses = getLeaveRequestStatus(['pending', 'canceled']);
        if (!$status)
            return LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
                ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
                ->leftJoin('users', 'staffs.user_id', 'users.id')
                ->whereNotIn('leave_requests.status', $statuses)
                ->count();
        return LeaveRequest::join('leave_types', 'leave_requests.leave_type_id', 'leave_types.id')
            ->leftJoin('staffs', 'leave_requests.staff_id', 'staffs.id')
            ->leftJoin('users', 'staffs.user_id', 'users.id')
            ->where('leave_requests.status', getLeaveRequestStatus($status))
            ->count();
    }


    public function hrLeaveTracker()
    {
        $hrleavetrackers = YearlyStaffLeavesTracker::join('staffs', 'staffs.id', 'yearly_staff_leaves_trackers.staff_id')
            ->join('users', 'users.id', 'staffs.user_id')
            ->join('leave_types', 'leave_types.id', 'yearly_staff_leaves_trackers.leave_type_id')
            ->select('yearly_staff_leaves_trackers.*', 'users.name as staff_name', 'leave_types.name as leave_type_name');

        if (\request('all'))
            $hrleavetrackers->get();
        return SearchRepo::of($hrleavetrackers)
            ->addColumn('action', function ($hrleavetracker) {
                $str = '';
                $str .= '<a href="' . url('admin/hrleaverequests/staffleavehistory/' . $hrleavetracker->staff_id) . '"  class="btn badge btn-outline-dark btn-sm load-page"><i class="fa fa-exclamation-circle"></i> More</a>';
                return $str;
            })->make();
    }

    public function viewLeaveTracker()
    {
        return view($this->folder . 'hrleavestracker');
    }

    public function viewStaffLeaveHistory($staff_id)
    {
        $staff = Staff::join('users', 'users.id', 'staffs.user_id')
            ->select('staffs.*', 'users.name')->find($staff_id);
        $myLeaveBalance = YearlyStaffLeavesTracker::where([
            'staff_id' => $staff->id,
            'leave_type_id' => getAnnualLeaveTypeId(),
            'year' => date('Y'),
        ])->get();
//        dd($myLeaveBalance);
        return view($this->folder . 'staff_leave_history', compact('staff', 'myLeaveBalance'));
    }

    public function updateLeaveTracker($id)
    {
        $data = \request()->all();
        $leave_tracker = YearlyStaffLeavesTracker::findOrFail($id);
        $remaining_days = ($leave_tracker->max_days_allowed + $data['prev_year_bal']) - $data['days_covered'];
        $leave_tracker->update([
            'days_covered' => $data['days_covered'],
            'prev_year_bal' => $data['prev_year_bal'],
            'days_remaining' => $remaining_days
        ]);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave tracker updated successfully']);

    }
}
