<?php

namespace App\Http\Controllers\Admin\Staffexits;

use App\Http\Controllers\Controller;
use App\Models\Core\Staff;
use Illuminate\Http\Request;

use App\Models\Core\StaffExit;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class StaffExitsController extends Controller
{
    /**
     * return staffexit's index view
     */
    public function index()
    {
        return view($this->folder . 'index');
    }

    /**
     * store staffexit
     */
    public function storeStaffExit()
    {
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        $action = (request()->id) ? "updated" : "saved";
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Staff exit request' . $action . ' successfully']);
    }

    /**
     * return staffexit values
     */
    public function listStaffExits()
    {
        $staffexits = StaffExit::leftJoin('users', 'users.id', 'staff_exits.staff_user_id')
            ->leftJoin('users as users1', 'users1.id', 'staff_exits.user_id')
            ->leftJoin('staffs', 'staffs.user_id', 'staff_exits.staff_user_id')
            ->select('staff_exits.*', 'staffs.employment_status', 'users.name as staff', 'users1.name as discharged_by');
        if (\request('all'))
            return $staffexits->get();
        return SearchRepo::of($staffexits)
            ->addColumn('Discharge_reason', function ($staffexit) {
                return limit_string_words($staffexit->discharge_reason, 18);
            })
            ->addColumn('Staff_status', function ($staffexit) {
                return getStaffEmploymentStatusButton($staffexit->employment_status);
            })
            ->addColumn('action', function ($staffexit) {
                $str = '';
                $json = json_encode($staffexit);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'staffexit_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/staffexits/delete').'\',\''.$staffexit->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }


    /**
     * return staff values
     */
    public function listDismissedStaff()
    {
        $staff = Staff::join('users', 'users.id', 'staffs.user_id')
            ->leftJoin('staff_exits', 'users.id', 'staff_exits.staff_user_id')
            ->leftJoin('line_of_businesses', 'line_of_businesses.id', 'staffs.line_of_business_id')
            ->leftJoin('departments', 'departments.id', 'staffs.department_id')
            ->leftJoin('staff_roles', 'staff_roles.id', 'staffs.staff_role_id')
            ->leftJoin('positions', 'positions.id', 'staffs.position_id')
            ->where([
                ['staffs.employment_status', getStaffEmploymentStatus('dismissed')]
            ])->select("staffs.*", "users.name as name", "departments.name as department", "positions.name as position", "staff_roles.name as role",
                "users.phone", "users.email");
        if (\request('all'))
            return $staff->get();
        return SearchRepo::of($staff)
            ->addColumn('Employee_no', function ($staff) {
                return "<a onMouseOver=\"this.style.color='#ffb833'\" onMouseOut=\"this.style.color='#31b0d5'\" style='color: #31b0d5' class='load-page font-weight-bolder' href='" . url('admin/staffs/staff/' . $staff->id) . "'>" . $staff->employee_number . "</a>";
            })
            ->addColumn('role', function ($staff) {
                if (!$staff->role)
                    return "N/A";
                return $staff->role;
            })
            ->addColumn('Status', function ($staff) {
                return getStaffEmploymentStatusButton($staff->employment_status);
            })
            ->addColumn('action', function ($staff) {
                $str = '';
                $json = json_encode($staff);
                $str .= '<a href="' . url('admin/staffs/staff/' . $staff->id) . '"  class="btn load-page badge btn-success btn-sm"><i class="fa fa-eye"></i> View</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/staff/delete').'\',\''.$staff->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete staffexit
     */
    public function destroyStaffExit($staffexit_id)
    {
        $staffexit = StaffExit::findOrFail($staffexit_id);
        $staffexit->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'StaffExit deleted successfully']);
    }

}
