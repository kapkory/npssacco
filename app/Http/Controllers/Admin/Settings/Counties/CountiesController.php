<?php

namespace App\Http\Controllers\Admin\Settings\Counties;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\County;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class CountiesController extends Controller
{
    /**
     * return county's index view
     */
    public function index()
    {
        return view($this->folder . 'index', [

        ]);
    }

    /**
     * store county
     */
    public function storeCounty()
    {
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('counties', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if (\request()->id) {
            $action = "updated";
        } else {
            $action = "saved";
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'County ' . $action . ' successfully']);
    }

    /**
     * return county values
     */
    public function listCounties()
    {
        $counties = County::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $counties->get();
        return SearchRepo::of($counties)
            ->addColumn('action', function ($county) {
                $str = '';
                $json = json_encode($county);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'county_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/counties/delete').'\',\''.$county->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete county
     */
    public function destroyCounty($county_id)
    {
        $county = County::findOrFail($county_id);
        $county->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'County deleted successfully']);
    }

}
