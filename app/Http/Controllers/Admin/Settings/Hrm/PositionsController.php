<?php

namespace App\Http\Controllers\Admin\Settings\Hrm;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\Position;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class PositionsController extends Controller
{
    /**
     * return position's index view
     */
    public function index()
    {
        return view($this->folder . 'index', [

        ]);
    }

    /**
     * store position
     */
    public function storePosition()
    {
        request()->validate([
            'name' => 'required|unique:positions,name,' . \request('id'),
        ]);
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('positions', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if (\request()->id) {
            $action = "updated";
        } else {
            $action = "saved";
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Position ' . $action . ' successfully']);
    }

    /**
     * return position values
     */
    public function listPositions()
    {
        $positions = Position::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $positions->get();
        return SearchRepo::of($positions)
            ->addColumn('action', function ($position) {
                $str = '';
                $json = json_encode($position);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'position_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';

                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/positions/delete').'\',\''.$position->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete position
     */
    public function destroyPosition($position_id)
    {
        $position = Position::findOrFail($position_id);
        $position->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Position deleted successfully']);
    }
}
