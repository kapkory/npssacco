<?php

namespace App\Http\Controllers\Admin\Settings\Hrm;

use App\Models\Core\Identification;
use App\Models\Core\SpotCheck;
use App\Models\Core\SpotCheckCategory;
use App\Models\Core\TruckModel;
use App\Repositories\SearchRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;

class HrmConfigController extends Controller
{
    public function index()
    {
        return view($this->folder . 'index');
    }


}
