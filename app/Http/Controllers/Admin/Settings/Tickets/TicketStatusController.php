<?php

namespace App\Http\Controllers\Admin\Settings\Tickets;

use App\Models\Core\Ticket;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\TicketStatus;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class TicketStatusController extends Controller
{
    /**
     * return ticketstatus's index view
     */
    public function index(){
        return view($this->folder.'statuses',[

        ]);
    }

    /**
     * store ticketstatus
     */
    public function storeTicketStatus(){
        request()->validate([
            'name' => 'required|unique:ticket_statuses,name,' . \request('id'),
        ]);
        $data = \request()->all();
        $data['slug']= Str::slug(\request()->name);
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('ticket_statuses', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if(\request()->id){
            $action="updated";
        }else{
            $action="saved";
        }

        $this->autoSaveModel($data);
        return redirect()->back()->with('notice',['type'=>'success','message'=>'TicketStatus '.$action.' successfully']);
    }

    /**
     * return ticketstatus values
     */
    public function listTicketStatuses(){

        $ticketstatuses = TicketStatus::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $ticketstatuses->get();
//        if(\request('ticket_id')){
//            $ticket = Ticket::leftJoin('ticket_statuses','ticket_statuses.id','tickets.ticket_status_id')
//                ->where('tickets.id',request()->ticket_id)
//                ->select('tickets.*','ticket_statuses.id as ticket_status')
//                ->first();
//            $current = $ticket->ticket_status_id;
//            if($ticket->user_id == auth()->id()){ //user who created the ticket
//                return $ticketstatuses->whereIn('id',$this->getTicketStatus(['closed','open']))->get();
//            }
//            if($ticket->assigned_to == auth()->id() && !in_array($ticket->ticket_status,$this->getTicketStatus([''])))
//                return $ticketstatuses->whereIn('id',$this->getTicketStatus(['resolved','wait_for_response','in_progress']))->get();
//        return $ticketstatuses->get();
//        }

        return SearchRepo::of($ticketstatuses)
            ->addColumn('action',function($ticketstatus){
                $str = '';
                $json = json_encode($ticketstatus);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'ticketstatus_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/ticketstatuses/delete').'\',\''.$ticketstatus->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete ticketstatus
     */
    public function destroyTicketStatus($ticketstatus_id)
    {
        $ticketstatus = TicketStatus::findOrFail($ticketstatus_id);
        $ticketstatus->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'TicketStatus deleted successfully']);
    }

    public function getTicketStatus($state){

        $statuses = [
            'open' => 1,
            'in_progress' => 2,
            'wait_for_response' => 3,
            'resolved' => 4,
            'closed' => 5
        ];
        if (is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st){
                $states[] = $statuses[$st];
            }

            return $states;
        }
        return $statuses[$state];
    }
}
