<?php

namespace App\Http\Controllers\Admin\Settings\Tickets;

use App\Models\Core\IssueCategoryIssueSubcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\IssueSubCategory;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class IssueSubCategoriesController extends Controller
{
    /**
     * return issuesubcategory's index view
     */
    public function index()
    {
        return view($this->folder . 'issue_sub_categories', [
        ]);
    }

    /**
     * store issuesubcategory
     */
    public function storeIssueSubCategory()
    {
        request()->validate([
            'name' => 'required|unique:issue_sub_categories,name,' . \request('id'),
        ]);
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('issue_sub_categories', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if (\request()->id) {
            $action = "updated";
        } else {
            $action = "saved";
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'IssueSubCategory ' . $action . ' successfully']);
    }

    /**
     * return issuesubcategory values
     */
    public function listIssueSubCategories()
    {
        if (\request()->issue_category_id) {
            $id = \request()->issue_category_id;
            $data = IssueCategoryIssueSubcategory::join('issue_sub_categories', 'issue_sub_categories.id', 'issue_category_issue_sub_category.issue_sub_category_id')
                ->where([
                    ['issue_category_issue_sub_category.issue_category_id', $id],
                    ['issue_sub_categories.status', 1]
                ])->select('issue_sub_categories.*')->get();
            return $data;
        }
        $issuesubcategories = IssueSubCategory::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $issuesubcategories->get();
        return SearchRepo::of($issuesubcategories)
            ->addColumn('action', function ($issuesubcategory) {
                $str = '';
                $json = json_encode($issuesubcategory);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'issue_sub_category_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/issuesubcategories/delete').'\',\''.$issuesubcategory->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete issuesubcategory
     */
    public function destroyIssueSubCategory($issuesubcategory_id)
    {
        $issuesubcategory = IssueSubCategory::findOrFail($issuesubcategory_id);
        $issuesubcategory->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'IssueSubCategory deleted successfully']);
    }

}
