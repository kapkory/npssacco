<?php

namespace App\Http\Controllers\Admin\Settings\Tickets;

use App\Http\Controllers\Controller;
use App\Models\Core\CrownDepartment;
use App\Models\Core\DispositionIssueCategory;
use App\Models\Core\IssueCategory;
use App\Models\Core\IssueCategoryIssueSubcategory;
use Illuminate\Http\Request;

use App\Models\Core\Disposition;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class DispositionsController extends Controller
{
    /**
     * return disposition's index view
     */
    public function index()
    {

        return view($this->folder . 'dispositions', [

        ]);
    }

    /**
     * store disposition
     */
    public function storeDisposition()
    {
        request()->validate([
            'name' => 'required|unique:dispositions,name,' . \request('id'),
        ]);
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Disposition saved successfully']);
    }

    /**
     * return disposition values
     */
    public function listDispositions()
    {
        if (\request()->issue_category_id) {
            $id = \request()->issue_category_id;
            $data = DispositionIssueCategory::join('dispositions', 'dispositions.id', 'disposition_issue_category.disposition_id')
                ->where([
                    ['disposition_issue_category.issue_category_id', $id],
                    ['disposition_issue_category.issue_sub_category_id', \request()->issue_sub_category_id],
                    ['dispositions.status', 1]
                ])->select('dispositions.*')->get();
            return $data;
        }
        $dispositions = Disposition::join('users', 'users.id', 'dispositions.user_id')->where([
            ['dispositions.id', '>', 0],
            ['dispositions.status', 1]

        ])->select('dispositions.*', 'users.name as created_by');
        if (\request('all'))
            return $dispositions->get();
        return SearchRepo::of($dispositions)
            ->addColumn('disposition', function ($disposition) {
                return $disposition->name;
            })
            ->addColumn('action', function ($disposition) {
                $str = '';
                $json = json_encode($disposition);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'disposition_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/dispositions/delete').'\',\''.$disposition->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete disposition
     */
    public function destroyDisposition($disposition_id)
    {
        $disposition = Disposition::findOrFail($disposition_id);
        $disposition->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Disposition deleted successfully']);
    }

}
