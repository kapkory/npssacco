<?php

namespace App\Http\Controllers\Admin\Settings\Tickets;

use App\Http\Controllers\Controller;
use App\Models\Core\Disposition;
use App\Models\Core\DispositionIssueCategory;
use App\Models\Core\IssueCategoryIssueSubcategory;
use App\Models\Core\IssueSubCategory;
use Illuminate\Http\Request;

use App\Models\Core\IssueCategory;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;
use DB;
use Illuminate\Support\Facades\Session;

class IssueCategoriesController extends Controller
{
    /**
     * return issuecategory's index view
     */
    public function index()
    {
        return view($this->folder . 'issue_categories', [

        ]);
    }

    public function manageDispositionView($id)
    {
        $issue_category = IssueCategory::find($id);
        $existing = DispositionIssueCategory::where('issue_category_id', $id)->pluck('disposition_id')->toArray();
        $dispositions = Disposition::all();
        $first_sub_category = IssueSubCategory::take(1)->first();
        $sub_category_id = null;
        if ($first_sub_category) {
            $sub_category_id = $first_sub_category->id;
        }
        if (Session::has('issue_sub_category_id')) {
            $sub_category_id = Session::get('issue_sub_category_id');
        }

        return view($this->folder . 'manage_dispositions', [
            'issue_category' => $issue_category,
            'sub_category_id' => $sub_category_id,
            'dispositions' => $dispositions,
            'existing' => $existing
        ]);
    }

    public function manageIssueSubCategoryView($id)
    {
        $issue_category = IssueCategory::find($id);
        $existing = IssueCategoryIssueSubcategory::where('issue_category_id', $id)->pluck('issue_sub_category_id')->toArray();
        $issueSubCategories = IssueSubCategory::all();
        return view($this->folder . 'manage_issue_sub_categories', [
            'issue_category' => $issue_category,
            'issue_sub_categories' => $issueSubCategories,
            'existing' => $existing
        ]);
    }

    /**
     * store dispositions for an issue category
     */
    public function saveDisposition()
    {

        if (empty(\request()->disposition_ids))
            return redirect()->back()->with('notice', ['type' => 'warning', 'message' => 'You must select at least one disposition ']);
        $arr = [];
        foreach (\request()->disposition_ids as $disposition) {
            $arr[$disposition] = ['issue_sub_category_id' => \request()->issue_sub_category_id, 'disposition_id' => $disposition, 'issue_category_id' => \request()->issue_category_id];
        }
//        ['disposition_issue_category.issue_category_id',  \request()->issue_category_id],
//            ['disposition_issue_category.issue_sub_category_id', \request()->issue_sub_category_id],
        Session::put('issue_sub_category_id', \request()->issue_sub_category_id);
        $issue_category = IssueCategory::find(\request()->issue_category_id);
        $issue_category->dispositions()
            ->wherePivot('disposition_issue_category.issue_category_id','=', \request()->issue_category_id)
            ->wherePivot('disposition_issue_category.issue_sub_category_id','=', \request()->issue_sub_category_id)

            ->sync($arr);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Disposition saved and updated successfully']);

    }


    /**
     * store saveIssueSubCategory for an issue category
     */
    public function saveIssueSubCategory()
    {

        if (empty(\request()->issue_sub_categories_ids))
            return redirect()->back()->with('notice', ['type' => 'warning', 'message' => 'You must select at least one issue sub category ']);
        $issue_category = IssueCategory::find(\request()->issue_category_id);
        $issue_category->issueSubCategories()->sync(\request()->issue_sub_categories_ids);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Issue Sub Categories saved and updated successfully']);

    }

    function removeElementWithValue($array, $key, $value)
    {
        foreach ($array as $subKey => $subArray) {
            if ($subArray[$key] == $value) {
                unset($array[$subKey]);
            }
        }
        return $array;
    }

    /**
     * store issuecategory
     */
    public function storeIssueCategory()
    {
        request()->validate([
            'name' => 'required|unique:issue_categories,name,' . \request('id'),
        ]);
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('issue_categories', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Issue Category saved successfully']);

    }

    /**
     * return issuecategory values
     */
    public function listIssueCategories()
    {
        $issuecategories = IssueCategory::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $issuecategories->get();
        return SearchRepo::of($issuecategories)
            ->addColumn('action', function ($issuecategory) {
                $str = '';
                $json = json_encode($issuecategory);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'issuecategory_modal\');" class="btn badge btn-info btn-sm text-uppercase"><i class="fa fa-edit"></i> Edit</a>';
                $str .= '&nbsp;&nbsp;<a href="' . url('admin/settings/tickets/issuecategories/issuesubcategories/' . $issuecategory->id) . '"  class="btn badge btn-dark btn-sm load-page text-uppercase"><i class="fas fa-cogs"></i> Manage Issue SubCategories</a>';
                $str .= '&nbsp;&nbsp;<a href="' . url('admin/settings/tickets/issuecategories/dispositions/' . $issuecategory->id) . '"  class="btn badge btn-success btn-sm load-page text-uppercase"><i class="fas fa-cogs"></i> Manage Dispositions</a>';

                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/issuecategories/delete').'\',\''.$issuecategory->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    public function listIssueCategoriesDispositions()
    {
        $id = \request()->issue_category_id;
        $dispositions = DispositionIssueCategory::join('dispositions', 'dispositions.id', 'disposition_issue_category.disposition_id')->where([
            ['disposition_issue_category.issue_category_id', $id],
            ['disposition_issue_category.issue_sub_category_id', \request('issue_sub_category_id')],
            ['dispositions.status', 1]
        ]);
//        $issue_source = \request()->issue_source;
//        if (\request()->issue_source) {
//            if ($issue_source == 1 || $issue_source == 2) {
//                $dispositions = $dispositions->where([
//                    ['disposition_issue_category.call_source', \request()->issue_source],
//                ])->orWhere([
//                    ['disposition_issue_category.issue_category_id', $id],
//                    ['disposition_issue_category.issue_sub_category_id', \request('issue_sub_category_id')],
//                    ['disposition_issue_category.call_source', 3]
//                ]);
//            } else {
//                $dispositions = $dispositions;
//            }
//        }
        $dispositions = $dispositions->select('dispositions.*')->get();
        return $dispositions;
    }

    /**
     * delete issuecategory
     */
    public function destroyIssueCategory($issuecategory_id)
    {
        $issuecategory = IssueCategory::findOrFail($issuecategory_id);
        $issuecategory->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'IssueCategory deleted successfully']);
    }

    public function listIssueSubCategories()
    {
        $id = \request()->issue_category_id;
        $subs = IssueCategoryIssueSubcategory::join('issue_sub_categories', 'issue_sub_categories.id', 'issue_category_issue_sub_category.issue_sub_category_id')->where([
            ['issue_category_issue_sub_category.issue_category_id', $id],
            ['issue_sub_categories.status', 1]
        ]);
        $subs = $subs->select('issue_sub_categories.*')->get();
        return $subs;
    }

    public function getDispositions()
    {

        $dispositions = Disposition::all();
        $existing = DispositionIssueCategory::where([
            ['issue_category_id', \request()->issue_category_id],
            ['issue_sub_category_id', \request()->issue_sub_category_id]
        ])->pluck('disposition_id')->toArray();
        return view($this->folder . 'dispositions', [
            'dispositions' => $dispositions,
            'existing' => $existing
        ]);
    }
}
