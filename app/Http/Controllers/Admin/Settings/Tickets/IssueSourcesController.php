<?php

namespace App\Http\Controllers\Admin\Settings\Tickets;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\IssueSource;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class IssueSourcesController extends Controller
{
    /**
     * return issuesource's index view
     */
    public function index()
    {
        return view($this->folder . 'sources', [

        ]);
    }

    /**
     * store issuesource
     */
    public function storeIssueSource()
    {
        request()->validate([
            'name' => 'required|unique:issue_sources,name,' . \request('id'),
        ]);
        $data = \request()->all();

        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('issue_sources', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if (\request()->id) {
            $action = "updated";
        } else {
            $action = "saved";
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Issue Source ' . $action . ' successfully']);

    }

    /**
     * return issuesource values
     */
    public function listIssueSources()
    {
        $issuesources = IssueSource::where([
            ['id', '>', 0],
            ['status', 1]
        ]);

        if (\request('all'))
            return $issuesources->get();
        return SearchRepo::of($issuesources)
            ->addColumn('action', function ($issuesource) {
                $str = '';
                $json = json_encode($issuesource);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'issuesource_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/issuesources/delete').'\',\''.$issuesource->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete issuesource
     */
    public function destroyIssueSource($issuesource_id)
    {
        $issuesource = IssueSource::findOrFail($issuesource_id);
        $issuesource->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'IssueSource deleted successfully']);
    }
}
