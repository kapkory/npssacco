<?php

namespace App\Http\Controllers\Admin\Settings\Tickets;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\Branch;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class BranchesController extends Controller
{
    /**
     * return branch's index view
     */
    public function index()
    {
        return view($this->folder . 'index', [

        ]);
    }

    /**
     * store branch
     */
    public function storeBranch()
    {
        request()->validate([
            'name' => 'required|unique:branches,name,' . \request('id'),
        ]);
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('branches', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if (\request()->id) {
            $action = "updated";
        } else {
            $action = "saved";
        }
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Branch ' . $action . ' successfully']);
    }

    /**
     * return branch values
     */
    public function listBranches()
    {
        $branches = Branch::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $branches->get();
        return SearchRepo::of($branches)
            ->addColumn('action', function ($branch) {
                $str = '';
                $json = json_encode($branch);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'branch_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/branches/delete').'\',\''.$branch->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete branch
     */
    public function destroyBranch($branch_id)
    {
        $branch = Branch::findOrFail($branch_id);
        $branch->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Branch deleted successfully']);
    }

}
