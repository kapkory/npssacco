<?php

namespace App\Http\Controllers\Admin\Settings\Config;

use App\Http\Controllers\Controller;
use App\Models\Core\AppraisalQuestionCategory;
use App\Models\Core\AppraisalQuestion;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Repositories\SearchRepo;

class AppraisalQuestionsController extends Controller
{

    /**
     * store leave type
     */
    public function storeQuestion()
    {
        request()->validate([
            'question' => 'required|unique:appraisal_questions,question,' . \request('id'),
            'position' => 'required|numeric|unique:appraisal_questions,position,' . \request('id'),
            'category_id' => 'required'
        ]);
        $data = \request()->all();
        unset($data['category_id']);
        $data['user_id'] = request()->user()->id;
        $data['appraisal_question_category_id'] = request()->category_id;
        $this->autoSaveModel($data);
        $action = "added";
        if (\request()->id)
            $action = "updated";
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Question '.$action.' successfully']);
    }

    /**
     * return questions values
     */
    public function listQuestions()
    {
        $questions = AppraisalQuestion::join('users', 'users.id', 'appraisal_questions.user_id')
            ->leftJoin('appraisal_question_categories', 'appraisal_questions.appraisal_question_category_id', 'appraisal_question_categories.id')
            ->select('appraisal_questions.*','appraisal_question_categories.name as category','appraisal_question_categories.id as category_id', 'users.name as created_by');
        if (\request('all'))
            return $questions->get();
        return SearchRepo::of($questions)
            ->addColumn('Created_at', function ($questionCategory) {
                $date_str = Carbon::parse($questionCategory->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('action', function ($disposition) {
                $str = '';
                $json = json_encode($disposition);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'appraisalquestion_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/dispositions/delete').'\',\''.$disposition->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete question
     */
    public function destroyQuestion($question_id)
    {
        $question = AppraisalQuestion::findOrFail($question_id);
        $question->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Question deleted successfully']);
    }

}
