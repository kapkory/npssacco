<?php

namespace App\Http\Controllers\Admin\Settings\Config;

use App\Http\Controllers\Controller;
use App\Models\Core\AppraisalQuestionCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Repositories\SearchRepo;

class AppraisalQuestionCategoriesController extends Controller
{

    /**
     * store leave type
     */
    public function storeQuestionCategory()
    {
        request()->validate([
            'name' => 'required|unique:appraisal_question_categories,name,' . \request('id')
        ]);
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        $this->autoSaveModel($data);
        $action = "added";
        if (\request()->id)
            $action = "updated";
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Question category '.$action.' successfully']);
    }

    /**
     * return question categories values
     */
    public function listQuestionCategories()
    {
        $questionCategories = AppraisalQuestionCategory::join('users', 'users.id', 'appraisal_question_categories.user_id')
            ->select('appraisal_question_categories.*', 'users.name as created_by');
        if (\request('all'))
            return $questionCategories->get();
        return SearchRepo::of($questionCategories)
            ->addColumn('Description', function ($questionCategory) {
                return limit_string_words($questionCategory->description,15);
            })
            ->addColumn('Created_at', function ($questionCategory) {
                $date_str = Carbon::parse($questionCategory->created_at)->toDayDateTimeString();
                return $date_str;
            })
            ->addColumn('action', function ($disposition) {
                $str = '';
                $json = json_encode($disposition);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'appraisalquestioncategory_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/dispositions/delete').'\',\''.$disposition->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete question category
     */
    public function destroyQuestionCategory($questionCategory_id)
    {
        $questionCategory = AppraisalQuestionCategory::findOrFail($questionCategory_id);
        $questionCategory->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Question category deleted successfully']);
    }

}
