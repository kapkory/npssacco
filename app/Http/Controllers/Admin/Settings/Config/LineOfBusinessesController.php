<?php

namespace App\Http\Controllers\Admin\Settings\Config;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Core\LineOfBusiness;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class LineOfBusinessesController extends Controller
{
    /**
     * return lineofbusiness's index view
     */
    public function index()
    {
        return view($this->folder . 'index', [

        ]);
    }

    /**
     * store lineofbusiness
     */
    public function storeLineOfBusiness()
    {
        request()->validate([
            'name' => 'required|unique:line_of_businesses,name,' . \request('id'),
        ]);
        $data = \request()->all();
        unset($data['account_manager_user_id']);
        $data['user_id'] = request()->user()->id;
        $lob = $this->autoSaveModel($data);
        $lob->updateAccountManager(request()->account_manager_user_id);
        $action = "saved";
        if (\request()->id)
            $action = "updated";


        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'LOB ' . $action . ' successfully']);
    }

    /**
     * return lineofbusiness values
     */
    public function listLineOfBusinesses()
    {
        $lineofbusinesses = LineOfBusiness::leftJoin('account_managers','account_managers.line_of_business_id','line_of_businesses.id')
            ->leftJoin('users','users.id','account_managers.user_id')
        ->where([
            ['line_of_businesses.id', '>', 0]
        ])
        ->select('line_of_businesses.*','users.name as account_manager');
        if (\request('all'))
            return $lineofbusinesses->get();
        return SearchRepo::of($lineofbusinesses)
            ->addColumn('account_manager', function($lineofbusiness){
                return ($lineofbusiness->account_manager) ? $lineofbusiness->account_manager : "N/A";
            })->addColumn('account_manager_id', function($lineofbusiness){
                return @$lineofbusiness->getAccountManager()->id;
            })
            ->addColumn('action', function ($lineofbusiness) {
                $str = '';
                $json = json_encode($lineofbusiness);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'lineofbusiness_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/lineofbusinesses/delete').'\',\''.$lineofbusiness->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete lineofbusiness
     */
    public function destroyLineOfBusiness($lineofbusiness_id)
    {
        $lineofbusiness = LineOfBusiness::findOrFail($lineofbusiness_id);
        $lineofbusiness->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'LineOfBusiness deleted successfully']);
    }
}
