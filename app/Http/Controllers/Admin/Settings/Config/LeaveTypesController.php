<?php

namespace App\Http\Controllers\Admin\Settings\Config;

use App\Http\Controllers\Controller;
use App\Models\Core\LeaveType;
use Illuminate\Http\Request;

use App\Repositories\SearchRepo;

class LeaveTypesController extends Controller
{

    /**
     * store leave type
     */
    public function storeLeaveType()
    {
        request()->validate([
            'name' => 'required|unique:leave_types,name,' . \request('id'),
            'is_paid' => 'required|numeric',
            'entitlement' => 'required|numeric',
        ]);
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        $this->autoSaveModel($data);
        $action = "added";
        if (\request()->id)
            $action = "updated";
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave type '.$action.' successfully']);
    }

    /**
     * return leave types values
     */
    public function listLeaveTypes()
    {
        $leavetypes = LeaveType::join('users', 'users.id', 'leave_types.user_id')
            ->select('leave_types.*', 'users.name as created_by');
        if (\request('all'))
            return $leavetypes->get();
        return SearchRepo::of($leavetypes)
            ->addColumn('Description', function ($leavetype) {
                return limit_string_words($leavetype->description,15);
            })
            ->addColumn('Entitlement', function ($leavetype) {
                if(fmod($leavetype->entitlement, 1) !== 0.00)
                    return number_format($leavetype->entitlement,1);
                return number_format($leavetype->entitlement);
            })
            ->addColumn('Is_paid', function ($leavetype) {
                return ($leavetype->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($disposition) {
                $str = '';
                $json = json_encode($disposition);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'leavetype_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/dispositions/delete').'\',\''.$disposition->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete leave type
     */
    public function destroyLeaveType($leavetype_id)
    {
        $leavetype = LeaveType::findOrFail($leavetype_id);
        $leavetype->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Leave type deleted successfully']);
    }

}
