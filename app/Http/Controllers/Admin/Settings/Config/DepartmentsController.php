<?php

namespace App\Http\Controllers\Admin\Settings\Config;

use App\Http\Controllers\Controller;
use App\Models\Core\CalltronixDepartment;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\Department;
use App\Models\Core\DepartmentHead;
use App\Models\Core\StaffRole;
use App\Repositories\SearchRepo;
use Illuminate\Http\Request;

class DepartmentsController extends Controller
{
    /**
     * return department's index view
     */
    public function index()
    {
        return view($this->folder . 'index', [

        ]);
    }

    /**
     * store department
     */
    public function storeDepartment()
    {
        request()->validate([
            'name' => 'required|unique:departments,name,' . \request('id'),
            'department_code' => 'required|string|min:2|max:2|unique:departments,department_code,' . \request('id'),
            'description' => 'required'
        ]);
        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        $data['department_code'] = strtoupper(request()->department_code);
        $action = "added";
        if (\request()->id)
            $action = "updated";
        $this->autoSaveModel($data);
        $this->syncSubDepartmentsCount();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Department ' . $action . ' successfully']);
    }

    /**
     * return department values
     */
    public function listDepartments()
    {
        $departments = Department::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $departments->get();
        return SearchRepo::of($departments)
            ->addColumn('sub_departments', function ($department){
                return $department->sub_departments_count;
            })
            ->addColumn('action', function ($department) {
                $str = '';
                $json = json_encode($department);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'department_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str .= '&nbsp;| &nbsp;<a href="' . url('admin/settings/config/departments/roles/' . $department->id) . '"  class="btn badge btn-dark btn-sm load-page"><i class="fa fa-user-secret"></i> Roles</a>';
                $str .= '&nbsp;|&nbsp;<a href="' . url('admin/settings/config/issuecategories/dispositions/' . $department->id) . '"  class="btn badge btn-success btn-sm load-page text-uppercase"><i class="fas fa-cogs"></i> Ticket Dispositions</a>';


                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/departments/delete').'\',\''.$department->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * list department heads
     */
    public function listDepartmentHeads()
    {
        $departments = DepartmentHead::join('departments', 'departments.id', 'department_heads.department_id')
            ->leftJoin('users', 'users.id', 'department_heads.user_id')
            ->where([
                ['department_heads.id', '>', 0]
            ])->select('department_heads.*','departments.name as department', 'users.name as head');
        if (\request('all'))
            return $departments->get();
        return SearchRepo::of($departments)
            ->addColumn('action', function ($department) {
                $str = '';
                $json = json_encode($department);
                $str .= '&nbsp;&nbsp;<a href="javascript:void(0)" onclick="runPlainRequest(\'' . url(request()->user()->role . '/settings/config/departments/heads/remove') . '\',\'' . $department->id . '\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Remove</a>';

//                $str .= '<a href="#" onclick="runPlainRequest(\'' . url(request()->user()->role . '/settings/config/departments/heads/remove') . '\',\'' . $department->id . '\');"  class="btn badge btn-danger btn-sm "><i class="fa fa-times"></i> Remove</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/departments/delete').'\',\''.$department->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }


    /**
     * saved department heads
     */
    public function saveDepartmentHeads()
    {
        request()->validate([
            'department_id' => 'required',
            'user_id' => 'required'
        ]);
        $exists = DepartmentHead::where([
            ['user_id' , \request()->user_id],
//            ['department_id', \request()->department_id]
        ])->exists();
        if ($exists)
            return \Response::json(["errors" => [
                "user_id" => ["This user is already a head of department"],
            ]], 422);
        $data = \request()->all();
        $action = "saved";
        if (\request()->id)
            $action = "updated";

        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Department ' . $action . ' successfully']);
    }

    /**
     * delete department
     */
    public function destroyDepartment($department_id)
    {
        $department = DepartmentHead::findOrFail($department_id);
        $department->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Department deleted successfully']);
    }
    public function removeDepartmentHead($department_id)

    {
        $department = DepartmentHead::findOrFail($department_id);
        $department->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Department head removed successfully']);
    }

    public function departmentRolesView($id)
    {
        $department = DepartmentHead::find($id);

        return view($this->folder . 'department_roles', [
            'department' => $department
        ]);


    }

    public function listDepartmentRoles($id)
    {
        $departments = StaffRole::join('departments', 'departments.id', 'staff_roles.department_id')
            ->where([
                ['department_id', $id]
            ])->select('staff_roles.*', 'staff_roles.name as department_role', 'departments.name as department');
        if (\request('all'))
            return $departments->get();
        return SearchRepo::of($departments)
            ->addColumn('action', function ($department) {
                $str = '';
                $json = json_encode($department);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'department_role_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';

                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/departments/delete').'\',\''.$department->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    public function saveDepartmentRole()
    {

        request()->validate([
            'name' => 'required|unique:staff_roles,name,' . \request()->id . ',id,department_id,' . \request()->department_id
        ]);
        $data = \request()->all();
        $action = "saved";
        if (\request()->id)
            $action = "updated";
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Department Role ' . $action . ' successfully']);
    }
    /**
     * sync subdepartments count
     */
    public function syncSubDepartmentsCount()
    {
        $departments = Department::all();
        foreach($departments as $department){
            $count = Department::where('parent_department_id',$department->id)->count();
            if($count > 0)
                $department->update(['sub_departments_count'=>$count]);

            $department->update(['staff_count'=>0]);
        }
    }
}
