<?php

namespace App\Http\Controllers\Admin\Staffappraisals;

use App\Http\Controllers\Controller;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\StaffAppraisal;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class StaffAppraisalsController extends Controller
{
    /**
     * return staffappraisal's index view
     */
    public function index(){
        $staffs = Staff::join('users','users.id','staffs.user_id')
            ->select('staffs.*','users.name','users.avatar','users.role')
            ->get();
        return view($this->folder.'index',compact('staffs'));
    }

    /**
     * return staff items data
     */
    public function getStaffItemsData($staff_id)
    {
//        $appraisals = StaffAppraisal::join('users','staff_appraisals.user_id','users.id')
//            ->where('staff_appraisals.staff_id',$staff_id)
//            ->paginate();

        $staff = Staff::join('users','users.id','staffs.user_id')
            ->where('staffs.id',$staff_id)
            ->select('staffs.*','users.name','users.email','users.phone','users.avatar as avatar')
            ->first();
        return view($this->folder."staff_appraisals_list",compact('staff'));
    }

    /**
     * store staffappraisal
     */
    public function storeStaffAppraisal(){
        request()->validate([
            'period_from'=>'required|date|before:yesterday',
            'period_to'=>'required|date|after:'.request()->period_from
        ]);
        $data = \request()->all();
        unset($data['period_from']);
        unset($data['period_to']);
        $data['user_id'] = request()->user()->id;
        $data['review_period_from'] = request()->period_from;
        $data['review_period_to'] = request()->period_to;
        $this->autoSaveModel($data);

        $action="saved";
        if(\request()->id)
            $action="updated";

        return redirect()->back()->with('notice',['type'=>'success','message'=>'Staff Appraisal '.$action.' successfully']);
    }

    /**
     * return staffappraisal values
     */
    public function listStaffAppraisals($staff_id){
        $staffappraisals = StaffAppraisal::where([
            ['staff_id',$staff_id]
        ])->select('staff_appraisals.*','staff_appraisals.review_period_from as period_from','staff_appraisals.review_period_to as period_to');
        if(\request('all'))
            return $staffappraisals->get();
        return SearchRepo::of($staffappraisals)
            ->addColumn('Period_from', function($staffappraisal){
                $bdate_str = Carbon::parse($staffappraisal->period_from)->isoFormat('Do MMM Y');
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('Period_to', function($staffappraisal){
                $bdate_str = Carbon::parse($staffappraisal->period_to)->isoFormat('Do MMM Y');
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('Created_at', function($staffappraisal){
                $bdate_str = Carbon::parse($staffappraisal->created_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('Status', function($staffappraisal){
                if($staffappraisal->status ==0)
                    return "<small class='text-warning'><b>Pending</b></small>";
                return "<small class='text-success'><b>Complete</b></small>";
            })
            ->addColumn('action',function($staffappraisal){
                $str = '';
                $json = json_encode($staffappraisal);
                if($staffappraisal->status ==0){
                    $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'staffappraisal_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                    $str.='&nbsp;<a href="'.url('admin/staffappraisals/staffappraisal/questions/'.$staffappraisal->id."/question").'" class="btn badge btn-primary btn-sm load-page">Proceed <i class="fa fa-arrow-alt-circle-right"></i></a>';
                }else{
                    $str.='&nbsp;<a href="'.url('admin/staffappraisals/staffappraisal/questions/'.$staffappraisal->id."/results").'" class="btn badge btn-primary btn-sm load-page">Results <i class="fa fa-eye"></i></a>';
                }
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/staffappraisals/delete').'\',\''.$staffappraisal->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }
    /**
     * return staffappraisal values
     */
    public function listAllStaffAppraisals(){
        $staffappraisals = StaffAppraisal::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $staffappraisals->get();
        return SearchRepo::of($staffappraisals)
            ->addColumn('action',function($staffappraisal){
                $str = '';
                $json = json_encode($staffappraisal);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'staffappraisal_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/staffappraisals/delete').'\',\''.$staffappraisal->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete staffappraisal
     */
    public function destroyStaffAppraisal($staffappraisal_id)
    {
        $staffappraisal = StaffAppraisal::findOrFail($staffappraisal_id);
        $staffappraisal->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'StaffAppraisal deleted successfully']);
    }

}
