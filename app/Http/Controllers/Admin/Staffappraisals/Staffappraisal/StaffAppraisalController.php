<?php

namespace App\Http\Controllers\Admin\Staffappraisals\Staffappraisal;

use App\Http\Controllers\Controller;
use App\Models\Core\AppraisalQuestion;
use App\Models\Core\AppraisalQuestionResponse;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\StaffAppraisal;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class StaffAppraisalController extends Controller
{
    /**
     * return staffappraisal's index view
     */
    public function index(){
        $staffs = Staff::join('users','users.id','staffs.user_id')
            ->select('staffs.*','users.name','users.avatar','users.role')
            ->get();
        return view($this->folder.'index',compact('staffs'));
    }
    /**
     * return staffappraisal's index view
     */
    public function viewQuestion($staff_appraisal_id,$question_position=1){
        $staff_appraisal = StaffAppraisal::whereId($staff_appraisal_id)->first();
        $staff = $this->getStaffDetails($staff_appraisal->staff_id);
        $question = $this->getNextQuestion($question_position);
        $next_position = $this->getNextQuestionPosition($question_position);
        $questions_total_count = AppraisalQuestion::all()->count();
        return view($this->folder.'questions',compact('staff_appraisal','staff','question','next_position','questions_total_count'));
    }
    /**
     * return staffappraisal's index view
     */
    public function viewStaffAppraisalResults($staff_appraisal_id){
        $staff_appraisal = StaffAppraisal::whereId($staff_appraisal_id)->first();
        $staff = $this->getStaffDetails($staff_appraisal->staff_id);
        $results = AppraisalQuestionResponse::join('appraisal_questions','appraisal_questions.id','appraisal_question_responses.appraisal_question_id')
            ->join('appraisal_question_categories','appraisal_questions.appraisal_question_category_id','appraisal_question_categories.id')
            ->where('appraisal_question_responses.staff_appraisal_id',$staff_appraisal_id)
            ->select('appraisal_question_responses.*','appraisal_questions.question','appraisal_questions.position','appraisal_question_categories.name as category','appraisal_question_categories.description as category_description')
            ->get();
        return view($this->folder.'appraisal_results',compact('staff_appraisal','staff','results'));
    }

    /**
     * return next question
     */
    public function getNextQuestion($position){
        $question = AppraisalQuestion::join('appraisal_question_categories','appraisal_question_categories.id','appraisal_questions.appraisal_question_category_id')
            ->select('appraisal_questions.id','appraisal_questions.question','appraisal_questions.position','appraisal_question_categories.name as category')
            ->where('appraisal_questions.position',$position)
            ->first();
        $max = AppraisalQuestion::where('id','>',0)->max('position');
        if(!$question)
            for($i = $position; $i <= $max; $i++){
                $question = AppraisalQuestion::join('appraisal_question_categories','appraisal_question_categories.id','appraisal_questions.appraisal_question_category_id')
                    ->select('appraisal_questions.id','appraisal_questions.question','appraisal_questions.position','appraisal_question_categories.name as category')
                    ->where('appraisal_questions.position',$i)
                    ->first();
                if($question)
                    break;
            }
        return $question;
    }
    /**
     * return next question position
     */
    public function getNextQuestionPosition($position){
        $next = $position + 1;
        $max = AppraisalQuestion::where('id','>',0)->max('position');
        $question_ids = AppraisalQuestion::where('id','>',0)->pluck('id');
        if($next > $max)
            return null;

        for($i = $position+1; $i <= $max; $i++){
            $question = AppraisalQuestion::join('appraisal_question_categories','appraisal_question_categories.id','appraisal_questions.appraisal_question_category_id')
                ->select('appraisal_questions.id','appraisal_questions.question','appraisal_questions.position','appraisal_question_categories.name as category')
                ->where('appraisal_questions.position',$i)
                ->first();
            if($question){
                $next = $i;
                break;
            }
        }
        return  $next;
    }

    /**
     * return staff items data
     */
    public function getStaffItemsData($staff_id)
    {
//        $appraisals = StaffAppraisal::join('users','staff_appraisals.user_id','users.id')
//            ->where('staff_appraisals.staff_id',$staff_id)
//            ->paginate();

        $staff = Staff::join('users','users.id','staffs.user_id')
            ->where('staffs.id',$staff_id)
            ->select('staffs.*','users.name','users.email','users.phone','users.avatar as avatar')
            ->first();
        return view($this->folder."staff_appraisals_list",compact('staff'));
    }

    /**
     * store staffappraisal
     */
    public function storeStaffAppraisal(){
        request()->validate([
            'period_from'=>'required|date|before:yesterday',
            'period_to'=>'required|date|after:'.request()->period_from
        ]);
        $data = \request()->all();
        unset($data['period_from']);
        unset($data['period_to']);
        $data['user_id'] = request()->user()->id;
        $data['review_period_from'] = request()->period_from;
        $data['review_period_to'] = request()->period_to;
        $this->autoSaveModel($data);
        $action="saved";
        if(\request()->id)
            $action="updated";

        return redirect()->back()->with('notice',['type'=>'success','message'=>'Staff Appraisal '.$action.' successfully']);
    }

    /**
     * return staffappraisal values
     */
    public function listStaffAppraisals($staff_id){
        $staffappraisals = StaffAppraisal::where([
            ['staff_id',$staff_id]
        ]);
        if(\request('all'))
            return $staffappraisals->get();
        return SearchRepo::of($staffappraisals)
            ->addColumn('period_from', function($staffappraisal){
                $bdate_str = Carbon::parse($staffappraisal->review_period_from)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('period_to', function($staffappraisal){
                $bdate_str = Carbon::parse($staffappraisal->review_period_to)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('Created_at', function($staffappraisal){
                $bdate_str = Carbon::parse($staffappraisal->created_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('Status', function($staffappraisal){
                if($staffappraisal->status ==0)
                    return "<small class='text-warning-600'><b>Pending</b></small>";
                return "<small class='text-success-800'><b>Complete</b></small>";
            })
            ->addColumn('action',function($staffappraisal){
                $str = '';
                $json = json_encode($staffappraisal);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'staffappraisal_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str.='<a href="'.url('admin/staffappraisals/staffappraisal/questions/1').'" class="btn badge btn-primary btn-sm load-page"><i class="fa fa-edit"></i> Proceed</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/staffappraisals/delete').'\',\''.$staffappraisal->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }
    /**
     * return staffappraisal values
     */
    public function listAllStaffAppraisals(){
        $staffappraisals = StaffAppraisal::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $staffappraisals->get();
        return SearchRepo::of($staffappraisals)
            ->addColumn('action',function($staffappraisal){
                $str = '';
                $json = json_encode($staffappraisal);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'staffappraisal_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/staffappraisals/delete').'\',\''.$staffappraisal->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete staffappraisal
     */
    public function destroyStaffAppraisal($staffappraisal_id)
    {
        $staffappraisal = StaffAppraisal::findOrFail($staffappraisal_id);
        $staffappraisal->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'StaffAppraisal deleted successfully']);
    }

    /**
     * store question
     * response
     */
    public function storeQuestionResponse()
    {
        request()->validate([
            'question_option'=>'required',
            'question_id'=>'required',
            'staff_appraisal_id'=>'required',
        ]);
        $question = AppraisalQuestion::whereId(request()->question_id)->first();
        $appraisal = StaffAppraisal::whereId(request()->staff_appraisal_id)->first();
        $data = [];
        $data['form_model'] = AppraisalQuestionResponse::class;
        $data['id'] = request()->id;
        $data['appraisal_question_id'] = request()->question_id;
        $data['staff_appraisal_id'] = request()->staff_appraisal_id;
        $data['score'] = request()->question_option;
        $response = $this->autoSaveModel($data);
        $next_pos = $this->getNextQuestionPosition($question->position);
        if(!$next_pos){
            $appraisal->status = 1;
            $appraisal->save();
            return redirect('admin/staffappraisals/staffappraisal/questions/'.request()->staff_appraisal_id.'/results')->with('notice',['type'=>'success','message'=>'Staff Appraisal process completed.']);
        }
        return redirect('admin/staffappraisals/staffappraisal/questions/'.request()->staff_appraisal_id.'/question/'.$next_pos);
    }

}
