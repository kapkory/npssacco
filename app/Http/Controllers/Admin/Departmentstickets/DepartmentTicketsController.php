<?php

namespace App\Http\Controllers\Admin\Departmentstickets;

use App\Http\Controllers\Controller;
use App\Models\Core\CalltronixDepartment;
use App\Models\Core\TicketUpdate;
use App\Repositories\StatusRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\Ticket;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class DepartmentTicketsController extends Controller
{
    /**
     * return department created tickets view
     */
    public function viewCreatedDepartmentTickets($department_id)
    {
        $department = CalltronixDepartment::whereId($department_id)->first();
        return view($this->folder . 'created', compact('department'));
    }

    /**
     * return department created tickets view
     */
    public function viewAssignedDepartmentTickets($department_id)
    {
        $department = CalltronixDepartment::whereId($department_id)->first();
        return view($this->folder . 'assigned', compact('department'));
    }

    /**
     * return ticket values
     */
    public function listCreatedTickets($department_id)
    {
        $assigned = false;
        $status = request()->status;
        if (request()->count)
            return [
                'open' => $this->getTickets($department_id, 'open', $assigned, true),
                'closed' => $this->getTickets($department_id, 'closed', $assigned, true),
                'in_progress' => $this->getTickets($department_id, 'in_progress', $assigned,true),
                'resolved' => $this->getTickets($department_id, 'resolved', $assigned,true),
                'all_tickets' => $this->getTickets($department_id, null, $assigned,true),
            ];

        $tickets = $this->getTickets($department_id, $status,$assigned);
        if (\request('all'))
            return $tickets->get();
        return SearchRepo::of($tickets)
            ->addColumn('ticket_no', function ($ticket) {
                return '<button style="border-radius:0px" href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"  class="btn btn-group load-page btn-success btn-sm badge font-weight-bolder" data-popup="tooltip" title="Click to view more details">' . "CC" . $ticket->ticket_id . '&nbsp;<i class="fa fa-arrow-alt-circle-right"></i></button>';
            })
//            ->addColumn('assigned_to', function ($ticket) {
//                $user = @User::find($ticket->assigned_to)->name;
//                return $user;
//            })
            ->addColumn('created_by', function ($ticket) {
                return ($ticket->is_system == 0) ? $ticket->created_by : '<b>System</b>';
            })
            ->addColumn('Created_at', function ($ticket) {
                return Carbon::parse($ticket->created_at)->toDayDateTimeString();
            })
            ->addColumn('status', function ($ticket) {
                $statusClass = [1 => 'fas fa-clock', 2 => 'fas fa-spinner', 3 => 'fas fa-hourglass', 4 => 'fa fa-check', 5 => 'fas fa-thumbs-up'];
                $color = [1 => 'danger', 2 => 'warning', 3 => 'info', 4 => 'success', 5 => 'success', 6 => 'teal'];
                return '<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn badge  btn-outline-' . $color[$ticket->ticket_status_id] . ' btn-sm"><i class="' . $statusClass[$ticket->ticket_status_id] . '"></i> ' . StatusRepository::getTicketStatus($ticket->ticket_status_id) . '</a>';
            })
            ->addColumn('action', function ($ticket) {
                $str = '<div class="btn-group">';
                $json = json_encode($ticket);
//                $str .= '&nbsp;&nbsp;<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn btn-dark badge"><i class="fa fa-info-circle"></i> More &nbsp;</a>';

                $str .= '<a href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"   class="btn badge btn-success btn-sm load-page"><i class="fa fa-eye"></i> View</a>';
                $str .= '</div>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/tickets/delete').'\',\''.$ticket->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * return ticket values
     */
    public function listAssignedTickets($department_id)
    {
        $assigned = true;
        $status = request()->status;
        if (request()->count)
            return [
                'open' => $this->getTickets($department_id, 'open', $assigned, true),
                'closed' => $this->getTickets($department_id, 'closed', $assigned, true),
                'in_progress' => $this->getTickets($department_id, 'in_progress', true,true),
                'resolved' => $this->getTickets($department_id, 'resolved', $assigned,true),
                'all_tickets' => $this->getTickets($department_id, null, $assigned,true),
            ];

        $tickets = $this->getTickets($department_id, $status,$assigned);
        if (\request('all'))
            return $tickets->get();
        return SearchRepo::of($tickets)
            ->addColumn('ticket_no', function ($ticket) {
                return '<button style="border-radius:0px" href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"  class="btn btn-group load-page btn-success btn-sm badge font-weight-bolder" data-popup="tooltip" title="Click to view more details">' . "CC" . $ticket->ticket_id . '&nbsp;<i class="fa fa-arrow-alt-circle-right"></i></button>';
            })
//            ->addColumn('assigned_to', function ($ticket) {
//                $user = @User::find($ticket->assigned_to)->name;
//                return $user;
//            })
            ->addColumn('created_by', function ($ticket) {
                return ($ticket->is_system == 0) ? $ticket->created_by : '<b>System</b>';
            })
            ->addColumn('Created_at', function ($ticket) {
                return Carbon::parse($ticket->created_at)->toDayDateTimeString();
            })
            ->addColumn('status', function ($ticket) {
                $statusClass = [1 => 'fas fa-clock', 2 => 'fas fa-spinner', 3 => 'fas fa-hourglass', 4 => 'fa fa-check', 5 => 'fas fa-thumbs-up'];
                $color = [1 => 'danger', 2 => 'warning', 3 => 'info', 4 => 'success', 5 => 'success', 6 => 'teal'];
                return '<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn badge  btn-outline-' . $color[$ticket->ticket_status_id] . ' btn-sm"><i class="' . $statusClass[$ticket->ticket_status_id] . '"></i> ' . StatusRepository::getTicketStatus($ticket->ticket_status_id) . '</a>';
            })
            ->addColumn('action', function ($ticket) {
                $str = '<div class="btn-group">';
                $json = json_encode($ticket);
//                $str .= '&nbsp;&nbsp;<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn btn-dark badge"><i class="fa fa-info-circle"></i> More &nbsp;</a>';

                $str .= '<a href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"   class="btn badge btn-success btn-sm load-page"><i class="fa fa-eye"></i> View</a>';
                $str .= '</div>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/tickets/delete').'\',\''.$ticket->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete ticket
     */
    public function getTickets($department_id, $status = null, $assigned = false, $count = false)
    {
        if (!$status || $status == 'all')
            $status = false;
        $tickets = Ticket::query()
            ->leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
            ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
            ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
            ->leftjoin('users', 'users.id', 'tickets.user_id')
            ->leftjoin('users as assigned_user', 'assigned_user.id', 'tickets.assigned_to')
            ->leftjoin('departments as department_to', 'department_to.id', 'tickets.department_id_to')
            ->leftjoin('departments as department_from', 'department_from.id', 'tickets.department_id_from')
            ->leftjoin('line_of_businesses', 'tickets.line_of_business_id', 'line_of_businesses.id')
            ->where([
                ['tickets.status', 1],
//                ['tickets.department_id_from', $department_id],
            ])
            ->when($status, function ($tickets) use ($status) {
                return $tickets->where('tickets.ticket_status_id', getTicketStatus($status));
            });
        if ($assigned)
            $tickets = $tickets->where('tickets.department_id_to', $department_id);
        else
            $tickets = $tickets->where('tickets.department_id_from', $department_id);

        if ($count)
            return $tickets->count();

        return $tickets->select('tickets.*', 'department_to.name as department_to', 'department_from.name as department_from', 'line_of_businesses.name as LOB', 'tickets.ticket_status_id',
            'tickets.disposition_id', 'tickets.id as ticket_id', 'tickets.id as id', 'tickets.user_id as user_id', 'users.name as created_by',
            'assigned_user.name as assigned_to', 'issue_categories.name as issue_category', 'dispositions.name as disposition', 'tickets.ticket_status_id as ticket_status_id ',
            'ticket_statuses.name as status')
            ->orderBy('tickets.id', 'desc');
    }

}
