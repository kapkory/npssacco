<?php

namespace App\Http\Controllers\Admin\Tickets\Ticket;

use App\Http\Controllers\Controller;
use App\Models\Core\File;
use App\Models\Core\Ticket;
use App\Models\Core\TicketFile;
use App\Repositories\FileRepository;
use App\Repositories\SearchRepo;
use App\Repositories\StatusRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TicketFilesController extends Controller
{
    public function listTicketFiles($ticket_id)
    {
        $ticketFiles = File::join('users', 'files.user_id', 'users.id')
            ->where([
                ['files.model_id', $ticket_id],
                ['model_slug_id', StatusRepository::getModelSlugId('ticket')]
            ])
            ->select('files.*', 'users.name as uploaded_by');
        if (\request('all'))
            return $ticketFiles->get();
        return SearchRepo::of($ticketFiles)
            ->addColumn('size', function ($ticketFile) {
                return $this->bytesToHuman($ticketFile->size);
            })
            ->addColumn('Created_at', function ($ticketFile) {
                return Carbon::parse($ticketFile->created_at)->toDayDateTimeString();
            })
            ->addColumn('name', function ($file) {
                return '<a href="' . url("admin/tickets/ticket/file/download/$file->id") . '"> <span title="' . $file->name . '">' . $file->fileName . '</span>&nbsp;&nbsp;<i class="fa fa-download"></i></a>';
            })
            ->addColumn('action', function ($ticketFile) {
                $str = '';
                $str .= '<a href="' . url("admin/tickets/ticket/file/download/$ticketFile->id") . '" class="btn btn-success"><i class="fa fa-download"></i> Download</a>';
                return $str;
            })
            ->make();
    }

    public function downloadFile($id)
    {
        $file = TicketFile::findOrFail($id);
        return FileRepository::download($file);
    }

}
