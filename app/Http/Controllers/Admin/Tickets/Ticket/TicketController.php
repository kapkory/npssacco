<?php

namespace App\Http\Controllers\Admin\Tickets\Ticket;

use App\Models\Core\Customer;
use App\Models\Core\File;
use App\Models\Core\Staff;
use App\Models\Core\Ticket;
use App\Models\Core\TicketStatus;
use App\Models\Core\TicketUpdate;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{

    public function getTicketDetails($id)
    {
        $ticket = Ticket::leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
            ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
            ->leftjoin('users as users_1', 'users_1.id', 'tickets.user_id')
            ->leftjoin('users as users_2', 'users_2.id', 'tickets.assigned_to')
            ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
            ->leftjoin('departments', 'departments.id', 'tickets.department_id_from')
            ->where([
                ['tickets.id', $id],
            ])->select('tickets.*', 'departments.name as department', 'users_2.id as assigned_id', 'users_1.name as created_by', 'users_2.name as assigned_to', 'issue_categories.name as issue_category',
                'dispositions.name as disposition', 'ticket_statuses.name as status')->first();;

        return view($this->folder . 'more_details', [
            'ticket' => $ticket
        ]);

    }

    public function view($ticket_id)
    {
        $this->inner_menu = 'all_tickets';
        $ticket = $this->getTicketById($ticket_id);
        $staff = $this->getStaffById(@$ticket->staff_id);
        if (\request()->tabs) {
            $updates = TicketUpdate::where('ticket_id', $ticket_id)->count();
            return [
                'update_history' => $updates,
                'files' => File::where([
                    ['files.model_id', $ticket_id],
                    ['model_slug_id', StatusRepository::getModelSlugId('ticket')]
                ])->count()
            ];
        }
        return view($this->folder . 'index', [
            'ticket' => $ticket,
            'staff' => $staff
        ]);
    }

    public function updateTicket($id)
    {
        $statuses = TicketStatus::all();
        $ticket = $this->getTicketById($id);

        return view($this->folder . 'update_ticket', [
            'ticket' => $ticket,
            'statuses' => $statuses->toArray()
        ]);
    }

    public function updateTicketModal($ticket_id)
    {
        $statuses = TicketStatus::all();
        $statuses = $statuses->toArray();
        $ticket = $this->getTicketById($ticket_id);

        return view($this->folder . 'update_ticket_form_modal', compact('ticket', 'statuses'));
    }

    public function acknowledgeTicket($ticket_id)
    {
        $ticket = $this->getTicketById($ticket_id);

        return view($this->folder . 'acknowledge_form', compact('ticket'));
    }

    public function reassignTicket($ticket_id)
    {
        $ticket = $this->getTicketById($ticket_id);

        return view($this->folder . 'reassign_ticket_form', compact('ticket'));
    }

    public function cancelTicket($id)
    {
        \request()->validate([
            'reason' => 'required'
        ]);
        $data = \request()->all();
        $ticket = Ticket::findOrFail($id);
        $ticket_status = getTicketStatus('canceled');
        $ticket->update(['ticket_status_id' => $ticket_status]);
        $this->saveTicketUpdates($ticket, $data['reason']);
        return redirect()->to('admin/tickets/ticket/' . $ticket->id)->with('notice', ['type' => 'success', 'message' => 'Ticket Cancelled successfully']);

    }
}
