<?php

namespace App\Http\Controllers\Admin\Tickets;

use App\Exports\TicketsExport;
use App\Mail\TicketMail;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\Disposition;
use App\Models\Core\LineOfBusiness;
use App\Models\Core\Staff;
use App\Models\Core\Ticket;
use App\Models\Core\TicketFile;
use App\Models\Core\TicketUpdate;
use App\Repositories\FileRepository;
use App\Repositories\SearchRepo;
use App\Repositories\StatusRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class TicketsController extends Controller
{


    public function index()
    {
        return view($this->folder . 'index');
    }

    public function createTicket()
    {
        $resolved_tickets = Ticket::where([
            ['user_id', auth()->id()],
            ['ticket_status_id', 4] //resolved
        ])->get();
        if (count($resolved_tickets) > 2)
            session()->flash('notice', ['type' => 'error', 'message' => 'Please close or reopen resolved tickets to proceed with ticket creation!']);
        $this->inner_menu = 'create_ticket';
        return view($this->folder . 'create', compact('resolved_tickets'));
    }

    /**
     * store ticket
     */
    public function storeTicket()
    {
        request()->validate([
            'issue_source_id' => 'required',
            'subject' => 'required',
            'branch_id' => 'required',
            'department_id' => 'required',
            'issue_category_id' => 'required',
            'issue_sub_category_id' => 'required',
            'disposition_id' => 'required',
            'ticket_status_id' => 'required',
            'priority' => 'required',
            'impact' => 'required',
            'assigned_to' => 'required',
            'requester_id' => 'required',
            'description' => 'required',

        ]);
        $data = \request()->all();
        if (request()->acknowledged){
            request()->validate([
                'expected_start_date' => 'required|date',
                'time' => 'required',
                'tat' => 'required',
            ]);
            unset($data['time']);
        }

        $initial_status = null;
        if (\request()->id) {
            $ticket = Ticket::whereId(\request()->id)->first();
            $initial_status = @$ticket->ticket_status_id;
        }

        if (\request('time') == "hours")
            $data['tat'] = \request('tat') * 60;
        else if (\request('time') == "days")
            $data['tat'] = \request('tat') * 24 * 60;
        if (!\request()->id)
            $data['user_id'] = request()->user()->id;
        //save ticket
        $ticket = $this->autoSaveModel($data);
        $ticket_to_user = $this->getUserDetails($ticket->assigned_to);
        $ticket_from_user = $this->getUserDetails($ticket->user_id);
        $ticket->department_id_from = @$ticket_from_user->department_id;
        $ticket->department_id_to = @$ticket_to_user->department_id;
        $ticket->save();
        if (Session::has('tmp_files')) {
            $tmp_files = request()->session()->get('tmp_files');
            $this->storeTempTicketFiles($ticket, $tmp_files);
        }
        $ticket_status = $ticket->ticket_status_id;
        $this->saveTicketUpdates($ticket, \request()->description);
        if (\request()->id) {
            if (\request()->re_assigned)
                $notice = $this->sendTicketMail($ticket->id, 0, 0, 1);
            else if (!\request()->acknowledged) {
                if ($initial_status !== 4 && $ticket_status == 4 && $ticket->assigned_to == auth()->id()) //resolving ticket
                    $ticket->update(['resolved_at' => Carbon::now()]);
                if ($initial_status !== 5 && $ticket_status == 5 && $ticket->user_id == auth()->id()) //closing ticket
                    $ticket->update(['closed_at' => Carbon::now()]);
                $notice = $this->sendTicketMail($ticket->id, 0, 0);
            } else {
                if (request()->acknowledged)
                    $ticket->update(['acknowledged_at' => Carbon::now()]);

                $notice = $this->sendTicketMail($ticket->id, 0, 1);
            }
            $action = "updated";
        } else {
            $action = "saved";
            $notice = $this->sendTicketMail($ticket->id, 1);
        }
        if ($notice)
            return redirect()->to('admin/tickets/ticket/' . $ticket->id)->with('notice', $notice);
        return redirect()->to('admin/tickets/ticket/' . $ticket->id)->with('notice', ['type' => 'success', 'message' => 'Ticket ' . $action . ' successfully']);
    }

    /**
     * List  tickets
     */
    public function listTickets()
    {
        $user = auth()->user();

        if (\request()->getall) {
            $tickets = TicketUpdate::getAllTickets();
            if (\request()->tabs)
                return [
                    'all_tickets' => TicketUpdate::getAllTickets(true),
                    'open' => TicketUpdate::getAllTicketsByStatus(1, true),
                    'in_progress' => TicketUpdate::getAllTicketsByStatus(2, true),
                    'resolved' => TicketUpdate::getAllTicketsByStatus(4, true),
                    'closed' => TicketUpdate::getAllTicketsByStatus(5, true),
                    'cancelled' => TicketUpdate::getAllTicketsByStatus(6, true),
                ];

        }

        if (\request()->from_date && \request()->to_date) {
            if (\request()->tabs)
                return [
                    'all_tickets' => TicketUpdate::getAllTickets(true),
                    'open' => TicketUpdate::getAllTicketsByStatus(1, true),
                    'in_progress' => TicketUpdate::getAllTicketsByStatus(2, true),
                    'resolved' => TicketUpdate::getAllTicketsByStatus(4, true),
                    'closed' => TicketUpdate::getAllTicketsByStatus(5, true),
                    'cancelled' => TicketUpdate::getAllTicketsByStatus(6, true),
                ];
        }
        if (\request('all'))
            return $tickets->get();
        return SearchRepo::of($tickets)
            ->addColumn('ticket_no', function ($ticket) {
                return '<button style="border-radius:0px" href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"  class="btn btn-group load-page btn-success btn-sm badge font-weight-bolder" data-popup="tooltip" title="Click to view more details">' . "CC" . $ticket->ticket_id . '&nbsp;<i class="fa fa-arrow-alt-circle-right"></i></button>';
            })
            ->addColumn('created_by', function ($ticket) {
                return ($ticket->is_system == 0) ? $ticket->created_by : '<b>System</b>';
            })
            ->addColumn('Created_at', function ($ticket) {
                return Carbon::parse($ticket->created_at)->toDayDateTimeString();
            })
            ->addColumn('status', function ($ticket) {
                $statusClass = [1 => 'fas fa-clock', 2 => 'fas fa-spinner', 3 => 'fas fa-hourglass', 4 => 'fa fa-check', 5 => 'fas fa-thumbs-up', 6 => 'fas fa-times-circle'];
                $color = [1 => 'danger', 2 => 'warning', 3 => 'info', 4 => 'success', 5 => 'success', 6 => 'warning', 7 => 'warning'];
                return '<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn badge  btn-outline-' . $color[$ticket->ticket_status_id] . ' btn-sm"><i class="' . $statusClass[$ticket->ticket_status_id] . '"></i> ' . str_replace('_', ' ', ucfirst(getTicketStatus($ticket->ticket_status_id))) . '</a>';
            })
            ->addColumn('action', function ($ticket) {
                $str = '<div class="btn-group">';
                $json = json_encode($ticket);
//                $str .= '&nbsp;&nbsp;<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn btn-dark badge"><i class="fa fa-info-circle"></i> More &nbsp;</a>';

                $str .= '<a href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"   class="btn badge btn-success btn-sm load-page"><i class="fa fa-eye"></i> View</a>';
                $str .= '</div>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/tickets/delete').'\',\''.$ticket->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * List  tickets by status
     */
    public function listTicketsByStatus($status_id)
    {
        $tickets = TicketUpdate::getAllTicketsByStatus($status_id);

        if (\request()->from_date && \request()->to_date) {
//            $tickets = $tickets->whereRaw("DATE(tbl_ticket_updates.created_at) BETWEEN '" . $from_date . "' AND '" . $to_date . "'");
//            $tickets = $tickets;
            if (\request()->tabs)
                return [
                    'all_tickets' => TicketUpdate::getAllTickets(true),
                    'open' => TicketUpdate::getAllTicketsByStatus(1, true),
                    'in_progress' => TicketUpdate::getAllTicketsByStatus(2, true),
                    'resolved' => TicketUpdate::getAllTicketsByStatus(4, true),
                    'closed' => TicketUpdate::getAllTicketsByStatus(5, true),
                    'cancelled' => TicketUpdate::getAllTicketsByStatus(6, true),
                ];
        }
        if (\request('all'))
            return $tickets->get();
        return SearchRepo::of($tickets)
            ->addColumn('ticket_no', function ($ticket) {
                return '<button style="border-radius:0px" href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"  class="btn btn-group load-page btn-success btn-sm badge font-weight-bolder" data-popup="tooltip" title="Click to view more details">' . "CC" . $ticket->ticket_id . '&nbsp;<i class="fa fa-arrow-alt-circle-right"></i></button>';
            })
            ->addColumn('created_by', function ($ticket) {
                return ($ticket->is_system == 0) ? $ticket->created_by : '<b>System</b>';
            })
            ->addColumn('Created_at', function ($ticket) {
                return Carbon::parse($ticket->created_at)->toDayDateTimeString();
            })
            ->addColumn('status', function ($ticket) {
                $statusClass = [1 => 'fas fa-clock', 2 => 'fas fa-spinner', 3 => 'fas fa-hourglass', 4 => 'fa fa-check', 5 => 'fas fa-thumbs-up', 6 => 'fas fa-times-circle'];
                $color = [1 => 'danger', 2 => 'warning', 3 => 'info', 4 => 'success', 5 => 'success', 6 => 'warning', 7 => 'warning'];
                return '<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn badge  btn-outline-' . $color[$ticket->ticket_status_id] . ' btn-sm"><i class="' . $statusClass[$ticket->ticket_status_id] . '"></i> ' . StatusRepository::getTicketStatus($ticket->ticket_status_id) . '</a>';
            })
            ->addColumn('action', function ($ticket) {
                $str = '<div class="btn-group">';
                $json = json_encode($ticket);
//                $str .= '&nbsp;&nbsp;<a href="#more_ticket_info_modal" onclick="getMoreDetails(' . $ticket->ticket_id . ')" data-toggle="modal" class="btn btn-dark badge"><i class="fa fa-info-circle"></i> More &nbsp;</a>';

                $str .= '<a href="' . url('admin/tickets/ticket/' . $ticket->ticket_id) . '"   class="btn badge btn-success btn-sm load-page"><i class="fa fa-eye"></i> View</a>';
                $str .= '</div>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/tickets/delete').'\',\''.$ticket->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * Uploading temp images from file uploader
     */
    public function saveTmpFiles()
    {
        $storage_path = "app/public/tickets/tmp/" . request()->user()->id . '/';
        $temp_path = 'storage/tickets/tmp/' . request()->user()->id . '/';
        $delete_url="admin/tickets/tmp/delete";
        return FileRepository::saveTmpFiles($storage_path, $temp_path, $delete_url);

    }

    public function storeTempTicketFiles($ticket, $files)
    {

        foreach ($files as $file) {
            $new_path = $path = '/tickets/' . \Illuminate\Support\Carbon::now()->format('Y/m/d') . '/' . $ticket->id;
            if (file_exists($file['file_path'])) {
                $file['file_path'] = str_replace(storage_path("app"), "", $file['file_path']);
                $disk = env('FILESYSTEM_DRIVER', 'local');
                $res = Storage::disk($disk)->put($new_path . '/' . $file['file_name'], Storage::disk('local')->get($file['file_path']));
                if ($res) {
                    \App\Models\Core\File::create([
                        'name' => $file['file_name'],
                        'model_id' => $ticket->id,
                        'mime_type' => $file['mime_type'],
                        'model_slug_id' => StatusRepository::getModelSlugId('ticket'),
                        'size' => $file['file_size'],
                        'user_id' => request()->user()->id,
                        'path' => $new_path . '/' . $file['file_name']
                    ]);
                    Storage::disk('local')->delete($file['file_path']);
                }
            }
        }
    }

    public function sanitizeTickets()
    {
//        $tickets = Ticket::where('created_at', '<', Carbon::today())->get();
        $tickets = Ticket::where('id', '>', 0)->get();

        foreach ($tickets as $ticket) {
            $from_staff = @Staff::where('user_id', $ticket->user_id)->first();
            $to_staff = @Staff::where('user_id', $ticket->assigned_to)->first();

            if ($from_staff && $to_staff) {
                $ticket->department_id_from = $from_staff->department_id;
                $ticket->department_id_to = $to_staff->department_id;
                $ticket->save();
            } else {
                $ticket->department_id_to = $ticket->department_id;
                $ticket->save();

//                dd('User assigned: '. $ticket->assigned_to,'Created by: '. User::whereId($ticket->user_id),$ticket);

            }


//            TicketUpdate::where('ticket_id',$ticket->id)->delete();
//            TicketFile::where('ticket_id',$ticket->id)->delete();
//            $ticket->delete();
        }

        return;
    }

    public function syncStaffLobDetails()
    {
        $staffs = Staff::all();
        $count = 0;
        $missing = [];
        foreach ($staffs as $staff) {
            $user = User::whereId($staff->user_id)->first();
            if ($user) {
                $user->department_id = $staff->department_id;
                $user->save();
            } else {
                $count++;
                $missing[] = $staff;
                $staff->delete(); //remove staff records with no user details
            }
        }
    }
    public function deleteTmpFiles()
    {
        $file_key = \request()->all('key');
        $tmp_files = \request()->session()->get('tmp_files');
        foreach ($tmp_files as $key => $tmp_file) {
            if ($tmp_file['key'] == $file_key['key']) {
                $file_path = $tmp_file['file_path'];
                unset($tmp_files[$key]);
                \request()->session()->put('tmp_files', $tmp_files);
                unlink($file_path);
                return 0;
            }

        }
    }
}
