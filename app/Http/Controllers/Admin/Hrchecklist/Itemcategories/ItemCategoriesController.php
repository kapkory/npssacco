<?php

namespace App\Http\Controllers\Admin\Hrchecklist\Itemcategories;

use App\Http\Controllers\Controller;
use App\Models\Core\EmployeeOnboardingChecklist;
use App\Models\Core\ItemCategory;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\OnboardingChecklistItem;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class ItemCategoriesController extends Controller
{
    /**
     * return itemCategory's index view
     */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }
    /**
     * return itemCategory's index view
     */
    public function viewItems($item_category_id){
        $itemcategory = ItemCategory::whereId($item_category_id)->firstOrFail();
        return view($this->folder.'items',compact('itemcategory'));
    }
    /**
     * return itemCategory's index view
     */
    public function assignStaffItems($staff_id){

        $staff = Staff::whereId($staff_id)->first();
        $item_categories = ItemCategory::all();
        $existing = EmployeeOnboardingChecklist::where([
            ['staff_id',$staff_id],
            ['return_status',getEmployeeChecklistItemReturnStatus('assigned')]
        ])->pluck('onboarding_checklist_item_id')->toArray();
        return view($this->folder.'assign_staff_items',compact('item_categories','staff','existing'));
    }

    /**
     * store item category
     */
    public function storeItemCategory(){
        request()->validate([
            'name' => 'required|unique:item_categories,name,' . \request('id'),
        ]);
        $data = \request()->all();
        $data['form_model'] = ItemCategory::class;
        $data['user_id'] = request()->user()->id;

        if(\request()->id)
            $action="updated";
        else
            $action="saved";
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice',['type'=>'success','message'=>'ChecklistItem '.$action.' successfully']);
    }

    /**
     * return item categories values
     */
    public function listItemcategories(){
        $itemcategories = ItemCategory::leftJoin('users','users.id','item_categories.user_id')
            ->leftJoin('onboarding_checklist_items','onboarding_checklist_items.item_category_id','item_categories.id')
            ->select('item_categories.*','users.name as created_by')
            ->groupBy('item_categories.id');
        if(\request('all'))
            return $itemcategories->get();
        return SearchRepo::of($itemcategories)
            ->addColumn('Created_at', function($itemcategory){
                $bdate_str = Carbon::parse($itemcategory->created_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('action',function($itemcategory){
                $str = '';
                $json = json_encode($itemcategory);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'itemcategory_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                $str .= '&nbsp;&nbsp;<a href="' . url('admin/hrchecklist/itemcategories/items/' . $itemcategory->id) . '"  class="btn badge btn-success btn-sm load-page text-uppercase"><i class="fas fa-cogs"></i> Manage Items ('.$itemcategory->onboardingChecklistItems->count().')</a>';

//                    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url('/admin/hrchecklist/itemcategories/delete').'\',\''.$itemcategory->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * return item category items values
     */
    public function listItemCategoryItems($item_category_id){
        $onboardingchecklistitems = OnboardingChecklistItem::leftJoin('users','users.id','onboarding_checklist_items.user_id')
            ->where('onboarding_checklist_items.item_category_id',$item_category_id)
            ->select('onboarding_checklist_items.*','users.name as created_by');
        if(\request('all'))
            return $onboardingchecklistitems->get();
        return SearchRepo::of($onboardingchecklistitems)
            ->addColumn('Is_returnable',function($onboardingchecklistitem){
                if($onboardingchecklistitem->is_returnable == 1)
                    return "Yes";
                return "No";
            })
            ->addColumn('Created_at', function($onboardingchecklistitem){
                $bdate_str = Carbon::parse($onboardingchecklistitem->created_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('action',function($onboardingchecklistitem){
                $str = '';
                $json = json_encode($onboardingchecklistitem);
                $str.='<a id="edit_button_item_'.$onboardingchecklistitem->id.'" href="#" onclick="editChecklistItem(this)" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" class="btn badge btn-info btn-sm edit-button-item"><i class="fa fa-edit"></i> Edit</a>';
//                    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/hrchecklist/items/delete').'\',\''.$onboardingchecklistitem->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete item category
     */
    public function destroyItemCategory($itemcategory_id)
    {
        $itemcategory = ItemCategory::findOrFail($itemcategory_id);
        $itemcategory->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Item category deleted successfully']);
    }

}
