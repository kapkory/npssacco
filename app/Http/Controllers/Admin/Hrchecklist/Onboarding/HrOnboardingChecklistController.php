<?php

namespace App\Http\Controllers\Admin\Hrchecklist\Onboarding;

use App\Http\Controllers\Controller;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\DispositionIssueCategory;
use App\Models\Core\ItemCategory;
use App\Models\Core\OnboardingChecklistItem;
use App\Models\Core\Staff;
use App\Models\Core\Ticket;
use App\Models\Core\TicketUpdate;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Illuminate\Http\Request;

use App\Models\Core\EmployeeOnboardingChecklist;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;
use Snowfire\Beautymail\Beautymail;

class HrOnboardingChecklistController extends Controller
{
    /**
     * return employeeonboardingchecklist's index view
     */
    public function index(){
        $staffs = Staff::join('users','users.id','staffs.user_id')
            ->select('staffs.*','users.name','users.avatar','users.role')
            ->get();
        return view($this->folder.'index',compact('staffs'));
    }

    /**
     * store employeeonboardingchecklist
     */
    public function storeEmployeeOnboardingChecklist(){
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if(!isset($data['user_id'])) {
            if (Schema::hasColumn('employeeonboardingchecklists', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        if(\request()->id)
            $action="updated";
        else
            $action="saved";
        $this->autoSaveModel($data);
        return redirect()->back()->with('notice',['type'=>'success','message'=>'EmployeeOnboardingChecklist '.$action.' successfully']);
    }

    /**
     * return employeeonboardingchecklist values
     */
    public function listEmployeeOnboardingChecklists(){
        $employeeonboardingchecklists = EmployeeOnboardingChecklist::where([
            ['id','>',0]
        ]);
        if(\request('all'))
            return $employeeonboardingchecklists->get();
        return SearchRepo::of($employeeonboardingchecklists)
            ->addColumn('action',function($employeeonboardingchecklist){
                $str = '';
                $json = json_encode($employeeonboardingchecklist);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'employeeonboardingchecklist_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/employeeonboardingchecklists/delete').'\',\''.$employeeonboardingchecklist->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete employeeonboardingchecklist
     */
    public function destroyEmployeeOnboardingChecklist($employeeonboardingchecklist_id)
    {
        $employeeonboardingchecklist = EmployeeOnboardingChecklist::findOrFail($employeeonboardingchecklist_id);
        $employeeonboardingchecklist->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'EmployeeOnboardingChecklist deleted successfully']);
    }

    /**
     * return staff items data
     */
    public function getStaffItemsData($staff_id)
    {
        $items = OnboardingChecklistItem::join('employee_onboarding_checklists','employee_onboarding_checklists.onboarding_checklist_item_id','onboarding_checklist_items.id')
            ->where('employee_onboarding_checklists.staff_id',$staff_id)
            ->get();
        $staff = Staff::join('users','users.id','staffs.user_id')
            ->where('staffs.id',$staff_id)
            ->select('staffs.*','users.name','users.email','users.phone','users.avatar as avatar')
            ->first();
        return view($this->folder."staff_item_details",compact('items','staff'));
    }

    /**
     * list staff's assigned
     * items
     */
    public function listStaffAssignedItemDetails($staff_id)
    {
        $onboardingchecklistitems = EmployeeOnboardingChecklist::leftJoin('users','users.id','employee_onboarding_checklists.user_id')
            ->leftJoin('onboarding_checklist_items','onboarding_checklist_items.id','employee_onboarding_checklists.onboarding_checklist_item_id')
            ->where([
                ['employee_onboarding_checklists.staff_id',$staff_id],
                ['employee_onboarding_checklists.return_status',getEmployeeChecklistItemReturnStatus('assigned')]
            ])
            ->select('onboarding_checklist_items.*','users.name as Assigned_by','employee_onboarding_checklists.created_at as assigned_at','employee_onboarding_checklists.id as employee_onboarding_checklists_id');
        if(\request('all'))
            return $onboardingchecklistitems->get();
        return SearchRepo::of($onboardingchecklistitems)
            ->addColumn('Name',function($onboardingchecklistitem){
                return "<span title=".$onboardingchecklistitem->description." data-toggle='tooltip'>".$onboardingchecklistitem->name."</span>";
            })
            ->addColumn('Is_returnable',function($onboardingchecklistitem){
                if($onboardingchecklistitem->is_returnable == 1)
                    return "Yes";
                return "No";
            })
            ->addColumn('checklist_item_id',function($onboardingchecklistitem){
                return $onboardingchecklistitem->id;
            })
            ->addColumn('Assigned_at', function($onboardingchecklistitem){
                $bdate_str = Carbon::parse($onboardingchecklistitem->assigned_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('action',function($onboardingchecklistitem){
                $str = '';
                $json = json_encode($onboardingchecklistitem);
//                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'assign_checklistitem_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i></a>';
                $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/hrchecklist/onboarding/staff-items-details-remove').'\',\''.$onboardingchecklistitem->employee_onboarding_checklists_id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> Unassign</a>';
                return $str;
            })->make();
    }
    /**
     * list staff's assigned
     * items
     */
    public function listStaffUnAssignedItemDetails($staff_id)
    {
        $assigned_ids = EmployeeOnboardingChecklist::where('staff_id',$staff_id)->pluck('onboarding_checklist_item_id');
        $onboardingchecklistitems = OnboardingChecklistItem::leftJoin('employee_onboarding_checklists','onboarding_checklist_items.id','employee_onboarding_checklists.onboarding_checklist_item_id')
            ->leftJoin('users','users.id','employee_onboarding_checklists.user_id')
//            ->where('employee_onboarding_checklists.staff_id',$staff_id)
            ->whereNotIn('onboarding_checklist_items.id',$assigned_ids)
            ->select('onboarding_checklist_items.*','users.name as created_by','employee_onboarding_checklists.created_at as assigned_at');
        if(\request('all'))
            return $onboardingchecklistitems->get();
        return SearchRepo::of($onboardingchecklistitems)
            ->addColumn('Name',function($onboardingchecklistitem){
                return "<span title=".$onboardingchecklistitem->description." data-toggle='tooltip'>".$onboardingchecklistitem->name."</span>";
            })
            ->addColumn('Is_returnable',function($onboardingchecklistitem){
                if($onboardingchecklistitem->is_returnable == 1)
                    return "Yes";
                return "No";
            })
            ->addColumn('Assigned_at', function($onboardingchecklistitem){
                $bdate_str = Carbon::parse($onboardingchecklistitem->assigned_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('action',function($onboardingchecklistitem){
                $str = '';
                $json = json_encode($onboardingchecklistitem);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'assign_checklistitem_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i></a>';
                $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/hrchecklist/onboarding/staff-items-details-remove').'\',\''.$onboardingchecklistitem->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-times"></i> remove</a>';
                return $str;
            })->make();
    }

    /**
     * store staff
     * assigned item
     */
    public function storeStaffAssignedItem($staff_id)
    {
        request()->validate([
            'checklist_item_ids'=>'required',
            'checklist_item_ids.*'=>'numeric',
//            'assigned_at'=>'nullable|date:before:tomorrow'
        ]);
        $notice = null;
        $staff = Staff::whereId($staff_id)->first();
        $data = request()->all();
        $checklist_item_ids = request()->checklist_item_ids;

        $num=0;
        if(!$checklist_item_ids)
            return response(['errors'=>['checklist_item_ids'=>['Please check some fields to proceed ']]],422);
        foreach($checklist_item_ids as $item_id){
            $item = OnboardingChecklistItem::whereId($item_id)->first();
            $item_category = ItemCategory::whereId($item->item_category_id)->first();
            $data['onboarding_checklist_item_id'] = $item_id;
            $data['staff_id'] = $staff_id;
            $data['return_status'] = getEmployeeChecklistItemReturnStatus('assigned');
            $data['user_id'] = auth()->id();
            $new_item = EmployeeOnboardingChecklist::updateOrCreate(['staff_id'=>$staff_id,'onboarding_checklist_item_id'=>$item_id],$data);
            if($new_item->created_at == $new_item->updated_at){
                $num++;
                if($item->is_notifiable == 1){
                    $ticket_data['department_id'] = $item->department_id;
                    $ticket_data['disposition_id'] = $item->disposition_id;
                    $ticket_data['issue_category_id'] = 1;
                    $ticket_data['ticket_status_id'] = 1;
                    $ticket_data['line_of_business_id'] = $staff->line_of_business_id;
                    $ticket_data['assigned_to'] = @CalltronixDepartmentHead::where('department_id',$item->department_id)->first()->user_id;
                    $ticket_data['status'] = 1;
                    $ticket_data['is_system'] = 1;
                    $ticket_data['user_id'] = auth()->id();
                    $ticket_data['staff_id'] = $staff_id;
                    $ticket_data['form_model'] = Ticket::class;
                    $ticket = $this->autoSaveModel($ticket_data);

                    if($ticket){
                        //save data to ticket updates table for normal tickets
                        $this->saveTicketUpdates($ticket, 'New Staff Checklist Item.(required)',1);
                        $notice = $this->sendTicketMail($ticket->id,1);
                    }
                }
//
            }
        }
        EmployeeOnboardingChecklist::where('staff_id',$staff_id)->whereNotIn('onboarding_checklist_item_id',$checklist_item_ids)->delete();

        if($notice)
            return redirect()->back()->with('notice', $notice);
        return redirect()->back()->with('notice',['type'=>'success','message'=>$num.' Checklist items assigned to employee successfully']);
    }

    /**
     * unassign checklist item from
     * staff
     */
    public function removeEmployeeOnboardingChecklist($employeeonboardingchecklist_id)
    {
        $employeeonboardingchecklist = EmployeeOnboardingChecklist::findOrFail($employeeonboardingchecklist_id);
        $employeeonboardingchecklist->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Employee OnboardingChecklist deleted successfully']);
    }

}
