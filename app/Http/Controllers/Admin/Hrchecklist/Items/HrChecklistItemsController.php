<?php

namespace App\Http\Controllers\Admin\Hrchecklist\Items;

use App\Http\Controllers\Controller;
use App\Models\Core\ItemCategory;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Core\OnboardingChecklistItem;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class HrChecklistItemsController extends Controller
{
    /**
     * return onboardingchecklistitem's index view
     */
    public function index(){
        return view($this->folder.'index',[

        ]);
    }

    /**
     * store onboardingchecklistitem
     */
    public function storeOnboardingChecklistItem(){

        request()->validate([
            'name'=>'required|string',
            'description'=>'required|string',
            'is_notifiable'=>'required|numeric',
            'is_returnable'=>'required|numeric'
        ]);
        if(\request()->is_notifiable == 1)
            request()->validate([
                'department_id'=>'required|numeric',
                'disposition_id'=>'required|numeric',
            ]);

        $data = \request()->all();
        $data['user_id'] = request()->user()->id;
        if(\request()->is_notifiable == 0){
            unset($data['department_id']);
            unset($data['disposition_id']);
        }
        $this->autoSaveModel($data);
        if(\request()->id)
            $action="updated";
        else
            $action="saved";
        return redirect()->back()->with('notice',['type'=>'success','message'=>'ChecklistItem '.$action.' successfully']);
    }

    /**
     * return onboardingchecklistitem values
     */
    public function listOnboardingChecklistItems(){
        $onboardingchecklistitems = OnboardingChecklistItem::leftJoin('users','users.id','onboarding_checklist_items.user_id')
            ->select('onboarding_checklist_items.*','users.name as created_by');
        if(\request('all'))
            return $onboardingchecklistitems->get();
        return SearchRepo::of($onboardingchecklistitems)
            ->addColumn('Is_returnable',function($onboardingchecklistitem){
                if($onboardingchecklistitem->is_returnable == 1)
                    return "Yes";
                return "No";
            })
            ->addColumn('Created_at', function($onboardingchecklistitem){
                $bdate_str = Carbon::parse($onboardingchecklistitem->created_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('action',function($onboardingchecklistitem){
                $str = '';
                $json = json_encode($onboardingchecklistitem);
                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'onboardingchecklistitem_modal\');" class="btn badge btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>';
//                    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/hrchecklist/items/delete').'\',\''.$onboardingchecklistitem->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete onboardingchecklistitem
     */
    public function destroyOnboardingChecklistItem($onboardingchecklistitem_id)
    {
        $onboardingchecklistitem = OnboardingChecklistItem::findOrFail($onboardingchecklistitem_id);
        $onboardingchecklistitem->delete();
        return redirect()->back()->with('notice',['type'=>'success','message'=>'Checklist Item deleted successfully']);
    }

}
