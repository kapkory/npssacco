<?php

namespace App\Http\Controllers\Admin\Hrchecklist\Staffexit;

use App\Http\Controllers\Controller;
use App\Models\Core\EmployeeOnboardingChecklist;
use App\Models\Core\OnboardingChecklistItem;
use App\Models\Core\Staff;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class HrExitChecklistController extends Controller
{
    /**
     * return employeeonboardingchecklist's index view
     */
    public function index(){
        $staffs = Staff::join('users','users.id','staffs.user_id')
            ->select('staffs.*','users.name','users.avatar','users.role')
            ->get();
        return view($this->folder.'index',compact('staffs'));
    }
    /**
     * return staff items data
     */
    public function getStaffItemsData($staff_id)
    {
        $items = OnboardingChecklistItem::join('employee_onboarding_checklists','employee_onboarding_checklists.onboarding_checklist_item_id','onboarding_checklist_items.id')
            ->where('employee_onboarding_checklists.staff_id',$staff_id)
            ->get();
        $staff = Staff::join('users','users.id','staffs.user_id')
            ->where('staffs.id',$staff_id)
            ->select('staffs.*','users.name','users.email','users.phone','users.avatar as avatar')
            ->first();
        return view($this->folder."staff_item_details",compact('items','staff'));
    }

    /**
     * list staff's assigned
     * and returnable items
     */
    public function listStaffAssignedReturnableItemDetails($staff_id)
    {
        $onboardingchecklistitems = EmployeeOnboardingChecklist::leftJoin('users','users.id','employee_onboarding_checklists.user_id')
            ->leftJoin('onboarding_checklist_items','onboarding_checklist_items.id','employee_onboarding_checklists.onboarding_checklist_item_id')
            ->where('employee_onboarding_checklists.staff_id',$staff_id)
            ->where('onboarding_checklist_items.is_returnable',1)
            ->select('onboarding_checklist_items.*','users.name as Assigned_by','employee_onboarding_checklists.created_at as assigned_at','employee_onboarding_checklists.id as employee_onboarding_checklists_id','employee_onboarding_checklists.date_returned','employee_onboarding_checklists.return_status');
        if(\request('all'))
            return $onboardingchecklistitems->get();
        return SearchRepo::of($onboardingchecklistitems)
            ->addColumn('Name',function($onboardingchecklistitem){
                return "<span title=".$onboardingchecklistitem->description." data-toggle='tooltip'>".$onboardingchecklistitem->name."</span>";
            })
            ->addColumn('Is_returnable',function($onboardingchecklistitem){
                $yes_str = "Yes";
                if($onboardingchecklistitem->is_returnable == 1)
                    if($onboardingchecklistitem->return_status == getEmployeeChecklistItemReturnStatus('returned'))
                        return "Yes&nbsp;&nbsp;<span class='text-success font-weight-bold small'><i class='fa fa-check-circle'></i> returned</span>";
                    else
                        return "Yes";
                return "No";
            })
            ->addColumn('checklist_item_id',function($onboardingchecklistitem){
                return $onboardingchecklistitem->id;
            })
            ->addColumn('Assigned_at', function($onboardingchecklistitem){
                $bdate_str = Carbon::parse($onboardingchecklistitem->assigned_at)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('Returned_at', function($onboardingchecklistitem){
                if(!$onboardingchecklistitem->date_returned)
                    return "";
                $bdate_str = Carbon::parse($onboardingchecklistitem->date_returned)->toDayDateTimeString();
                return "<small><b>".$bdate_str."</b></small>";
            })
            ->addColumn('action',function($onboardingchecklistitem){
                $str = '';
                $json = json_encode($onboardingchecklistitem);
//                $str.='<a href="#" data-model="'.htmlentities($json, ENT_QUOTES, 'UTF-8').'" onclick="prepareEdit(this,\'assign_checklistitem_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i></a>';
                if($onboardingchecklistitem->return_status !== getEmployeeChecklistItemReturnStatus('returned'))
                    $str.='&nbsp;&nbsp;<a href="#" onclick="showAssignedChecklistItem(\''.$onboardingchecklistitem->employee_onboarding_checklists_id.'\');" class="btn badge btn-outline-success btn-sm"><i class="fa fa-check"></i> Return</a>';
                return $str;
            })->make();
    }

    /**
     * return staff
     * assigned item
     */
    public function returnStaffAssignedItem($staff_id)
    {
        request()->validate([
            'item_condition'=>'required',
            'date_returned'=>'required|date|before:tomorrow'
        ]);

        $date_returned = (request()->date_returned) ? request()->date_returned : Carbon::now();
        $item = EmployeeOnboardingChecklist::whereId(request()->id)->first();
        if($item){
            $item->date_returned = $date_returned;
            $item->returned_condition = request()->item_condition;
            $item->return_status = getEmployeeChecklistItemReturnStatus('returned');
            $item->user_id = auth()->id();
            $item->save();
        }

        return redirect()->back()->with('notice',['type'=>'success','message'=>'Checklist item marked as returned']);
    }
}
