<?php

namespace App\Http\Controllers\User;

use App\Models\Core\LeaveRequest;
use App\Models\Core\LeaveType;
use App\Models\Core\Staff;
use App\Repositories\SearchRepo;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;

use Notification;
use App\Notifications\GeneralNotification;

class UserController extends Controller
{
    public function profile()
    {
        $user = request()->user();
        $staff = null;
        if($user->is_staff == 1){
            $staff_det = Staff::where("user_id",$user->id)->first();
            if($staff_det)
                $staff = $this->getStaffDetails($staff_det->id);
        }

        return view($this->folder . 'profile', compact('user','staff'));
    }
    public function myLeaves()
    {
        $user = request()->user();
        if($user->is_staff !== 1)
            return redirect('user/profile')->send()->with('notice', ['type' => 'error', 'message' => 'This page is only allowed for users with staff profile.']);

        return view($this->folder . 'my_leaves',compact('user'));
    }
    /**
     * return staff leaves data
     */
    public function listUserLeavesDetails()
    {
        $staff = @Staff::where('user_id',auth()->id())->first();
        if(!$staff)
            return [];
        $leaves_data = LeaveType::leftJoin('yearly_staff_leaves_trackers',function($join) use ($staff) {
            $join->on('yearly_staff_leaves_trackers.leave_type_id','leave_types.id')
                ->leftJoin('staffs', 'yearly_staff_leaves_trackers.staff_id', 'staffs.id')
                ->where([
                    ['staffs.id',$staff->id],
                    ['yearly_staff_leaves_trackers.year',date('Y')],
                ]);
        })
            ->select('leave_types.created_at','leave_types.id','leave_types.entitlement','leave_types.name as leave_type', 'leave_types.is_paid','yearly_staff_leaves_trackers.days_covered','yearly_staff_leaves_trackers.days_remaining','yearly_staff_leaves_trackers.max_days_allowed','yearly_staff_leaves_trackers.compensation_days');
        if (\request('all'))
            return $leaves_data->get();
        return SearchRepo::of($leaves_data)
            ->addColumn('Days_covered', function ($leavedata) {
                return number_format($leavedata->days_covered);
            })
            ->addColumn('Entitled_days', function ($leavedata) {
                return ($leavedata->max_days_allowed) ? number_format($leavedata->max_days_allowed) : number_format($leavedata->entitlement);
            })
            ->addColumn('Days_remaining', function ($leavedata) {
                $days =  ($leavedata->days_remaining) ? number_format($leavedata->days_remaining) : number_format($leavedata->entitlement);
                if($leavedata->compensation_days > 0)
                    return $days." <small> (".number_format($leavedata->compensation_days).")</small>&nbsp;&nbsp;<i style='color: #0a6edb' class='fa fa-question-circle' data-toggle='tooltip' title='".$leavedata->compensation_days." compensation days included'></i>";
                return $days;
            })
            ->addColumn('Is_paid', function ($leavedata) {
                return ($leavedata->is_paid == 0) ? "No" : "Yes";
            })
            ->addColumn('action', function ($leavedata) {
                $str = '';
                $str.='<a href="#more_leave_type_info_modal" onclick="getMoreDetails(\''.$leavedata->id.'\');" data-toggle="modal" class="btn badge btn-outline-dark btn-sm"><i class="fa fa-exclamation-circle"></i> More</a>';
                return $str;
            })->make();
    }



    public function updateProfile()
    {
        request()->validate([
            'name' => 'required',
//            'username' => 'required','unique:users,username,'.auth()->id(),
//            'phone_number' => ['unique:users','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
        ]);
        request()->user()->update([
            'name' => request()->name
        ]);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Profile updated successfully']);
    }


    public function updateProfilePicture()
    {
        request()->validate([
            'avatar' => 'mimes:jpeg,jpg,png|required|max:2048'
        ]);
        if (request()->user_id) {
            $user = User::find(request()->user_id);
        } else {
            $user = request()->user();
        }

        $image_path = storage_path() . '/app/public/profile-pics/' . $user->avatar;
        if ($user->avatar != "user.jpg" && @getimagesize($image_path))
            unlink($image_path);

        $image = request()->file('avatar');
        $ext = $image->getClientOriginalExtension();
        $image_original_name = $image->getClientOriginalName();
        $image_name = str_replace('.' . $ext, '', $image_original_name);
        $name = $user->id . '_' . $image_name . "." . $ext;
        $user->avatar = $name;
        $user->update();
        $image->move(storage_path() . '/app/public/profile-pics', $name);
        return back()->with('notice',['type'=>'success','message'=>'Profile image updated successfully']);
    }

    public function updatePassword()
    {
        $currentuser = request()->user();

        request()->validate([
//            'old_password' => ['required', new OldPasswordValidator()],
            'password' => 'required'
        ]);
        $currentuser->password = bcrypt(request('password'));
        $currentuser->update();

        $user = request()->user();
//        Notification::send($user, new GeneralNotification('password_update',$currentuser));
        return back()->with('notice',['type'=>'success','message'=>'Password updated successfully']);
    }

    /**
     * set my default branch
     */
    public function changeCampaign()
    {
        request()->validate([
            'campaign_id' => 'required|numeric'
        ]);

        request()->user()->update([
            'campaign_id' => request()->campaign_id
        ]);
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Campaign switched successfully']);

    }
}
