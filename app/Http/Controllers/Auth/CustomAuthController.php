<?php

namespace App\Http\Controllers\Auth;


use App\Models\Core\Member;
use App\Models\Core\Staff;
use App\Repositories\AfricasTalkingGateway;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;

class CustomAuthController extends Controller
{
    /**
     * Login user
     */

    public function login()
    {
        \request()->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

//        $this->syncUserStaffEmployeeNos();
        $username = \request()->username;
        if (strpos($username, '@') !== false) {
            \request()->validate([
                'username' => 'required|email'
            ]);
//            if (preg_match('/^[A-Za-z0-9\.]*@(calltronix)[.](com)$/', $username, $matches) === 1 && $matches[0] === $username) {
//            } else {
//                return redirect()->back()->withErrors(['username' => ['Please use a valid email address: (***calltronix.com)']]);
////                return response(['errors' => ['username' => ['Please use a valid email address: (***calltronix.com)']]], 422);
//            }
        }

        $user_name_field = filter_var(request()->username, FILTER_VALIDATE_EMAIL)
            ? 'email'
            : 'employee_number';

        request()->merge([
            $user_name_field => request()->username
        ]);

        if (Auth::attempt(request()->only($user_name_field, 'password'))) {
            $user = Auth::user();
            if (session('url.intended'))
                return redirect(session('url.intended'));//return ['force_redirect' => session('url.intended')];
            return redirect('home');
            //return ['force_redirect' => 'home'];
        }

        if (Auth::attempt(['email' => request('email'), 'password' => \request('password')])) {
            //user logged in
            $user = Auth::user();
            return redirect('home');
//            return ['force_redirect' => 'home'];

        } else {
            return redirect()->back()->withErrors(['username' => ['Invalid Username / Password.']])->withInput();

//            return response(['errors' => ['username' => ['Invalid Username / Password.']]], 422);
        }
    }

    /**
     * Register New user with
     * master company
     */
    public function registerUser()
    {
        $this->validateData();

        $user_data = [
            'firstname' => request()->first_name,
            'lastname' => request()->last_name,
            'middlename' => request()->middle_name,
            'name' => request()->first_name . ' ' . request()->last_name,
            'email' => request()->email,
            'phone' => request()->phone,
            'password' => request()->password,
            'department_id' => 1,
            'role' => 'member',
            'form_model' => User::class,
            'email_verified_at' => date("Y-m-d H:i:s")
        ];
        $user = $this->autoSaveModel($user_data);
        $this->saveMemberDetails($user, request()->all());
        Auth::login($user);
        return ['force_redirect' => $user->role];
    }

    protected function saveMemberDetails($user)
    {
        $member_data = [
            'user_id' => $user->id,
            'id_number' => request()->id_number,
            'occupation' => request()->occupation,
            'county_id' => request()->county_id,
            'form_model' => Member::class,
        ];
        $member = $this->autoSaveModel($member_data);
        $africastalking = new AfricasTalkingGateway();
        $message = "Hello " . $user->name .
            ".\n" . "Thank you for submitting your membership registration membership No " .str_pad($member->id,6,0, STR_PAD_LEFT).
            ".\n" . "Use your phone number or email to access your member portal".
             ".\n" . "Kindly upload necessary documents under documents section.".
             ".\n" . "We will review your application and get back to you.";

        $reponse = $africastalking->sendMessage($user->phone, $message);
        return $member;

    }

    protected function formatPhone($phone)
    {
        $len = strlen($phone);
        if ($len == 10) {
            $phone = "repl" . $phone;
            $phone = str_replace('repl07', '+2547', $phone);
        }
        if ($len == 12) {
            $phone = '+' . $phone;
        }

        return $phone;
    }

    public function validateData()
    {
        request()->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'middle_name' => ['nullable', 'string', 'max:255'],
            'id_number' => ['required', 'numeric'],
            'occupation' => ['required'],
            'location' => ['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],//
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'phone' => ['required', 'unique:users,phone', 'regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],

        ]);
    }

}
