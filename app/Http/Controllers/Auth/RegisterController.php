<?php

namespace App\Http\Controllers\Auth;

use App\Models\Core\CalltronixDepartment;
use App\Models\Core\Member;
use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;


    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'phone' => ['unique:users,phone','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
            'phone' => ['unique:users,phone','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
    public function register(){
        $this->validateData();
        $user_data = [
            'firstname'=>request()->first_name,
            'lastname'=>request()->last_name,
            'name'=>request()->first_name . ' '.request()->last_name,
            'email'=>request()->email,
            'phone'=>request()->phone,
            'password'=>request()->password,
            'role'=>'member',
            'form_model'=>User::class,
            'email_verified_at'=>date("Y-m-d H:i:s")
        ];
        $user = $this->autoSaveModel($user_data);
        Auth::login($user);
        event(new Registered($user));
        return ['force_redirect'=>'home'];
    }
    protected function saveMemberDetails($user_id){
        $member_data = [
            'user_id'=>$user_id,
            'id_number'=>request()->id_number,
            'occupation'=>request()->occupation,
            'county_id'=>request()->county_id,
            'form_model'=>Member::class,
            'email_verified_at'=>date("Y-m-d H:i:s")
        ];
        $member = $this->autoSaveModel($member_data);
        return $member;

    }
    protected function formatPhone($phone){
        $len = strlen($phone);
        if($len==10){
            $phone = "repl".$phone;
            $phone = str_replace('repl07','+2547',$phone);
        }
        if($len==12){
            $phone = '+'.$phone;
        }

        return $phone;
    }

    public function validateData(){
        request()->validate([
            'first_name' => ['required','string','max:255'],
            'last_name' => ['required','string','max:255'],
            'email' => ['required','string','email','max:255','unique:users'],//
            'password' => ['required','string','min:6','confirmed'],
            'phone' => ['nullable','unique:users,phone','regex:/^((\+?254|0){1}[7]{1}[0-9]{8})$/'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

//    protected function create(array $data)
//    {
//        return User::create([
//            'name' => $data['name'],
//            'email' => $data['email'],
//            'password' => Hash::make($data['password']),
//        ]);
//    }

public function showRegistrationForm() {

    return view('auth.register');
}

}
