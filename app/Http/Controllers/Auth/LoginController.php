<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login()
    {
        \request()->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);


        if (Auth::attempt(['email' => request('email'), 'password' => \request('password')],request()->remember)) {
            $user = Auth::user();

            if (session('url.intended'))
//                return ['force_redirect'=>session('url.intended')];
                return redirect(session('url.intended'));
//            return ['force_redirect'=>'home'];
            return redirect('home');
        }
        $email = \request('email');

        if (Auth::attempt(['email' => request('email'), 'password' => \request('password')], request()->remember)) {
            //user logged in
            $user = Auth::user();
            return redirect('home');

        } else {
            return back()->withErrors(['username' => ['Invalid credentials.']]);

//            return response(['errors' => ['email' => ['Invalid email or password']]], 422);
        }
    }

    protected function formatPhone($phone)
    {
        $len = strlen($phone);
        if ($len == 10) {
            $phone = "repl" . $phone;
            $phone = str_replace('repl07', '+2547', $phone);
        }
        if ($len == 12) {
            $phone = '+' . $phone;
        }

        return $phone;
    }

}
