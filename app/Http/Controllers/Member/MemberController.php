<?php

namespace App\Http\Controllers\Member;

use App\Models\Core\Company;
use App\Models\Core\Property;
use App\Models\Core\Tenant;
use App\Models\Core\Unit;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\User;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MemberController extends Controller
{
    /**
     * return user's index view
     */
    public function index()
    {
        return view($this->folder . 'index');
    }


}
