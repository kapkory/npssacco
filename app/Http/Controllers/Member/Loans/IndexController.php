<?php

namespace App\Http\Controllers\Member\Loans;

use App\Http\Controllers\Controller;
use App\Models\Core\Payment;
use App\Repositories\MpesaRepository;
use App\Repositories\StatusRepository;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){
        return view($this->folder.'index');
    }

    public function payLoan(){
        $amount = \request('amount');
        $phone = '254712137367';

        $pay = new Payment();
        $pay->subscriber_id = 1;
        $pay->subscriber_package_id = 1;
        $pay->amount= $amount;
        $pay->status= 0;
        $pay->save();

//        $mpesa = new MpesaRepository();
//        $mpesa->stkPush($pay->id,$phone,$amount);
       return redirect()->back()->with('notice',['type'=>'success','message'=>'payment Prompt has been sent to your phone']);
    }
}
