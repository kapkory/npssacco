<?php

namespace App\Http\Controllers\Member\Tickets;

use App\Models\Core\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Core\Ticket;
use App\Repositories\SearchRepo;
use Illuminate\Support\Facades\Schema;

class TicketsController extends Controller
{
    /**
     * return ticket's index view
     */
    public function index()
    {
        return view($this->folder . 'index', [

        ]);
    }
    public function createTicket($phone=null){
        $customer=null;
        $method = \request()->method();


        if ($method=='POST')
        {

            request()->validate([
                'phone'=>['required','unique:customers,phone','regex:/^([7]{1}[0-9]{8})$/'],

            ]);
            $phone= \request()->phone;
            $customer = Customer::where('phone', '254'.$phone)->first();


        }
        if($phone){
            $customer = Customer::where('phone', '254'.$phone)->first();
        }

        return view($this->folder . 'create', [
            'customer'=>$customer,
            'phone'=>$phone

        ]);

    }

    /**
     * store ticket
     */
    public function storeTicket()
    {
        request()->validate($this->getValidationFields());
        $data = \request()->all();
        if (!isset($data['user_id'])) {
            if (Schema::hasColumn('tickets', 'user_id'))
                $data['user_id'] = request()->user()->id;
        }
        $this->autoSaveModel($data);
        return redirect()->back();
    }

    /**
     * return ticket values
     */
    public function listTickets()
    {
        $tickets = Ticket::where([
            ['id', '>', 0]
        ]);
        if (\request('all'))
            return $tickets->get();
        return SearchRepo::of($tickets)
            ->addColumn('action', function ($ticket) {
                $str = '';
                $json = json_encode($ticket);
                $str .= '<a href="#" data-model="' . htmlentities($json, ENT_QUOTES, 'UTF-8') . '" onclick="prepareEdit(this,\'ticket_modal\');" class="btn badge btn-info btn-sm"><i class="fa fa-edit"></i> Edit</a>';
                //    $str.='&nbsp;&nbsp;<a href="#" onclick="deleteItem(\''.url(request()->user()->role.'/tickets/delete').'\',\''.$ticket->id.'\');" class="btn badge btn-outline-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>';
                return $str;
            })->make();
    }

    /**
     * delete ticket
     */
    public function destroyTicket($ticket_id)
    {
        $ticket = Ticket::findOrFail($ticket_id);
        $ticket->delete();
        return redirect()->back()->with('notice', ['type' => 'success', 'message' => 'Ticket deleted successfully']);
    }
}
