<?php

namespace App\Http\Controllers\Api;

use App\Models\Core\County;
use App\Models\Core\Exam;
use App\Models\Core\Level;
use App\Models\Core\Order;
use App\Models\Core\Payment;
use App\Models\Core\Question;
use App\Models\Core\QuestionTitle;
use App\Models\Core\Student;
use App\Models\Core\Subject;
use App\Models\Core\SubscriberPackage;
use App\Models\Core\Topic;
use App\Repositories\StatusRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{

    public function mpesaCallback(){
        header("Content-Type: application/json");
        $order_code = \request('payment_id');
        $resp =  json_decode(file_get_contents('php://input'));
//        Storage::disk('local')->put('mpesa.txt', file_get_contents('php://input'));
//        Storage::disk('local')->put('mpesa.txt','we have been hit');
        //success
        $resultCode = $resp->Body->stkCallback->ResultCode;


        $pay = Payment::findOrFail($order_code);
        $pay->transaction_reference = $resp->Body->stkCallback->CheckoutRequestID;
        $pay->transaction_description = $resp->Body->stkCallback->ResultDesc;
        if ($resultCode == 0)
            $pay->status = 1;
        else
            $pay->status = 0;
        $pay->save();

        echo '{"ResultCode": 0, "ResultDesc": "The service was accepted successfully", "ThirdPartyTransID": "1234567890"}';
    }

    //returns user subscriber packages based on its order code or users id
    public function fetchSubscriberPackages($order_code){
        $subscriber_package = SubscriberPackage::where('order_code',$order_code)->first();
        if (!$subscriber_package){
            $user_id = $order_code;
            $subscriber_packages = SubscriberPackage::where([
                ['subscriber_id',$user_id],
                ['status',StatusRepository::getSubscriberPackageStatus('processing')]
            ])->get();
            if ($subscriber_packages->isEmpty()){
                return redirect('subscriber');
            }
        }
        else{
            //user id not known when the callback is hit
            $user_id = $subscriber_package->subscriber_id;
            $subscriber_packages = SubscriberPackage::where('order_code',$order_code)->get();
        }
        return ['subscriber_packages'=>$subscriber_packages,'user_id'=>$user_id];
    }

    //updates subscriber dependencies on successful payment
    public function updateSubscriberPackageTable($sub_package,$user_id){
        $sub_package->status = StatusRepository::getSubscriberPackageStatus('active');
        if ($sub_package->expires_on){
            if (Carbon::parse($sub_package->expires_on)->gt(Carbon::now()))
                $expires_on = Carbon::parse($sub_package->expires_on)->addMonths($sub_package->package->duration)->toDateTimeString();
            else
                $expires_on = Carbon::now()->addMonths($sub_package->package->duration)->toDateTimeString();
        }
        else
            $expires_on = Carbon::now()->addMonths($sub_package->package->duration + 1)->toDateTimeString();


        $sub_package->expires_on = $expires_on;
        $sub_package->save();
        $this->updateStudentSubscription($user_id,$sub_package, $expires_on);

    }

    public function updateStudentSubscription($user_id,$sub_package,$expires_on){
        $student_by_package = Student::where([
            ['subscriber_package_id',$sub_package->id],
            ['subscriber_id',$user_id]
        ])->get();
        if (!$student_by_package->isEmpty()){
            //if we had saved the subsdcription_package_id to student
             Student::where([
                ['subscriber_package_id',$sub_package->id],
                ['subscriber_id',$user_id]
            ])->update(['expires_on'=>$expires_on]);
        }else{
            //subscriber learners package
            $count_learners = Student::where('subscriber_id',$user_id)
                ->where('class_id',$sub_package->package->level_id)
                ->count();
//            dd($count_learners);
            if ($count_learners > 0){
                if ($count_learners >= $sub_package->number_of_learners){
                    Student::where('subscriber_id',$user_id)
                        ->where('class_id',$sub_package->package->level_id)
                        ->update(['expires_on'=>$expires_on,'subscriber_package_id'=>$sub_package->id]);

                }
                else{
                    Student::where('subscriber_id',$user_id)
                        ->where('class_id',$sub_package->package->level_id)
                        ->limit($count_learners)
                        ->update(['expires_on'=>$expires_on,'subscriber_package_id'=>$sub_package->id]);
                }
            }
        }
    }
}
