<?php

namespace App\Http\Controllers;

use App\Mail\TicketMail;
use App\Models\Core\LeaveType;
use App\Models\Core\Staff;
use App\Models\Core\Ticket;
use App\Models\Core\TicketUpdate;
use App\Models\Core\YearlyStaffLeavesTracker;
use App\Repositories\ModelSaverRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $folder = "";

    public function __construct()
    {
        $class = get_class($this);

        $class = str_replace('App\\Http\\Controllers\\', "", $class);

        $arr = explode('\\', $class);
        unset($arr[count($arr) - 1]);
        $folder = implode('.', $arr) . '.';
        $this->folder = 'core.' . strtolower($folder);

    }

    function saveModel($data)
    {
        $model_saver = new ModelSaverRepository();
        $model = $model_saver->saveModel($data);
        return $model;
    }

    function autoSaveModel($data)
    {
        $model_saver = new ModelSaverRepository();
        $model = $model_saver->saveModel($data);
        return $model;
    }

    function getValidationFields($fillables = null)
    {
        $data = request()->all();
        if ($fillables) {
            $fillables = $fillables;
        } else {
            $model = new $data['form_model']();
            $fillables = $model->getFillable();
        }
        $validation_array = [];
        foreach ($fillables as $field) {
            $validation_array[$field] = 'required';
        }
        if (in_array("file", $fillables)) {
            $validation_array['file'] = 'required|max:50000';
        }
        $validation_array['id'] = '';
        $validation_array['form_model'] = '';
        return $validation_array;
    }

    public function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }

    /**
     * get staff details
     */
    public function getStaffDetails($staff_id)
    {
        return Staff::join('users', 'users.id', 'staffs.user_id')
            ->leftJoin('line_of_businesses', 'line_of_businesses.id', 'staffs.line_of_business_id')
            ->leftJoin('departments', 'departments.id', 'staffs.department_id')
            ->leftJoin('staff_roles', 'staff_roles.id', 'staffs.staff_role_id')
            ->leftJoin('positions', 'positions.id', 'staffs.position_id')
            ->where([
                ['staffs.id', $staff_id]
            ])->select("staffs.*", "users.name", "users.is_available", "users.avatar", "users.firstname", "line_of_businesses.name as lob", "users.middlename", "users.lastname", "users.dob",
                "departments.name as department", "positions.name as position", "staff_roles.name as role",
                "users.phone", "users.email", "users.alternate_email")
            ->first();
    }

    /**
     * get staff by user id
     */
    public function getUserDetails($user_id)
    {
        $staff = Staff::where('user_id', $user_id)->first();
        $user = User::whereId($user_id)->first();
        if ($staff && $user) {
            $user->department_id = $staff->department_id;
            $user->save();
        }
        return $user;
    }

    /**
     * save ticket updates
     */
    protected function saveTicketUpdates($ticket, $comment, $system = false)
    {
        $ticket_log = new TicketUpdate();
        $ticket_log->ticket_id = $ticket->id;
        $ticket_log->user_id = ($system) ? 0 : auth()->id();
        $ticket_log->issue_category_id = $ticket->issue_category_id;
        $ticket_log->comment = $comment;
        $ticket_log->assigned_to = $ticket->assigned_to;
        $ticket_log->disposition_id = $ticket->disposition_id;
        $ticket_log->ticket_status_id = $ticket->ticket_status_id;
        $ticket_log->status = 1;
        $ticket_log->save();
    }

    /**
     * send Ticket Mail
     */
    public function sendTicketMail($ticket_id, $is_initial, $is_acknowledgement = 0, $reassigned = 0)
    {
        $auth_user_email = null;
        $notice = null;
        $user = auth()->user();
        if ($user)
            $auth_user_email = $user->email;
//        try {
        Mail::send(new TicketMail($ticket_id, $auth_user_email, $is_initial, $is_acknowledgement, $reassigned));
//        } catch (\Exception $e) {
//            $mail_error_message = "Opps! Email alert failed to send, You may need to escalate this manually.";
//            $notice = ['type' => 'error', 'message' => $mail_error_message];
//        }
        return $notice;
    }

    /**
     * get the staff id of a logged in
     * user
     */
    public function getAuthStaffId()
    {
        $staff = Staff::join('users', 'users.id', 'staffs.user_id')
            ->where([
                ['users.id', auth()->id()]
            ])->select("staffs.*", "users.name", "users.firstname", "users.middlename", "users.lastname", "users.dob",
                "users.email", "users.phone", "users.email")
            ->first();
        return @$staff->id;
    }

    public function truncateTables()
    {
        DB::table('files')->truncate();
        DB::table('appraisal_question_responses')->truncate();
        DB::table('compensation_requests')->truncate();
        DB::table('employee_onboarding_checklists')->truncate();
        DB::table('leave_requests')->truncate();
        DB::table('offduty_requests')->truncate();
        DB::table('offduty_request_dates')->truncate();
        DB::table('staff_appraisals')->truncate();
        DB::table('staff_beneficiaries')->truncate();
        DB::table('staff_next_of_kin')->truncate();
        DB::table('staff_spouse_or_childrens')->truncate();
        DB::table('vacancies')->truncate();
        DB::table('yearly_staff_leaves_trackers')->truncate();
        return;
    }

    /**
     * get staff Available
     * leave days -specific leaveType
     */
    public function staffAvailableLeaveTypeDays($staff_id, $leave_type_id)
    {
        $available_days = @LeaveType::whereId($leave_type_id)->first()->entitlement;
        if (!$available_days)
            $available_days = 0;

        $staff_yearly_data = YearlyStaffLeavesTracker::where([
            ['staff_id', $staff_id],
            ['leave_type_id', $leave_type_id],
            ['year', date('Y')]
        ])->first();
        if ($staff_yearly_data)
            $available_days = $staff_yearly_data->days_remaining;

        return (double)$available_days;
    }


    /**
     * calculate and increment
     * staff annual leave days based on the
     * month they joined ->each month = 1.75 days.
     */
    public function syncStaffAvailableAnnualLeaveDays($user_id)
    {
        $staff = @Staff::where('user_id', $user_id)->first();
        $leave_type_id = getAnnualLeaveTypeId();
        $annual_leave_type = @LeaveType::whereId($leave_type_id)->first();
        if (!$annual_leave_type)
            return redirect()->back()->with('notice', ['type' => 'error', 'message' => "Couldn't sync available leave days. Please report this to the IT team."]);

        $days_count = 0;
//        $last_synced_at = null;
//        $last_synced_at = $staff->annual_leave_days_last_sync_date;
//        $months_date_from = ($last_synced_at) ? $last_synced_at : $staff->employment_date;
        $last_synced_at = $staff->annual_leave_days_last_sync_date;
        if ($last_synced_at == null && date('Y', strtotime($staff->employment_date)) < date('Y')) {
            $last_synced_at = Carbon::today()->copy()->startOfYear();
        } elseif ($last_synced_at == null && date('Y', strtotime($staff->employment_date)) == date('Y')) {
            $last_synced_at = $staff->employment_date;
        } else $last_synced_at = $last_synced_at;
        $months_date_from = $last_synced_at;
        $months_date_to = Carbon::today();

        if ($months_date_from) {
            $months_date_from = Carbon::parse($months_date_from);
            $months_count = $months_date_to->diffInMonths($months_date_from);
            if ($months_count > 0) {
//                $new_date = $months_date_from->addMonths($months_count);
                $new_date = Carbon::now();
                $staff->annual_leave_days_last_sync_date = $new_date;
                $staff->save();
                $days_count = $months_count * 1.75;
            }
        }

        $data = [
            'staff_id' => $staff->id,
            'leave_type_id' => $leave_type_id,
            'year' => date('Y'),
        ];

        $data1 = $data;
        $staff_yearly_data = YearlyStaffLeavesTracker::where([
            'staff_id' => $staff->id,
            'leave_type_id' => $leave_type_id,
            'year' => date('Y'),
        ])->first();
        $entitled_days = $staff_yearly_data ? $staff_yearly_data->max_days_allowed : 0;

        if (@$staff->annual_leave_days_last_sync_date)
            $entitled_days += $days_count;
        else
            $entitled_days = $days_count;

        $data1['max_days_allowed'] = $entitled_days;

        if ($staff_yearly_data) {
            $staff_yearly_data->update($data1);
        } else {
            $prev_staff_yearly_data = YearlyStaffLeavesTracker::where([
                'staff_id' => $staff->id,
                'leave_type_id' => $leave_type_id,
                'year' => date('Y') - 1,
            ])->first();
            $days_remaining = $prev_staff_yearly_data ? $prev_staff_yearly_data->days_remaining : 0;
            $staff_yearly_data = YearlyStaffLeavesTracker::create([
                'staff_id' => $data1['staff_id'],
                'leave_type_id' => $data1['leave_type_id'],
                'year' => $data1['year'],
                'max_days_allowed' => $data1['max_days_allowed'],
                'days_remaining' => $days_remaining,
                'prev_year_bal' => $days_remaining,
            ]);
        }
//
        if ($staff_yearly_data) {
            //Update days remaining : ($maximum days allowed + $previous year balance) - $days covered
            $remaining_days = ($staff_yearly_data->max_days_allowed + $staff_yearly_data->prev_year_bal) - $staff_yearly_data->days_covered;
            $staff_yearly_data->days_remaining = $remaining_days;
            $staff_yearly_data->save();
        }
        return 0;
    }

    function getTicketById($id)
    {
        $ticket = Ticket::leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
            ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
            ->leftjoin('users as users_1', 'users_1.id', 'tickets.user_id')
            ->leftjoin('users as users_2', 'users_2.id', 'tickets.assigned_to')
            ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
            ->leftjoin('departments', 'departments.id', 'tickets.department_id')
            ->where([
                ['tickets.id', $id],
            ])->select('tickets.*', 'users_2.id as assigned_id', 'users_1.name as created_by', 'users_1.name as assigned_to',
                'issue_categories.name as issue_category','departments.name as department_name',
                'dispositions.name as disposition', 'ticket_statuses.name as status')->first();
        return $ticket;
    }
    function getStaffById($id){
       $staff= Staff::join('users', 'users.id', 'staffs.user_id')
            ->leftJoin('line_of_businesses', 'line_of_businesses.id', 'staffs.line_of_business_id')
            ->leftJoin('departments', 'departments.id', 'staffs.department_id')
            ->leftJoin('staff_roles', 'staff_roles.id', 'staffs.staff_role_id')
            ->leftJoin('positions', 'positions.id', 'staffs.position_id')
            ->where([
                ['staffs.id', $id]
            ])->select("staffs.*", "users.name", "users.firstname",  "users.middlename", "users.lastname", "users.dob",
                "users.email", "departments.name as department", "positions.name as position", "staff_roles.name as role",
                "users.phone", "users.email")
            ->first();
       return $staff;
    }
    function deleteTempFiles(){
        $file_key = \request()->all('key');
        $tmp_files = \request()->session()->get('tmp_files');
        foreach ($tmp_files as $key => $tmp_file) {
            if ($tmp_file['key'] == $file_key['key']) {
                $file_path = $tmp_file['file_path'];
                unset($tmp_files[$key]);
                \request()->session()->put('tmp_files', $tmp_files);
                @unlink($file_path);
                return 0;
            }

        }
    }


}
