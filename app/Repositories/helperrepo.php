<?php

use Carbon\Carbon;

if (!function_exists('autoForm')) {
    function autoForm($elements, $action, $classes = [], $model = null)
    {
        $model_form = null;
        if (!is_array($elements)) {
            $model_form = $elements;
            $elements = new $elements();
            $elements = $elements->getfillable();
            $elements['form_model'] = $model_form;
        }
        $formRepository = new \App\Repositories\FormRepository();
        return $formRepository->autoGenerate($elements, $action, $classes, $model);
    }
}

/**
 * Truncate a string after a given number of words -- limit number of words
 */
if (!function_exists('limit_string_words')) {
    function limit_string_words($text, $words_limit)
    {
        if (str_word_count($text, 0) > $words_limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$words_limit]) . ' ...';
        }
        return $text;
    }
}
/**
 * Get current logged in user
 */
if (!function_exists('getUser')) {
    function getUser($user_id = null)
    {
        if ($user_id)
            return \App\User::whereId($user_id)->first();
        return auth()->user();
    }
}

if (!function_exists('getUserStatus')) {
    function getUserStatus($state)
    {
        return \App\Repositories\StatusRepository::getUserStatus($state);
    }
}
if (!function_exists('getStaffEmploymentStatus')) {
    function getStaffEmploymentStatus($state)
    {
        return \App\Repositories\StatusRepository::getStaffEmploymentStatus($state);
    }
}
if (!function_exists('getStaffGenderInitial')) {
    function getStaffGenderInitial($state)
    {
        if ($state == 2)
            return "F";
        return "M";
    }
}
if (!function_exists('getGender')) {
    function getGender($state)
    {
        return \App\Repositories\StatusRepository::getGender($state);
    }
}
if (!function_exists('getOffdutyRequestStatus')) {
    function getOffdutyRequestStatus($state)
    {
        return \App\Repositories\StatusRepository::getOffdutyRequestStatus($state);
    }
}
if (!function_exists('getCompensationStatus')) {
    function getCompensationStatus($state)
    {
        return \App\Repositories\StatusRepository::getCompensationStatus($state);
    }
}
if (!function_exists('getOffdutyRequestDateStatus')) {
    function getOffdutyRequestDateStatus($state)
    {
        return \App\Repositories\StatusRepository::getOffdutyRequestDateStatus($state);
    }
}

if (!function_exists('getLeaveRequestStatus')) {
    function getLeaveRequestStatus($state)
    {
        return \App\Repositories\StatusRepository::getLeaveRequestStatus($state);
    }
}
if (!function_exists('getLeaveRequestStatusButton')) {
    function getLeaveRequestStatusButton($state)
    {
        $is_operation_manager = false;
        if (auth()->user())
            if (auth()->user()->is_operation_manager == 1)
                $is_operation_manager = true;
        $stat_index = getLeaveRequestStatus($state);
        $btn['pending'] = [0 => 'primary', 1 => 'spinner', 2 => ' Pending Request'];
        if($is_operation_manager)
            $btn['pending'] = [0 => 'primary', 1 => 'spinner', 2 => ' Pending AM Request'];
        $btn['am_approved'] = [0 => 'success', 1 => 'check', 2 => ' AM approved'];
        $btn['hod_approved'] = [0 => 'success', 1 => 'check', 2 => ' HOD approved'];
        $btn['hr_approved'] = [0 => 'success', 1 => 'thumbs-up', 2 => ' HR approved'];
        $btn['am_rejected'] = [0 => 'danger', 1 => 'exclamation', 2 => ' AM declined'];
        $btn['am_declined'] = [0 => 'danger', 1 => 'exclamation', 2 => ' AM declined'];
        $btn['hod_rejected'] = [0 => 'danger', 1 => 'exclamation', 2 => ' HOD declined'];
        $btn['hod_declined'] = [0 => 'danger', 1 => 'exclamation', 2 => ' HOD declined'];
        $btn['hr_rejected'] = [0 => 'danger', 1 => 'exclamation-triangle', 2 => ' HR declined'];
        $btn['hr_declined'] = [0 => 'danger', 1 => 'exclamation-triangle', 2 => ' HR declined'];
        $btn['canceled'] = [0 => 'warning', 1 => 'times', 2 => ' Canceled'];

        return "<button class=\"btn btn-" . $btn[$stat_index][0] . " badge  badge-pill font-weight-bold btn-sm\"> <i class='fa fa-" . $btn[$stat_index][1] . "'></i>  " . $btn[$stat_index][2] . "</span>";
//        return "<span class=\"border border-".$btn[$stat_index][0]."   badge-pill text-".$btn[$stat_index][0]." btn-sm\"> <i class='fa fa-".$btn[$stat_index][1]."'></i>  ".$btn[$stat_index][2]."</span>";
    }
}
if (!function_exists('getStaffLeaveRequestStatusButton')) {
    function getStaffLeaveRequestStatusButton($state)
    {
        $status_str = getLeaveRequestStatus($state);
        $status = $status_str;
        if ($status_str == "pending")
            $status = "<span class='text-primary'><i class='fa fa-spinner'></i> <b>Pending</b></span>";
        if ($status_str == "am_approved")
            $status = "<span class='text-primary'><i class='fa fa-spinner'></i> <b>Pending</b></span>";
        if ($status_str == "hod_approved")
            $status = "<span class='text-primary'><i class='fa fa-spinner'></i> <b>Pending</b></span>";
        else if ($status_str == "hr_approved")
            $status = "<span class='text-success'><i class='fa fa-thumbs-up'></i> <b>Approved</b></span>";
        else if ($status_str == "hr_rejected"|| $status_str == "hr_declined")
            $status = "<span class='text-danger'><i class='fa fa-thumbs-down'></i> <b>Declined</b></span>";
        else if ($status_str == "hod_rejected"|| $status_str == "hod_declined")
            $status = "<span class='text-danger'><i class='fa fa-thumbs-down'></i> <b>Declined</b></span>";
        else if ($status_str == "am_rejected" || $status_str == "am_declined")
            $status = "<span class='text-danger'><i class='fa fa-thumbs-down'></i> <b>Declined</b></span>";
        else if ($status_str == "canceled")
            $status = "<span class='text-danger'><i class='fa fa-thumbs-down'></i> <b>Canceled</b></span>";
        return $status;
    }
}

if (!function_exists('getOffdutyRequestStatusButton')) {
    function getOffdutyRequestStatusButton($state)
    {
        $stat_index = getOffdutyRequestStatus($state);
        $btn['pending'] = [0 => 'primary', 1 => 'spinner', 2 => ' Pending'];
        $btn['approved'] = [0 => 'success', 1 => 'thumbs-up', 2 => ' Approved'];
        $btn['rejected'] = [0 => 'danger', 1 => 'exclamation', 2 => ' Rejected'];

        return "<button class=\"btn btn-" . $btn[$stat_index][0] . " badge  badge-pill font-weight-bold btn-sm\"> <i class='fa fa-" . $btn[$stat_index][1] . "'></i>  " . $btn[$stat_index][2] . "</span>";
//        return "<span class=\"border border-".$btn[$stat_index][0]."   badge-pill text-".$btn[$stat_index][0]." btn-sm\"> <i class='fa fa-".$btn[$stat_index][1]."'></i>  ".$btn[$stat_index][2]."</span>";
    }
}
if (!function_exists('getCompensationStatusButton')) {
    function getCompensationStatusButton($state)
    {
        $stat_index = getCompensationStatus($state);
        $btn['pending'] = [0 => 'primary', 1 => 'spinner', 2 => ' Pending'];
        $btn['approved'] = [0 => 'success', 1 => 'thumbs-up', 2 => ' Approved'];
        $btn['rejected'] = [0 => 'danger', 1 => 'exclamation', 2 => ' Rejected'];

        return "<button class=\"btn btn-" . $btn[$stat_index][0] . " badge  badge-pill font-weight-bold btn-sm\"> <i class='fa fa-" . $btn[$stat_index][1] . "'></i>  " . $btn[$stat_index][2] . "</span>";
//        return "<span class=\"border border-".$btn[$stat_index][0]."   badge-pill text-".$btn[$stat_index][0]." btn-sm\"> <i class='fa fa-".$btn[$stat_index][1]."'></i>  ".$btn[$stat_index][2]."</span>";
    }
}
if (!function_exists('getOffdutyRequestDateStatusButton')) {
    function getOffdutyRequestDateStatusButton($state)
    {
        $stat_index = getOffdutyRequestDateStatus($state);
        $btn['pending'] = [0 => 'primary', 1 => 'spinner', 2 => ' Pending'];
        $btn['pending_compensation'] = [0 => 'primary', 1 => 'spinner', 2 => ' Pending Compensation'];
        $btn['approved_offduty'] = [0 => 'success', 1 => 'check', 2 => ' Approved Off-duty'];
        $btn['approved_compensation'] = [0 => 'success', 1 => 'thumbs-up', 2 => ' Approved Compensation'];
        $btn['rejected_offduty'] = [0 => 'danger', 1 => 'exclamation', 2 => ' Rejected Off-duty'];
        $btn['rejected_compensation'] = [0 => 'danger', 1 => 'exclamation', 2 => ' Rejected Compensation'];

        return "<button class=\"btn btn-" . $btn[$stat_index][0] . " badge  badge-pill font-weight-bold btn-sm\"> <i class='fa fa-" . $btn[$stat_index][1] . "'></i>  " . $btn[$stat_index][2] . "</span>";
//        return "<span class=\"border border-".$btn[$stat_index][0]."   badge-pill text-".$btn[$stat_index][0]." btn-sm\"> <i class='fa fa-".$btn[$stat_index][1]."'></i>  ".$btn[$stat_index][2]."</span>";
    }
}
if (!function_exists('getStaffEmploymentStatusButton')) {
    function getStaffEmploymentStatusButton($state)
    {
        $stat_index = getStaffEmploymentStatus($state);
        $btn['on_leave'] = [0 => 'primary', 1 => 'spinner', 2 => ' On Leave'];
        $btn['active'] = [0 => 'success', 1 => 'check', 2 => ' Active'];
        $btn['suspended'] = [0 => 'warning', 1 => 'exclamation', 2 => ' Suspended'];
        $btn['dismissed'] = [0 => 'danger', 1 => 'exclamation', 2 => ' Discharged'];

//        return "<button class=\"btn btn-" . $btn[$stat_index][0] . " badge  badge-pill font-weight-bold btn-sm\"> <i class='fa fa-" . $btn[$stat_index][1] . "'></i>  " . $btn[$stat_index][2] . "</span>";
        return "<span class=\"border border-".$btn[$stat_index][0]." badge badge-pill text-".$btn[$stat_index][0]." btn-sm\"> <i class='fa fa-".$btn[$stat_index][1]."'></i>  ".$btn[$stat_index][2]."</span>";
    }
}
if (!function_exists('getEmployeeChecklistItemReturnStatus')) {
    function getEmployeeChecklistItemReturnStatus($state)
    {
        return \App\Repositories\StatusRepository::getEmployeeChecklistItemReturnStatus($state);
    }
}

if (!function_exists('userCan')) {
    function userCan($slug)
    {
        return request()->user()->isAllowedTo($slug);
    }
}

if (!function_exists('userHas')) {
    function userHas($user_id, $slug, $section = 'writing')
    {
        return \App\User::where('id', '=', $user_id)->first()->isAllowedTo($slug, $section);
    }
}
if (!function_exists('formatDeadline')) {
    function formatDeadline($date)
    {

        $date = \Carbon\Carbon::createFromTimestamp(strtotime($date));
        if ($date->isPast()) {
            $div_pre = '<strong class="text-danger">';
            $pre = "(late)";
            $days = $date->diffInDays();
            $hours = $date->copy()->addDays($days)->diffInHours();
            $minutes = $date->copy()->addDays($days)->addHours($hours)->diffInMinutes();
            $days_string = $days . 'D ';
            $hours_string = $hours . "H ";
        } else {
            $pre = '';
            $days = $date->diffInDays();
            $hours = $date->copy()->subDays($days)->diffInHours();
            if ($days > 0 || $hours > 5) {
                $div_pre = '<strong class="text-success">';
            } else {
                $div_pre = '<strong class="text-warning">';
            }
            $minutes = $date->copy()->subDays($days)->subHour($hours)->diffInMinutes();
            $days_string = $days . 'D ';
            $hours_string = $hours . "H ";
        }
        if ($days == 0)
            $days_string = "";
        if ($hours == 0)
            $hours_string = "";
        return $div_pre . $days_string . $hours_string . $minutes . "Mins " . $pre . '</strong>';
    }
}

if (!function_exists('formatDeadline1')) {
    function formatDeadline1($date)
    {
        $date = \Carbon\Carbon::createFromTimestamp(strtotime($date));
        if ($date->isPast())
            $div_pre = '<strong class="text-danger">';
        else
            $div_pre = '<strong class="text-success">';

        return $div_pre . $date->isoFormat('ddd, Do MMM Y') . '</strong>';
    }
}

if (!function_exists('getDaysDifference')) {
    function getDaysDifference($date_from, $date_to, $formated = false)
    {
        if (!$date_from)
            return 0;
        if ($date_to)
            $date_to = Carbon::parse($date_to);
        else
            $date_to = Carbon::today();

        $date_from = Carbon::parse($date_from);
        if ($date_to < $date_from)
            return 0;
        $date_diff = $date_to->diffInDaysFiltered(function (Carbon $date) {
            return !$date->isWeekend();
        }, $date_from);

        if (!$formated)
            return $date_diff;
        else
            if ($date_diff > 0)
                $div_pre = '<strong class="text-danger">';
            else
                $div_pre = '<strong class="text-success">';

        return $div_pre . number_format($date_diff) . '</strong>';
    }
}
if (!function_exists('getTimeDifferenceInDaysAndHours')) {
    function getTimeDifferenceInDaysAndHours($date_from, $date_to, $formated = false)
    {
        if (!$date_from)
            return 0;
        if ($date_to)
            $date_to = Carbon::parse($date_to);
        else
            $date_to = Carbon::today();
        $date_from = Carbon::parse($date_from);
        $div_pre = '<strong class="text-info">';
        $days = $date_from->diffInDays($date_to);
        $hours = $date_from->copy()->addDays($days)->diffInHours($date_to);
        $minutes = $date_from->copy()->addDays($days)->addHours($hours)->diffInMinutes($date_to);
        $days_string = ($days == 1) ? $days . 'Day ' : $days . 'Days ';
        $hours_string = ($hours == 1) ? $hours . 'Hr ' : $hours . 'Hrs ';

        if ($days == 0)
            $days_string = "";
        if ($hours == 0)
            $hours_string = "";
        return $div_pre . $days_string . $hours_string . $minutes . "Mins " . '</strong>';
    }
}

if (!function_exists('getTicketStatus')) {
     function getTicketStatus($state)
    {
        return @\App\Repositories\StatusRepository::getTicketStatus($state);
    }
}
if (!function_exists('getTicketAge')) {
    function getTicketAge($ticket_id)
    {
        $ticket = \App\Models\Core\Ticket::whereId($ticket_id)->first();
        if ($ticket) {
            $date_from = $ticket->created_at;
            if (!$date_from)
                return "";
            $date_to = Carbon::now();
            if ($ticket->closed_at)
                $date_to = $ticket->closed_at;

            return getTimeDifferenceInDaysAndHours($date_from, $date_to);
        }
        return "";
    }
}

if (!function_exists('getHostDomain')) {
    /**
     * get the site address
     * domain
     */
    function getHostDomain($address)
    {
        $address = stripcslashes($address);
        $address = trim($address, '"');
        $parseUrl = parse_url(trim($address));
        if (!isset($parseUrl['host']))
            dd(stripslashes($address), stripcslashes($address), $parseUrl);
        $domain = trim($parseUrl['host'] ? $parseUrl['host'] : array_shift(explode('/', $parseUrl['path'], 2)));
        return str_ireplace('www.', '', $domain);
    }
}

if (!function_exists('getAnnualLeaveTypeId')) {
    function getAnnualLeaveTypeId()
    { // Annual leave type id
        $annual_leave_type_id = 2;
        return $annual_leave_type_id;
    }
}


