<?php
/**
 * Created by PhpStorm.
 * User: iankibet
 * Date: 5/12/18
 * Time: 10:16 AM
 */

namespace App\Repositories;


class StatusRepository
{
    public static function getTicketStatus($state)
    {
        $statuses = [
            'Open' => 1,
            'open' => 1,
            'In Progress' => 2,
            'in_progress' => 2,
            'Wait for Response' => 3,
            'waiting_for_response' => 3,
            'Resolved' => 4,
            'resolved' => 4,
            'Closed' => 5,
            'closed' => 5,
            'Cancelled' => 6,
            'cancelled' => 6,
            'Canceled' => 6,
            'canceled' => 6
        ];
        return self::checkState($state,$statuses);
    }

    public static function getTicketPriority($state)
    {
        $statuses = [
            'Low' => 1,
            'Normal' => 2,
            'High' => 3,
            'Urgent' => 4,

        ];
        return self::checkState($state,$statuses);
    }
    public static function getModelSlugId($state)
    {
        $statuses = [
            'ticket' => 1,
        ];
        return self::checkState($state,$statuses);
    }

    public static function getGender($state)
    {
        $statuses = [
            'Male' => 1,
            'Female' => 2,
        ];
        return self::checkState($state,$statuses);
    }

    public static function getMaritalStatus($state = null)
    {
        if(!$state)
            return "";
        $statuses = [
            'Married' => 1,
            'Single' => 2,

        ];
        return self::checkState($state,$statuses);
    }

    public static function getStaffEmploymentStatus($state)
    {
        $statuses = [
            'active' => 0,
            'on_leave' => 1,
            'suspended' => 2,
            'dismissed' => 3
        ];
        return self::checkState($state,$statuses);
    }

    public static function getEmployeeChecklistItemReturnStatus($state)
    {
        //(0-non_returnable,1-assigned,2-returned)
        $statuses = [
            'non_returnable' => 0,
            'assigned' => 1,
            'returned' => 2,
        ];
        return self::checkState($state,$statuses);
    }

    public static function getLeaveRequestStatus($state)
    {
        //(0-pending,1-hod_approved, 2-hr_approved,3-hod_rejected, 4-hr_rejected, 5-canceled by me)
        $statuses = [
            'pending' => 0,
            'hod_approved'=>1,
            'hr_approved'=>2,
            'hod_rejected'=>3,
            'hod_declined'=>3,
            'hr_rejected'=>4,
            'hr_declined'=>4,
            'cancelled'=>5,
            'canceled'=>5,
            'Canceled'=>5,
            'Cancelled'=>5,
            'am_approved'=>6,
            'am_rejected'=>7,
            'am_declined'=>7,
        ];
        return self::checkState($state,$statuses);
    }
    public static function getOffdutyRequestStatus($state)
    {
        //(0-pending,1-approved, 2-rejected)
        $statuses = [
            'pending' => 0,
            'approved'=>1,
            'rejected'=>2
        ];
        return self::checkState($state,$statuses);
    }
    public static function getCompensationStatus($state)
    {
        //(0-pending,1-approved, 2-rejected)
        $statuses = [
            'pending' => 0,
            'approved'=>1,
            'rejected'=>2
        ];
        return self::checkState($state,$statuses);
    }
    public static function getOffdutyRequestDateStatus($state)
    {
        // 0-pending, 1-approved_offduty, 2-rejected_offduty, 3-pending_compensation, 4-approved_compensation, 4-rejected_compensation
        $statuses = [
            'pending' => 0,
            'approved_offduty'=>1,
            'rejected_offduty'=>2,
            'pending_compensation'=>3,
            'approved_compensation'=>4,
            'rejected_compensation'=>5
        ];
        return self::checkState($state,$statuses);
    }


    public static function checkState($state,$statuses){
        if (is_numeric($state))
            $statuses = array_flip($statuses);
        if(is_array($state)){
            $states  = [];
            foreach($state as $st) {
                $states[] = $statuses[$st];
            }
            return $states;
        }
        return $statuses[$state];
    }
}
