<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPassword extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    protected $content;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content)
    {
        $this->content = $content;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $markdown = $this->markdown('emails.mail')
            ->subject($this->content['subject'])
            ->with('content',$this->content);
        return $markdown;

    }
}
