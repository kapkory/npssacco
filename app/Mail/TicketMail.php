<?php

namespace App\Mail;

use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\LineOfBusiness;
use App\Models\Core\Ticket;
use App\Models\Core\TicketUpdate;
use App\User;
use Carbon\CarbonInterval;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Snowfire\Beautymail\Beautymail;

class TicketMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $ticket_id;
    protected $is_initial;
    protected $is_acknowledgement;
    protected $auth_user_email;
    protected $is_reassigned;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($ticket_id, $auth_user_email = null, $is_initial, $is_acknowledgement = 0, $reassigned = 0)
    {
        $this->ticket_id = $ticket_id;
        $this->is_initial = $is_initial;
        $this->auth_user_email = $auth_user_email;
        $this->is_acknowledgement = $is_acknowledgement;
        $this->is_reassigned = $reassigned;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title_type = "Ticket Updates";
        $ticket = Ticket::leftjoin('issue_categories', 'tickets.issue_category_id', 'issue_categories.id')
            ->leftjoin('dispositions', 'tickets.disposition_id', 'dispositions.id')
            ->leftjoin('users as users_1', 'users_1.id', 'tickets.user_id')
            ->leftjoin('users as users_2', 'users_2.id', 'tickets.assigned_to')
            ->leftjoin('ticket_statuses', 'tickets.ticket_status_id', 'ticket_statuses.id')
            ->leftjoin('departments', 'departments.id', 'tickets.department_id')
            ->where([
                ['tickets.id', $this->ticket_id],
            ])->select('tickets.*', 'departments.name as department', 'users_2.id as assigned_id', 'users_1.name as created_by', 'users_2.name as assigned_to', 'issue_categories.name as issue_category',
                'users_1.email as creator_email', 'users_1.name as creator_name', 'users_1.line_of_business_id as lob_from','users_2.email as assignee_email', 'users_2.email as assignee_name', 'dispositions.name as disposition', 'ticket_statuses.name as status')
            ->first();
        //get hod emails
        $hod_emails = $ticket->getHodEmails();
        //get account managers emails
//        $account_managers_emails = $ticket->getAccountManagerEmails();
        if ($this->is_initial) {
            $message = "Kindly note the enclosed ticket has just been created and assigned to you. ";
            $time = 0;
            $title_type = "Ticket Creation";
        } else if ($this->is_reassigned) {
            $message = "Kindly note the enclosed ticket has just been re-assigned to you. ";
            $time = 0;
            $title_type = "Ticket Re-Assigned";
        } else {
            $message = "The Enclosed ticket is updated as below :-";
            if ($ticket->tat) {
                $time = CarbonInterval::minute($ticket->tat)->cascade()->forHumans();
                $time = "<b>" . $time . "</b>";
            }
        }

        if ($this->is_acknowledgement) {
            $message = "We acknowledge the receipt of your ticket and is updated as below:-";
            $title_type = "Ticket Acknowledgement";
        }
        $subject = "WorkMate >> Ticket Number CC" . $ticket->id . " | " . $ticket->issue_category;
        //get ticket description
        $description = @TicketUpdate::where('ticket_id', $ticket->id)->first()->comment;
        //if ticket is being acknowledge send email to the creator and copy the assignee and hod
        if ($this->is_acknowledgement) {
            $to_email = $ticket->creator_email;
            $cc_emails = [$ticket->assignee_email];
            $name = ($ticket->creator_name) ? $ticket->creator_name : "System";
            $reply_to = $ticket->assignee_email;
        } else {
            //check if current user is creator
            if ($this->auth_user_email && $this->auth_user_email == @$ticket->creator_email) {
                $to_email = $ticket->assignee_email;
                $cc_emails = [$ticket->creator_email];
                $reply_to = $ticket->creator_email;
                $name = $ticket->assigned_to;
            } else {
                $cc_emails = [$ticket->assignee_email];
                $to_email = ($ticket->creator_email) ? $ticket->creator_email : $ticket->assignee_email;
                $reply_to = $ticket->assignee_email;
                $name = ($ticket->creator_name) ? $ticket->creator_name : "System";
            }

        }
        if (count($hod_emails) > 0) //cc hods
            $cc_emails = array_unique(array_merge($cc_emails, $hod_emails), SORT_REGULAR);
        $data = [
            'subject' => $subject,
            'to_email' => $to_email,
            'cc_emails' => $cc_emails,
            'assigned_to' => $ticket->assigned_to,
            'is_acknowledgement' => $this->is_acknowledgement,
            'name' => $name,
            'reply_to' => $reply_to,
            'time' => @$time,
            'message' => @$message,
            'title_type' => @$title_type,
            'description' => @$description,
            'link' => url('admin/tickets/ticket/' . $ticket->id)
        ];

        $files = $ticket->ticketFiles;
        $mail = $this->markdown('emails.ticket_mail', compact('data', 'ticket'))
            ->to($data['to_email'], $data['name'])
            ->cc($cc_emails)
            ->subject($data['subject'])
            ->replyTo($data['reply_to']);
        if (count($files) > 0) {
            foreach ($files as $file) {
                $mail->attach(storage_path('app/' . $file->path), ['as' => $file->name]);
            }
        }
        return $mail;
    }
}
