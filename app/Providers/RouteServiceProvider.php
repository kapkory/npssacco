<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Str;
use PHPUnit\Util\Exception;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

//        $this->customSanitizeUrl();

    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }


    /**
     * remove public from url
     * remove index.php from url
     */
    protected function customSanitizeUrl()
    {

        $is_localhost = env('IS_LOCALHOST',false);

        $current_url = request()->getRequestUri();
//        dd($is_localhost,$current_url);
        //get my public ip
//        $my_public_ip_address = $this->getMyPublicIp();
        $condition = Str::contains($current_url, 'index.php');
        if(!$is_localhost)
            $condition = Str::contains($current_url, 'public') || Str::contains($current_url, 'index.php');

        if ($condition) {
            if(!$is_localhost)
                $current_url = str_replace('public', '', $current_url);


            $current_url = str_replace('index.php', '', $current_url);
            if (strlen($current_url) > 0) {
                header("Location: $current_url", true, 301);
                exit;
            }
        }
        dd($is_localhost,$current_url);

    }

    /**
     * get my public address
     */
    public function getMyPublicIp()
    {
        $public_ip = null;
        /**
         * for now it works
         * reasons why Tarus won't recommend:
         * No explanation as to why I/WE should use the domain myip.opendns.com / resolver1.opendns.com
         * also no guarantees that the two domains will always be live hence we've to catch this.... just in case );
         */

        try {
            $public_ip = trim(shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));
        } catch (Exception $e) {

        }
        return $public_ip;
    }

    function getUserIP()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }
}
