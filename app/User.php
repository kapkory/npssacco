<?php

namespace App;

use App\Models\Core\CalltronixDepartment;
use App\Models\Core\CalltronixDepartmentHead;
use App\Models\Core\Department;
use App\Models\Core\DepartmentHead;
use App\Models\Core\LeaveRequest;
use App\Models\Core\LeaveType;
use App\Models\Core\Staff;
use App\Models\Core\TicketFile;
use App\Models\Core\YearlyStaffLeavesTracker;
use App\Models\Core\PermissionGroup;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','firstname', 'lastname','middlename', 'email', 'alternate_email', 'dob', 'password','phone','role','status','employee_number','department_id','department_id','line_of_business_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function permissionGroup(){
        return $this->belongsTo(PermissionGroup::class);
    }
    public function ticketFiles(){
        return $this->hasMany(TicketFile::class);
    }
    public function leaveRequests(){
        return $this->hasMany(LeaveRequest::class);
    }

    public function isAllowedTo($slug){
        $department = $this->department;
        if(!$department)
            return false;
        $permissions = json_decode($department->permissions);
        if(in_array($slug,$permissions))
            return true;
        return false;
    }

    /**
     * return user's calltronix
     * department details
     */
    public function getDepartment($user_id=null)
    {
        $department_id = null;
        $user_id = ($user_id) ? $user_id : $this->id;
        $user = User::whereId($user_id)->first();
        if($user)
            $department_id = $this->department_id;
        $department = Department::whereId($department_id)->first();
        return @$department;
    }

    public function isDepartmentHead(){
        $department_head = DepartmentHead::where('user_id',$this->id)->first();
        if($department_head)
            return true;
        return false;
    }

    public function getMyLeaveBalanceData(){
        $annual_leave_type_id = getAnnualLeaveTypeId();
        if($this->is_staff == 0)
            return null;
        $staff = Staff::where('user_id',$this->id)->first();
        $leavetype = LeaveType::whereId($annual_leave_type_id)->first();
        $data['days_covered'] = 0;
        $data['compensation_days'] = 0;
        $data['days_remaining'] = ($leavetype->entitlement) ? $leavetype->entitlement : 0;
        $data['days_entitled'] = ($leavetype->entitlement) ? $leavetype->entitlement : 0;

        $staff_yearly_data = YearlyStaffLeavesTracker::where([
            ['staff_id',$staff->id],
            ['leave_type_id',$annual_leave_type_id],
            ['year',Carbon::today()->format('Y')]
        ])->first();
        if($staff_yearly_data){
            $data['days_covered'] = $staff_yearly_data->days_covered;
            $data['days_remaining'] = $staff_yearly_data->days_remaining;
            $data['days_entitled'] = $staff_yearly_data->max_days_allowed;
            $data['compensation_days'] = $staff_yearly_data->compensation_days;
        }

        return $data;
    }


}
