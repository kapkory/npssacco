-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 27, 2020 at 08:58 PM
-- Server version: 8.0.21-0ubuntu0.20.04.4
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nps`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_account_managers`
--

CREATE TABLE `tbl_account_managers` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `line_of_business_id` bigint UNSIGNED NOT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_account_managers`
--

INSERT INTO `tbl_account_managers` (`id`, `user_id`, `line_of_business_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 36, 14, 0, '2020-07-06 16:57:31', '2020-07-06 16:57:31'),
(2, 37, 15, 0, '2020-07-06 16:58:32', '2020-07-06 16:58:32'),
(3, 37, 22, 0, '2020-07-06 16:58:45', '2020-07-06 16:58:45'),
(4, 32, 16, 0, '2020-07-06 17:00:05', '2020-07-06 17:00:05'),
(5, 32, 11, 0, '2020-07-06 17:00:28', '2020-07-06 17:00:28'),
(6, 34, 16, 0, '2020-07-06 17:01:47', '2020-07-06 17:01:47'),
(7, 33, 12, 0, '2020-07-06 17:02:35', '2020-07-06 17:02:35'),
(8, 44, 2, 0, '2020-07-06 17:03:21', '2020-07-06 17:03:21'),
(9, 33, 9, 0, '2020-07-06 17:03:56', '2020-07-06 17:03:56'),
(10, 33, 17, 0, '2020-07-06 17:04:19', '2020-07-06 17:04:19'),
(11, 36, 1, 0, '2020-07-06 17:05:42', '2020-07-06 17:05:42'),
(12, 33, 3, 0, '2020-07-06 17:06:19', '2020-07-06 17:06:19');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appraisal_questions`
--

CREATE TABLE `tbl_appraisal_questions` (
  `id` bigint UNSIGNED NOT NULL,
  `appraisal_question_category_id` bigint UNSIGNED NOT NULL,
  `question` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `position` int UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_appraisal_questions`
--

INSERT INTO `tbl_appraisal_questions` (`id`, `appraisal_question_category_id`, `question`, `status`, `position`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Regular and punctual attendance in accordance with company’s days and hours of operation', 1, 1, 22, '2020-05-22 06:43:39', '2020-05-22 06:43:39'),
(2, 2, 'Produces high levels of outputs in a timely manner under normal and pressure conditions', 1, 2, 22, '2020-05-22 06:45:13', '2020-05-22 06:45:13'),
(3, 3, 'Produces highest quality, error free work that is in keeping with the style of the departmental goals.', 1, 3, 22, '2020-05-22 06:45:31', '2020-05-22 06:45:31'),
(4, 4, 'Shows familiarity with the professional and technical requirements of the position and its relevance and contribution to the company’s mission, Vision, and objectives.', 1, 4, 22, '2020-05-22 06:45:55', '2020-05-22 06:45:55'),
(5, 5, 'Undertakes initiatives and or participates in programmes to improve personal capacity to carry out job functions and responsibilities.  Maintains good on-the-job deportment', 1, 5, 22, '2020-05-22 06:46:21', '2020-05-22 06:46:21'),
(6, 6, 'Identifies and offers solutions to operational problems, deficiencies and constraints; demonstrates astuteness in judging appropriateness of acting promptly and independently, or after consultation with manager(s)', 1, 6, 22, '2020-05-22 06:46:38', '2020-05-22 06:46:38'),
(7, 7, 'Demonstrates the ability to rigorously assess situations, evaluate response options and consequences and to select /recommend appropriate intervention or action', 1, 7, 22, '2020-05-22 06:48:04', '2020-05-22 06:48:04'),
(8, 8, 'Consistently carries out responsibilities to effectively and efficiently achieve desired outcomes or outputs within agreed schedules and deadlines.  Can be relied upon to act or ensure that actions are carried out in the best interest of the Company.', 1, 8, 22, '2020-05-22 06:48:27', '2020-05-22 06:48:27'),
(9, 9, 'Consistently exhibits good oral, writing and listening skills.  Interacts in a professional manner with peers, subordinates and the general public', 1, 9, 22, '2020-05-22 06:48:54', '2020-05-22 06:48:54'),
(10, 10, 'Willing to volunteer, share responsibilities and contribute to the completion of tasks, especially under situations of pressure', 1, 10, 22, '2020-05-22 06:49:16', '2020-05-22 06:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appraisal_question_categories`
--

CREATE TABLE `tbl_appraisal_question_categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_appraisal_question_categories`
--

INSERT INTO `tbl_appraisal_question_categories` (`id`, `name`, `description`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Punctuality/Attendance', 'Punctuality/Attendance', 22, '2020-05-22 06:38:10', '2020-05-22 06:38:10'),
(2, 'Productivity', 'Productivity', 22, '2020-05-22 06:38:29', '2020-05-22 06:38:29'),
(3, 'Quality of Work', 'Quality of Work', 22, '2020-05-22 06:39:40', '2020-05-22 06:39:40'),
(4, 'Knowledge of Job', 'Knowledge of Job', 22, '2020-05-22 06:40:22', '2020-05-22 06:40:22'),
(5, 'Personal Development', 'Personal Development', 22, '2020-05-22 06:40:36', '2020-05-22 06:40:36'),
(6, 'Initiative', 'Initiative', 22, '2020-05-22 06:41:07', '2020-05-22 06:41:07'),
(7, 'Judgment', 'Judgment', 22, '2020-05-22 06:41:18', '2020-05-22 06:41:18'),
(8, 'Reliability/Dependability', 'Reliability/Dependability', 22, '2020-05-22 06:41:49', '2020-05-22 06:41:49'),
(9, 'Communication', 'Communication', 22, '2020-05-22 06:41:59', '2020-05-22 06:41:59'),
(10, 'Teamwork', 'Teamwork', 22, '2020-05-22 06:42:18', '2020-05-22 06:42:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_appraisal_question_responses`
--

CREATE TABLE `tbl_appraisal_question_responses` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_appraisal_id` bigint UNSIGNED NOT NULL,
  `appraisal_question_id` bigint UNSIGNED NOT NULL,
  `score` double UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_branches`
--

CREATE TABLE `tbl_branches` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_branches`
--

INSERT INTO `tbl_branches` (`id`, `name`, `description`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Mombasa', '', 0, 1, '2020-10-17 16:44:00', '2020-10-17 16:44:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_calltronix_departments`
--

CREATE TABLE `tbl_calltronix_departments` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `department_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_count` bigint UNSIGNED NOT NULL DEFAULT '0',
  `parent_department_id` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `sub_departments_count` tinyint UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_calltronix_departments`
--

INSERT INTO `tbl_calltronix_departments` (`id`, `user_id`, `name`, `description`, `status`, `created_at`, `updated_at`, `department_code`, `staff_count`, `parent_department_id`, `sub_departments_count`) VALUES
(1, 22, 'IT', 'IT', 1, NULL, '2020-08-31 09:00:27', 'SD', 1, 0, 0),
(2, 22, 'Telephony', 'TL', 1, NULL, '2020-07-17 08:42:00', 'TL', 0, 0, 0),
(3, 22, 'Innovative Marketing', 'Innovative Marketing', 1, NULL, '2020-07-17 08:42:00', 'IM', 0, 0, 0),
(5, 22, 'Sales and Marketing', 'Sales and Marketing Department', 1, '2020-04-18 06:14:55', '2020-06-10 05:56:07', 'SL', 0, 0, 0),
(6, 22, 'Training and Development', 'Training and Development', 1, '2020-04-18 06:16:32', '2020-09-03 07:46:45', 'TD', 1, 0, 0),
(7, 22, 'Finance', 'Finance', 1, '2020-04-18 06:16:51', '2020-05-18 05:56:30', 'FN', 0, 0, 0),
(8, 22, 'Human Resources', 'Human Resources', 1, '2020-04-18 06:17:06', '2020-07-17 08:42:00', 'HR', 0, 0, 0),
(9, 22, 'Contact Centre Operations', 'Contact Center Operations', 1, '2020-04-18 06:17:32', '2020-08-31 13:25:47', 'CC', 24, 0, 1),
(10, 22, 'Business Development', 'Business Development', 1, '2020-04-18 06:18:15', '2020-05-18 05:56:30', 'BD', 0, 0, 0),
(11, 22, 'Project Management', 'Project Management', 1, '2020-04-19 09:31:16', '2020-05-18 05:56:30', 'PM', 0, 0, 0),
(12, 22, 'Quality Assurance', 'Quality Assurance', 1, '2020-05-18 11:47:11', '2020-07-17 08:41:59', 'QA', 0, 9, 0),
(13, 22, 'Support Staff', 'People providing supplementary/additional help like cleaning', 1, '2020-06-10 06:02:42', '2020-06-12 10:39:05', 'SS', 0, 0, 0),
(14, 22, 'Director', 'Director', 1, '2020-06-10 06:04:14', '2020-06-10 06:04:14', 'DI', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_calltronix_department_heads`
--

CREATE TABLE `tbl_calltronix_department_heads` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `calltronix_department_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_calltronix_department_heads`
--

INSERT INTO `tbl_calltronix_department_heads` (`id`, `user_id`, `calltronix_department_id`, `status`, `created_at`, `updated_at`) VALUES
(5, 8, 3, 1, '2020-04-19 10:46:18', '2020-04-19 10:46:18'),
(6, 11, 5, 1, '2020-04-19 10:46:37', '2020-04-19 10:46:37'),
(7, 14, 6, 1, '2020-04-19 10:51:26', '2020-04-19 10:51:26'),
(8, 9, 7, 1, '2020-04-19 10:51:44', '2020-04-19 10:51:44'),
(9, 7, 8, 1, '2020-04-19 10:51:56', '2020-04-19 10:51:56'),
(10, 12, 9, 1, '2020-04-19 10:52:08', '2020-04-19 10:52:08'),
(11, 15, 11, 1, '2020-04-19 10:52:40', '2020-04-19 10:52:40'),
(12, 27, 1, 1, '2020-05-14 14:35:17', '2020-05-14 14:35:17'),
(13, 39, 12, 1, '2020-07-17 08:42:57', '2020-07-17 08:42:57'),
(14, 1, 2, 1, '2020-09-26 21:25:41', '2020-09-26 21:25:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_compensation_requests`
--

CREATE TABLE `tbl_compensation_requests` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_id` bigint UNSIGNED NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `days` double UNSIGNED NOT NULL DEFAULT '0',
  `approved_at` datetime DEFAULT NULL,
  `rejected_at` datetime DEFAULT NULL,
  `action_by` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hr_approve_comment` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `hr_reject_reason` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contract_types`
--

CREATE TABLE `tbl_contract_types` (
  `id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_counties`
--

CREATE TABLE `tbl_counties` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `capital` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_counties`
--

INSERT INTO `tbl_counties` (`id`, `name`, `code`, `capital`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Mombasa', '1', 'Monbasa City', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(2, 'Kwale', '2', 'Kwale', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(3, 'Kilifi', '3', 'Kilifi', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(4, 'Tana River', '4', 'Hola', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(5, 'Lamu', '5', 'Lamu', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(6, 'Taita-Taveta', '6', 'Voi', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(7, 'Garissa', '7', 'Garissa', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(8, 'Wajir', '8', 'Wajir', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(9, 'Mandera', '9', 'Mandera', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(10, 'Marsabit', '10', 'Marsabit', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(11, 'Isiolo', '11', 'Isiolo', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(12, 'Meru', '12', 'Meru', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(13, 'Tharaka-Nithi', '13', 'Chuka', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(14, 'Embu', '14', 'Embu', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(15, 'Kitui', '15', 'Kitui', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(16, 'Machakos', '16', 'Machakos', 1, '2018-07-07 03:29:40', '2018-07-07 03:29:40'),
(17, 'Makueni', '17', 'Wote', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(18, 'Nyandarua', '18', 'Ol Kalou', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(19, 'Nyeri', '19', 'Nyeri', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(20, 'Kirinyaga', '20', 'Kerugoya/Kutus', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(21, 'Murang\'a', '21', 'Murang\'a', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(22, 'Kiambu', '22', 'Kiambu', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(23, 'Turkana', '23', 'Lodwar', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(24, 'West Pokot', '24', 'Kapenguria', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(25, 'Samburu', '25', 'Maralal', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(26, 'Trans-Nzoia', '26', 'Kitale', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(27, 'Uasin Gishu', '27', 'Eldoret', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(28, 'Elgeyo-Marakwet', '28', 'Iten', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(29, 'Nandi', '29', 'Kapsabet', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(30, 'Baringo', '30', 'Kabarnet', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(31, 'Laikipia', '31', 'Rumuruti', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(32, 'Nakuru', '32', 'Nakuru', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(33, 'Narok', '33', 'Narok', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(34, 'Kajiado', '34', 'Kajiado', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(35, 'Kericho', '35', 'Kericho', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(36, 'Bomet', '36', 'Bomet', 1, '2018-07-07 03:29:41', '2018-07-07 03:29:41'),
(37, 'Kakamega', '37', 'Kakamega', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(38, 'Vihiga', '38', 'Vihiga', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(39, 'Bungoma', '39', 'Bungoma', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(40, 'Busia', '40', 'Busia', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(41, 'Siaya', '41', 'Siaya', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(42, 'Kisumu', '42', 'Kisumu', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(43, 'Homa Bay', '43', 'Homa Bay', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(44, 'Migori', '44', 'Migori', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(45, 'Kisii', '45', 'Kisii', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(46, 'Nyamira', '46', 'Nyamira', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42'),
(47, 'Nairobi', '47', 'Nairobi City', 1, '2018-07-07 03:29:42', '2018-07-07 03:29:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departments`
--

CREATE TABLE `tbl_departments` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_department_id` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `sub_departments_count` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `staff_count` bigint UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_departments`
--

INSERT INTO `tbl_departments` (`id`, `user_id`, `name`, `description`, `department_code`, `parent_department_id`, `sub_departments_count`, `status`, `created_at`, `updated_at`, `staff_count`) VALUES
(1, 1, 'IT', '', NULL, 0, 0, 1, NULL, NULL, 0),
(2, 1, 'Telephony', '', NULL, 0, 0, 1, NULL, NULL, 0),
(3, 1, 'Innovative Marketing', '', NULL, 0, 0, 1, NULL, NULL, 0),
(5, 1, 'Sales', 'Sales Department', NULL, 0, 0, 1, '2020-04-18 06:14:55', '2020-04-18 06:14:55', 0),
(6, 1, 'Training and Development', 'Training and Development', NULL, 0, 0, 1, '2020-04-18 06:16:32', '2020-04-18 06:16:32', 0),
(7, 1, 'Finance', 'Finance', NULL, 0, 0, 1, '2020-04-18 06:16:51', '2020-04-18 06:16:51', 0),
(8, 1, 'Human Resources', 'Human Resources', NULL, 0, 0, 1, '2020-04-18 06:17:06', '2020-04-19 09:30:34', 0),
(9, 1, 'Contact Center Operations', 'Contact Center Operations', NULL, 0, 0, 1, '2020-04-18 06:17:32', '2020-04-18 06:17:32', 0),
(10, 1, 'Business Development', 'Business Development', NULL, 0, 0, 1, '2020-04-18 06:18:15', '2020-04-18 06:18:15', 0),
(11, 1, 'Project Management', 'Project Management', NULL, 0, 0, 1, '2020-04-19 09:31:16', '2020-04-19 09:31:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_department_heads`
--

CREATE TABLE `tbl_department_heads` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `department_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_department_heads`
--

INSERT INTO `tbl_department_heads` (`id`, `user_id`, `department_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 27, 1, 1, '2020-09-26 23:08:34', '2020-09-26 23:08:34');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dispositions`
--

CREATE TABLE `tbl_dispositions` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_dispositions`
--

INSERT INTO `tbl_dispositions` (`id`, `user_id`, `description`, `name`, `status`, `created_at`, `updated_at`) VALUES
(3, 1, 'Unable to Login to CRM', 'Unable to Login to CRM', 1, NULL, NULL),
(4, 1, 'CRM Login request', 'CRM Login request', 1, NULL, '2020-04-18 05:42:44'),
(5, 1, NULL, 'Domain Login Request', 1, '2020-04-18 05:43:04', '2020-04-18 05:43:04'),
(6, 1, 'Biometric Login Request', 'Biometric Login Request', 1, '2020-04-18 05:54:44', '2020-04-18 05:54:44'),
(7, 1, 'CRM Password Reset', 'CRM Password Reset', 1, '2020-04-18 05:54:55', '2020-04-18 05:54:55'),
(8, 1, 'Domain Password Reset', 'Domain Password Reset', 1, '2020-04-18 05:55:05', '2020-04-18 05:55:05'),
(9, 1, 'CRM Downtime/Unreachable', 'CRM Downtime/Unreachable', 1, '2020-04-18 05:55:13', '2020-04-18 05:55:13'),
(10, 1, 'CRM Disposition Addition Request', 'CRM Disposition Addition Request', 1, '2020-04-18 05:55:20', '2020-04-18 05:55:20'),
(11, 1, 'IT Meeting request', 'IT Meeting request', 1, '2020-04-18 05:57:53', '2020-04-18 05:57:53'),
(12, 1, 'Internet Downtime', 'Internet Downtime', 1, '2020-04-18 05:57:59', '2020-04-18 05:57:59'),
(13, 1, 'Sapaad Login Request', 'Sapaad Login Request', 1, '2020-04-18 05:58:09', '2020-04-18 05:58:09'),
(14, 1, 'Faulty Keyboard', 'Faulty Keyboard', 1, '2020-04-18 05:58:18', '2020-04-18 05:58:18'),
(15, 1, 'Faulty Mouse', 'Faulty Mouse', 1, '2020-04-18 05:58:27', '2020-04-18 05:58:27'),
(16, 1, 'Faulty Monitor', 'Faulty Monitor', 1, '2020-04-18 05:58:33', '2020-04-18 05:58:33'),
(17, 1, 'Network/Internet Access', 'Network/Internet Access', 1, '2020-04-18 05:58:39', '2020-04-18 05:58:39'),
(18, 1, 'Quemetrics CSE Login Request', 'Quemetrics CSE Login Request', 1, '2020-04-18 05:58:45', '2020-04-18 05:58:45'),
(19, 1, 'Quemetrics Supervisor Login Request', 'Quemetrics Supervisor Login Request', 1, '2020-04-18 05:58:52', '2020-04-18 05:58:52'),
(20, 1, 'Quemetrics Downtime/Unreachable', 'Quemetrics Downtime/Unreachable', 1, '2020-04-18 05:58:58', '2020-04-18 05:58:58'),
(21, 1, 'CSE unable to Login to Queuemtrics', 'CSE unable to Login to Queuemtrics', 1, '2020-04-18 05:59:05', '2020-04-18 05:59:05'),
(22, 1, 'CSE unable to hear Customer', 'CSE unable to hear Customer', 1, '2020-04-18 05:59:12', '2020-04-18 05:59:12'),
(23, 1, 'Customer unable to hear CSE', 'Customer unable to hear CSE', 1, '2020-04-18 05:59:19', '2020-04-18 05:59:19'),
(24, 1, 'Faulty Headsets', 'Faulty Headsets', 1, '2020-04-18 05:59:25', '2020-04-18 05:59:25'),
(25, 1, 'Request for Leave', 'Request for Leave', 1, '2020-04-18 05:59:31', '2020-04-18 05:59:31'),
(26, 1, 'Request for Contract', 'Request for Contract', 1, '2020-04-18 05:59:38', '2020-04-18 05:59:38'),
(27, 1, 'Request for Personal Meeting', 'Request for Personal Meeting', 1, '2020-04-18 05:59:44', '2020-04-18 05:59:44'),
(28, 1, 'Salary Query Request', 'Salary Query Request', 1, '2020-04-18 05:59:51', '2020-04-18 05:59:51'),
(29, 1, 'Bank Details Change', 'Bank Details Change', 1, '2020-04-18 05:59:58', '2020-04-18 05:59:58'),
(30, 1, 'Request for Transport', 'Request for Transport', 1, '2020-04-18 06:00:09', '2020-04-18 06:00:09'),
(31, 1, 'Request for Petty Cash', 'Request for Petty Cash', 1, '2020-04-18 06:00:19', '2020-04-18 06:00:19'),
(32, 1, 'Request for Refund', 'Request for Refund', 1, '2020-04-18 06:00:30', '2020-04-18 06:00:30'),
(33, 1, 'Request for Training Material', 'Request for Training Material', 1, '2020-04-18 06:00:37', '2020-04-18 06:00:37'),
(34, 1, 'Training room request', 'Training room request', 1, '2020-04-18 06:00:45', '2020-04-18 06:00:45'),
(35, 1, 'SoftSkill Training Request', 'SoftSkill Training Request', 1, '2020-04-18 06:00:53', '2020-04-18 06:00:53'),
(36, 1, 'New Product Training Request', 'New Product Training Request', 1, '2020-04-18 06:01:01', '2020-04-18 06:01:01'),
(37, 1, 'New CSE training Request', 'New CSE training Request', 1, '2020-04-18 06:01:15', '2020-04-18 06:01:15'),
(38, 1, 'Telephony Training Request', 'Telephony Training Request', 1, '2020-04-18 06:01:23', '2020-04-18 06:01:23'),
(39, 1, 'CRM Training Request', 'CRM Training Request', 1, '2020-04-18 06:01:33', '2020-04-18 06:01:33'),
(40, 1, 'Request for Design', 'Request for Design', 1, '2020-04-18 06:01:39', '2020-04-18 06:01:39'),
(41, 1, 'Request for Meeting', 'Request for Meeting', 1, '2020-04-18 06:01:47', '2020-04-18 06:01:47'),
(42, 1, 'Onboarding and Recruitment', 'Onboarding and Recruitment', 1, '2020-04-18 06:22:13', '2020-04-18 06:22:13'),
(43, 1, 'Network Cabling', 'Network Cabling', 1, '2020-04-18 06:22:33', '2020-04-18 06:22:33'),
(44, 1, 'CRM Feature/Change Request', 'CRM Feature/Change Request', 1, '2020-04-19 05:20:00', '2020-05-27 14:24:42'),
(45, 21, 'Email Account Request', 'Email Account Request', 1, '2020-05-09 19:25:45', '2020-05-09 19:25:45'),
(46, 21, 'Queuemetrics  Login Request', 'Queuemetrics  Login Request', 1, '2020-05-09 19:27:30', '2020-05-09 19:27:30'),
(47, 21, 'Laptop Request', 'Laptop Request', 1, '2020-05-09 19:32:57', '2020-05-09 19:32:57'),
(48, 21, 'Headset Request', 'Headset Request', 1, '2020-05-09 19:33:09', '2020-05-09 19:33:09'),
(49, 21, 'Phone Request', 'Phone Request', 1, '2020-05-09 19:33:20', '2020-05-09 19:33:20'),
(50, 21, 'PC Request', 'PC Request', 1, '2020-05-09 19:38:16', '2020-05-09 19:38:16'),
(51, 22, 'Advance Salary', 'Advance Salary', 1, '2020-05-18 11:48:29', '2020-05-18 11:48:29'),
(52, 22, 'Commission Request', 'Commission Request', 1, '2020-05-18 11:48:50', '2020-05-18 11:48:50'),
(53, 22, 'Delayed Commission', 'Delayed Commission', 1, '2020-05-18 11:50:58', '2020-05-18 11:50:58'),
(54, 22, 'Delayed refund', 'Delayed refund', 1, '2020-05-18 11:51:14', '2020-05-18 11:51:14'),
(55, 22, 'Sapaad Downtime', 'Sapaad Downtime', 1, '2020-05-19 11:39:54', '2020-05-19 11:39:54'),
(56, 22, 'List upload', 'List upload', 1, '2020-05-22 07:05:35', '2020-05-22 07:05:35'),
(57, 22, 'Call recordings request', 'Call recordings request', 1, '2020-05-23 09:32:24', '2020-05-23 09:32:24'),
(58, 22, 'Calibration request', 'Calibration request', 1, '2020-05-23 09:32:37', '2020-05-23 09:32:37'),
(59, 22, 'Reports request', 'Reports request', 1, '2020-05-23 09:32:50', '2020-05-23 09:32:50'),
(60, 22, 'Scripts request', 'Scripts request', 1, '2020-05-23 09:33:08', '2020-05-23 09:33:08'),
(61, 1, 'CRM Errors/Bugs', 'CRM Errors/Bugs', 1, '2020-05-28 17:16:55', '2020-05-28 17:16:55'),
(62, 1, 'CRM Data Upload', 'CRM Data Upload', 1, '2020-06-02 09:57:43', '2020-06-02 09:57:43'),
(63, 30, NULL, 'Refresher training CRM', 1, '2020-06-03 11:04:05', '2020-06-03 11:04:05'),
(64, 30, NULL, 'Refresher training customer experience', 1, '2020-06-03 11:04:22', '2020-06-03 11:04:22'),
(65, 30, NULL, 'Refresher training sales skills', 1, '2020-06-03 11:04:32', '2020-06-03 11:04:32'),
(66, 30, NULL, 'Refresher training telephony', 1, '2020-06-03 11:04:45', '2020-06-03 11:04:45'),
(67, 30, NULL, 'Refresher training emails', 1, '2020-06-03 11:05:06', '2020-06-03 11:05:06'),
(68, 22, 'Connectivity Issues', 'Connectivity Issues', 1, '2020-06-08 06:51:03', '2020-06-08 06:51:03'),
(69, 22, 'Change of inconsistent details on CRM (Intranet) -Staff details ie wrongly spelled names etc.', 'Change of inconsistent details on CRM (Intranet) -Staff details', 1, '2020-06-12 12:21:40', '2020-06-18 08:00:14'),
(70, 22, 'Difficulty Logging in(Intranet)', 'Difficulty Logging in(Intranet)', 1, '2020-06-12 12:27:37', '2020-06-12 12:27:37'),
(71, 22, 'Divert calls from another number to the sip-line', 'Divert Calls to sip-line', 1, '2020-06-18 08:58:10', '2020-06-18 08:59:43'),
(72, 22, '3CX', '3CX', 1, '2020-07-13 12:10:12', '2020-07-13 12:10:12'),
(73, 22, 'Sales order email alert', 'Sales order email alert', 1, '2020-07-24 10:44:08', '2020-07-24 10:44:08'),
(74, 22, 'CRM data request', 'CRM data request', 1, '2020-07-27 08:56:06', '2020-07-27 08:56:06'),
(75, 1, 'Difficulty in Logging In', 'Difficulty in Logging In', 1, '2020-07-31 08:31:18', '2020-07-31 08:31:18'),
(76, 22, 'Queuemetrics Wrong Data Display', 'Queuemetrics Wrong Data Display', 1, '2020-08-07 11:01:09', '2020-08-07 11:01:09'),
(77, 1, 'Escalation', 'Escalation', 1, '2020-08-18 02:35:48', '2020-08-18 02:35:48'),
(78, 22, 'Unable to Send Emails', 'Unable to Send Emails', 1, '2020-08-20 05:57:40', '2020-08-20 05:57:40'),
(79, 1, 'Email Password Reset', 'Email Password Reset', 1, '2020-08-28 08:00:53', '2020-08-28 08:01:12'),
(80, 22, 'Queuemetrics Pauses', 'Queuemetrics Pauses', 1, '2020-09-01 11:52:48', '2020-09-01 11:52:48'),
(81, 22, 'ie request to log calls made outside working hours. currently not being logged by queuemetrics.', 'Queuemetrics Feature Request', 1, '2020-09-02 08:55:36', '2020-09-02 08:55:36');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_disposition_issue_category`
--

CREATE TABLE `tbl_disposition_issue_category` (
  `id` bigint UNSIGNED NOT NULL,
  `disposition_id` int NOT NULL,
  `issue_category_id` int NOT NULL,
  `issue_sub_category_id` int NOT NULL,
  `is_fcr` tinyint NOT NULL DEFAULT '0',
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_disposition_issue_category`
--

INSERT INTO `tbl_disposition_issue_category` (`id`, `disposition_id`, `issue_category_id`, `issue_sub_category_id`, `is_fcr`, `status`, `created_at`, `updated_at`) VALUES
(45, 3, 1, 4, 0, 1, '2020-10-04 09:26:24', '2020-10-04 09:32:56'),
(46, 4, 1, 4, 0, 1, '2020-10-04 09:26:24', '2020-10-04 09:32:57'),
(47, 5, 1, 4, 0, 1, '2020-10-04 09:26:24', '2020-10-04 09:32:57'),
(48, 3, 1, 1, 0, 1, '2020-10-04 09:33:47', '2020-10-04 09:33:47'),
(49, 4, 1, 1, 0, 1, '2020-10-04 09:33:47', '2020-10-04 09:33:47'),
(50, 5, 1, 1, 0, 1, '2020-10-04 09:33:47', '2020-10-04 09:33:47'),
(51, 3, 1, 2, 0, 1, '2020-10-04 09:34:39', '2020-10-04 09:37:25'),
(52, 7, 1, 2, 0, 1, '2020-10-04 09:34:39', '2020-10-04 09:37:25'),
(53, 8, 1, 2, 0, 1, '2020-10-04 09:34:39', '2020-10-04 09:37:25'),
(54, 15, 1, 2, 0, 1, '2020-10-04 09:37:25', '2020-10-04 09:37:25'),
(55, 16, 1, 2, 0, 1, '2020-10-04 09:37:25', '2020-10-04 09:37:25'),
(56, 17, 1, 2, 0, 1, '2020-10-04 09:37:25', '2020-10-04 09:37:25'),
(57, 51, 2, 2, 0, 1, '2020-10-17 18:39:49', '2020-10-17 18:39:49'),
(58, 52, 2, 2, 0, 1, '2020-10-17 18:39:49', '2020-10-17 18:39:49'),
(59, 53, 2, 2, 0, 1, '2020-10-17 18:39:50', '2020-10-17 18:39:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee_onboarding_checklists`
--

CREATE TABLE `tbl_employee_onboarding_checklists` (
  `id` bigint UNSIGNED NOT NULL,
  `onboarding_checklist_item_id` bigint UNSIGNED NOT NULL,
  `staff_id` bigint UNSIGNED NOT NULL,
  `return_status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `date_returned` datetime DEFAULT NULL,
  `returned_condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_employee_onboarding_checklists`
--

INSERT INTO `tbl_employee_onboarding_checklists` (`id`, `onboarding_checklist_item_id`, `staff_id`, `return_status`, `date_returned`, `returned_condition`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 10, 73, 1, NULL, NULL, 7, '2020-08-03 13:54:43', '2020-08-03 13:54:43'),
(2, 11, 73, 1, NULL, NULL, 7, '2020-08-03 13:54:47', '2020-08-03 13:54:47'),
(3, 12, 73, 1, NULL, NULL, 7, '2020-08-03 13:54:51', '2020-08-03 13:54:51'),
(4, 13, 73, 1, NULL, NULL, 7, '2020-08-03 13:54:55', '2020-08-03 13:54:55'),
(5, 14, 73, 1, NULL, NULL, 7, '2020-08-03 13:54:59', '2020-08-03 13:54:59'),
(6, 16, 73, 1, NULL, NULL, 7, '2020-08-03 13:55:03', '2020-08-03 13:55:03'),
(7, 18, 73, 2, '2020-07-31 00:00:00', 'Good', 7, '2020-08-03 13:55:07', '2020-08-03 13:55:54'),
(8, 19, 73, 1, NULL, NULL, 7, '2020-08-03 13:55:11', '2020-08-03 13:55:11'),
(9, 10, 76, 1, NULL, NULL, 7, '2020-08-03 13:56:46', '2020-08-03 13:56:46'),
(10, 11, 76, 1, NULL, NULL, 7, '2020-08-03 13:56:50', '2020-08-03 13:56:50'),
(11, 12, 76, 1, NULL, NULL, 7, '2020-08-03 13:56:54', '2020-08-03 13:56:54'),
(12, 13, 76, 1, NULL, NULL, 7, '2020-08-03 13:56:58', '2020-08-03 13:56:58'),
(13, 14, 76, 1, NULL, NULL, 7, '2020-08-03 13:57:01', '2020-08-03 13:57:01'),
(14, 15, 76, 1, NULL, NULL, 7, '2020-08-03 13:57:05', '2020-08-03 13:57:05'),
(15, 16, 76, 1, NULL, NULL, 7, '2020-08-03 13:57:08', '2020-08-03 13:57:08'),
(16, 17, 76, 1, NULL, NULL, 7, '2020-08-03 13:57:11', '2020-08-03 13:57:11'),
(17, 18, 76, 1, NULL, NULL, 7, '2020-08-03 13:57:15', '2020-08-03 13:57:15'),
(18, 19, 76, 1, NULL, NULL, 7, '2020-08-03 13:57:19', '2020-08-03 13:57:19'),
(19, 10, 20, 1, NULL, NULL, 7, '2020-09-19 10:24:40', '2020-09-19 10:24:40'),
(20, 11, 20, 1, NULL, NULL, 7, '2020-09-19 10:24:44', '2020-09-19 10:24:44'),
(21, 12, 20, 1, NULL, NULL, 7, '2020-09-19 10:24:47', '2020-09-19 10:24:47'),
(22, 13, 20, 1, NULL, NULL, 7, '2020-09-19 10:24:51', '2020-09-19 10:24:51'),
(23, 15, 20, 1, NULL, NULL, 7, '2020-09-19 10:24:55', '2020-09-19 10:24:55'),
(24, 16, 20, 1, NULL, NULL, 7, '2020-09-19 10:24:58', '2020-09-19 10:24:58'),
(25, 17, 20, 1, NULL, NULL, 7, '2020-09-19 10:25:01', '2020-09-19 10:25:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_failed_jobs`
--

CREATE TABLE `tbl_failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_failed_jobs`
--

INSERT INTO `tbl_failed_jobs` (`id`, `connection`, `queue`, `payload`, `exception`, `failed_at`) VALUES
(1, 'database', 'default', '{\"uuid\":\"a2dd8adb-5983-44dc-ab33-f6e93b082a63\",\"displayName\":\"App\\\\Notifications\\\\TicketMailNotification\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":13:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";a:1:{i:0;i:22;}s:9:\\\"relations\\\";a:1:{i:0;s:10:\\\"department\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:40:\\\"App\\\\Notifications\\\\TicketMailNotification\\\":15:{s:12:\\\"\\u0000*\\u0000ticket_id\\\";i:147;s:18:\\\"\\u0000*\\u0000auth_user_email\\\";s:27:\\\"daniel.tarus@calltronix.com\\\";s:13:\\\"\\u0000*\\u0000is_initial\\\";i:0;s:21:\\\"\\u0000*\\u0000is_acknowledgement\\\";i:0;s:13:\\\"\\u0000*\\u0000reassigned\\\";i:0;s:15:\\\"\\u0000*\\u0000auth_user_id\\\";i:22;s:2:\\\"id\\\";s:36:\\\"228340e9-6014-452d-8771-b56fd3165008\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ErrorException: Trying to get property \'view\' of non-object in /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/Channels/MailChannel.php:92\nStack trace:\n#0 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/Channels/MailChannel.php(92): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, \'Trying to get p...\', \'/var/www/html/c...\', 92, Array)\n#1 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/Channels/MailChannel.php(63): Illuminate\\Notifications\\Channels\\MailChannel->buildView(true)\n#2 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/NotificationSender.php(146): Illuminate\\Notifications\\Channels\\MailChannel->send(Object(App\\User), Object(App\\Notifications\\TicketMailNotification))\n#3 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/NotificationSender.php(105): Illuminate\\Notifications\\NotificationSender->sendToNotifiable(Object(App\\User), \'1d38fd9f-f176-4...\', Object(App\\Notifications\\TicketMailNotification), \'mail\')\n#4 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Support/Traits/Localizable.php(19): Illuminate\\Notifications\\NotificationSender->Illuminate\\Notifications\\{closure}()\n#5 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/NotificationSender.php(107): Illuminate\\Notifications\\NotificationSender->withLocale(NULL, Object(Closure))\n#6 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/ChannelManager.php(54): Illuminate\\Notifications\\NotificationSender->sendNow(Object(Illuminate\\Database\\Eloquent\\Collection), Object(App\\Notifications\\TicketMailNotification), Array)\n#7 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Notifications/SendQueuedNotifications.php(94): Illuminate\\Notifications\\ChannelManager->sendNow(Object(Illuminate\\Database\\Eloquent\\Collection), Object(App\\Notifications\\TicketMailNotification), Array)\n#8 [internal function]: Illuminate\\Notifications\\SendQueuedNotifications->handle(Object(Illuminate\\Notifications\\ChannelManager))\n#9 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(33): call_user_func_array(Array, Array)\n#10 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#11 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(91): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#12 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(35): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#13 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/Container.php(592): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#14 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#15 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(Illuminate\\Notifications\\SendQueuedNotifications))\n#16 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Notifications\\SendQueuedNotifications))\n#17 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#18 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(Illuminate\\Notifications\\SendQueuedNotifications), false)\n#19 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(Illuminate\\Notifications\\SendQueuedNotifications))\n#20 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Notifications\\SendQueuedNotifications))\n#21 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#22 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Notifications\\SendQueuedNotifications))\n#23 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/Jobs/Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#24 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(356): Illuminate\\Queue\\Jobs\\Job->fire()\n#25 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(306): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#26 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(132): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#27 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(112): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#28 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#29 [internal function]: Illuminate\\Queue\\Console\\WorkCommand->handle()\n#30 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(33): call_user_func_array(Array, Array)\n#31 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/Util.php(36): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#32 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(91): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#33 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(35): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#34 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Container/Container.php(592): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#35 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Console/Command.php(134): Illuminate\\Container\\Container->call(Array)\n#36 /var/www/html/calltronixcrm/vendor/symfony/console/Command/Command.php(258): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#37 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#38 /var/www/html/calltronixcrm/vendor/symfony/console/Application.php(911): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#39 /var/www/html/calltronixcrm/vendor/symfony/console/Application.php(264): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#40 /var/www/html/calltronixcrm/vendor/symfony/console/Application.php(140): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#41 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Console/Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#42 /var/www/html/calltronixcrm/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#43 /var/www/html/calltronixcrm/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#44 {main}', '2020-09-07 09:50:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_files`
--

CREATE TABLE `tbl_files` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` double NOT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_id` int NOT NULL,
  `model_slug_id` int NOT NULL,
  `user_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_files`
--

INSERT INTO `tbl_files` (`id`, `name`, `size`, `path`, `mime_type`, `model_id`, `model_slug_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Invoice Ian 1723643277146581.pdf', 22588, '/tickets/2020/10/25/1/Invoice Ian 1723643277146581.pdf', 'application/pdf', 1, 1, 1, '2020-10-25 07:07:02', '2020-10-25 07:07:02'),
(2, 'Invoice Ian 1723554162225719.pdf', 22844, '/tickets/2020/10/25/1/Invoice Ian 1723554162225719.pdf', 'application/pdf', 1, 1, 1, '2020-10-25 07:07:03', '2020-10-25 07:07:03'),
(3, 'roll-out-of-1517-short-c.jpg', 48634, '/tickets/2020/10/25/1/roll-out-of-1517-short-c.jpg', 'image/jpeg', 1, 1, 1, '2020-10-25 07:07:03', '2020-10-25 07:07:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_issue_categories`
--

CREATE TABLE `tbl_issue_categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_issue_categories`
--

INSERT INTO `tbl_issue_categories` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Request', 'Request', 1, NULL, '2020-04-18 05:04:10'),
(2, 'Complaint', '', 1, NULL, '2020-04-18 05:04:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_issue_category_issue_sub_category`
--

CREATE TABLE `tbl_issue_category_issue_sub_category` (
  `id` bigint UNSIGNED NOT NULL,
  `issue_category_id` int NOT NULL,
  `issue_sub_category_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_issue_category_issue_sub_category`
--

INSERT INTO `tbl_issue_category_issue_sub_category` (`id`, `issue_category_id`, `issue_sub_category_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-09-27 05:31:41', '2020-09-27 05:31:41'),
(3, 1, 2, 1, '2020-09-27 05:32:05', '2020-09-27 05:32:05'),
(4, 2, 1, 1, '2020-10-17 18:19:15', '2020-10-17 18:19:15'),
(5, 2, 2, 1, '2020-10-17 18:19:16', '2020-10-17 18:19:16'),
(6, 2, 3, 1, '2020-10-17 18:19:16', '2020-10-17 18:19:16'),
(7, 2, 4, 1, '2020-10-17 18:19:16', '2020-10-17 18:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_issue_sources`
--

CREATE TABLE `tbl_issue_sources` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_issue_sources`
--

INSERT INTO `tbl_issue_sources` (`id`, `user_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Email', 1, '2020-10-17 16:58:31', '2020-10-17 16:58:31'),
(2, 1, 'Call', 1, '2020-10-17 16:58:37', '2020-10-17 16:58:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_issue_sub_categories`
--

CREATE TABLE `tbl_issue_sub_categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_issue_sub_categories`
--

INSERT INTO `tbl_issue_sub_categories` (`id`, `name`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Order', 1, 1, '2020-09-27 05:11:21', '2020-09-27 05:11:21'),
(2, 'Payments', 1, 1, '2020-09-27 05:11:29', '2020-09-27 05:11:29'),
(3, 'Sub Category one', 1, 1, '2020-09-27 05:11:45', '2020-09-27 05:11:45'),
(4, 'Sub Category two', 1, 1, '2020-09-27 05:11:59', '2020-09-27 05:11:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item_categories`
--

CREATE TABLE `tbl_item_categories` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_item_categories`
--

INSERT INTO `tbl_item_categories` (`id`, `name`, `description`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 'Logins/Access', 'Logins/Access', 1, '2020-05-09 19:23:18', '2020-05-09 19:23:18'),
(3, 'Work station', 'Work station', 1, '2020-05-09 19:31:29', '2020-05-09 19:31:29'),
(4, 'Stationeries', 'Stationeries', 1, '2020-05-09 19:42:43', '2020-05-09 19:42:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jobs`
--

CREATE TABLE `tbl_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `queue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint UNSIGNED NOT NULL,
  `reserved_at` int UNSIGNED DEFAULT NULL,
  `available_at` int UNSIGNED NOT NULL,
  `created_at` int UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leave_approval_levels`
--

CREATE TABLE `tbl_leave_approval_levels` (
  `id` bigint UNSIGNED NOT NULL,
  `department_id` bigint UNSIGNED NOT NULL,
  `approving_department_id` bigint UNSIGNED NOT NULL,
  `position` tinyint UNSIGNED DEFAULT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leave_requests`
--

CREATE TABLE `tbl_leave_requests` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_user_id` bigint UNSIGNED NOT NULL,
  `assuming_staff_user_id` bigint UNSIGNED DEFAULT NULL,
  `leave_type_id` bigint UNSIGNED NOT NULL,
  `next_action_department_id` bigint UNSIGNED DEFAULT NULL,
  `date_from` datetime DEFAULT NULL,
  `date_to` datetime DEFAULT NULL,
  `is_halfday` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `days_out` double NOT NULL DEFAULT '0',
  `date_returned` datetime DEFAULT NULL,
  `reason` longtext COLLATE utf8mb4_unicode_ci,
  `canceled_at` datetime DEFAULT NULL,
  `cancel_reason` longtext COLLATE utf8mb4_unicode_ci,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `request_state` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leave_request_actions`
--

CREATE TABLE `tbl_leave_request_actions` (
  `id` bigint UNSIGNED NOT NULL,
  `leave_request_id` bigint UNSIGNED NOT NULL,
  `action_by_user_id` bigint UNSIGNED NOT NULL,
  `action` tinyint UNSIGNED NOT NULL,
  `action_at` tinyint UNSIGNED NOT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leave_types`
--

CREATE TABLE `tbl_leave_types` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `entitlement` double UNSIGNED NOT NULL DEFAULT '0',
  `is_paid` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_leave_types`
--

INSERT INTO `tbl_leave_types` (`id`, `name`, `description`, `entitlement`, `is_paid`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Paternity', 'Paternity', 14, 1, 27, '2020-05-04 07:19:30', '2020-05-14 09:15:48'),
(2, 'Annual Leave', 'Annual Leave', 21, 1, 1, '2020-05-09 18:59:32', '2020-05-09 18:59:32'),
(3, 'Sick Leave', 'Sick Leave', 14, 1, 1, '2020-05-09 19:01:18', '2020-05-09 19:01:18'),
(4, 'Compassionate Leave', 'Compassionate Leave', 5, 1, 1, '2020-05-09 19:01:56', '2020-05-09 19:01:56'),
(5, 'Marternity Leave', 'Maternity Leave', 90, 1, 1, '2020-05-09 19:02:33', '2020-05-09 19:02:33'),
(6, 'Birthday Leave', 'Birthday Leave', 1, 1, 1, '2020-05-09 19:03:18', '2020-05-09 19:03:18'),
(7, 'Extra Day Off', 'Extra Day Off', 1, 1, 1, '2020-05-09 19:03:43', '2020-05-09 19:03:43'),
(8, 'Unpaid Leave', 'Unpaid Leave', 1, 0, 1, '2020-05-09 19:04:09', '2020-05-09 19:04:09'),
(9, 'Emergency Leave / 0.5 days', 'Emergency Leave / 0.5 days', 1, 1, 22, '2020-07-24 03:40:07', '2020-07-24 03:40:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_line_of_businesses`
--

CREATE TABLE `tbl_line_of_businesses` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `name` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_line_of_businesses`
--

INSERT INTO `tbl_line_of_businesses` (`id`, `user_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 22, 'Crown Paints PLC', 1, NULL, '2020-07-06 17:05:42'),
(2, 22, 'Twiga Foods', 1, NULL, '2020-07-06 17:03:21'),
(3, 22, 'Carrefour Kenya', 1, NULL, '2020-05-18 11:55:06'),
(4, 1, 'Aquamist', 1, NULL, NULL),
(5, 1, 'Calltronix', 1, NULL, NULL),
(6, 22, 'West Kenya Sugar', 1, NULL, '2020-05-18 11:54:29'),
(7, 1, 'FIT Africa', 1, NULL, '2020-04-16 04:25:25'),
(8, 1, 'Truckmart Africa', 1, NULL, NULL),
(9, 22, 'Koko Network', 1, '2020-04-16 04:25:07', '2020-05-18 11:55:40'),
(10, 22, 'iSoko', 1, '2020-05-18 11:56:24', '2020-05-18 12:27:43'),
(11, 22, 'Artcaffe', 1, '2020-05-19 11:38:51', '2020-05-19 11:38:51'),
(12, 22, 'SWVL', 1, '2020-05-22 07:58:43', '2020-05-22 07:58:43'),
(13, 1, 'Giift', 1, '2020-05-28 17:14:56', '2020-05-28 17:14:56'),
(14, 22, 'Crown Paints', 1, '2020-06-11 18:19:15', '2020-06-11 18:19:15'),
(15, 22, 'TruckMart', 1, '2020-06-11 18:19:15', '2020-06-11 18:19:15'),
(16, 22, 'Aquamist Ltd', 1, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(17, 22, 'Carrefour', 1, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(18, 22, 'Marketways ', 1, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(19, 22, 'Valley View Office', 1, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(20, 22, 'Head Office', 1, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(21, 22, 'Zamara', 1, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(22, 22, 'KIM-FAY', 1, '2020-06-11 18:19:17', '2020-06-11 18:19:17'),
(23, 22, 'Zamara/Elite Digital', 1, '2020-06-11 18:19:17', '2020-06-11 18:19:17'),
(24, 22, 'Orbit Products Africa Ltd', 1, '2020-07-13 12:08:58', '2020-07-13 12:08:58'),
(25, 22, 'Software Engineering', 1, '2020-07-14 08:19:49', '2020-08-18 09:00:56'),
(26, 22, 'UNHCR', 1, '2020-08-18 08:59:37', '2020-08-18 08:59:37'),
(27, 22, 'Proto Energy', 1, '2020-09-01 10:06:36', '2020-09-01 10:06:36'),
(28, 22, 'Text Book Centre.', 1, '2020-09-01 10:07:02', '2020-09-01 10:07:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_members`
--

CREATE TABLE `tbl_members` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `id_number` int DEFAULT NULL,
  `occupation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int DEFAULT NULL,
  `employer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `county_id` int DEFAULT NULL,
  `gender` tinyint DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_copy` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payslip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_migrations`
--

CREATE TABLE `tbl_migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_migrations`
--

INSERT INTO `tbl_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_10_151403_entrust_setup_tables', 1),
(5, '2020_03_11_105718_create_departments_table', 2),
(7, '2020_03_11_111211_create_dispositions_table', 2),
(9, '2020_03_11_102840_create_line_of_businesses_table', 3),
(10, '2020_03_11_105916_create_issue_categories_table', 3),
(13, '2020_03_11_113707_create_tickets_table', 5),
(14, '2020_03_13_145533_create_ticket_updates_table', 6),
(15, '2019_04_14_162819_create_departments_table', 7),
(16, '2020_03_11_105718_create_calltronix_departments_table', 8),
(17, '2020_04_13_163900_add_department_id_to_users_table', 9),
(18, '2020_04_14_133851_add_fields_to_users_table', 10),
(19, '2020_04_14_163757_create_test_models_table', 11),
(21, '2020_04_16_074424_create_positions_table', 12),
(22, '2020_04_16_083512_create_staff_roles_table', 13),
(24, '2020_04_16_162345_create_staff_next_of_kin_table', 14),
(25, '2020_04_16_164131_create_staff_spouse_or_childrens_table', 14),
(26, '2020_04_16_165506_create_staff_beneficiaries_table', 14),
(29, '2020_04_14_173022_create_staff_table', 15),
(30, '2020_02_11_175410_create_disposition_issue_categories_table', 16),
(31, '2020_04_19_094303_create_calltronix_department_heads_table', 17),
(33, '2020_04_20_152129_create_files_table', 18),
(34, '2020_04_24_082527_add_acknowledged_to_tickets_table', 19),
(35, '2020_04_29_124551_create_onboarding_checklist_items_table', 20),
(36, '2020_04_29_130849_create_employee_onboarding_checklists_table', 20),
(37, '2020_04_29_143821_update_users_table_add_avatar_column', 20),
(38, '2020_04_29_212658_update_staffs_table_add_and_rename_columns', 20),
(39, '2020_05_02_190901_create_leave_types_table', 21),
(40, '2020_05_03_022941_create_leave_requests_table', 21),
(41, '2020_05_07_204618_update_leaverequests_table_rename_column_assumed_by', 22),
(42, '2020_05_07_212405_add_column_canceled_at_to_leaverequests', 22),
(43, '2020_05_08_191746_create_item_categories_table', 22),
(44, '2020_05_08_191906_add_columns_to_onboarding_checklist_items', 22),
(45, '2020_05_11_153935_create_staff_appraisals_table', 23),
(46, '2020_05_11_154511_create_appraisal_question_categories_table', 23),
(47, '2020_05_11_154848_create_appraisal_questions_table', 23),
(48, '2020_05_13_101930_create_vacancies_table', 24),
(49, '2020_05_14_125345_update_leave_requests_add_columns', 25),
(50, '2020_05_15_090017_create_ticket_files_table', 26),
(51, '2020_05_15_140848_create_jobs_table', 27),
(52, '2020_05_17_083519_update_calltronix_departments_add_department_code', 27),
(53, '2020_05_17_093344_update_calltronix_departments_add_department_staff_count', 27),
(54, '2020_05_17_094041_update_staffs_table_add_employee_number_details', 27),
(55, '2020_05_17_103452_update_users_add_column_is_staff', 27),
(56, '2020_05_18_081849_update_departments_add_column_is_hr', 27),
(57, '2020_05_19_104955_update_table_leave_requests_add_column_returning_date', 28),
(58, '2020_05_19_111433_create_yearly_staff_leaves_trackers_table', 28),
(59, '2020_05_20_061552_update_users_table_add_employee_number_column', 28),
(60, '2020_05_20_143604_create_failed_jobs_table', 29),
(61, '2020_05_20_161600_update_staff_appraisals_add_column_statuses_columns', 29),
(62, '2020_05_21_075010_create_appraisal_question_responses_table', 29),
(63, '2020_05_28_102902_create_offduty_requests_table', 30),
(64, '2020_05_28_142751_update_tickets_table_add_columns', 30),
(65, '2020_05_28_163843_create_compensation_requests_table', 30),
(66, '2020_05_28_165927_create_offduty_request_dates_table', 30),
(67, '2020_05_31_124426_update_request_dates_table_add_columns', 30),
(68, '2020_05_31_220309_update_offduty_tables_add_comments_table', 30),
(69, '2020_06_01_152235_update_users_table_make_middlename_nullable', 31),
(70, '2020_06_01_154337_update_users_add_alternate_email_field', 31),
(71, '2020_06_01_214930_update_compensation_requests_add_comments_fields', 31),
(72, '2020_06_02_074759_update_yearly_staff_leaves_trackers_add_compensation_days', 31),
(73, '2020_06_08_195430_update_users_table_add_import_check_fields', 32),
(74, '2020_06_08_200016_update_staffs_table_add_default_field_values_and_make_nullable', 32),
(75, '2020_06_10_005428_update_table_users_add_is_available_column', 32),
(76, '2020_06_11_090258_update_tickets_add_closed_and_resolved_at_time_fields', 33),
(77, '2020_06_18_123527_update_calltronix_departments_add_parent_department_id_column', 34),
(78, '2020_07_02_052803_update_tickets_add_from_and_to_departments_ids', 34),
(79, '2020_07_02_071417_update_tickets_add_lines_of_businesses_from_and_to_ids_fields', 34),
(80, '2020_07_02_094329_create_account_managers_table', 34),
(81, '2020_07_02_142156_add_cexpected_start_date_to_tickets', 34),
(82, '2020_07_17_115327_update_users_table_remove_unique_constraint_on_phone', 35),
(83, '2020_07_18_212351_update_leave_requests_add_fields', 36),
(84, '2020_07_23_111102_update_tables_fields_change_from_integer_to_double', 36),
(85, '2020_07_28_082828_update_users_table_add_column_is_operation_manager', 36),
(86, '2020_07_29_203621_create_staff_exits_table', 36),
(87, '2020_08_02_210823_update_staff_tabe_add_annual_leave_days_last_synced_date', 36),
(88, '2020_08_21_154412_update_yearly_staff_leaves_trackers_table', 37),
(89, '2020_08_24_115706_add_days_out_to_leaverequests_table', 37),
(90, '2020_09_26_135714_create_permission_groups_table', 38),
(91, '2020_09_27_002903_drop_departments_table', 39),
(92, '2020_09_27_003426_re_create_departments_table', 40),
(93, '2020_09_27_004721_create_department_heads_table', 41),
(94, '2020_09_27_005824_update_staff_roles_add_department_id', 42),
(95, '2020_09_27_013712_update_permission_group_add_is_hr', 43),
(96, '2020_09_27_014001_rename_users_department_id_to_permission_group_id', 44),
(97, '2020_09_27_020636_update_departments_add_staff_count', 45),
(98, '2020_09_26_115412_create_issue_sources_table', 46),
(99, '2020_09_26_170628_create_issue_sub_categories_table', 46),
(100, '2020_09_26_191613_issue_category_issue_sub_category_table', 46),
(101, '2020_09_26_200907_create_branches_table', 46),
(102, '2020_09_26_211638_add_slug_to_ticket_statuses_table', 46),
(103, '2020_09_27_175410_create_disposition_issue_categories_table', 47),
(108, '2020_09_27_133114_rename_calltronix_department_id_to_department_id', 48),
(109, '2020_09_27_134444_rename_staff_calltronix_department_id_to_department_id', 49),
(110, '2020_09_27_135624_update_ticket_columns_remove_calltronix', 49),
(111, '2020_10_04_125150_create_contract_types_table', 49),
(112, '2020_10_17_215642_add_columns_to_tickets_table', 50),
(113, '2020_10_18_082837_create_files_table', 51),
(114, '2020_10_03_235446_create_leave_approval_levels_table', 52),
(115, '2020_10_04_000030_create_leave_requests_table', 52),
(116, '2020_10_04_000031_create_leave_request_actions_table', 52),
(117, '2020_10_27_170520_create_members_table', 52),
(118, '2020_10_27_171212_create_counties_table', 53);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offduty_requests`
--

CREATE TABLE `tbl_offduty_requests` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_id` bigint UNSIGNED NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `days` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `approved_at` datetime DEFAULT NULL,
  `rejected_at` datetime DEFAULT NULL,
  `action_by` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hod_approve_comment` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `hod_reject_reason` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_offduty_requests`
--

INSERT INTO `tbl_offduty_requests` (`id`, `staff_id`, `description`, `status`, `days`, `approved_at`, `rejected_at`, `action_by`, `created_at`, `updated_at`, `hod_approve_comment`, `hod_reject_reason`) VALUES
(6, 28, 'Good Morning,\r\nI am kindly requesting for a day off on Friday to go to the hospital for my check up', 1, 1, '2020-09-02 21:35:26', NULL, 12, '2020-09-02 04:38:50', '2020-09-02 18:35:26', 'Dear Ann, please approve so that she can go for medical check-up', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_offduty_request_dates`
--

CREATE TABLE `tbl_offduty_request_dates` (
  `id` bigint UNSIGNED NOT NULL,
  `offduty_request_id` bigint UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `compensation_request_id` bigint UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_offduty_request_dates`
--

INSERT INTO `tbl_offduty_request_dates` (`id`, `offduty_request_id`, `date`, `created_at`, `updated_at`, `status`, `compensation_request_id`) VALUES
(8, 6, '2020-09-04', '2020-09-02 04:38:50', '2020-09-02 18:35:26', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_onboarding_checklist_items`
--

CREATE TABLE `tbl_onboarding_checklist_items` (
  `id` bigint UNSIGNED NOT NULL,
  `item_category_id` bigint UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_returnable` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_notifiable` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `disposition_id` bigint UNSIGNED DEFAULT NULL,
  `calltronix_department_id` bigint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_onboarding_checklist_items`
--

INSERT INTO `tbl_onboarding_checklist_items` (`id`, `item_category_id`, `name`, `description`, `is_returnable`, `user_id`, `created_at`, `updated_at`, `is_notifiable`, `disposition_id`, `calltronix_department_id`) VALUES
(1, 1, 'Bank Account Details', 'Bank Account Details', 0, 1, '2020-05-04 06:36:19', '2020-05-09 19:17:35', 0, NULL, NULL),
(2, 1, 'Medical card', 'Medical card', 0, 1, '2020-05-09 19:18:02', '2020-05-09 19:18:02', 0, NULL, NULL),
(3, 1, 'N.H.I.F', 'N.H.I.F', 0, 1, '2020-05-09 19:18:39', '2020-05-09 19:18:39', 0, NULL, NULL),
(4, 1, 'N.S.S.F', 'N.H.I.F', 0, 1, '2020-05-09 19:19:11', '2020-05-09 19:19:11', 0, NULL, NULL),
(5, 1, 'School certificates', 'School certificates', 0, 1, '2020-05-09 19:20:02', '2020-05-09 19:20:02', 0, NULL, NULL),
(6, 1, 'Passport Photos', 'Passport Photos', 0, 1, '2020-05-09 19:20:35', '2020-05-09 19:20:35', 0, NULL, NULL),
(7, 1, 'National I.D', 'National I.D', 0, 1, '2020-05-09 19:21:03', '2020-05-09 19:21:03', 0, NULL, NULL),
(8, 1, 'CV/Resume', 'CV/Resume', 0, 1, '2020-05-09 19:21:19', '2020-05-09 19:21:19', 0, NULL, NULL),
(9, 1, 'Recommendation Letter(s)', 'Recommendation Letter(s)', 0, 1, '2020-05-09 19:22:13', '2020-05-09 19:22:13', 0, NULL, NULL),
(10, 2, 'Biometric', 'Biometric', 0, 1, '2020-05-09 19:24:16', '2020-05-09 19:24:16', 1, 6, 1),
(11, 2, 'Email Account', 'Email Account', 0, 1, '2020-05-09 19:26:45', '2020-05-09 19:26:45', 1, 45, 1),
(12, 2, 'QueueMetrics login', 'QueueMetrics login', 0, 1, '2020-05-09 19:28:48', '2020-05-09 19:28:48', 1, 18, 2),
(13, 2, 'C.R.M logins', 'C.R.M logins', 0, 1, '2020-05-09 19:30:00', '2020-05-09 19:31:08', 1, 4, 1),
(14, 2, 'Domain logins', 'Domain logins', 0, 1, '2020-05-09 19:30:28', '2020-05-09 19:30:28', 1, 5, 1),
(15, 3, 'Laptop', 'Laptop', 1, 1, '2020-05-09 19:36:59', '2020-05-09 19:36:59', 1, 47, 1),
(16, 3, 'P.C (Desktop)', 'P.C (Desktop) i.e Monitor, keyboard, system unit and mouse', 1, 1, '2020-05-09 19:40:42', '2020-05-09 19:40:42', 1, 50, 1),
(17, 3, 'Phone', 'Phone', 1, 1, '2020-05-09 19:41:18', '2020-05-09 19:41:18', 1, 49, 1),
(18, 3, 'Headset', 'Headset', 1, 1, '2020-05-09 19:41:54', '2020-05-09 19:41:54', 1, 48, 1),
(19, 4, 'Notebook and Pen', 'Notebook and Pen', 0, 1, '2020-05-09 19:43:50', '2020-05-09 19:43:50', 0, NULL, NULL),
(20, 4, 'Badge card', 'Badge card', 1, 1, '2020-05-09 19:44:33', '2020-05-09 19:44:33', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_password_resets`
--

CREATE TABLE `tbl_password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_password_resets`
--

INSERT INTO `tbl_password_resets` (`email`, `token`, `created_at`) VALUES
('ann.kimani@calltronix.co.ke', '$2y$10$pWzbOj5EUHwx9dJ/rPAQLOdp3ZlNpYF/qZDc.Tyt2z8BCSEs9ir.G', '2020-04-19 10:26:34'),
('richard.ndolo@calltronix.co.ke', '$2y$10$ZkkQFn5BVpX137Mu4ZlDhOhlWDulPqvSot4u3fW72zp8NtSgtwax2', '2020-04-19 10:41:51'),
('joseph.zablon@calltronix.co.ke', '$2y$10$55iBQCTy6kiXzInD1dQ4fe3YrrxLCNxaZYEeDlSS.zM5791LjRmAe', '2020-04-19 10:44:15'),
('iandancun@gmail.com', '$2y$10$byxpdMlBN0yBU/J6NyjTA.pGYrYlr0z9reeXbx2ObyNMEKToMSR7m', '2020-05-14 10:16:37'),
('Calltronixtraining@gmail.com', '$2y$10$oCXvROPhl6bRTdmz3o7KPefv.8YAzcYu6boniqwQPE7ZSVisW8yfO', '2020-05-15 06:31:12'),
('barbra.malesi@calltronix.co.ke', '$2y$10$qErcBt2frTOrGXqkKMjZp.AsTGzWp6ykdIsQXvUgs2zk5KXUlhRKW', '2020-05-23 09:21:24'),
('tarusdaniel89@gmail.com', '$2y$10$qbD79M3RQQd4nAW9bBDe3eULKkYBrCtGmFKZ2h8jtMyRXlYWmqKlC', '2020-05-23 09:40:05'),
('jasmine.musyimi@calltronix.co.ke', '$2y$10$x6aUhZlJvyWhKX5TjDrrjuKUgejmWtb6sdH5WKuOLJJBuMKQOB1gC', '2020-06-12 11:27:47'),
('victor.masika@calltronix.co.ke', '$2y$10$P0FYECPQL4y4afjomD/Ace4MA2L55oiPblD/gdtiLNt6b/ce3ToGS', '2020-06-15 06:31:21'),
('phyllis.langat@calltronix.co.ke', '$2y$10$Ez3HaUvJonmHWYPjnyFWauB5/ASowpDEdXYDfrrnVdOcR6x8qhK9u', '2020-06-23 12:56:06'),
('phyllys.langat@calltronix.co.ke', '$2y$10$5TG7PCKK7pior0LoRf51S.RSEHmmq1EApww7GZXXFmQ0O4nFm0D7.', '2020-06-23 13:01:28'),
('faith.karimi@calltronix.co.ke', '$2y$10$kkYy655V0uIx3qPOOPpyCuoM8Ii4Uv9UbbCOxxcetOSeEF4ga.fdW', '2020-06-24 07:24:11'),
('mercy.kyalo@calltronix.co.ke', '$2y$10$MOxNL.cL4ZG0WkSidCfBuugt0AlRf8aYV76Pl09ityHhkrUFiM2j2', '2020-06-24 07:24:17'),
('eliston.mwangi@calltronix.co.ke', '$2y$10$puoQxM.UwmeJKFWra4Fz1uJU0MNbqfHMg9OF/vuuwsm8/05a7jmcu', '2020-06-24 07:24:35'),
('sharon.ingari@calltronix.co.ke', '$2y$10$SMoK3g57ieJLdIe2wiOMB.ynYeBd7/Hw4xZ6Rf7NJO4eV15vVAKLO', '2020-06-25 06:52:42'),
('victor.vuluku@calltronix.co.ke', '$2y$10$UQs8FbAu11rtvZPKao4eZueAubaGhxXEoovogciGKA8oINkUhTEQG', '2020-06-30 10:27:30'),
('faith.richard@calltronix.co.ke', '$2y$10$i4oPz.36avtJF8.HWIqRjO7L9tLd8JMMINI1y8jMWPqAk.5eHFyjy', '2020-06-30 11:15:23'),
('faith.mungania@calltronix.co.ke', '$2y$10$aCO0XhNURhyaWAvjBbY72u/EKo/HRHLSv1sFuk75A0hQYlJk3VNXS', '2020-07-01 07:02:28'),
('priscilla.gatonye@calltronix.co.ke', '$2y$10$j1j4o3lwMUOGgn8MFml/XefRht19avvwDAjh4pJD.QvrnDr50aSdC', '2020-07-03 07:04:34'),
('daniel.tarus@calltronix.co.ke', '$2y$10$nMK7/UBEHx0E/YyKH9ts7ee.WP4yKJ1jNw31nL90oy/IrIgk2brDG', '2020-07-03 07:44:47'),
('elizabeth.mulwa@calltronix.com', '$2y$10$SjE.CqzP7guTS63ch5Ux5.gSOeL6SqFGAjUhBGpf93vyGZ.SQzqgW', '2020-07-18 09:39:31'),
('evans.oloo@calltronix.com', '$2y$10$fmZ/3O2XyXDWq2Q2nPl6L.Fg8kMVpgbpAkKeDMU9pmJKha./x/V32', '2020-08-06 07:58:23'),
('roselinda.awinda@calltronix.com', '$2y$10$kt3UBtqlX/sLukNfE8zSqOPgWzpx6nmLZLiKvePD/6hTDitDEH8Ui', '2020-08-06 13:33:56'),
('arnold.otieno@calltronix.com', '$2y$10$k6dOPz04OTamoyVCKn46sOjl.8lwDDolyQuJsDlsJEL7MeJ54IK1y', '2020-08-17 06:04:55'),
('tahshin.valli@calltronix.com', '$2y$10$L0Qts1.JLZyRNTWp2ko5ZeJhACgs2VPR984oJb6ZpzBAoH.RBw.R.', '2020-08-21 11:05:53'),
('allankirui57@gmail.com', '$2y$10$Oc74Faw8wCwTVwf03C.fKumc2ISVZie1Rc/cabq5OlRhyUmL2EbUm', '2020-09-01 14:54:38'),
('mateen@calltronix.com', '$2y$10$1cnDf9.Jftw8eUoJ4sb2NOC7jtzwJPQ8IA6qFit5qMpIdOfOuRBty', '2020-09-07 08:38:26'),
('sheryl.waga@calltronix.com', '$2y$10$IpQr3jyv3vWvebnWO5EBcuKKRHZ4f64sv3EdgdoYEAiasbxpfgxxO', '2020-09-14 06:58:09'),
('allan.kirui@calltronix.com', '$2y$10$DGmbNJhwUvlMrRQ25DLceuIvduQCM405KYf03FPv6KKEI//pu3zZu', '2020-09-14 07:01:39'),
('daniel.tarus@calltronix.com', '$2y$10$p7vuEKkBgK89YRjP8jx.M.ZEsY0.bs0dQ935FRddFgs2dIscQ1Lty', '2020-09-14 08:38:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permission_groups`
--

CREATE TABLE `tbl_permission_groups` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_id` bigint UNSIGNED NOT NULL,
  `is_default` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_hr` tinyint UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_permission_groups`
--

INSERT INTO `tbl_permission_groups` (`id`, `name`, `slug`, `description`, `permissions`, `user_id`, `is_default`, `created_at`, `updated_at`, `is_hr`) VALUES
(1, 'Default User', 'default-user', 'Dashboard and Ticketing permissions', '[\"view_admin_dashboard\",\"tickets_creation_and_all_tickets\"]', 1, 0, NULL, '2020-05-13 10:01:20', 0),
(2, 'H.R.', 'hr', 'H.R Department', '[\"view_admin_dashboard\",\"tickets_creation_and_all_tickets\",\"manage_staff\",\"manage_a_staff\",\"update_staff_profile\",\"request_leave\",\"HR_manage_compensations_requests\",\"staff_other_requests\",\"HR_leave_requests\",\"manage_HR_checklist\",\"view_staff_exits\",\"discharge_staff\"]', 1, 0, '2020-05-09 18:13:33', '2020-09-01 14:20:08', 1),
(3, 'H.O.Ds', 'hods', 'H.O.Ds', '[\"view_admin_dashboard\",\"tickets_creation_and_all_tickets\",\"manage_staff\",\"manage_a_staff\",\"request_leave\",\"HOD_manage_offduty_requests\",\"staff_other_requests\",\"HOD_leave_requests\"]', 1, 0, '2020-05-09 18:26:17', '2020-09-02 18:13:53', 0),
(4, 'I.T. Heads', 'it-heads', 'I.T Heads', '[\"view_admin_dashboard\",\"view_admin_dashboard_tickets_reports\",\"tickets_creation_and_all_tickets\",\"view_all_system_tickets\",\"manage_staff\",\"view_users\",\"manage_a_staff\",\"request_leave\",\"HOD_manage_offduty_requests\",\"staff_other_requests\",\"HOD_leave_requests\",\"manage_user_permissions\",\"system_settings\"]', 1, 0, '2020-05-09 18:31:54', '2020-08-17 09:17:32', 0),
(5, 'I.T. Developing Team', 'it-developing-team', 'Developers managing the systsem', '[\"view_admin_dashboard\",\"tickets_creation_and_all_tickets\",\"manage_staff\",\"view_users\",\"manage_a_staff\",\"update_staff_profile\",\"request_leave\",\"staff_other_requests\",\"manage_user_permissions\",\"system_settings\"]', 22, 0, '2020-05-13 09:56:35', '2020-09-08 09:17:11', 0),
(6, 'I.T Staff', 'it-staff', 'I.T Staff', '[\"view_admin_dashboard\",\"tickets_creation_and_all_tickets\",\"manage_staff\",\"view_users\",\"manage_a_staff\",\"request_leave\"]', 22, 0, '2020-05-13 10:18:43', '2020-05-24 17:09:12', 0),
(7, 'Default Staff', 'default-staff', 'Default Staff', '[\"view_admin_dashboard\",\"tickets_creation_and_all_tickets\",\"request_leave\",\"staff_other_requests\"]', 22, 0, '2020-06-11 10:25:58', '2020-06-11 10:27:04', 0),
(8, 'Account Managers', 'account-managers', 'Account Managers', '[\"view_admin_dashboard\",\"tickets_creation_and_all_tickets\",\"request_leave\",\"staff_other_requests\",\"AM_CSE_leave_requests\"]', 22, 0, '2020-08-04 09:56:27', '2020-08-04 09:57:08', 0),
(9, 'Targetter', 'targetter', 'Targetter', '[\"manage_a_staff\",\"update_staff_profile\"]', 22, 0, '2020-09-26 11:11:34', '2020-09-26 11:24:41', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_positions`
--

CREATE TABLE `tbl_positions` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_positions`
--

INSERT INTO `tbl_positions` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Software Engineer', 'Software Engineer', 0, '2020-04-16 05:17:16', '2020-07-14 08:25:49'),
(2, 'Junior Software Engineer', 'Junior Software Engineer', 0, '2020-04-16 05:17:44', '2020-07-14 08:26:02'),
(3, 'Internship', 'Internship', 0, '2020-04-16 05:17:57', '2020-04-16 05:17:57'),
(4, 'Operations Manager', 'Operations Manager', 0, '2020-04-27 08:56:16', '2020-04-27 08:56:16'),
(5, 'Human Resource', 'Human Resource', 0, '2020-05-09 18:40:30', '2020-05-09 18:40:30'),
(6, 'Training & Development', 'Training & Development', 0, '2020-05-09 19:00:50', '2020-05-09 19:00:50'),
(7, 'Account Manager', 'Account Manager', 0, '2020-05-09 19:01:01', '2020-05-09 19:01:01'),
(8, 'Team Captain', 'Team Captain', 0, '2020-05-09 19:01:10', '2020-05-09 19:01:10'),
(9, 'Quality Assurance Manager', 'Quality Assurance Manager', 0, '2020-05-09 19:01:20', '2020-05-09 19:01:20'),
(10, 'Quality Analysts', 'Quality Analysts', 0, '2020-05-09 19:01:32', '2020-05-09 19:01:32'),
(11, 'Customer Service Executive', 'Customer Service Executive', 0, '2020-05-09 19:01:47', '2020-05-09 19:01:47'),
(12, 'Customer Service Interns', 'Customer Service Interns', 0, '2020-05-09 19:01:58', '2020-05-09 19:01:58'),
(13, 'Office Assistant (House-Keeping)', 'Office Assistant (House-Keeping)', 0, '2020-05-09 19:02:08', '2020-05-09 19:02:08'),
(14, 'IT Support', 'IT Support', 0, '2020-05-09 19:02:16', '2020-05-09 19:02:16'),
(15, 'Engagement Coordinator', 'Engagement Coordinator', 0, '2020-05-09 19:02:28', '2020-05-09 19:02:52'),
(16, 'Head Graphics Designer', 'Head Graphics Designer', 0, '2020-05-09 19:03:01', '2020-05-09 19:03:01'),
(17, 'Junior graphics Designer', 'Junior graphics Designer', 0, '2020-05-09 19:03:14', '2020-05-09 19:03:14'),
(18, 'Sales and Marketing', 'Sales and Marketing', 0, '2020-05-09 19:03:25', '2020-05-09 19:03:25'),
(19, 'Quality Analyst', 'Quality Analyst', 0, '2020-06-11 18:19:15', '2020-06-11 18:19:15'),
(20, 'IT support Assistant', 'IT support Assistant', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(21, 'Office Assistant', 'Office Assistant', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(22, 'Head of Quality Assurance', 'Head of Quality Assurance', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(23, 'Head of Operations', 'Head of Operations', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(24, 'Digital Marketing Manager', 'Digital Marketing Manager', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(25, 'IT Manager (Telephony)', 'IT Manager (Telephony)', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(26, 'Head of IT (Development)', 'Head of IT (Development)', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(27, 'Financial Controller', 'Financial Controller', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(28, 'Projects Manager', 'Projects Manager', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(29, 'Managing Director', 'Managing Director', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(30, 'Graphics Design Head', 'Graphics Design Head', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(31, 'Sales and Marketing Executive', 'Sales and Marketing Executive', 0, '2020-06-11 18:19:16', '2020-06-11 18:19:16'),
(32, 'Graphic Designer', 'Graphic Designer', 0, '2020-06-11 18:19:17', '2020-06-11 18:19:17'),
(33, 'Human Resource Manager', 'Human Resource Manager', 0, '2020-06-11 18:19:18', '2020-06-11 18:19:18'),
(34, 'R & D Technologist', 'R & D Technologist', 0, '2020-06-23 14:58:52', '2020-06-23 14:58:52'),
(35, 'Senior Software Engineer', 'Senior Software Engineer', 0, '2020-07-14 08:26:15', '2020-07-14 08:26:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staffs`
--

CREATE TABLE `tbl_staffs` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `created_by` int NOT NULL,
  `line_of_business_id` int NOT NULL DEFAULT '0',
  `department_id` int NOT NULL,
  `staff_role_id` int NOT NULL DEFAULT '0',
  `gender` tinyint DEFAULT NULL,
  `marital_status` tinyint DEFAULT NULL,
  `position_id` tinyint DEFAULT NULL,
  `id_number` bigint UNSIGNED DEFAULT NULL,
  `alternate_phone` bigint DEFAULT NULL,
  `postal_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residential_physical_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employment_date` date NOT NULL,
  `employment_status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `dismissal_date` date DEFAULT NULL,
  `bank_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_branch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_number` bigint DEFAULT NULL,
  `nssf_number` bigint DEFAULT NULL,
  `nhif_number` bigint DEFAULT NULL,
  `kra_pin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_roll_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medical_history` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dep_unique_no` bigint UNSIGNED NOT NULL DEFAULT '0',
  `employee_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_leave_days_last_sync_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_staffs`
--

INSERT INTO `tbl_staffs` (`id`, `user_id`, `created_by`, `line_of_business_id`, `department_id`, `staff_role_id`, `gender`, `marital_status`, `position_id`, `id_number`, `alternate_phone`, `postal_address`, `residential_physical_address`, `employment_date`, `employment_status`, `dismissal_date`, `bank_name`, `bank_branch`, `account_number`, `nssf_number`, `nhif_number`, `kra_pin`, `pay_roll_number`, `medical_history`, `created_at`, `updated_at`, `dep_unique_no`, `employee_number`, `annual_leave_days_last_sync_date`) VALUES
(1, 1, 7, 20, 1, 10, 1, NULL, 1, 32239754, NULL, NULL, NULL, '2019-06-03', 0, NULL, NULL, NULL, NULL, 4545, 45455, '45454', '4545', NULL, '2020-04-16 17:14:29', '2020-09-08 09:33:27', 2, 'CSD-002M/20', '2020-09-08'),
(5, 22, 7, 20, 1, 3, 1, NULL, 1, 32643999, NULL, NULL, NULL, '2020-04-27', 0, NULL, NULL, NULL, NULL, 2030618325, 14576507, 'A009293038R', NULL, NULL, '2020-05-10 12:39:41', '2020-09-08 09:33:27', 3, 'CSD-003M/20', '2020-09-08'),
(8, 27, 7, 20, 1, 2, 1, NULL, 26, 31108652, NULL, NULL, NULL, '2019-04-03', 0, NULL, NULL, NULL, NULL, 111154151, 12355058, 'A009hhnjdsde', NULL, NULL, '2020-05-14 05:29:16', '2020-09-08 09:33:27', 1, 'CSD-001M/19', '2020-09-08'),
(10, 14, 7, 19, 6, 12, 1, NULL, 6, 31882668, NULL, NULL, NULL, '2020-03-18', 0, NULL, NULL, NULL, NULL, 285858, 1252444, 'Ai0998gh78', '5484548', NULL, '2020-05-17 17:34:08', '2020-09-08 09:33:27', 1, 'CTD-001M/18', '2020-09-08'),
(11, 30, 7, 20, 1, 10, 2, 2, 2, 31880522, NULL, NULL, NULL, '2020-05-04', 0, NULL, NULL, NULL, NULL, 285858, 1252444, 'Ai0998gh780', '5484548', NULL, '2020-05-18 07:15:27', '2020-09-08 09:33:27', 6, 'CSD-006M/20', '2020-09-08'),
(14, 10, 7, 19, 2, 4, 1, NULL, 25, 22669584, NULL, NULL, NULL, '2017-04-01', 0, NULL, NULL, NULL, NULL, 98765, 3456789, 'wert3drerty3', NULL, NULL, '2020-05-27 14:37:27', '2020-09-08 09:33:27', 2, 'CTL-002M/16', '2020-09-08'),
(17, 60, 7, 5, 1, 2, 1, 1, 4, 23654775, 703133853, NULL, 'Nairobi', '2020-05-28', 0, NULL, 'KCB', 'Westlands', 1332452, 2345678, 2345678, 'AQI567897', NULL, NULL, '2020-05-28 10:06:34', '2020-09-08 09:33:27', 7, 'CSD-007M/20', '2020-09-08'),
(18, 19, 7, 20, 1, 3, 2, NULL, 1, 32525516, NULL, NULL, NULL, '2019-06-03', 0, NULL, NULL, NULL, NULL, 78888, 6778, 'A009105991Z', NULL, NULL, '2020-06-02 06:45:26', '2020-09-08 09:33:27', 8, 'CSD-008F/19', '2020-09-08'),
(19, 65, 7, 14, 9, 0, 1, NULL, 11, 29395681, NULL, NULL, NULL, '2018-01-03', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:15', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(20, 55, 7, 14, 9, 0, 2, NULL, 11, 35072559, NULL, NULL, NULL, '2017-09-01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:15', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(21, 41, 7, 19, 12, 24, 1, NULL, 19, 32563964, NULL, NULL, NULL, '2017-11-21', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:15', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(22, 66, 7, 19, 12, 24, 2, NULL, 10, 21869326, NULL, NULL, NULL, '2018-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:15', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(23, 32, 7, 14, 9, 0, 1, NULL, 7, 27802913, NULL, NULL, NULL, '2018-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:15', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(24, 35, 7, 19, 12, 24, 2, NULL, 19, 28444340, NULL, NULL, NULL, '2018-03-28', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:15', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(25, 36, 7, 14, 9, 0, 2, NULL, 7, 29416545, NULL, NULL, NULL, '2018-08-30', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:15', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(26, 37, 7, 15, 9, 21, 2, NULL, 7, 30325806, NULL, NULL, NULL, '2017-07-02', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(27, 67, 7, 16, 9, 0, 2, NULL, 11, 27347918, NULL, NULL, NULL, '2018-07-02', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(28, 33, 7, 17, 9, 0, 2, NULL, 7, 33535103, NULL, NULL, NULL, '2018-07-02', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(29, 34, 7, 16, 9, 0, 2, NULL, 7, 29515219, NULL, NULL, NULL, '2018-07-02', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(30, 68, 7, 15, 9, 0, 1, NULL, 11, 29244949, NULL, NULL, NULL, '2018-02-22', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(31, 69, 7, 10, 9, 0, 1, NULL, 11, 30947524, NULL, NULL, NULL, '2018-07-22', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(32, 70, 7, 14, 9, 0, 1, NULL, 11, 28590131, NULL, NULL, NULL, '2019-07-15', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(33, 71, 7, 10, 9, 0, 2, NULL, 11, 281258000, NULL, NULL, NULL, '2019-07-15', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(34, 72, 7, 18, 9, 22, 2, NULL, 11, 25342910, NULL, NULL, NULL, '2018-02-05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(35, 73, 7, 19, 2, 5, 1, 2, 20, 35811531, NULL, NULL, NULL, '2018-12-05', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(36, 74, 7, 19, 8, 26, 2, NULL, 21, 27349292, NULL, NULL, NULL, '2017-02-01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(37, 40, 7, 19, 12, 24, 2, NULL, 19, 24260629, NULL, NULL, NULL, '2017-06-03', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(38, 39, 7, 5, 12, 14, 2, NULL, 22, 22558181, NULL, NULL, NULL, '2017-04-01', 3, '2020-08-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-08-27 06:34:50', 0, NULL, NULL),
(39, 42, 7, 5, 12, 24, 2, NULL, 19, 28504506, NULL, NULL, NULL, '2017-10-02', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(40, 44, 7, 2, 9, 0, 2, NULL, 7, 27350687, NULL, NULL, NULL, '2017-07-31', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(41, 12, 7, 19, 9, 0, 1, NULL, 23, 28190960, NULL, NULL, NULL, '2017-05-09', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(42, 8, 7, 20, 3, 0, 2, NULL, 24, 33651820, NULL, NULL, NULL, '2019-04-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(43, 11, 7, 20, 5, 0, 1, NULL, 18, 28848194, NULL, NULL, NULL, '2019-06-04', 3, '2020-08-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-03 07:55:53', 0, NULL, NULL),
(44, 9, 7, 20, 7, 0, 1, NULL, 27, 1690337, NULL, NULL, NULL, '2019-06-04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(45, 15, 7, 20, 11, 0, 1, NULL, 28, 22853252, NULL, NULL, NULL, '2017-03-23', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(46, 57, 7, 20, 14, 0, 1, NULL, 29, 35081509, NULL, NULL, NULL, '2017-01-01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(47, 62, 7, 20, 3, 0, 1, NULL, 30, 25348838, NULL, NULL, NULL, '2019-06-03', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(48, 63, 7, 20, 9, 0, 2, NULL, 31, 471817, NULL, NULL, NULL, '2019-08-01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(49, 75, 7, 20, 8, 26, 2, NULL, 21, 28342906, NULL, NULL, NULL, '2019-08-01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:16', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(50, 76, 7, 17, 9, 0, 2, NULL, 11, 29663393, NULL, NULL, NULL, '2019-08-13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(51, 77, 7, 17, 9, 0, 2, NULL, 11, 32775871, NULL, NULL, NULL, '2019-08-13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(52, 78, 7, 17, 9, 0, 2, NULL, 11, 30523209, NULL, NULL, NULL, '2019-08-13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:28', 0, NULL, '2020-09-08'),
(53, 79, 7, 17, 9, 0, 1, NULL, 11, 29882038, NULL, NULL, NULL, '2019-08-13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(54, 48, 7, 2, 9, 0, 2, NULL, 11, 34908636, NULL, NULL, NULL, '2019-09-02', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(55, 80, 7, 20, 3, 0, 2, NULL, 32, 33241431, NULL, NULL, NULL, '2019-09-02', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(56, 52, 7, 18, 9, 22, 2, NULL, 11, 33584483, NULL, NULL, NULL, '2020-05-15', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(57, 61, 7, 14, 9, 0, 1, NULL, 11, 33280386, NULL, NULL, NULL, '2019-10-01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(58, 81, 7, 5, 1, 10, 1, NULL, 14, 32646800, NULL, NULL, NULL, '2019-11-04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(59, 82, 7, 22, 9, 0, 2, NULL, 11, 28010357, NULL, NULL, NULL, '2019-11-04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(60, 83, 7, 2, 9, 0, 1, NULL, 11, NULL, NULL, NULL, NULL, '2019-11-15', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(61, 84, 7, 2, 9, 0, 2, NULL, 11, NULL, NULL, NULL, NULL, '2019-11-15', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(62, 85, 7, 14, 9, 0, 2, NULL, 11, 23898176, NULL, NULL, NULL, '2019-12-18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(63, 47, 7, 23, 9, 0, 2, NULL, 11, 28472201, NULL, NULL, NULL, '2019-12-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(64, 86, 7, 17, 9, 0, 1, NULL, 11, 30372716, NULL, NULL, NULL, '2019-12-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(65, 87, 7, 16, 9, 22, 1, NULL, 11, 24180834, NULL, NULL, NULL, '2019-12-17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:17', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(66, 88, 7, 15, 9, 0, 2, NULL, 11, 33243531, NULL, NULL, NULL, '2020-01-03', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(67, 89, 7, 23, 9, 0, 1, NULL, 11, 31034643, NULL, NULL, NULL, '2020-01-03', 3, '2020-07-26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-03 07:51:34', 0, NULL, NULL),
(68, 7, 7, 19, 8, 0, 2, NULL, 33, 22170653, NULL, NULL, NULL, '2020-01-06', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(69, 90, 7, 2, 9, 0, 1, NULL, 11, 33322947, NULL, NULL, NULL, '2020-02-24', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(70, 91, 7, 16, 9, 0, 1, NULL, 11, 24982171, NULL, NULL, NULL, '2020-02-24', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(71, 92, 7, 2, 9, 22, 2, 1, 11, 24592460, NULL, NULL, NULL, '2020-03-04', 3, '2020-06-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-03 07:52:42', 0, NULL, NULL),
(72, 46, 7, 2, 9, 0, 2, NULL, 11, 32839540, NULL, NULL, NULL, '2020-03-04', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(73, 93, 7, 16, 9, 22, 2, NULL, 11, 30748068, NULL, NULL, NULL, '2020-03-06', 3, '2020-07-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-03 07:59:53', 0, NULL, NULL),
(74, 94, 7, 22, 9, 0, 2, NULL, 11, 32683463, NULL, NULL, NULL, '2020-03-06', 3, '2020-09-07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-07 06:51:59', 0, NULL, NULL),
(75, 95, 7, 2, 9, 0, 2, NULL, 11, 30016610, NULL, NULL, NULL, '2020-03-09', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(76, 96, 7, 2, 9, 0, 2, NULL, 11, 31166724, NULL, NULL, NULL, '2020-03-09', 3, '2020-08-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-03 07:53:59', 0, NULL, NULL),
(77, 54, 7, 2, 9, 0, 2, NULL, 11, 29525788, NULL, NULL, NULL, '2020-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(78, 97, 7, 2, 9, 0, 2, NULL, 11, 29300229, NULL, NULL, NULL, '2020-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(79, 98, 7, 2, 9, 0, 2, NULL, 11, 31427079, NULL, NULL, NULL, '2020-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(80, 99, 7, 2, 9, 0, 2, NULL, 11, 27346304, NULL, NULL, NULL, '2020-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:18', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(81, 100, 7, 2, 9, 0, 2, NULL, 11, 29689472, NULL, NULL, NULL, '2020-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(82, 51, 7, 2, 9, 0, 1, NULL, 11, 30918849, NULL, NULL, NULL, '2020-03-12', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(83, 101, 7, 2, 9, 0, 1, NULL, 11, 29579656, NULL, NULL, NULL, '2020-03-13', 3, '2020-09-05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-07 08:28:22', 0, NULL, NULL),
(84, 49, 7, 2, 9, 0, 1, NULL, 11, 33470028, NULL, NULL, NULL, '2020-03-13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(85, 102, 7, 4, 9, 0, 2, NULL, 11, 28760966, NULL, NULL, NULL, '2020-03-17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(86, 50, 7, 2, 9, 22, 2, 2, 11, 33442200, NULL, NULL, NULL, '2020-03-17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(87, 103, 7, 2, 9, 0, 1, NULL, 11, 29748171, NULL, NULL, NULL, '2020-03-17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(88, 104, 7, 2, 9, 0, 1, NULL, 11, 28597110, NULL, NULL, NULL, '2020-03-17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(89, 105, 7, 2, 9, 0, 1, NULL, 11, 36445526, NULL, NULL, NULL, '2020-03-17', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(90, 106, 7, 19, 8, 26, 2, NULL, 21, 27489629, NULL, NULL, NULL, '2020-05-06', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-11 18:19:19', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(91, 107, 22, 19, 2, 5, 1, NULL, 14, 26711878, NULL, NULL, NULL, '2020-06-08', 3, '2020-09-13', NULL, NULL, NULL, 180260936, 4200877, 'A005269684M', NULL, NULL, '2020-06-16 13:21:25', '2020-09-14 09:12:32', 0, NULL, '2020-09-08'),
(92, 110, 22, 2, 9, 22, 2, 2, 11, 28875152, NULL, NULL, NULL, '2020-06-16', 0, NULL, NULL, NULL, NULL, 2012382493, 6304357, 'A010032015L', NULL, NULL, '2020-06-23 14:39:07', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(93, 111, 22, 2, 9, 22, 2, NULL, 11, 32351780, NULL, NULL, NULL, '2020-06-16', 3, '2020-08-07', NULL, NULL, NULL, 2017354642, 7271967, 'A008107744R', NULL, NULL, '2020-06-23 14:47:03', '2020-08-31 08:40:46', 0, NULL, NULL),
(94, 112, 22, 20, 1, 27, 2, NULL, 34, 34387880, NULL, NULL, NULL, '2020-06-22', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-23 15:01:59', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(95, 113, 22, 3, 9, 22, 1, NULL, 11, 0, NULL, NULL, NULL, '2019-07-01', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-23 15:07:36', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(96, 114, 22, 1, 9, 22, 1, NULL, 11, 25199602, NULL, NULL, NULL, '2017-06-05', 0, NULL, NULL, NULL, NULL, 544579917, 3297451, 'A006023104U', NULL, NULL, '2020-06-30 10:44:25', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(97, 115, 7, 2, 9, 22, 1, 2, 11, 35768991, NULL, NULL, 'Nyayo Estate', '2020-07-01', 0, NULL, 'Kenya Commercial Bank', 'Moi Avenue', 12380879957, 2052102031, 8901043, 'A011914528V', NULL, 'Yes. Asthmatic', '2020-07-09 08:31:32', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(98, 116, 7, 1, 9, 22, 2, 2, 11, 29480376, NULL, NULL, 'Utawala', '2020-07-01', 3, '2020-08-15', 'SMB', 'Donholm', 122246976001, 2013995514, 6772428, 'A0066354960', NULL, 'None', '2020-07-09 08:49:08', '2020-09-03 07:48:21', 1, 'CCC-001F/20', NULL),
(99, 117, 7, 2, 9, 22, 2, 2, 11, 29422333, NULL, NULL, 'Kasarani-Equity', '2020-07-01', 0, NULL, 'Kenya Commercial Bank', 'Kimathi Street', 1161714731, 2003838985, 4960789, 'A009220313V', NULL, 'None', '2020-07-09 11:39:06', '2020-09-08 09:33:29', 2, 'CCC-002F/20', '2020-09-08'),
(100, 119, 7, 10, 9, 22, 2, 2, 3, 34773189, 719724227, '1158-00606 Nairobi', 'Ndovu Close, Moi Otiende (Langata)', '2020-07-22', 0, NULL, 'Cooperative Bank', 'Westlands', 116588488700, 2031193971, 14689587, 'A013909161H', NULL, 'None', '2020-07-23 07:16:05', '2020-09-08 09:33:29', 3, 'CCC-003F/20', '2020-09-08'),
(101, 120, 22, 2, 9, 22, 1, 2, 11, 3124575, NULL, NULL, NULL, '2020-04-13', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-03 13:27:00', '2020-09-08 09:33:29', 4, 'CCC-004M/20', '2020-09-08'),
(102, 122, 7, 1, 9, 22, 2, 2, 11, 27871030, 713923149, NULL, NULL, '2020-08-01', 0, NULL, 'Kenya Commercial Bank', 'Kimathi Street', 1267066180, 2014299135, NULL, 'A010466595G', NULL, 'None', '2020-08-18 06:32:56', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(103, 123, 7, 26, 9, 22, 2, 2, 11, 776874, NULL, NULL, NULL, '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-18 08:23:05', '2020-08-18 09:30:02', 5, 'CCC-005F/20', NULL),
(104, 124, 7, 26, 9, 22, 1, NULL, 11, 251215, 257691037649, NULL, NULL, '2020-08-18', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-18 09:29:31', '2020-08-18 09:29:31', 0, NULL, NULL),
(105, 125, 7, 26, 9, 22, 1, 2, 11, 117690, NULL, NULL, 'Kawangware, Coast Rd.', '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'None', '2020-08-18 10:09:42', '2020-08-18 10:09:42', 0, NULL, NULL),
(106, 126, 7, 26, 9, 22, 1, 1, 11, 886851, NULL, NULL, NULL, '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'None', '2020-08-18 10:19:31', '2020-08-18 10:19:31', 6, 'CCC-006M/20', NULL),
(107, 127, 7, 26, 9, 22, 2, 1, 11, 10068968, NULL, NULL, 'Kawangware', '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'None', '2020-08-18 11:32:05', '2020-08-18 11:32:05', 7, 'CCC-007F/20', NULL),
(108, 129, 7, 26, 9, 22, 1, 2, 11, 790493, NULL, NULL, NULL, '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'None', '2020-08-18 11:36:01', '2020-08-18 11:36:01', 8, 'CCC-008M/20', NULL),
(109, 130, 7, 26, 9, 22, 2, 2, 11, 33414741, NULL, NULL, NULL, '2020-08-16', 0, NULL, NULL, NULL, NULL, 2027410899, NULL, NULL, NULL, 'None', '2020-08-18 11:39:17', '2020-08-18 11:39:17', 9, 'CCC-009F/20', NULL),
(110, 131, 7, 26, 9, 22, 1, 1, 11, 805306, NULL, NULL, 'Kasarani', '2020-08-16', 0, NULL, NULL, NULL, NULL, 2008255411, 5728879, NULL, NULL, 'None', '2020-08-18 11:44:01', '2020-08-18 11:44:01', 10, 'CCC-010M/20', NULL),
(111, 133, 7, 26, 9, 22, 2, 2, 11, 33414741, 722874082, NULL, 'Donholm', '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'none', '2020-08-18 11:54:46', '2020-08-18 11:54:46', 11, 'CCC-011F/20', NULL),
(112, 134, 7, 26, 9, 22, 1, NULL, 11, 848998, NULL, NULL, 'Nasra, Gardens (Kayole, Umoja)', '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'None', '2020-08-18 12:00:35', '2020-08-18 12:00:35', 12, 'CCC-012M/20', NULL),
(113, 135, 7, 26, 9, 22, 2, 2, 11, 34961814, NULL, NULL, 'Huruma, Kiamaiko', '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'None', '2020-08-18 12:04:50', '2020-08-18 12:04:50', 13, 'CCC-013F/20', NULL),
(114, 136, 7, 26, 9, 22, 1, 2, 11, 38825836, 763935856, NULL, 'Eastleigh, 12th Avenue', '2020-08-16', 0, NULL, 'EQUITY BANK', NULL, 120179646730, 2029597518, 14437042, 'A014798882X', NULL, 'None', '2020-08-18 12:15:36', '2020-08-18 12:15:36', 14, 'CCC-014M/20', NULL),
(115, 145, 7, 26, 9, 22, 2, 2, 11, 857488, NULL, NULL, 'Kasarani, Mwiki', '2020-08-16', 0, NULL, 'EQUITY BANK', 'Knut House', 350175266806, NULL, NULL, NULL, NULL, 'None', '2020-08-19 06:52:27', '2020-08-19 06:52:27', 15, 'CCC-015F/20', NULL),
(116, 150, 7, 19, 6, 12, 2, 1, 6, 24061288, NULL, NULL, 'mokoyeti West Rd Karen', '2020-08-19', 3, '2020-08-19', 'EQUITY BANK', 'Karen', 170194578292, NULL, NULL, NULL, NULL, 'None', '2020-08-19 07:34:06', '2020-08-26 09:27:53', 16, 'CCC-016F/20', NULL),
(117, 153, 7, 26, 9, 22, 1, 2, 11, 297633, 778130228, NULL, 'Imara Daima', '2020-08-16', 0, NULL, 'EQUITY BANK', 'Mombasa Road', 0, 2021243944, 7627896, 'A012003351Y', NULL, 'None', '2020-08-19 12:14:02', '2020-08-19 12:14:02', 16, 'CCC-016M/20', NULL),
(118, 157, 7, 26, 9, 22, 1, NULL, 11, 10184833, NULL, NULL, NULL, '2020-08-16', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-26 08:24:46', '2020-08-26 08:24:46', 17, 'CCC-017M/20', NULL),
(119, 158, 7, 22, 9, 22, 2, 2, 11, 30097683, NULL, NULL, 'Imara Daima', '2020-07-01', 0, NULL, 'Equity Bank', 'Kitengela', 700167560982, 2012228789, 6278978, 'A010047030A', NULL, 'None', '2020-08-31 07:57:23', '2020-09-08 09:33:29', 18, 'CCC-018F/20', '2020-09-08'),
(120, 159, 7, 2, 9, 22, 2, 1, 12, 700686811, NULL, NULL, NULL, '2020-07-01', 0, NULL, 'Kenya Commercial Bank', 'Kayole', 1221571850, NULL, 9977756, 'A0119683925', NULL, 'None', '2020-08-31 08:04:52', '2020-09-08 09:33:29', 19, 'CCC-019F/20', '2020-09-08'),
(121, 160, 7, 4, 9, 22, 1, NULL, 11, 32298942, NULL, NULL, 'Kahawa Sukari', '2020-07-13', 0, NULL, 'Equity Bank', 'Fourways', 66062802850, 2024583369, 8792884, NULL, NULL, 'None', '2020-08-31 08:22:23', '2020-09-08 09:33:29', 20, 'CCC-020M/20', '2020-09-08'),
(122, 161, 7, 20, 1, 3, 1, 2, 1, 30745754, 728775241, NULL, 'Gachie, Gacharage Junction', '2020-07-14', 0, NULL, 'Kenya Commercial Bank', 'Sotik', 1138750778, 2015391342, 6966529, 'A0071380440', NULL, 'None', '2020-08-31 08:27:11', '2020-09-08 09:33:29', 0, NULL, '2020-09-08'),
(123, 162, 7, 1, 9, 22, 2, 1, 11, 32360705, NULL, NULL, NULL, '2020-08-01', 0, NULL, 'Stanbic', NULL, NULL, 2026039818, 9682643, 'A009008055T', NULL, 'None', '2020-08-31 08:36:18', '2020-09-08 09:33:29', 21, 'CCC-021F/20', '2020-09-08'),
(124, 163, 7, 1, 9, 22, 2, 2, 11, 32674537, 791158594, '11526-00200 Naironi', 'Gitaru', '2020-07-16', 0, NULL, 'ABSA', 'Garden City', 2041938776, NULL, NULL, NULL, NULL, 'None', '2020-08-31 08:46:30', '2020-09-08 09:33:29', 22, 'CCC-022F/20', '2020-09-08'),
(125, 164, 7, 20, 1, 3, 1, 2, 1, 30084857, NULL, NULL, NULL, '2020-08-06', 0, NULL, NULL, NULL, NULL, 20194587, 8217249, 'A006916974T', NULL, 'None', '2020-08-31 09:00:27', '2020-09-08 09:33:29', 4, 'CSD-004M/20', '2020-09-08'),
(126, 167, 7, 1, 9, 22, 2, 2, 11, 25160328, 724990453, NULL, NULL, '2020-08-12', 0, NULL, 'Cooperative Bank', 'Coop House', 1108010662100, 949201812, 2268147, 'A003864169I', NULL, 'None', '2020-08-31 09:46:16', '2020-08-31 09:46:16', 0, NULL, NULL),
(127, 168, 7, 22, 9, 22, 2, 2, 11, 33484865, 725539959, NULL, NULL, '2020-08-16', 0, NULL, 'Kenya Commercial Bank', 'Kencom', 1253139628, 2022396474, 9716705, 'A0118770135', NULL, 'None', '2020-08-31 12:48:28', '2020-08-31 12:48:28', 0, NULL, NULL),
(128, 169, 7, 19, 9, 22, 1, 2, 11, 28623523, NULL, NULL, 'Kinoo', '2020-08-16', 0, NULL, 'Family', 'Wote', 88000003943, 2008482616, 4020044, 'A006598552N', NULL, 'None', '2020-08-31 13:09:00', '2020-08-31 13:09:00', 23, 'CCC-023M/20', NULL),
(129, 171, 7, 19, 9, 22, 1, 1, 11, 26589625, 711574667, NULL, NULL, '2020-08-16', 0, NULL, NULL, NULL, NULL, 640986919, 3279065, 'A005597159N', NULL, 'None', '2020-08-31 13:20:35', '2020-08-31 13:20:35', 24, 'CCC-024M/20', NULL),
(130, 172, 7, 3, 9, 22, 1, 2, 11, 33280264, NULL, NULL, 'Uhuru, Buru Buru', '2020-08-16', 0, NULL, 'Cooperative Bank', 'Buru buru', 1108104044900, 2014568028, 6868119, 'A009906570N', NULL, 'None', '2020-08-31 13:25:47', '2020-08-31 13:25:47', 25, 'CCC-025M/20', NULL),
(131, 177, 7, 19, 6, 12, 2, 2, 6, 30446506, NULL, '2297', 'Kasarani', '2020-09-01', 0, NULL, 'Cooperative Bank', 'Githurai Kimbo', 1108461743500, 2008987674, 5869246, 'A006901813K', NULL, 'None', '2020-09-03 07:46:45', '2020-09-03 07:46:45', 2, 'CTD-002F/20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_appraisals`
--

CREATE TABLE `tbl_staff_appraisals` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_id` bigint UNSIGNED NOT NULL,
  `review_period_from` date NOT NULL,
  `review_period_to` date NOT NULL,
  `staff_comments` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `supervisor_comments` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_beneficiaries`
--

CREATE TABLE `tbl_staff_beneficiaries` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `relationship` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_staff_beneficiaries`
--

INSERT INTO `tbl_staff_beneficiaries` (`id`, `staff_id`, `name`, `phone`, `email`, `relationship`, `created_at`, `updated_at`) VALUES
(1, 107, 'Ian harry Ntwari', NULL, NULL, 'Son', '2020-08-18 11:32:05', '2020-08-18 11:32:05'),
(2, 114, 'Alasa Abdi Dii', 721917521, NULL, 'Mother', '2020-08-18 12:15:36', '2020-08-18 12:15:36'),
(3, 129, 'Litron Gayalwa', 711574667, NULL, 'Wife', '2020-08-31 13:20:35', '2020-08-31 13:20:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_exits`
--

CREATE TABLE `tbl_staff_exits` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `staff_user_id` bigint UNSIGNED NOT NULL,
  `discharge_reason` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `discharged_at` datetime DEFAULT NULL,
  `status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_staff_exits`
--

INSERT INTO `tbl_staff_exits` (`id`, `user_id`, `staff_user_id`, `discharge_reason`, `discharged_at`, `status`, `created_at`, `updated_at`) VALUES
(1, 7, 150, '150', '2020-08-19 00:00:00', 0, '2020-08-26 09:27:53', '2020-08-26 09:27:53'),
(2, 7, 39, '39', '2020-08-26 00:00:00', 0, '2020-08-27 06:34:50', '2020-08-27 06:34:50'),
(3, 7, 111, '111', '2020-08-07 00:00:00', 0, '2020-08-31 08:40:46', '2020-08-31 08:40:46'),
(4, 7, 116, '116', '2020-08-15 00:00:00', 0, '2020-09-03 07:48:20', '2020-09-03 07:48:20'),
(5, 7, 89, '89', '2020-07-26 00:00:00', 0, '2020-09-03 07:51:34', '2020-09-03 07:51:34'),
(6, 7, 92, '92', '2020-06-27 00:00:00', 0, '2020-09-03 07:52:42', '2020-09-03 07:52:42'),
(7, 7, 96, '96', '2020-08-01 00:00:00', 0, '2020-09-03 07:53:59', '2020-09-03 07:53:59'),
(8, 7, 11, '11', '2020-08-10 00:00:00', 0, '2020-09-03 07:55:53', '2020-09-03 07:55:53'),
(9, 7, 93, '93', '2020-07-21 00:00:00', 0, '2020-09-03 07:59:53', '2020-09-03 07:59:53'),
(10, 7, 94, '94', '2020-09-07 00:00:00', 0, '2020-09-07 06:51:59', '2020-09-07 06:51:59'),
(11, 7, 101, '101', '2020-09-05 00:00:00', 0, '2020-09-07 08:28:22', '2020-09-07 08:28:22'),
(12, 7, 107, '107', '2020-09-13 00:00:00', 0, '2020-09-14 09:12:32', '2020-09-14 09:12:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_next_of_kin`
--

CREATE TABLE `tbl_staff_next_of_kin` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_id` int NOT NULL,
  `nok_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nok_phone` bigint NOT NULL,
  `nok_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nok_relationship` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_staff_next_of_kin`
--

INSERT INTO `tbl_staff_next_of_kin` (`id`, `staff_id`, `nok_name`, `nok_phone`, `nok_email`, `nok_relationship`, `created_at`, `updated_at`) VALUES
(1, 98, 'Fahheen', 726019322, NULL, 'Son', '2020-07-09 08:49:08', '2020-07-09 08:49:08'),
(2, 99, 'Joseph Mwanzia', 724764245, NULL, 'Father', '2020-07-09 11:39:06', '2020-07-09 11:39:06'),
(3, 99, 'Jackline Mwanzia', 729097732, NULL, 'Mother', '2020-07-09 11:39:06', '2020-07-09 11:39:06'),
(4, 100, 'Florence Muindi', 724699333, NULL, 'Mother', '2020-07-23 07:16:05', '2020-07-23 07:16:05'),
(5, 106, 'Hulufe Ogate', 7088770999, NULL, 'Sister', '2020-08-18 10:19:31', '2020-08-18 10:19:31'),
(6, 107, 'Johnson Twagirimana', 700331609, NULL, 'Husband', '2020-08-18 11:32:05', '2020-08-18 11:32:05'),
(7, 110, 'Phylis Maiyo', 716330559, NULL, 'Wife', '2020-08-18 11:44:01', '2020-08-18 11:44:01'),
(8, 111, 'Aaleya Iman Joseph', 722874082, NULL, 'Son', '2020-08-18 11:54:46', '2020-08-18 11:54:46'),
(9, 111, 'Aaleya Iman Joseph', 722874082, NULL, 'Son', '2020-08-18 11:54:46', '2020-08-18 11:54:46'),
(10, 111, 'Himca Yunis Ali', 722874082, NULL, 'Mother', '2020-08-18 11:54:46', '2020-08-18 11:54:46'),
(11, 112, 'Martha Nyapar', 727340005, NULL, 'Relative', '2020-08-18 12:00:35', '2020-08-18 12:00:35'),
(12, 113, 'Dahaba Yussuf Hassan', 724162653, NULL, 'Mother', '2020-08-18 12:04:50', '2020-08-18 12:04:50'),
(13, 114, 'Alasa Abdi Dii', 721917521, NULL, 'Mother', '2020-08-18 12:15:36', '2020-08-18 12:15:36'),
(14, 116, 'Teresiah Thom', 722756320, NULL, 'Sister', '2020-08-19 07:34:06', '2020-08-19 07:34:06'),
(15, 117, 'Marie Tuyishimire', 725060351, NULL, 'Sister', '2020-08-19 12:14:02', '2020-08-19 12:14:02'),
(16, 119, 'Emmanuel Muuo', 718773229, NULL, 'Brother', '2020-08-31 07:57:23', '2020-08-31 07:57:23'),
(17, 119, 'Veronica Muthuo', 721704160, NULL, 'Mother', '2020-08-31 07:57:23', '2020-08-31 07:57:23'),
(18, 121, 'Peter Gatua', 722405653, NULL, 'Father', '2020-08-31 08:22:23', '2020-08-31 08:22:23'),
(19, 123, 'Jeremiah Anakoli', 725756614, NULL, 'Husband', '2020-08-31 08:36:18', '2020-08-31 08:36:18'),
(20, 124, 'Joseph Wairire', 721484145, NULL, 'Father', '2020-08-31 08:46:30', '2020-08-31 08:46:30'),
(21, 124, 'Mariam W. Mwangi', 715288000, NULL, 'Mother', '2020-08-31 08:46:30', '2020-08-31 08:46:30'),
(22, 125, 'Eunice  Didinya', 718882844, NULL, 'Sister', '2020-08-31 09:00:27', '2020-08-31 09:00:27'),
(23, 125, 'Damaris wamuyu', 701832423, NULL, 'Girlfriend', '2020-08-31 09:00:27', '2020-08-31 09:00:27'),
(24, 128, 'David Nzangi', 722785857, NULL, 'Father', '2020-08-31 13:09:00', '2020-08-31 13:09:00'),
(25, 129, 'Litron Gayalwa', 711574667, NULL, 'Wife', '2020-08-31 13:20:35', '2020-08-31 13:20:35'),
(26, 130, 'Priscilla Oyoo', 722470870, NULL, 'Mother', '2020-08-31 13:25:47', '2020-08-31 13:25:47'),
(27, 130, 'Gidell Oloo', 727004461, NULL, 'Brother', '2020-08-31 13:25:47', '2020-08-31 13:25:47'),
(28, 131, 'Peninah Mutenge', 715850928, NULL, 'Mother', '2020-09-03 07:46:45', '2020-09-03 07:46:45');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_roles`
--

CREATE TABLE `tbl_staff_roles` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `department_id` bigint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_staff_roles`
--

INSERT INTO `tbl_staff_roles` (`id`, `name`, `created_at`, `updated_at`, `department_id`) VALUES
(1, 'HOD', '2020-04-16 06:40:07', '2020-04-16 06:40:07', NULL),
(2, 'HOD', '2020-04-16 06:46:30', '2020-04-16 06:46:30', NULL),
(3, 'Software Developer', '2020-04-16 06:48:41', '2020-04-16 06:48:41', NULL),
(4, 'HOD', '2020-04-16 06:48:54', '2020-04-16 06:48:54', NULL),
(5, 'Support', '2020-04-16 06:49:24', '2020-04-16 06:49:24', NULL),
(6, 'Intern', '2020-04-16 06:49:35', '2020-04-16 06:49:35', NULL),
(7, 'HOD', '2020-04-16 06:49:48', '2020-04-16 06:49:48', NULL),
(8, 'Staff', '2020-04-16 06:49:59', '2020-04-16 06:49:59', NULL),
(9, 'Quality Assurance', '2020-04-16 06:50:18', '2020-04-16 06:50:18', NULL),
(10, 'Junior Software Developer', '2020-04-16 07:16:45', '2020-04-16 07:16:45', NULL),
(11, 'H.O.D', '2020-05-09 18:45:10', '2020-05-09 18:45:10', NULL),
(12, 'Training Staff', '2020-05-15 06:36:24', '2020-05-15 06:36:24', NULL),
(13, 'H.O.D', '2020-06-12 10:19:47', '2020-06-12 10:19:47', NULL),
(14, 'H.O.D', '2020-06-12 10:20:00', '2020-06-12 10:20:00', NULL),
(15, 'H.O.D', '2020-06-12 10:20:08', '2020-06-12 10:20:08', NULL),
(16, 'H.O.D', '2020-06-12 10:20:40', '2020-06-12 10:20:40', NULL),
(17, 'H.O.D', '2020-06-12 10:21:06', '2020-06-12 10:21:06', NULL),
(18, 'H.O.D', '2020-06-12 10:21:17', '2020-06-12 10:21:17', NULL),
(19, 'H.O.D', '2020-06-12 10:21:29', '2020-06-12 10:21:29', NULL),
(20, 'Quality Assurance', '2020-06-12 10:23:00', '2020-06-12 10:24:00', NULL),
(21, 'Account Manager', '2020-06-12 10:23:32', '2020-06-12 10:23:32', NULL),
(22, 'Customer Service Executive', '2020-06-12 10:25:04', '2020-06-12 10:25:04', NULL),
(23, 'H.O.D', '2020-06-12 10:25:23', '2020-06-12 10:25:23', NULL),
(24, 'Quality Analyst', '2020-06-12 13:12:00', '2020-06-12 13:12:00', NULL),
(25, 'Staff', '2020-06-16 13:08:47', '2020-06-16 13:08:47', NULL),
(26, 'Support Staff', '2020-06-16 13:59:24', '2020-06-16 13:59:24', NULL),
(27, 'R & D Technologist', '2020-06-23 14:59:15', '2020-06-23 14:59:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_staff_spouse_or_childrens`
--

CREATE TABLE `tbl_staff_spouse_or_childrens` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` bigint DEFAULT NULL,
  `relationship` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_staff_spouse_or_childrens`
--

INSERT INTO `tbl_staff_spouse_or_childrens` (`id`, `staff_id`, `name`, `phone`, `relationship`, `created_at`, `updated_at`) VALUES
(1, 99, 'Jackson Mwendwa', 0, 'Child', '2020-07-09 11:39:06', '2020-07-09 11:39:06'),
(2, 100, 'N/A', NULL, NULL, '2020-07-23 07:16:05', '2020-07-23 07:16:05'),
(3, 106, 'Sara Abez', 7785476894, 'Spouse', '2020-08-18 10:19:31', '2020-08-18 10:19:31'),
(4, 110, 'Phylis Maiyo', 716330559, 'Spouse', '2020-08-18 11:44:01', '2020-08-18 11:44:01'),
(5, 110, 'Alban Rugwiro', NULL, 'Child', '2020-08-18 11:44:01', '2020-08-18 11:44:01'),
(6, 116, 'Kelly Gathoni Atieno', 722756320, 'Child', '2020-08-19 07:34:06', '2020-08-19 07:34:06'),
(7, 123, 'Hera Kasha', 725756614, 'Child', '2020-08-31 08:36:18', '2020-08-31 08:36:18'),
(8, 125, 'Damaris Wamuyu', 718324423, 'Spouse', '2020-08-31 09:00:27', '2020-08-31 09:00:27'),
(9, 129, 'Litron Gayalwa', 711574667, 'Spouse', '2020-08-31 13:20:35', '2020-08-31 13:20:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tickets`
--

CREATE TABLE `tbl_tickets` (
  `id` bigint UNSIGNED NOT NULL,
  `issue_category_id` int NOT NULL,
  `user_id` int NOT NULL,
  `ticket_status_id` int NOT NULL,
  `disposition_id` int NOT NULL,
  `assigned_to` int NOT NULL,
  `tat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` tinyint DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `solution` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `measures` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `status` int DEFAULT NULL,
  `acknowledged` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_system` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `staff_id` bigint UNSIGNED NOT NULL DEFAULT '0',
  `acknowledged_at` timestamp NULL DEFAULT NULL,
  `resolved_at` timestamp NULL DEFAULT NULL,
  `closed_at` timestamp NULL DEFAULT NULL,
  `department_id_from` bigint UNSIGNED DEFAULT NULL,
  `department_id_to` bigint UNSIGNED DEFAULT NULL,
  `expected_start_date` datetime DEFAULT NULL,
  `requester_id` int DEFAULT NULL,
  `issue_sub_category_id` int DEFAULT NULL,
  `issue_source_id` int DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_id` tinyint DEFAULT NULL,
  `department_id` tinyint DEFAULT NULL,
  `impact` tinyint DEFAULT NULL,
  `closure_comment` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `escalation_sent` tinyint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_tickets`
--

INSERT INTO `tbl_tickets` (`id`, `issue_category_id`, `user_id`, `ticket_status_id`, `disposition_id`, `assigned_to`, `tat`, `priority`, `start_time`, `end_time`, `solution`, `measures`, `description`, `status`, `acknowledged`, `created_at`, `updated_at`, `is_system`, `staff_id`, `acknowledged_at`, `resolved_at`, `closed_at`, `department_id_from`, `department_id_to`, `expected_start_date`, `requester_id`, `issue_sub_category_id`, `issue_source_id`, `subject`, `branch_id`, `department_id`, `impact`, `closure_comment`, `escalation_sent`) VALUES
(1, 2, 1, 1, 53, 10, NULL, 3, NULL, NULL, NULL, NULL, '<p>This is a test ticket</p>', NULL, 0, '2020-10-25 07:07:02', '2020-10-25 07:07:02', 0, 0, NULL, NULL, NULL, 1, 2, NULL, 1, 2, 1, 'This is a test ticket', 1, 2, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ticket_files`
--

CREATE TABLE `tbl_ticket_files` (
  `id` bigint UNSIGNED NOT NULL,
  `ticket_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` double UNSIGNED DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ticket_statuses`
--

CREATE TABLE `tbl_ticket_statuses` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_ticket_statuses`
--

INSERT INTO `tbl_ticket_statuses` (`id`, `user_id`, `name`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Open', NULL, 1, '2020-02-15 11:24:06', '2020-02-15 11:24:06'),
(2, 1, 'In Progress', NULL, 1, '2020-02-15 11:25:49', '2020-02-15 11:25:49'),
(4, 1, 'Resolved', NULL, 1, '2020-02-15 11:26:10', '2020-02-15 11:26:10'),
(5, 1, 'Closed', NULL, 1, '2020-02-15 11:26:17', '2020-02-15 11:26:17'),
(6, 1, 'Cancelled', 'cancelled', 1, '2020-10-25 04:42:37', '2020-10-25 04:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_ticket_updates`
--

CREATE TABLE `tbl_ticket_updates` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` int NOT NULL,
  `ticket_id` int NOT NULL,
  `issue_category_id` int NOT NULL,
  `assigned_to` int NOT NULL,
  `disposition_id` int NOT NULL,
  `ticket_status_id` int NOT NULL,
  `tat` datetime DEFAULT NULL,
  `comment` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_ticket_updates`
--

INSERT INTO `tbl_ticket_updates` (`id`, `user_id`, `ticket_id`, `issue_category_id`, `assigned_to`, `disposition_id`, `ticket_status_id`, `tat`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 2, 10, 53, 1, NULL, '<p>This is a test ticket</p>', 1, '2020-10-25 07:07:03', '2020-10-25 07:07:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` bigint UNSIGNED NOT NULL,
  `firstname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `middlename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `lastname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alternate_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint DEFAULT NULL,
  `role` enum('admin','member','callcenter') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member',
  `permission_group_id` int NOT NULL DEFAULT '1',
  `line_of_business_id` int NOT NULL DEFAULT '0',
  `department_id` int NOT NULL DEFAULT '0',
  `is_hod` tinyint NOT NULL DEFAULT '0',
  `status` tinyint NOT NULL DEFAULT '1',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_staff` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `employee_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `import_status` tinyint UNSIGNED NOT NULL DEFAULT '0',
  `is_available` tinyint UNSIGNED NOT NULL DEFAULT '1',
  `is_operation_manager` tinyint UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `firstname`, `middlename`, `lastname`, `name`, `dob`, `email`, `alternate_email`, `phone`, `role`, `permission_group_id`, `line_of_business_id`, `department_id`, `is_hod`, `status`, `avatar`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `is_staff`, `employee_number`, `import_status`, `is_available`, `is_operation_manager`) VALUES
(1, 'Indimuli', '0', 'Ian Madara', 'Indimuli Ian Madara', NULL, 'ian.madara@calltronix.com', NULL, NULL, 'admin', 5, 20, 1, 0, 1, '1_ian_madara.jpg', '2020-03-10 12:30:01', '$2y$10$EEppwfrqSHpOaWP.DVDfIODTfcH3CCSpSTWqRgAY7lm.SuwRvzg9e', NULL, '2020-03-10 12:30:01', '2020-07-15 06:54:04', 1, 'CSD-002M/20', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vacancies`
--

CREATE TABLE `tbl_vacancies` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `calltronix_department_id` bigint UNSIGNED NOT NULL,
  `position_id` bigint UNSIGNED NOT NULL,
  `application_deadline` datetime NOT NULL,
  `status` tinyint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_yearly_staff_leaves_trackers`
--

CREATE TABLE `tbl_yearly_staff_leaves_trackers` (
  `id` bigint UNSIGNED NOT NULL,
  `staff_id` bigint UNSIGNED NOT NULL,
  `leave_type_id` bigint UNSIGNED NOT NULL,
  `year` year NOT NULL DEFAULT '2020',
  `days_covered` double NOT NULL DEFAULT '0',
  `days_remaining` double NOT NULL DEFAULT '0',
  `max_days_allowed` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `compensation_days` tinyint NOT NULL DEFAULT '0',
  `prev_year_bal` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_yearly_staff_leaves_trackers`
--

INSERT INTO `tbl_yearly_staff_leaves_trackers` (`id`, `staff_id`, `leave_type_id`, `year`, `days_covered`, `days_remaining`, `max_days_allowed`, `created_at`, `updated_at`, `compensation_days`, `prev_year_bal`) VALUES
(1, 1, 2, 2020, 0, 14, 14, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(2, 5, 2, 2020, 0, 7, 7, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(3, 8, 2, 2020, 0, 14, 14, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(4, 10, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(5, 11, 2, 2020, 0, 7, 7, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(6, 14, 2, 2020, 0, 14, 14, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(7, 17, 2, 2020, 0, 5.25, 5.25, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(8, 18, 2, 2020, 0, 14, 14, '2020-09-08 09:33:27', '2020-09-08 09:33:27', 0, 0),
(9, 19, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(10, 20, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(11, 21, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(12, 22, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(13, 23, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(14, 24, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(15, 25, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(16, 26, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(17, 27, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(18, 28, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(19, 29, 2, 2020, 2, 12, 14, '2020-09-08 09:33:28', '2020-09-08 17:15:45', 0, 0),
(20, 30, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(21, 31, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(22, 32, 2, 2020, 2, 12, 14, '2020-09-08 09:33:28', '2020-09-08 17:16:41', 0, 0),
(23, 33, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(24, 34, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(25, 35, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(26, 36, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(27, 37, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(28, 39, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(29, 40, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(30, 41, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(31, 42, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(32, 44, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(33, 45, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(34, 46, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(35, 47, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(36, 48, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(37, 49, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(38, 50, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(39, 51, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(40, 52, 2, 2020, 0, 14, 14, '2020-09-08 09:33:28', '2020-09-08 09:33:28', 0, 0),
(41, 53, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(42, 54, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(43, 55, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(44, 56, 2, 2020, 0, 5.25, 5.25, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(45, 57, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(46, 58, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(47, 59, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(48, 60, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(49, 61, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(50, 62, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(51, 63, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(52, 64, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(53, 65, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(54, 66, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(55, 68, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(56, 69, 2, 2020, 0, 10.5, 10.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(57, 70, 2, 2020, 0, 10.5, 10.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(58, 72, 2, 2020, 0, 10.5, 10.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(59, 75, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(60, 77, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(61, 78, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(62, 79, 2, 2020, 2, 6.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 17:15:01', 0, 0),
(63, 80, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(64, 81, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(65, 82, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(66, 84, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(67, 85, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(68, 86, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(69, 87, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(70, 88, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(71, 89, 2, 2020, 0, 8.75, 8.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(72, 90, 2, 2020, 0, 7, 7, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(73, 91, 2, 2020, 0, 5.25, 5.25, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(74, 92, 2, 2020, 0, 3.5, 3.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(75, 94, 2, 2020, 0, 3.5, 3.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(76, 95, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(77, 96, 2, 2020, 0, 14, 14, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(78, 97, 2, 2020, 0, 3.5, 3.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(79, 99, 2, 2020, 0, 3.5, 3.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(80, 100, 2, 2020, 0, 1.75, 1.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(81, 101, 2, 2020, 0, 7, 7, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(82, 102, 2, 2020, 0, 1.75, 1.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(83, 103, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(84, 104, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(85, 105, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(86, 106, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(87, 107, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(88, 108, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(89, 109, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(90, 110, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(91, 111, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(92, 112, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(93, 113, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(94, 114, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(95, 115, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(96, 117, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(97, 118, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(98, 119, 2, 2020, 0, 3.5, 3.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(99, 120, 2, 2020, 0, 3.5, 3.5, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(100, 121, 2, 2020, 0, 1.75, 1.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(101, 122, 2, 2020, 0, 1.75, 1.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(102, 123, 2, 2020, 0, 1.75, 1.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(103, 124, 2, 2020, 0, 1.75, 1.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(104, 125, 2, 2020, 0, 1.75, 1.75, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(105, 126, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(106, 127, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(107, 128, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(108, 129, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(109, 130, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0),
(110, 131, 2, 2020, 0, 0, 0, '2020-09-08 09:33:29', '2020-09-08 09:33:29', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_account_managers`
--
ALTER TABLE `tbl_account_managers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_appraisal_questions`
--
ALTER TABLE `tbl_appraisal_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_appraisal_question_categories`
--
ALTER TABLE `tbl_appraisal_question_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_appraisal_question_responses`
--
ALTER TABLE `tbl_appraisal_question_responses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_branches`
--
ALTER TABLE `tbl_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_calltronix_departments`
--
ALTER TABLE `tbl_calltronix_departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_calltronix_departments_department_code_unique` (`department_code`);

--
-- Indexes for table `tbl_calltronix_department_heads`
--
ALTER TABLE `tbl_calltronix_department_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_compensation_requests`
--
ALTER TABLE `tbl_compensation_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_contract_types`
--
ALTER TABLE `tbl_contract_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_counties`
--
ALTER TABLE `tbl_counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_departments_department_code_unique` (`department_code`);

--
-- Indexes for table `tbl_department_heads`
--
ALTER TABLE `tbl_department_heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_dispositions`
--
ALTER TABLE `tbl_dispositions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_disposition_issue_category`
--
ALTER TABLE `tbl_disposition_issue_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_employee_onboarding_checklists`
--
ALTER TABLE `tbl_employee_onboarding_checklists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_failed_jobs`
--
ALTER TABLE `tbl_failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_files`
--
ALTER TABLE `tbl_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_issue_categories`
--
ALTER TABLE `tbl_issue_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_issue_category_issue_sub_category`
--
ALTER TABLE `tbl_issue_category_issue_sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_issue_sources`
--
ALTER TABLE `tbl_issue_sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_issue_sub_categories`
--
ALTER TABLE `tbl_issue_sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_item_categories`
--
ALTER TABLE `tbl_item_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_jobs`
--
ALTER TABLE `tbl_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_jobs_queue_index` (`queue`);

--
-- Indexes for table `tbl_leave_approval_levels`
--
ALTER TABLE `tbl_leave_approval_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_leave_requests`
--
ALTER TABLE `tbl_leave_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_leave_request_actions`
--
ALTER TABLE `tbl_leave_request_actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_leave_types`
--
ALTER TABLE `tbl_leave_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_line_of_businesses`
--
ALTER TABLE `tbl_line_of_businesses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_members`
--
ALTER TABLE `tbl_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_migrations`
--
ALTER TABLE `tbl_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_offduty_requests`
--
ALTER TABLE `tbl_offduty_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_offduty_request_dates`
--
ALTER TABLE `tbl_offduty_request_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_onboarding_checklist_items`
--
ALTER TABLE `tbl_onboarding_checklist_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_password_resets`
--
ALTER TABLE `tbl_password_resets`
  ADD KEY `tbl_password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_permission_groups`
--
ALTER TABLE `tbl_permission_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_positions`
--
ALTER TABLE `tbl_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staffs`
--
ALTER TABLE `tbl_staffs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_staffs_employee_number_unique` (`employee_number`);

--
-- Indexes for table `tbl_staff_appraisals`
--
ALTER TABLE `tbl_staff_appraisals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staff_beneficiaries`
--
ALTER TABLE `tbl_staff_beneficiaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staff_exits`
--
ALTER TABLE `tbl_staff_exits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staff_next_of_kin`
--
ALTER TABLE `tbl_staff_next_of_kin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staff_roles`
--
ALTER TABLE `tbl_staff_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_staff_spouse_or_childrens`
--
ALTER TABLE `tbl_staff_spouse_or_childrens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_tickets`
--
ALTER TABLE `tbl_tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ticket_files`
--
ALTER TABLE `tbl_ticket_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ticket_statuses`
--
ALTER TABLE `tbl_ticket_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_ticket_updates`
--
ALTER TABLE `tbl_ticket_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tbl_users_email_unique` (`email`),
  ADD UNIQUE KEY `tbl_users_employee_number_unique` (`employee_number`);

--
-- Indexes for table `tbl_vacancies`
--
ALTER TABLE `tbl_vacancies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_yearly_staff_leaves_trackers`
--
ALTER TABLE `tbl_yearly_staff_leaves_trackers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_account_managers`
--
ALTER TABLE `tbl_account_managers`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_appraisal_questions`
--
ALTER TABLE `tbl_appraisal_questions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_appraisal_question_categories`
--
ALTER TABLE `tbl_appraisal_question_categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_appraisal_question_responses`
--
ALTER TABLE `tbl_appraisal_question_responses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_branches`
--
ALTER TABLE `tbl_branches`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_calltronix_departments`
--
ALTER TABLE `tbl_calltronix_departments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_calltronix_department_heads`
--
ALTER TABLE `tbl_calltronix_department_heads`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_compensation_requests`
--
ALTER TABLE `tbl_compensation_requests`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_contract_types`
--
ALTER TABLE `tbl_contract_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_counties`
--
ALTER TABLE `tbl_counties`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tbl_departments`
--
ALTER TABLE `tbl_departments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_department_heads`
--
ALTER TABLE `tbl_department_heads`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_dispositions`
--
ALTER TABLE `tbl_dispositions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `tbl_disposition_issue_category`
--
ALTER TABLE `tbl_disposition_issue_category`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `tbl_employee_onboarding_checklists`
--
ALTER TABLE `tbl_employee_onboarding_checklists`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `tbl_failed_jobs`
--
ALTER TABLE `tbl_failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_files`
--
ALTER TABLE `tbl_files`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_issue_categories`
--
ALTER TABLE `tbl_issue_categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_issue_category_issue_sub_category`
--
ALTER TABLE `tbl_issue_category_issue_sub_category`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_issue_sources`
--
ALTER TABLE `tbl_issue_sources`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_issue_sub_categories`
--
ALTER TABLE `tbl_issue_sub_categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_item_categories`
--
ALTER TABLE `tbl_item_categories`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_jobs`
--
ALTER TABLE `tbl_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `tbl_leave_approval_levels`
--
ALTER TABLE `tbl_leave_approval_levels`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_leave_requests`
--
ALTER TABLE `tbl_leave_requests`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_leave_request_actions`
--
ALTER TABLE `tbl_leave_request_actions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_leave_types`
--
ALTER TABLE `tbl_leave_types`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_line_of_businesses`
--
ALTER TABLE `tbl_line_of_businesses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_members`
--
ALTER TABLE `tbl_members`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_migrations`
--
ALTER TABLE `tbl_migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `tbl_offduty_requests`
--
ALTER TABLE `tbl_offduty_requests`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_offduty_request_dates`
--
ALTER TABLE `tbl_offduty_request_dates`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_onboarding_checklist_items`
--
ALTER TABLE `tbl_onboarding_checklist_items`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_permission_groups`
--
ALTER TABLE `tbl_permission_groups`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_positions`
--
ALTER TABLE `tbl_positions`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `tbl_staffs`
--
ALTER TABLE `tbl_staffs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `tbl_staff_appraisals`
--
ALTER TABLE `tbl_staff_appraisals`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_staff_beneficiaries`
--
ALTER TABLE `tbl_staff_beneficiaries`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_staff_exits`
--
ALTER TABLE `tbl_staff_exits`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_staff_next_of_kin`
--
ALTER TABLE `tbl_staff_next_of_kin`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_staff_roles`
--
ALTER TABLE `tbl_staff_roles`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_staff_spouse_or_childrens`
--
ALTER TABLE `tbl_staff_spouse_or_childrens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_tickets`
--
ALTER TABLE `tbl_tickets`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_ticket_files`
--
ALTER TABLE `tbl_ticket_files`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_ticket_statuses`
--
ALTER TABLE `tbl_ticket_statuses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_ticket_updates`
--
ALTER TABLE `tbl_ticket_updates`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT for table `tbl_vacancies`
--
ALTER TABLE `tbl_vacancies`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_yearly_staff_leaves_trackers`
--
ALTER TABLE `tbl_yearly_staff_leaves_trackers`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
