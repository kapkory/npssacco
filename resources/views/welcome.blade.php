<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Calltronix CRM</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta content="A premium admin dashboard template by Mannatthemes" name="description">
    <meta content="Mannatthemes" name="author"><!-- App favicon -->
    <link rel="shortcut icon" href="{{url('theme/')}}/assets/images/favicon.ico">
    <link href="{{url('theme/')}}/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
    <!-- App css -->
    <link href="{{url('theme/')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('theme/')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="{{url('theme/')}}/assets/css/metisMenu.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('theme/')}}/assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body><!-- Top Bar Start -->
<div class="topbar"><!-- Navbar -->
    <nav class="topbar-main"><!-- LOGO -->
        <div class="topbar-left">
            <a href="{{url('/')}}" class="logo"><span><img
                        src="{{url('theme/')}}/assets/images/logo.svg" alt="logo-small"
                        class="logo-sm"> </span>
            </a>
        </div>
        <!--topbar-left--><!--end logo-->
        <ul class="list-unstyled topbar-nav float-right mb-0">

            <li class="dropdown notification-list"><a
                    class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"
                    href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false"><i
                        class="dripicons-bell noti-icon"></i> <span
                        class="badge badge-danger badge-pill noti-icon-badge">2</span></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-lg"><!-- item--><h6 class="dropdown-item-text">
                        Notifications (2)</h6>
                    <div class="slimscroll notification-list"><!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success"><i class="mdi mdi-bell-ring"></i></div>
                            <p class="notify-details">Notification type<small class="text-muted">This module is coming
                                    soon</small></p></a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success"><i class="mdi mdi-bell-ring"></i></div>
                            <p class="notify-details">Notification type<small class="text-muted">This module is coming
                                    soon</small></p></a>

                    </div>
                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary">View
                        all <i class="fi-arrow-right"></i></a>
                </div>
            </li><!--end notification-list-->
            <li class="dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user pr-0"
                   data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false"
                   aria-expanded="false">
                    <img src="{{url('theme/')}}/assets/images/users/user.png" alt="profile-user" class="rounded-circle">
                    <span class="ml-1 nav-user-name hidden-sm">Ian Dancun <i class="mdi mdi-chevron-down"></i></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right"><a class="dropdown-item" href="javascript:void(0)"><i
                            class="dripicons-user text-muted mr-2"></i> Profile</a>

                    <a class="dropdown-item"
                       href="javascript:void(0)"><i
                            class="dripicons-gear text-muted mr-2"></i> Settings</a>

                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="javascript:void(0)"><i class="dripicons-exit text-muted mr-2"></i>
                        Logout</a></div>
            </li><!--end dropdown-->
            <li class="menu-item"><!-- Mobile menu toggle--> <a class="navbar-toggle nav-link" id="mobileToggle">
                    <div class="lines"><span></span> <span></span> <span></span></div>
                </a><!-- End mobile menu toggle--></li><!--end menu item--></ul><!--end topbar-nav-->
        <ul class="list-unstyled topbar-nav mb-0">
            <li class="hide-phone app-search">
                <form role="search" class=""><input type="text" placeholder="Search..." class="form-control"> <a
                        href="{{url('/')}}"><i class="fas fa-search"></i></a></form>
            </li>
        </ul><!--end topbar-nav--></nav><!-- end navbar--><!-- MENU Start -->
    <div class="navbar-custom-menu">
        <div class="container-fluid">
            <div id="navigation"><!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li class="has-submenu"><a href="{{url('/')}}">
                            <i class="ti-home"></i>
                            <span>Dashboard</span></a>


                    <li class="has-submenu"><a href="javascript:void(0)">
                            <i class="ti-headphone"></i>
                            <span>Tickets</span></a>
                        <ul class="submenu">
                            <li><a href="crm-index.html"><i class="dripicons-monitor"></i>Dashboard</a></li>
                            <li><a href="crm-contacts.html"><i class="dripicons-user-id"></i>Contacts</a></li>
                            <li><a href="crm-opportunities.html"><i class="dripicons-lightbulb"></i>Opportunities</a>
                            </li>
                            <li><a href="crm-leads.html"><i class="dripicons-toggles"></i>Leads</a></li>
                            <li><a href="crm-customers.html"><i class="dripicons-user-group"></i>Customers</a></li>
                        </ul><!--end submenu--></li><!--end has-submenu-->

                </ul>
                <!-- End navigation menu -->
            </div>
            <!-- end navigation -->
        </div>
        <!-- end container-fluid --></div><!-- end navbar-custom --></div><!-- Top Bar End -->
<div class="page-wrapper"><!-- Page Content-->
    <div class="page-content">
        <div class="container-fluid"><!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0);">theme</a></li>
                                <li class="breadcrumb-item"><a href="javascript:void(0);">CRM</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol><!--end breadcrumb--></div><!--end /div--><h4 class="page-title">Dashboard</h4></div>
                    <!--end page-title-box--></div><!--end col--></div><!--end row-->
            <!-- end page title end breadcrumb -->
            <div class="row">
                <div class="col-lg-12 col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <div id="crm_dash_2" class="apex-charts"></div>
                        </div><!--end card-body--></div><!--end card--></div><!-- end col-->
                <div class="col-lg-12 col-xl-4">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card crm-data-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4 align-self-center">
                                            <div class="data-icon"><i
                                                    class="far fa-smile rounded-circle bg-soft-warning"></i></div>
                                        </div><!-- end col-->
                                        <div class="col-8"><h3>63k</h3>
                                            <p class="text-muted font-14 mb-0">Happy Customers</p></div><!-- end col-->
                                    </div><!-- end row--></div><!--end card-body--></div><!--end card--></div>
                        <!-- end col-->
                        <div class="col-md-6">
                            <div class="card crm-data-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4 align-self-center">
                                            <div class="data-icon"><i
                                                    class="far fa-user rounded-circle bg-soft-success"></i></div>
                                        </div><!-- end col-->
                                        <div class="col-8"><h3>10k</h3>
                                            <p class="text-muted font-14 mb-0">New Customers</p></div><!-- end col-->
                                    </div><!-- end row--></div><!--end card-body--></div><!--end card--></div>
                        <!-- end col--></div><!--end row-->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card crm-data-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4 align-self-center">
                                            <div class="data-icon"><i
                                                    class="far fa-handshake rounded-circle bg-soft-secondary"></i></div>
                                        </div><!-- end col-->
                                        <div class="col-8"><h3>720</h3>
                                            <p class="text-muted font-14 mb-0">New Deals</p></div><!-- end col--></div>
                                    <!-- end row--></div><!--end card-body--></div><!--end card--></div><!-- end col-->
                        <div class="col-md-6">
                            <div class="card crm-data-card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4 align-self-center">
                                            <div class="data-icon"><i
                                                    class="far fa-registered rounded-circle bg-soft-pink"></i></div>
                                        </div><!-- end col-->
                                        <div class="col-8"><h3>964</h3>
                                            <p class="text-muted font-14 mb-0">New Register</p></div><!-- end col-->
                                    </div><!-- end row--></div><!--end card-body--></div><!--end card--></div>
                        <!-- end col--></div><!--end row-->
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col-sm-6"><p class="mb-0 text-muted font-13"><i
                                            class="mdi mdi-album mr-2 text-secondary"></i>New Leads</p></div>
                                <!-- end col-->
                                <div class="col-sm-6"><p class="mb-0 text-muted font-13"><i
                                            class="mdi mdi-album mr-2 text-warning"></i>New Leads Target</p></div>
                                <!-- end col--></div><!-- end row-->
                            <div class="progress bg-warning mb-3" style="height:5px;">
                                <div class="progress-bar bg-secondary" role="progressbar" style="width: 65%"
                                     aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <div class="d-flex justify-content-between"><p
                                    class="mb-0 text-muted text-truncate align-self-center"><span
                                        class="text-success"><i class="mdi mdi-trending-up"></i>1.5%</span> Up From Last
                                    Week</p>
                                <button type="button" class="btn btn-outline-info btn-sm">Leads Report</button>
                            </div>
                        </div><!--end card-body--></div><!--end card--></div><!--end col--></div><!--end row-->
            <div class="row">
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body"><h4 class="header-title mt-0 mb-3">Emails Report</h4>
                            <div class="">
                                <div id="d2_performance" class="apex-charts"></div>
                            </div>
                        </div><!--end card-body--></div><!--end card--></div><!--end col-->
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-body"><h4 class="header-title mt-0">Leads By Country</h4>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div id="world-map-markers" class="crm-dash-map"></div>
                                </div><!--end col-->
                                <div class="col-lg-4 align-self-center">
                                    <div class=""><span class="text-secondary">USA</span> <small
                                            class="float-right text-muted ml-3 font-13">81%</small>
                                        <div class="progress mt-2" style="height:3px;">
                                            <div class="progress-bar bg-pink" role="progressbar"
                                                 style="width: 81%; border-radius:5px;" aria-valuenow="81"
                                                 aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div class="mt-3"><span class="text-secondary">Greenland</span> <small
                                            class="float-right text-muted ml-3 font-13">68%</small>
                                        <div class="progress mt-2" style="height:3px;">
                                            <div class="progress-bar bg-secondary" role="progressbar"
                                                 style="width: 68%; border-radius:5px;" aria-valuenow="68"
                                                 aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div class="mt-3"><span class="text-secondary">Australia</span> <small
                                            class="float-right text-muted ml-3 font-13">48%</small>
                                        <div class="progress mt-2" style="height:3px;">
                                            <div class="progress-bar bg-purple" role="progressbar"
                                                 style="width: 48%; border-radius:5px;" aria-valuenow="48"
                                                 aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div class="mt-3"><span class="text-secondary">Brazil</span> <small
                                            class="float-right text-muted ml-3 font-13">32%</small>
                                        <div class="progress mt-2" style="height:3px;">
                                            <div class="progress-bar bg-warning" role="progressbar"
                                                 style="width: 32%; border-radius:5px;" aria-valuenow="32"
                                                 aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                </div><!--end col--></div><!--end row--></div><!--end card-body--></div><!--end card-->
                </div><!--end col--></div><!--end row-->
            <div class="row">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-body"><h4 class="header-title mt-0 mb-3">Leads Report</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th>Lead</th>
                                        <th>Email</th>
                                        <th>Phone No</th>
                                        <th>Company</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr><!--end tr--></thead>
                                    <tbody>
                                    <tr>
                                        <td><img src="{{url('theme/')}}/assets/images/users/user-10.jpg" alt=""
                                                 class="thumb-sm rounded-circle mr-2">Donald Gardner
                                        </td>
                                        <td>xyx@gmail.com</td>
                                        <td>+123456789</td>
                                        <td>Starbucks coffee</td>
                                        <td><span class="badge badge-soft-purple">New Lead</span></td>
                                        <td><a href="javascript:void(0)" class="mr-2"><i
                                                    class="fas fa-edit text-info font-16"></i></a> <a
                                                href="javascript:void(0)"><i
                                                    class="fas fa-trash-alt text-danger font-16"></i></a></td>
                                    </tr><!--end tr-->
                                    <tr>
                                        <td><img src="{{url('theme/')}}/assets/images/users/user-9.jpg" alt=""
                                                 class="thumb-sm rounded-circle mr-2">Matt Rosales
                                        </td>
                                        <td>xyx@gmail.com</td>
                                        <td>+123456789</td>
                                        <td>Mac Donald</td>
                                        <td><span class="badge badge-soft-purple">New Lead</span></td>
                                        <td><a href="javascript:void(0)" class="mr-2"><i
                                                    class="fas fa-edit text-info font-16"></i></a> <a
                                                href="javascript:void(0)"><i
                                                    class="fas fa-trash-alt text-danger font-16"></i></a></td>
                                    </tr><!--end tr-->
                                    <tr>
                                        <td><img src="{{url('theme/')}}/assets/images/users/user-8.jpg" alt=""
                                                 class="thumb-sm rounded-circle mr-2">Michael Hill
                                        </td>
                                        <td>xyx@gmail.com</td>
                                        <td>+123456789</td>
                                        <td>Life Good</td>
                                        <td><span class="badge badge-soft-danger">Lost</span></td>
                                        <td><a href="javascript:void(0)" class="mr-2"><i
                                                    class="fas fa-edit text-info font-16"></i></a> <a
                                                href="javascript:void(0)"><i
                                                    class="fas fa-trash-alt text-danger font-16"></i></a></td>
                                    </tr><!--end tr-->
                                    <tr>
                                        <td><img src="{{url('theme/')}}/assets/images/users/user-7.jpg" alt=""
                                                 class="thumb-sm rounded-circle mr-2">Nancy Flanary
                                        </td>
                                        <td>xyx@gmail.com</td>
                                        <td>+123456789</td>
                                        <td>Flipcart</td>
                                        <td><span class="badge badge-soft-purple">New Lead</span></td>
                                        <td><a href="javascript:void(0)" class="mr-2"><i
                                                    class="fas fa-edit text-info font-16"></i></a> <a
                                                href="javascript:void(0)"><i
                                                    class="fas fa-trash-alt text-danger font-16"></i></a></td>
                                    </tr><!--end tr-->
                                    <tr>
                                        <td><img src="{{url('theme/')}}/assets/images/users/user-6.jpg" alt=""
                                                 class="thumb-sm rounded-circle mr-2">Dorothy Key
                                        </td>
                                        <td>xyx@gmail.com</td>
                                        <td>+123456789</td>
                                        <td>Adidas</td>
                                        <td><span class="badge badge-soft-primary">Follow Up</span></td>
                                        <td><a href="javascript:void(0)" class="mr-2"><i
                                                    class="fas fa-edit text-info font-16"></i></a> <a
                                                href="javascript:void(0)"><i
                                                    class="fas fa-trash-alt text-danger font-16"></i></a></td>
                                    </tr><!--end tr-->
                                    <tr>
                                        <td><img src="{{url('theme/')}}/assets/images/users/user-5.jpg" alt=""
                                                 class="thumb-sm rounded-circle mr-2">Joseph Cross
                                        </td>
                                        <td>xyx@gmail.com</td>
                                        <td>+123456789</td>
                                        <td>Reebok</td>
                                        <td><span class="badge badge-soft-success">Converted</span></td>
                                        <td><a href="javascript:void(0)" class="mr-2"><i
                                                    class="fas fa-edit text-info font-16"></i></a> <a
                                                href="javascript:void(0)"><i
                                                    class="fas fa-trash-alt text-danger font-16"></i></a></td>
                                    </tr><!--end tr--></tbody>
                                </table>
                            </div>
                        </div><!--end card-body--></div><!--end card--></div><!--end col-->
                <div class="col-lg-4">
                    <div class="card">
                        <div class="card-body"><h4 class="header-title mt-0 mb-3">Activity</h4>
                            <div class="slimscroll crm-dash-activity">
                                <div class="activity"><i
                                        class="mdi mdi-checkbox-marked-circle-outline icon-success"></i>
                                    <div class="time-item">
                                        <div class="item-info">
                                            <div class="d-flex justify-content-between align-items-center"><h6
                                                    class="m-0">Task finished</h6><span
                                                    class="text-muted">5 minutes ago</span></div>
                                            <p class="text-muted mt-3">There are many variations of passages of Lorem
                                                Ipsum available, but the majority have suffered alteration. <a
                                                    href="javascript:void(0)" class="text-info">[more info]</a></p>
                                            <div><span class="badge badge-soft-secondary">Design</span> <span
                                                    class="badge badge-soft-secondary">HTML</span> <span
                                                    class="badge badge-soft-secondary">Css</span></div>
                                        </div>
                                    </div>
                                    <i class="mdi mdi-timer-off icon-pink"></i>
                                    <div class="time-item">
                                        <div class="item-info">
                                            <div class="d-flex justify-content-between align-items-center"><h6
                                                    class="m-0">Task Overdue</h6><span
                                                    class="text-muted">30 minutes ago</span></div>
                                            <p class="text-muted mt-3">There are many variations of passages of Lorem
                                                Ipsum available, but the majority have suffered alteration. <a
                                                    href="javascript:void(0)" class="text-info">[more info]</a></p>
                                            <div><span class="badge badge-soft-secondary">Python</span> <span
                                                    class="badge badge-soft-secondary">Django</span></div>
                                        </div>
                                    </div>
                                    <i class="mdi mdi-alert-decagram icon-purple"></i>
                                    <div class="time-item">
                                        <div class="item-info">
                                            <div class="d-flex justify-content-between align-items-center"><h6
                                                    class="m-0">New Task</h6><span
                                                    class="text-muted">50 minutes ago</span></div>
                                            <p class="text-muted mt-3">There are many variations of passages of Lorem
                                                Ipsum available, but the majority have suffered alteration. <a
                                                    href="javascript:void(0)" class="text-info">[more info]</a></p>
                                        </div>
                                    </div>
                                </div><!--end activity--></div><!--end crm-dash-activity--></div><!--end card-body-->
                    </div><!--end card--></div><!--end col--></div><!--end row--></div><!-- container --></div>
    <!-- end page content -->
    <footer class="footer text-center text-sm-left">
        <div class="boxed-footer">&copy; 2019 theme <span class="text-muted d-none d-sm-inline-block float-right">Crafted with <i
                    class="mdi mdi-heart text-danger"></i> by Mannatthemes</span></div>
    </footer><!--end footer--></div><!-- end page-wrapper --><!-- jQuery  -->
<script src="{{url('theme/')}}/assets/js/jquery.min.js"></script>
<script src="{{url('theme/')}}/assets/js/bootstrap.bundle.min.js"></script>
<script src="{{url('theme/')}}/assets/js/metisMenu.min.js"></script>
<script src="{{url('theme/')}}/assets/js/waves.min.js"></script>
<script src="{{url('theme/')}}/assets/js/jquery.slimscroll.min.js"></script>
<script src="{{url('theme/')}}/assets/plugins/moment/moment.js"></script>
<script src="{{url('theme/')}}/assets/plugins/apexcharts/apexcharts.min.js"></script>
<script src="{{url('theme/')}}/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="{{url('theme/')}}/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="{{url('theme/')}}/assets/pages/jquery.crm_dashboard.init.js"></script><!-- App js -->
<script src="{{url('theme/')}}/assets/js/app.js"></script>
</body>
</html>
