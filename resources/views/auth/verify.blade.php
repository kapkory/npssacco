@extends('auth.main')

@section('title')
    Verify Email
@endsection

@section('content')

    <div class="col-md-6 col-12 px-0">
        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
            <div class="card-header pb-1">
                <div class="card-title  text-center">
                    <img class="card-img-top" style="width: auto" src="{{ url('img/calltronix_logo_small.png') }}"
                         height="60" alt="Card image cap">

                    {{--                    <h4 class="text-center mb-2">Login to your account</h4>--}}
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">

                                        <div class="divider">
                                            <div class="divider-text text-uppercase text-muted"><small>Verification</small>
                                            </div>
                                        </div>


                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}<br>
                    {{ __('If you did not receive the email click the email below to request another') }}.
                    <div class="row"></div><br>
                    <div class="row justify-content-center">
                        <a class="btn btn-info btn-sm" href="javascript:void(0)" onclick="event.preventDefault();
                                                     document.getElementById('email_verification_form').submit();">
                            <i class="fa fa-redo"></i> <b>{{ __(' Resend ') }}</b></a>
                    </div>
                    <form id="email_verification_form" style="display: none" method="POST"
                          action="{{ route('verification.resend') }}">
                        @csrf
                    </form>


                </div>
            </div>
        </div>
    </div>

@endsection
