@extends('auth.main')
@section('title')
    Reset Password | Calltronix CRM
@endsection
@section('content')
    <div class="col-md-6 col-12 px-0">
        <div class="card disable-rounded-right d-flex justify-content-center mb-0 p-2 h-100">
            <div class="card-header pb-1">
                <div class="card-title  text-center">
                    <img class="card-img-top" style="width: auto" src="{{url('assets/images/cassavahub-logo.png')}}"
                         height="60" alt="Card image cap">

                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                            <label class="text-bold-600" for="emailaddress">Email address</label>
                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   value="{{ $email ?? old('email') }}" type="email" name="email" id="email"
                                   placeholder="Enter your email" required>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="text-bold-600" for="exampleInputPassword1">New Password</label>
                            <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password" required type="password" id="password"
                                   placeholder="Enter your password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif

                        </div>
                        <div class="form-group mb-2">
                            <label class="text-bold-600" for="exampleInputPassword2">Confirm New
                                Password</label>
                            <input class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                   name="password_confirmation" required type="password" id="password_confirmation"
                                   placeholder="Enter your password again">
                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <button  type="submit" class="btn btn-primary glow position-relative w-100">Reset my
                            password<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
        <div class="card-content">
            <img class="img-fluid" src="{{url('auth')}}/app-assets/images/pages/register.png"
                 alt="branding logo">
        </div>
    </div>
@endsection
