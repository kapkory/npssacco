@if(Request::ajax())
    @if(isset($_GET['t_optimized']))
        @yield('t_optimized')
    @elseif(isset($_GET['ta_optimized']))
        @yield('ta_optimized')
    @else

        <div class="system-title hidden">  @yield('title') </div>
        <section id="aruth-login" class="row flexbox-container">

            <div class="col-xl-8 col-11">
                <div class="card bg-authentication mb-0">
                    <div class="row m-0">
                        <!-- left section-login -->
                    @yield('content')
                    <!-- right section image -->

                    </div>
                </div>
            </div>
        </section>

    @endif
    @include('common.essential_js')
@else
    <!DOCTYPE html>

    <html class="loading" lang="en" data-textdirection="ltr">
    <!-- BEGIN: Head-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <title>@yield('title')</title>
        <link rel="shortcut icon" href="{{url('theme')}}/assets/images/icon.png">

        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700"
              rel="stylesheet">

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/vendors/css/vendors.min.css">
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/css/components.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/css/themes/dark-layout.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/css/themes/semi-dark-layout.min.css">
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" type="text/css"
              href="{{url('auth')}}/app-assets/css/core/menu/menu-types/vertical-menu.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/app-assets/css/pages/authentication.css">
        <!-- END: Page CSS-->
        <!-- jQuery  -->
        <script src="{{ url('theme') }}/assets/js/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" type="text/css" href="{{url('auth')}}/assets/css/style.css">
        <link href="{{ url('backend/assets/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css">
        <!-- END: Custom CSS-->

    </head>
    <!-- END: Head-->

    <!-- BEGIN: Body-->
    <body
        class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page"
        data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body system-container">
                <!-- login page start -->
                <div class="system-title hidden">@yield('title')</div>
                <section id="auth-logxin" class="row flexbox-container">
                    <div class="col-xl-8 col-11">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <!-- left section-login -->
                            @yield('content')
                            <!-- right section image -->

                            </div>
                        </div>
                    </div>
                </section>
                <!-- login page ends -->

            </div>
        </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{url('auth')}}/app-assets/vendors/js/vendors.min.js"></script>
    <script src="{{url('auth')}}/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
    <script src="{{url('auth')}}/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.min.js"></script>
    <script src="{{url('auth')}}/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{url('auth')}}/app-assets/js/scripts/configs/vertical-menu-light.min.js"></script>
    <script src="{{url('auth')}}/app-assets/js/core/app-menu.min.js"></script>
    <script src="{{url('auth')}}/app-assets/js/core/app.min.js"></script>
    <script src="{{url('auth')}}/app-assets/js/scripts/components.min.js"></script>
    <script src="{{url('auth')}}/app-assets/js/scripts/footer.min.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->
    <script src="{{ asset('js/jquery.history.js') }}"></script>
    <script src="{{ url('js/jquery.datetimepicker.js') }}"></script>
    <script src="{{ url('js/jquery.form.js') }}"></script>
    @include('common.javascript')
    @stack('footer-scripts')
    <!-- Custom main Js -->
    <input type="hidden" name="material_page_loaded" value="1">
    <script src="{{ url('sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ url('backend/assets/plugins/select2/select2.min.js') }}"></script>
    </body>
    <!-- END: Body-->
    </html>
@endif
