@extends('auth.main')
@section('title')
    Register | Calltronix CRM
@endsection

@section('content')
    <div class="col-md-12 col-12 px-0">
        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
            <div class="card-header text-center">
                <img class="card-img-top" style="width: auto" src="{{url('assets/images/cassavahub-logo.png')}}"
                     height="60" alt="Card image cap">

            </div>
            <div class="text-center">
                <p><small>Not a member? Create your own account, it takes less than a
                        minute</small>
                </p>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <form method="POST" action="{{ url('register-user') }}" class="ajax-post">
                        @csrf
                        <div class="row">
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="name">First Name</label>
                                <input type="text" class="form-control" name="first_name"
                                       placeholder="First Name">
                            </div>
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="name">Last Name</label>
                                <input type="text" class="form-control" name="last_name"
                                       placeholder="Last Name">
                            </div>
                        </div>
                        <div class="row">


                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="email">Phone number</label>
                                <input name="phone" class="form-control"
                                       placeholder="Phone Number">
                            </div>
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="name">ID Number</label>
                                <input type="text" class="form-control" name="id_number"
                                       placeholder="ID Number">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="email">Email address:</label>
                                <input name="email" class="form-control"
                                       placeholder="Email">
                            </div>
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="email">Occupation</label>
                                <input type="text" class="form-control" name="occupation"
                                       placeholder="Occupation">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="email">County </label>
                                <select name="county_id" class="form-control select2">
                                    <option></option>
                                </select>
                            </div>
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="email">Location</label>
                                <input type="text" class="form-control" name="location"
                                       placeholder="Location">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="password">Password</label>
                                <input type="password" name="password" class="form-control" id="password"
                                       placeholder="Password">
                            </div>
                            <div class="form-group mb-50 col-md-6">
                                <label class="text-bold-600" for="password_confirmation"> Confirm Password</label>
                                <input type="password" name="password_confirmation" id="password_confirmation"
                                       class="form-control"
                                       placeholder="Confirm Password">
                            </div>
                        </div>

                        <div class="row col-md-12 text-center">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary glow position-relative submit submit-btn">
                                    REGISTER <i class="bx bx-right-arrow-alt"></i></button>
                            </div>
                        </div>
                    </form>
                    <hr>
                    <div class="text-center"><small class="mr-25">Already have an account?</small><a
                            class="load-page" href="{{url('login')}}"><small>Sign in</small> </a></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            $('.select2').select2();
            autoFillSelect('county_id', '{{ url('counties/list?all=1') }}')
        })
    </script>

@endsection
