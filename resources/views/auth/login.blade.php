@extends('auth.main')

@section('title')
    Login | NPS Sacco
@endsection
@section('content')
    <div class="col-md-6 col-12 px-0">
        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
            <div class="card-header pb-1">
                <div class="card-title  text-center">
                    <img class="card-img-top" style="width: auto" src="{{url('assets/images/cassavahub-logo.png')}}" height="60" alt="Card image cap">


                    {{--                    <h4 class="text-center mb-2">Login to your account</h4>--}}
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="divider">
                        <div class="divider-text text-uppercase text-muted"><small> login with
                                email/employee no.</small>
                        </div>
                    </div>
                    <form class="loginfdorm" method="POST" action="{{ url('login-user') }}">
                        {{--                        d3ymcd8KCGyVRNCsSsPYtx1UGPpqxeexC7YHHV2T--}}
                        {{--                                                <input type="hidden" name="_token" value="">--}}
                        @csrf
                        @if (session('message'))
                            <div class="alert alert-danger m-t-10 small">
                                {!! session('message') !!}
                            </div>
                        @endif

                        <div class="form-group mb-50">
                            <label class="text-bold-600" for="username">Email / Employee no.</label>&nbsp;
                            <input name="username" type="text"
                                   class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}"
                                   placeholder="Email /Employee no." value="{{ old('username') }}">
                            @if ($errors->has('username'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
              </span>
                            @endif

                        </div>
                        <div class="form-group">
                            <label class="text-bold-600" for="password">Password</label>
                            <input name="password" type="password"
                                   class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
              </span>
                            @endif
                        </div>
                        <div
                            class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">
                            <div class="text-left">
                                <div class="checkbox checkbox-sm">
                                    <input type="checkbox" class="form-check-input" name="remember"
                                           id="checkbox-signin" {{ old('remember') ? 'checked' : '' }} >
                                    <label class="checkboxsmall" for="checkbox-signin"><small>Keep me logged
                                            in</small></label>
                                </div>
                            </div>
                            <div class="text-right">
                                @if (Route::has('password.request'))
                                    <a href="{{ route('password.request') }}"
                                       class="card-link load-page"><small>Forgot Password?</small></a>
                                @endif
                            </div>
                        </div>
                        <button  type="submit"
                                class="btn btn-primary glow w-100 position-relative submit submit-btn">
                            <b>Login</b><i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                    </form>
                    <hr>
                    <div class="text-center"><small class="mr-25">Don't have an account?</small> <br>
                        <small>Please <a href="{{url('register')}}" class="load-page">register</a> to
                            be a member</small>
                        {{--                        <a class="load-page" href="{{ url('register') }}"><small>Sign up</small></a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
        <div class="card-content">
            <img class="img-fluid" src="{{url('auth')}}/app-assets/images/pages/login.png"
                 alt="branding logo">
        </div>
    </div>


    <script type="text/javascript">
        $(function () {

            {{--            @if($notice = request()->session()->get('notice'))--}}
            {{--            @if($notice['type'] == 'warning')--}}
            {{--            toastr.warning('{{ $notice['message'] }}');--}}
            {{--            @elseif($notice['type'] == 'info')--}}
            {{--            toastr.info('{{ $notice['message'] }}');--}}
            {{--            @elseif($notice['type'] == 'error')--}}
            {{--            toastr.error('{{ $notice['message'] }}');--}}
            {{--            @elseif($notice['type'] == 'success')--}}
            {{--            toastr.info('{{ $notice['message'] }}');--}}
            {{--            @endif--}}
            {{--            @endif--}}
        });

        function validateEmailAddress() {

            var mail_input = $("input[name='username']").val();
            var mailformat = /^[A-Za-z0-9\.]*@(calltronix)[.](com)$/;

            if (mail_input.includes('@')) {
                // if (mail_input.includes('@calltronix.co.ke')) {
                //     let new_email = mail_input.replace("calltronix.co.ke", "calltronix.com");
                //     $("input[name='username']").val(new_email);
                //     $("input[name='username']").focus();
                //     console.log("Updated:" + mail_input)
                // }
                // if (mail_input.match(mailformat)) {
                //     $("input[name='username']").focus();
                //     $(this).find('.help-block').remove();
                //     $(this).closest(".form-group").removeClass('is-invalid');
                //     $(this).removeClass("is-invalid");
                //     $(this).closest(".invalid-feedback").remove();
                //     return true;
                // } else {
                //     $("input[name='username']").addClass('is-invalid');
                //     $("input[name='username']").closest(".form-group").find('.help-block').remove();
                //     $("input[name='username']").closest(".form-group").append('<small class="help-block invalid-feedback">Invalid email address</small>');
                //     $("input[name='username']").focus();
                //     return false;
                // }
            }
            return true;
        }

        {{--$(".loginform").submit(function (e) {--}}
        {{--    e.preventDefault(); // avoid to execute the actual submit of the form.--}}
        {{--    var form = $(this);--}}
        {{--    var btn = form.find(".submit-btn");--}}
        {{--    btn.prepend('<img class="processing-submit-image" style="height: 50px;margin:-10px !important;" src="{{ url("img/Ripple.gif") }}">');--}}
        {{--    btn.attr('disabled', true);--}}
        {{--    var url = form.attr('action');--}}

        {{--    $.ajax({--}}
        {{--        type: "POST",--}}
        {{--        url: url,--}}
        {{--        data: form.serialize(), // serializes the form's elements.--}}
        {{--        success: function (response) {--}}
        {{--            resetForm('loginform');--}}
        {{--            toastr.success('Login Successful..');--}}

        {{--            console.log("Login Response: " + response)--}}
        {{--            return false;--}}


        {{--            var btn = form.find(".submit-btn");--}}
        {{--            btn.find('img').remove();--}}
        {{--            btn.attr('disabled', false);--}}
        {{--            endLoading(response);--}}
        {{--            removeError();--}}
        {{--            if (response.call_back) {--}}
        {{--                window[response.call_back](response);--}}
        {{--                return false;--}}
        {{--            }--}}
        {{--            if (response.redirect) {--}}
        {{--                if (material_active == 1) {--}}
        {{--                    setTimeout(function () {--}}
        {{--                        var s_class = undefined;--}}
        {{--                        var params = getQueryParams(response.redirect);--}}
        {{--                        if (params.ta_optimized) {--}}
        {{--                            s_class = 'bootstrap_table';--}}
        {{--                        } else if (params.t_optimized) {--}}
        {{--                            s_class = 'ajax_tab_content';--}}
        {{--                        }--}}
        {{--                        ajaxLoad(response.redirect, s_class);--}}
        {{--                    }, 1300);--}}
        {{--                } else {--}}
        {{--                    window.location.href = response.redirect;--}}
        {{--                }--}}

        {{--            } else if (response.force_redirect) {--}}
        {{--                setTimeout(function () {--}}
        {{--                    window.location.href = response.force_redirect;--}}
        {{--                }, 1300);--}}
        {{--            } else {--}}
        {{--                let current_url = '{{ url()->current() }}';--}}
        {{--                try {--}}
        {{--                    return runAfterSubmit(response);--}}
        {{--                } catch (err) {--}}
        {{--                    setTimeout(function () {--}}
        {{--                        // window.location.href = current_url;--}}
        {{--                    }, 1300);--}}
        {{--                }--}}
        {{--            }--}}


        {{--        },--}}
        {{--        error: function (xhr, status, error) {--}}
        {{--            var btn = form.find(".submit-btn");--}}
        {{--            btn.find('img').remove();--}}
        {{--            btn.attr('disabled', false);--}}
        {{--            if (xhr.status == 422) {--}}
        {{--                form.find('.alert_status').remove();--}}
        {{--                var response = JSON.parse(xhr.responseText).errors;--}}
        {{--                for (field in response) {--}}
        {{--                    form.find("input[name='" + field + "']").addClass('is-invalid');--}}
        {{--                    form.find("input[name='" + field + "']").closest(".form-group").find('.help-block').remove();--}}
        {{--                    form.find("input[name='" + field + "']").closest(".form-group").append('<small class="help-block invalid-feedback">' + response[field] + '</small>');--}}

        {{--                    form.find("select[name='" + field + "']").addClass('is-invalid');--}}
        {{--                    form.find("select[name='" + field + "']").closest(".form-group").find('.help-block').remove();--}}
        {{--                    form.find("select[name='" + field + "']").closest(".form-group").append('<small class="help-block invalid-feedback">' + response[field] + '</small>');--}}

        {{--                    form.find("textarea[name='" + field + "']").addClass('is-invalid');--}}
        {{--                    form.find("textarea[name='" + field + "']").closest(".form-group").find('.help-block').remove();--}}
        {{--                    form.find("textarea[name='" + field + "']").closest(".form-group").append('<small class="help-block invalid-feedback">' + response[field] + '</small>');--}}
        {{--                }--}}

        {{--                jQuery(".invalid-feedback").css('display', 'block');--}}
        {{--            } else if (xhr.status == 406) {--}}
        {{--                form.find('#form-exception').remove();--}}
        {{--                form.find('.alert_status').remove();--}}
        {{--                form.prepend('<div id="form-exception" class="alert alert-warning"><strong>' + xhr.status + '</strong> ' + error + '<br/>' + xhr.responseText + '</div>');--}}
        {{--            } else {--}}
        {{--                form.find('#form-exception').remove();--}}
        {{--                form.find('.alert_status').remove();--}}
        {{--                form.prepend('<div id="form-exception" class="alert alert-danger"><strong>' + xhr.status + '</strong> ' + error + '<br/>(' + url + ')</div>');--}}
        {{--            }--}}

        {{--        },--}}
        {{--    });--}}
        {{--});--}}

        jQuery(document).on('click', '.is-invalid', function () {
            $(this).removeClass("is-invalid");
            $(this).closest(".invalid-feedback").remove();
        });
        jQuery(document).on('change', '.is-invalid', function () {
            $(this).removeClass("is-invalid");
            $(this).closest(".invalid-feedback").remove();
        });
        jQuery(document).on('click', '.form-group', function () {
            $(this).find('.help-block').remove();
            $(this).closest(".form-group").removeClass('is-invalid');
        });
        jQuery(document).on('click', '.form-control', function () {
            $(this).find('.help-block').remove();
            $(this).closest(".form-group").removeClass('is-invalid');
        });

        function resetForm(form_class) {
            $("." + form_class).find("input[type=text],textarea,select").val("");
            $("input[name='id']").val('');
        }

        function removeError() {
            setTimeout(function () {
                $("#form-exception").fadeOut();
                $("#form-success").fadeOut();
                $(".alert_status").fadeOut();
            }, 1200);

        }

    </script>
@endsection
