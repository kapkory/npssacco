@extends('layouts.admin')

@section('title')
    Dispositions for {{$issue_category->name}}
@endsection

@section('bread_crumb')
    <a href="{{ url('admin/settings/config') }}" class="breadcrumb-item load-page"> Tickets</a>
    <a href="{{ url('admin/settings/config/issuecategories') }}" class="breadcrumb-item load-page"> Issue Categories</a>
    <span class="breadcrumb-item active">Dispositions</span>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row col-md-12 mb-2">
                        <a href="#disposition_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
                                class="fa fa-plus"></i> ADD DISPOSITION</a>
                    </div>
                    <form method="post" class="ajax-post"
                          action="{{ url('admin/settings/tickets/issuecategories/dispositions') }}">
                        @csrf
                        <input type="hidden" name="issue_category_id" value="{{$issue_category->id}}">
                        <input type="hidden" name="issue_category_id" value="{{$issue_category->id}}">
                        @foreach($dispositions->chunk(3) as  $chunk)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group mb-3 mb-md-2">
                                        @foreach ($chunk as $disposition)
                                            <div class="form-check col-md-3  form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="disposition_ids[]" value="{{$disposition->id}}"
                                                           class="  @if(in_array($disposition->id,$existing))form-check-input-styled-success @else form-check-input-styled @endif"
                                                           @if(in_array($disposition->id,$existing))checked @endif  data-fouc>
                                                    {{$disposition->name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        @endforeach

                        <div class="text-right mr-5">
                            <button type="submit" class="btn btn-sm btn-success btn-labeled btn-labeled-left submit-btn text-uppercase"><b><i
                                        class="icon-paperplane"></i></b>
                                @if(!empty($existing)) Update @else Submit @endif
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @include('common.auto_modal',[
     'modal_id'=>'disposition_modal',
     'modal_title'=>'DISPOSITION FORM',
     'modal_content'=>autoForm(['name','description', 'form_model'=>\App\Models\Core\Disposition::class],"admin/settings/config/dispositions")
 ])
    <script>
        $(function () {
            autoFillSelect('issue_sub_category_id', '{{ url('admin/settings/config/issuesubcategories/list?all=1') }}')

            $(".form-control-min-search").select2({
                minimumResultsForSearch: Infinity
            });
            $("select[name='issue_sub_category_id']").select2({
                dropdownParent: $("#disposition_modal")
            });
            // Default initialization
            $('.form-check-input-styled').uniform();
            // Success
            $('.form-check-input-styled-success').uniform({
                wrapperClass: 'border-success text-success'
            });
        })
    </script>
@endsection


