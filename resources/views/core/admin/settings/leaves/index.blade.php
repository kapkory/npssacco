@extends('layouts.admin')

@section('bread_crumb')
    <span class="breadcrumb-item active">Leaves Configs</span>
@endsection

@section('title') Leaves Configs @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
                   'tabs'=>['leave_types','department_approval_levels'],
                   'tabs_folder'=>'core.admin.settings.leaves.tabs',
                   'base_url'=>'admin/settings/leaves'
                   ])
                </div>
            </div>
        </div>
    </div>
@endsection


