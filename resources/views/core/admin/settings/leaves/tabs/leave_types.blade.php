<a href="#leavetype_modal"  class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD LEAVE TYPE</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","Entitlement","Is_paid","users.name"=>"created_by","action"],
'data_url'=>'admin/settings/config/leavetypes/list',
'base_tbl'=>'leave_types'
])

    @include('common.auto_modal',[
         'modal_id'=>'leavetype_modal',
         'modal_title'=>'LEAVE TYPE FORM',
         'modal_content'=>autoForm(['name','description','entitlement','is_paid', 'form_model'=>\App\Models\Core\LeaveType::class],"admin/settings/config/leavetypes")
     ])
<script >
    $(".label_entitlement").append(' (No. of days)')
</script>
