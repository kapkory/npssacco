@extends('layouts.admin')

@section('title') Counties @endsection

@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <a href="#county_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD COUNTY</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["id",,"action"],
                    'data_url'=>'admin/settings/counties/list',
                    'base_tbl'=>'counties'
                    ])
                 </div>

               </div>

            </div>
        </div>
@endsection

@section('sidebar')
  @include('common.auto_modal',[
        'modal_id'=>'county_modal',
        'modal_title'=>'COUNTY FORM',
        'modal_content'=>autoForm(\App\Models\Core\County::class,"admin/settings/counties")
    ])
@endsection

