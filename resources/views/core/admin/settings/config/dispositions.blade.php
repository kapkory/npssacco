{{--{{dd($dispositions)}}--}}

<div class="form-group mb-0 ">
    <label for="issue_category_id" class="ml-2"><h5>Dispositions</h5></label>
    <div class="col-md-12">
        @foreach($dispositions->chunk(4) as $chunk)
            <div class="row">
                @foreach($chunk as $disposition)

                    <div class="form-check-inline checkbox col-md-3 my-2" style="margin-right:0;">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="disposition_ids[]" value="{{$disposition->id}}"  @if(in_array($disposition->id,$existing))checked @endif  class="custom-control-input" id="{{$disposition->id}}"
                                   data-parsley-multiple="groups"
                                   data-parsley-mincheck="2">
                            <label class="custom-control-label" for="{{$disposition->id}}">{{$disposition->name}}</label>
                        </div>
                    </div>

                @endforeach
            </div>


        @endforeach


    </div>
</div>
