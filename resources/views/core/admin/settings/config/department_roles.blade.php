@extends('layouts.admin')

@section('bread_crumb')
    <a href="{{ url('admin/settings/hrm?tab=departments') }}" class="breadcrumb-item load-page"> Departments</a>
    <span class="breadcrumb-item active">Roles</span>
@endsection

@section('title') {{$department->name}} Roles @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <a href="#department_role_modal" class="btn btn-info btn-sm clear-form float-right"
                       data-toggle="modal"><i
                            class="fa fa-plus"></i> ADD DEPARTMENT ROLE</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["departments.name"=>"department","staff_roles.name"=>"department_role","action"],
                    'data_url'=>'admin/settings/config/departments/roles/list/'.$department->id,
                    'base_tbl'=>'staff_roles'
                    ])

                    @include('common.auto_modal',[
                         'modal_id'=>'department_role_modal',
                         'modal_title'=>'DEPARTMENT ROLE FORM',
                         'modal_content'=>autoForm(['name', 'hidden_department_id', 'form_model'=>\App\Models\Core\StaffRole::class],"admin/settings/config/departments/roles")
                     ])
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function(){
            $("input[name='department_id']").val(`{{$department->id}}`)
        })

    </script>
@endsection



