@extends('layouts.admin')

@section('title')
  Ticket Dipositions {{$department->name}}
@endsection

@section('bread_crumb')
{{--    <a href="{{ url('admin/tickets') }}" class="breadcrumb-item load-page"> Tickets</a>--}}
    <li class="breadcrumb-item active">Ticket Dispositions</li>
@endsection
@section('content')
    <style type="text/css">
        /*.form-check-input-styled-success::before {*/
        /*    border-color: #58e2c4;*/
        /*    background-color: #58e2c4;*/
        /*}*/

    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body"><h4 class="header-title mt-0"></h4>

                    <form method="POST" class="ajax-post" action="{{url('admin/settings/config/issuecategories/dispositions/save')}}">
                        @csrf
                        <input type="hidden" name="" value="{{$department->id}}">
                        <input type="hidden" name="form_model" value="{{\App\Models\Core\CalltronixDepartment::class}}">


                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="issue_category_id" >Issue Type</label>
                                <select onchange="return fetchDispositions($(this).val())" class="select2 form-control custom-select" name="issue_category_id"
                                        style="width: 100%;">

                                </select>
                            </div>

                        </div>

                        <div class="dispositions">


                        </div>

                        <button type="submit" class="btn btn-primary submit-btn"><i class="fa fa-save"></i> Submit
                        </button>

                    </form>
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>
    <script type="text/javascript">

        $(function () {
            $('.select2').select2();
            $(".form-control-min-search").select2({
                minimumResultsForSearch: Infinity
            });
            autoFillSelect('disposition_id', '{{ url('admin/settings/config/dispositions/list?all=1') }}');
            autoFillSelect('issue_category_id', '{{ url('admin/settings/config/issuecategories/list?all=1') }}');

        });
        function fetchDispositions(id) {
            var department_id = `{{$department->id}}`
            ajaxLoad('{{ url("admin/settings/config/issuecategories/dispositions/ajax/get") }}?issue_category_id=' + id +'&department_id='+department_id, 'dispositions')

        }

    </script>
@endsection
