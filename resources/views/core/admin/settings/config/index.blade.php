@extends('layouts.admin')

@section('bread_crumb')
    <span class="breadcrumb-item active">Picklists Configurations</span>
@endsection

@section('title') Picklists Configurations @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
       'tabs'=>['departments'],
       'tabs_folder'=>'core.admin.settings.config.tabs',
       'base_url'=>'admin/settings/config'
       ])
                </div>
            </div>
        </div>
    </div>
@endsection


