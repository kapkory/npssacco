<a href="#issuecategory_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD ISSUE CATEGORY</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","action"],
'data_url'=>'admin/settings/config/issuecategories/list',
'base_tbl'=>'issue_categories'
])

@include('common.auto_modal',[
     'modal_id'=>'issuecategory_modal',
     'modal_title'=>'ISSUE CATEGORY FORM',
     'modal_content'=>autoForm(\App\Models\Core\IssueCategory::class,"admin/settings/config/issuecategories")
 ])
