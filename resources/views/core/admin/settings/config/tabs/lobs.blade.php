<a href="#lineofbusiness_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD LOB</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","users.name"=>"account_manager","action"],
'data_url'=>'admin/settings/config/lineofbusiness/list',
'base_tbl'=>'line_of_businesses'
])

@include('common.auto_modal',[
     'modal_id'=>'lineofbusiness_modal',
     'modal_title'=>'LINE OF BUSINESS FORM',
     'modal_content'=>autoForm(['name','account_manager_user_id', 'form_model'=>\App\Models\Core\LineOfBusiness::class],"admin/settings/config/lineofbusiness")
 ])

<script type="text/javascript">
    $(function () {
        $("select[name='account_manager_user_id']").addClass('select2')
        $(".select2").select2()
        autoFillSelect('account_manager_user_id','{{ url('admin/users/list?all=1') }}')
        $(".label_account_manager_user_id").html('Account Manager')
    })
</script>
