@if(Request::ajax())
    <script type="text/javascript">
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
        $(".form-control-select2").select2();

        $("select[name='route_id']").select2();
    </script>

@endif
<script src="{{ url('limitless/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script src="{{ url('limitless/global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
<script src="{{url('limitless/')}}/global_assets/js/plugins/forms/styling/switch.min.js"></script>
{{--    <script src="{{url('limitless/')}}/global_assets/js/demo_pages/form_checkboxes_radios.js"></script>--}}



<a href="#disposition_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD DISPOSITION</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","users.name"=>"created_by","action"],
'data_url'=>'admin/settings/config/dispositions/list',
'base_tbl'=>'dispositions'
])

    @include('common.auto_modal',[
         'modal_id'=>'disposition_modal',
         'modal_title'=>'DISPOSITION FORM',
         'modal_content'=>autoForm(['name','description', 'form_model'=>\App\Models\Core\Disposition::class],"admin/settings/config/dispositions")
     ])
<script type="text/javascript">
    var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));

    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
    $(function () {

        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
        $("select[name='issue_sub_category_id']").select2({
            dropdownParent: $("#disposition_modal")
        });
        // Default initialization
    })

</script>
