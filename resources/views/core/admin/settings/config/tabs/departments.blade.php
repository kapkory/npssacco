<a href="javascript:void(0)" class="btn btn-info btn-sm clear-form float-right hd-body"><i class="fa fa-plus"></i> ADD DEPARTMENT</a>

{{--<a href="#department_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i--}}
{{--        class="fa fa-plus"></i> ADD DEPARTMENT</a>--}}

@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","departments.sub_departments_count"=>"sub_departments","staff_count","action"],
'data_url'=>'admin/settings/config/departments/list',
'base_tbl'=>'departments'
])


@section('sidebar')
    @include('common.auto_modal_sidebar',[
         'modal_title'=>'DEPARTMENT FORM',
         'modal_content'=>autoForm(['name','description','department_code','parent_department_id','form_model'=>App\Models\Core\CalltronixDepartment::class],"admin/settings/config/departments")
     ])
@endsection


<script type="text/javascript">
    $(function () {
        $("select[name='parent_department_id']").addClass('select2')
        $(".select2").select2()
        autoFillSelect('parent_department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');
        $(".label_parent_department_id").html('Parent Department')
    })
</script>
