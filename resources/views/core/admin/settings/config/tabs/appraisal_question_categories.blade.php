<a href="#appraisalquestioncategory_modal"  class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD CATEGORY</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","description","users.name"=>"created_by","Created_at","action"],
'data_url'=>'admin/settings/config/appraisalquestioncategories/list',
'base_tbl'=>'appraisal_question_categories'
])

    @include('common.auto_modal',[
         'modal_id'=>'appraisalquestioncategory_modal',
         'modal_title'=>'QUESTION CATEGORY FORM',
         'modal_content'=>autoForm(['name','description', 'form_model'=>\App\Models\Core\AppraisalQuestionCategory::class],"admin/settings/config/appraisalquestioncategories")
     ])
<script >
    // $(".label_entitlement").append(' (No. of days)')
</script>
