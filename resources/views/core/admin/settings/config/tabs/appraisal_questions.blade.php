<a href="#appraisalquestion_modal"  class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD QUESTION</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["question","appraisal_question_categories.name"=>"category","position","Created_at","users.name"=>"created_by","action"],
'data_url'=>'admin/settings/config/appraisalquestions/list',
'base_tbl'=>'appraisal_questions'
])

    @include('common.auto_modal',[
         'modal_id'=>'appraisalquestion_modal',
         'modal_title'=>'APPRAISAL QUESTION FORM',
         'modal_content'=>autoForm(['category_id','question','position', 'form_model'=>\App\Models\Core\AppraisalQuestion::class],"admin/settings/config/appraisalquestions")
     ])
<script >
    $(function(){
        autoFillSelect('category_id','{{ url('admin/settings/config/appraisalquestioncategories/list?all=1') }}')
        $(".label_category_id").html('Question Category')
    })
</script>
