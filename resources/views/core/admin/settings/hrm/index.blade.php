@extends('layouts.admin')

@section('bread_crumb')
    <span class="breadcrumb-item active">HRM  Configurations</span>
@endsection

@section('title') HRM Picklists Configurations @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
       'tabs'=>['positions', 'departments', 'department_heads' ],
       'tabs_folder'=>'core.admin.settings.hrm.tabs',
       'base_url'=>'admin/settings/hrm'
       ])
                </div>
            </div>
        </div>
    </div>
@endsection


