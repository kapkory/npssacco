<a href="#department_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD DEPARTMENT</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","department_code","action"],
'data_url'=>'admin/settings/config/departments/list',
'base_tbl'=>'departments'
])

@include('common.auto_modal',[
     'modal_id'=>'department_modal',
     'modal_title'=>'DEPARTMENT FORM',
     'modal_content'=>autoForm(["name","department_code","description","form_model"=>\App\Models\Core\Department::class],"admin/settings/config/departments")
 ])
