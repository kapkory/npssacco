<a href="#position_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD POSITION</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","description","action"],
'data_url'=>'admin/settings/hrm/positions/list',
'base_tbl'=>'positions'
])

@include('common.auto_modal',[
    'modal_id'=>'position_modal',
    'modal_title'=>'POSITION FORM',
    'modal_content'=>autoForm(['name', 'description', 'form_model'=>\App\Models\Core\Position::class],"admin/settings/hrm/positions")
])
</div>
