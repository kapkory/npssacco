
<a href="#department_hod_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD HOD</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["department","head","action"],
'data_url'=>'admin/settings/config/departments/heads/list',
'base_tbl'=>'department_heads'
])

@include('common.auto_modal',[
     'modal_id'=>'department_hod_modal',
     'modal_title'=>'DEPARTMENT HEAD FORM',
     'modal_content'=>autoForm(['department_id', 'user_id', 'form_model'=>\App\Models\Core\DepartmentHead::class],"admin/settings/config/departments/heads")
 ])
<script type="text/javascript">


    $(function () {
        $(".label_department_id").html('Department');
        $(".label_user_id").html('Department Head');
        $("select[name='department_id']").select2({
            dropdownParent: $("#department_hod_modal")
        });
        $("select[name='user_id']").select2({
            dropdownParent: $("#department_hod_modal")
        });
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
        {{--autoFillSelect('disposition_id', '{{ url('admin/settings/config/dispositions/list?all=1') }}');--}}
        autoFillSelect('department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');
        autoFillSelect('user_id', '{{ url('admin/users/list?all=1') }}');
    });


</script>
