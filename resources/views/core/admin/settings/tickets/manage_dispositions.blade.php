@extends('layouts.admin')

@section('title')
    Dispositions for {{$issue_category->name}}
@endsection

@section('bread_crumb')
    <a href="{{ url('admin/settings/config') }}" class="breadcrumb-item load-page"> Tickets</a>
    <a href="{{ url('admin/settings/config/issuecategories') }}" class="breadcrumb-item load-page"> Issue Categories</a>
    <span class="breadcrumb-item active">Dispositions</span>
@endsection
@section('content')
    <style type="text/css">
        /*.form-check-input-styled-success::before {*/
        /*    border-color: #58e2c4;*/
        /*    background-color: #58e2c4;*/
        /*}*/
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: calc(1.5em + 0.75rem + 2px);
        }

    </style>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body"><h4 class="header-title mt-0"></h4>

                    <form method="POST" class="ajax-post" action="{{url('admin/settings/tickets/issuecategories/dispositions')}}">
                        @csrf
                        <input type="hidden" name="issue_category_id" value="{{$issue_category->id}}">
                        <input type="hidden" name="form_model" value="{{\App\Models\Core\CalltronixDepartment::class}}">
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="issue_sub_category_id" >Issue Sub Category</label>
                                <select onchange="return fetchDispositions($(this).val())" class="select2 form-control custom-select" name="issue_sub_category_id">

                                </select>
                            </div>

                        </div>

                        <div class="dispositions">


                        </div>

                        <button type="submit" class="btn btn-primary submit-btn"><i class="fa fa-save"></i> Submit
                        </button>

                    </form>
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>
    <script type="text/javascript">

        $(function () {
            $('.select2').select2();
            // $(".form-control-min-search").select2({
            //     minimumResultsForSearch: Infinity
            // });
            fetchDispositions(`{{@$sub_category_id}}`)
            autoFillSelect('disposition_id', '{{ url('admin/settings/config/dispositions/list?all=1') }}');
            autoFillSelect('issue_sub_category_id', '{{ url('admin/settings/tickets/issuesubcategories/list?all=1') }}', 'setSelectedSubCategory');

        });
        function fetchDispositions(id) {
            var issue_category_id = `{{$issue_category->id}}`
            ajaxLoad('{{ url("admin/settings/tickets/issuecategories/dispositions/ajax/get") }}?issue_category_id=' + issue_category_id +'&issue_sub_category_id='+id, 'dispositions')
        }
        function setSelectedSubCategory(){
            $("select[name='issue_sub_category_id']").val(`{{@$sub_category_id}}`)
        }
    </script>
@endsection
