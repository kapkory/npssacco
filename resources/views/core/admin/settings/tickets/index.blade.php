@extends('layouts.admin')

@section('bread_crumb')
    <span class="breadcrumb-item active">Ticket Configs</span>
@endsection

@section('title') Ticket Configs @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
                   'tabs'=>['statuses','issue_categories','issue_sub_categories','dispositions','issue_sources', 'branches'],
                   'tabs_folder'=>'core.admin.settings.tickets.tabs',
                   'base_url'=>'admin/settings/tickets'
                   ])
                </div>
            </div>
        </div>
    </div>
@endsection


