<a href="#ticketstatus_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD TICKET STATUS</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name", "action"],
'data_url'=>'admin/settings/tickets/statuses/list',
'base_tbl'=>'ticket_statuses'
])

@include('common.auto_modal',[
    'modal_id'=>'ticketstatus_modal',
    'modal_title'=>'TICKET STATUS FORM',
    'modal_content'=> autoForm(['name', 'form_model'=>\App\Models\Core\TicketStatus::class],"admin/settings/tickets/statuses")
])




