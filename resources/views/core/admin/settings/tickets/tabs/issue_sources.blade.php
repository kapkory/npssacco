
<a href="#issuesource_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD ISSUE SOURCE</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name", "action"],
'data_url'=>'admin/settings/tickets/issuesources/list',
'base_tbl'=>'issue_sources'
])

@include('common.auto_modal',[
    'modal_id'=>'issuesource_modal',
    'modal_title'=>'ISSUE SOURCE FORM',
    'modal_content'=> autoForm(['name', 'form_model'=>\App\Models\Core\IssueSource::class],"admin/settings/tickets/issuesources")
])



