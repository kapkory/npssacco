<a href="#branch_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD BRANCH</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","action"],
'data_url'=>'admin/settings/tickets/branches/list',
'base_tbl'=>'branches'
])

@include('common.auto_modal',[
    'modal_id'=>'branch_modal',
    'modal_title'=>'BRANCH FORM',
    'modal_content'=>autoForm(['name','form_model'=>\App\Models\Core\Branch::class],"admin/settings/tickets/branches")
])

