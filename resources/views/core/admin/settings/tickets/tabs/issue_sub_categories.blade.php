

@section('title')
    Issue Sub Categories
@endsection

@section('bread_crumb')
    <a href="{{ url('admin/settings/config') }}" class="breadcrumb-item load-page"> Settings</a>
    <span class="breadcrumb-item active"> Issue SubCategories</span>
@endsection

<a href="#issue_sub_category_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
        class="fa fa-plus"></i> ADD ISSUE SUBCATEGORY</a>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","action"],
'data_url'=>'admin/settings/tickets/issuesubcategories/list',
'base_tbl'=>'issue_sub_categories'
])

@include('common.auto_modal',[
     'modal_id'=>'issue_sub_category_modal',
     'modal_title'=>'ISSUE SUB CATEGORY FORM',
     'modal_content'=>autoForm(['name', 'form_model'=>\App\Models\Core\IssueSubCategory::class],"admin/settings/tickets/issuesubcategories")
 ])




