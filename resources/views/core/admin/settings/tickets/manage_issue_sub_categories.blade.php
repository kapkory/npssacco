@extends('layouts.admin')

@section('bread_crumb')
    <a href="{{ url('admin/settings/tickets') }}" class="breadcrumb-item load-page"> Ticket Config</a>
    <a href="{{ url('admin/settings/tickets?tab=issue_categories') }}" class="breadcrumb-item load-page"> Issue Categories</a>
    <span class="breadcrumb-item active">Issue Sub Categories</span>
@endsection

@section('title')
    Issue Sub Subcategories for {{$issue_category->name}}
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row col-md-12 mb-3">
                        <a href="#issue_sub_category_modal" class="btn btn-info btn-sm clear-form float-right"
                           data-toggle="modal"><i
                                class="fa fa-plus"></i> ADD ISSUE SUBCATEGORY</a>
                    </div>
                    <form method="post" class="ajax-post"
                          action="{{ url('admin/settings/tickets/issuecategories/issuesubcategories') }}">
                        @csrf
                        <input type="hidden" name="issue_category_id" value="{{$issue_category->id}}">

                        @foreach($issue_sub_categories->chunk(3) as  $chunk)

                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group mb-3 mb-md-2">
                                        @foreach ($chunk as $issue_sub_category)
                                            <div class="form-check col-md-3  form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="checkbox" name="issue_sub_categories_ids[]"
                                                           value="{{$issue_sub_category->id}}"
                                                           class="  @if(in_array($issue_sub_category->id,$existing))form-check-input-styled-success @else form-check-input-styled @endif"
                                                           @if(in_array($issue_sub_category->id,$existing))checked
                                                           @endif  data-fouc>
                                                    {{$issue_sub_category->name}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                        @endforeach


                        <div class="text-right mr-5">
                            <button type="submit"
                                    class="btn btn-sm btn-success btn-labeled btn-labeled-left submit-btn text-uppercase"><b><i
                                        class="icon-paperplane"></i></b>
                                @if(!empty($existing)) Update @else Submit @endif
                            </button>
                        </div>
                    </form>

                    @include('common.auto_modal',[
                         'modal_id'=>'issue_sub_category_modal',
                         'modal_title'=>'ISSUE SUB CATEGORY FORM',
                         'modal_content'=>autoForm(['name', 'form_model'=>\App\Models\Core\IssueSubCategory::class],"admin/settings/tickets/issuesubcategories")
                     ])
                </div>
            </div>
        </div>
    </div>
@endsection


