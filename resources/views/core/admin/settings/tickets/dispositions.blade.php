@foreach($dispositions->chunk(3) as  $chunk)
    <div class="row">

        <div class="form-group mb-3 mb-md-2 col-md-12">
            @foreach ($chunk as $disposition)
                <div class="form-check col-md-3  form-check-inline"
                     style="margin-left: 0!important; margin-right: 0!important;">
                    <label class="form-check-label">
                        <input type="checkbox" name="disposition_ids[]" value="{{$disposition->id}}"
                               class="  @if(in_array($disposition->id,$existing))form-check-input-styled-success @else form-check-input-styled @endif"
                               @if(in_array($disposition->id,$existing))checked @endif  data-fouc>
                        {{$disposition->name}}
                    </label>
                </div>
            @endforeach
        </div>


    </div>
@endforeach
