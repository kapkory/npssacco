@extends('layouts.admin')

@section('bread_crumb')
    <a href="{{ url('admin/staffs') }}" class="breadcrumb-item load-page"> Staffs</a>
    <span class="breadcrumb-item active">Create</span>
@endsection

@section('title') Create Staff @endsection

@section('content')
    {{--    @push('header-scripts')--}}
    <link href="{{url('theme')}}/plugins/dropify/css/dropify.min.css" rel="stylesheet">
    {{--    @endpush--}}
    {{--    @push('footer-scripts')--}}
    <script src="{{url('theme')}}/plugins/dropify/js/dropify.min.js"></script>
    {{--    @endpush--}}

    <div class="row">
        <div class="col-md-12">
            <div class="card dr-pro-pic">
                <div class="card-body">
                    <form method="POST" enctype="multipart/form-data" action="{{url('admin/staffs')}}"
                          class="ajax-post file_form">
                        @csrf
                        <input type="hidden" name="form_model" value="{{\App\Models\Core\Staff::class}}">
                        {{--                        <div class="card-box form-group">--}}
                        {{--                            <input type="file"  name="passport" id="input-file-now-custom-1"--}}
                        {{--                                   class="dropify">--}}
                        {{--                            --}}{{--                            <span>Passport</span>--}}
                        {{--                        </div>--}}
                        <fieldset>
                            <legend>Staff Information</legend>

                            <div class="row">
                                <div class=" form-group col-md-4">
                                    <label for="firstname">First Name <span class="text-danger"> *</span></label>
                                    <input type="text" placeholder="First Name" class="form-control" name="firstname"
                                           id="firstname">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="middlename">Middle Name</label>
                                    <input type="text" placeholder="Middle Name" class="form-control" name="middlename"
                                           id="middlename">
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="middlename">Last Name <span class="text-danger"> *</span></label>
                                    <input type="text" placeholder="Last Name" class="form-control" name="lastname"
                                           id="middlename">
                                </div>
                            </div>
                            <div class=" row">
                                <div class="form-group col-md-4">
                                    <label for="gender">Gender <span class="text-danger"> *</span></label>
                                    <select name="gender" class="form-control">
                                        <option value="">Please select ...</option>
                                        <option value="1">Male</option>
                                        <option value="2">Female</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="dob">Date of Birth</label>
                                    <input type="text" value="" placeholder="Date of Birth"
                                           autocomplete="off" onkeydown="return false" class="form-control dob"
                                           name="dob" id="dob">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="id_number">ID Number <span class="text-danger"> *</span></label>
                                    <input type="number" placeholder="ID Number" class="form-control"
                                           name="id_number" id="id_number">
                                </div>
                            </div>
                            <div class=" row">
                                <div class="form-group col-md-4">
                                    <label for="gender">Marital Status</label>
                                    <select name="marital_status" class="form-control">
                                        <option value="" selected>Please Select..</option>
                                        <option value="1">Married</option>
                                        <option value="2">Single</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="employment_date">Date of Employment <span class="text-danger"> *</span></label>
                                    <input type="text" placeholder="Date of Employment"
                                           class="form-control datepicker" name="employment_date" id="employment_date">
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="position_id">Position Of Employment <span class="text-danger"> *</span></label>
                                    <select name="position_id" class="select2 form-control">
                                        <option value="" selected>Please Select..</option>
                                    </select>
                                </div>
                            </div>
                            <div class=" row">
                                <div class="form-group col-md-4">
                                    <label for="calltronix_department_id">Department <span class="text-danger"> *</span></label>
                                    <select onchange="return  getDepartmentRoles($(this).val())"
                                            name="calltronix_department_id" class="form-control select2">
                                        <option value="" selected>Please Select..</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="staff_role_id">Role <span class="text-danger"> *</span></label>
                                    <select name="staff_role_id" class="select2 form-control">
                                        <option value="" selected>Please Select..</option>
                                    </select>
                                </div>
                                <div class="form-group  col-md-4">
                                    <label for="line_of_business_id">Line of Business <span class="text-danger"> *</span></label>
                                    <select class="select2 form-control custom-select" name="line_of_business_id"
                                            style="width: 100%;">

                                    </select>
                                </div>

                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Contact Information</legend>


                            <div class=" row">
                                <div class="form-group col-md-4">
                                    <label for="email">Email Address <span class="text-danger"> *</span></label>
                                    <input type="email" placeholder="Email" class="form-control"
                                           name="email" id="email">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="email">Phone Number</label>
                                    <input type="text" placeholder="Phone" class="form-control"
                                           name="phone" id="phone">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="alternate_phone">Alternate Phone </label>
                                    <input type="text" placeholder="Alternate Phone Number" class="form-control"
                                           name="alternate_phone" id="alternate_phone">
                                </div>
                            </div>
                            <div class=" row">
                                <div class="form-group col-md-6">
                                    <label for="postal_address">Postal Address</label>
                                    <input type="text" placeholder="Postal Address" class="form-control"
                                           name="postal_address" id="postal_address">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="residential_physical_address">Residential Physical Address</label>
                                    <input type="text" placeholder="Area of Residence" class="form-control"
                                           name="residential_physical_address" id="residential_physical_address">
                                </div>

                            </div>

                        </fieldset>
                        <fieldset>
                            <legend>Statutory Documents</legend>
                            <div class=" row">
                                <div class="form-group col-md-4">
                                    <label for="bank_name">Bank Name</label>
                                    <input type="text" placeholder="Bank Name" class="form-control"
                                           name="bank_name" id="bank_name">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="bank_branch">Bank Branch</label>
                                    <input type="text" placeholder="Bank Branch" class="form-control"
                                           name="bank_branch" id="bank_branch">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="account_number">Account Number</label>
                                    <input type="text" placeholder="Account Number" class="form-control"
                                           name="account_number" id="account_number">
                                </div>

                            </div>
                            <div class=" row">
                                <div class="form-group col-md-4">
                                    <label for="kra_pin">KRA PIN</label>
                                    <input type="text" placeholder="KRA PIN" class="form-control"
                                           name="kra_pin" id="kra_pin">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="nhif_number">NHIF Number</label>
                                    <input type="text" placeholder="NHIF Number" class="form-control"
                                           name="nhif_number" id="nhif_number">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="nssf_number">NSSF Number</label>
                                    <input type="text" placeholder="NSSF Number" class="form-control"
                                           name="nssf_number" id="nssf_number">
                                </div>

                            </div>
                            <div class=" row">
{{--                                <div class="form-group col-md-4">--}}
{{--                                    <label for="employee_number">Employee Number</label>--}}
{{--                                    <input type="text" placeholder="Employee Number" class="form-control"--}}
{{--                                           name="employee_number" id="postal_address">--}}
{{--                                </div>--}}
                                <div class="form-group col-md-4">
                                    <label for="pay_roll_number">Payroll Number</label>
                                    <input type="text" placeholder="Payroll Number" class="form-control"
                                           name="pay_roll_number" id="pay_roll_number">
                                </div>

                            </div>

                        </fieldset>
                        <fieldset>
                            <legend>Next of Kin Details</legend>
                            <div id="nok_details_div" class="row col-md-12">
                                <div class=" row">
                                    <div class="form-group col-md-4">
                                        <label for="nok_name">Name</label>
                                        <input type="text" placeholder="Name" class="form-control"
                                               name="nok_name[]" id="nok_name">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="Nok_phone">Contact</label>
                                        <input type="text" placeholder="7xxxxxxxx" class="form-control"
                                               name="nok_phone[]" id="Nok_phone">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="relationship">Relationship</label>
                                        <input type="text" placeholder="Enter Relationship" class="form-control"
                                               name="nok_relationship[]" id="relationship">
                                    </div>

                                </div>
                            </div>
                            <button onclick=" return addRow()" style="line-height: 15px;" type="button"
                                    class="badge badge-custom btn btn-sm btn-secondary">Add Row <i
                                    class="fa fa-plus"></i></button>


                        </fieldset>
                        <fieldset>
                            <legend>Spouse / Children <small>(optional)</small></legend>
                            <div id="spouse_children_details_div" class="row col-md-12">
                                <div class=" row">
                                    <div class="form-group col-md-4">
                                        <label for="s_or_c_name">Name</label>
                                        <input type="text" placeholder="Name" class="form-control"
                                               name="s_or_c_name[]" id="s_or_c_name">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="s_or_c_phone">Contact</label>
                                        <input type="text" placeholder="7xxxxxxxx" class="form-control"
                                               name="s_or_c_phone[]" id="s_or_c_phone">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="s_or_c_relationship">Relationship</label>
                                        <select style="width: 100%;" name="s_or_c_relationship[]" id="s_or_c_relationship"
                                                class="form-control">
                                            <option value="">Please select ...</option>
                                            <option value="Spouse">Spouse</option>
                                            <option value="Child">Child</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <button onclick=" return addSpouseRow()" style="line-height: 15px;" type="button"
                                    class="badge badge-custom btn btn-sm btn-secondary">Add Row <i
                                    class="fa fa-plus"></i></button>

                        </fieldset>
                        <fieldset>
                            <legend>Beneficiary <small>(optional)</small></legend>
                            <div id="beneficiary_details_div" class="row col-md-12">
                                <div class=" row">
                                    <div class="form-group col-md-4">
                                        <label for="beneficiary_name">Name</label>
                                        <input type="text" placeholder="Name" class="form-control"
                                               name="beneficiary_name[]" id="beneficiary_name[]">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="beneficiary_phone">Contact</label>
                                        <input type="text" placeholder="7xxxxxxxx" class="form-control"
                                               name="beneficiary_phone[]" id="beneficiary_phone">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="beneficiary_relationship">Relationship</label>
                                        <input type="text" placeholder="Relationship" class="form-control"
                                               name="beneficiary_relationship[]" id="beneficiary_relationship">
                                    </div>

                                </div>
                            </div>
                            <button onclick=" return addBeneficiaryRow()" style="line-height: 15px;" type="button"
                                    class="mb-2 badge badge-custom btn btn-sm btn-secondary">Add Row <i
                                    class="fa fa-plus"></i></button>

                        </fieldset>
                        <div class="form-group">
                            <label for="medical_history">Medical History <small>(If Any)</small></label>
                            <textarea rows="5" placeholder="Enter medical history if any..." name="medical_history"
                                      class="form-control"></textarea>
                            <button class="btn submit-btn btn-primary btn-sm text-light mt-3 float-left mb-0"><i
                                    class="fa fa-save"></i> Submit
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function getDepartmentRoles(id) {
            autoFillSelect('staff_role_id', '{{ url("admin/settings/config/departments/roles/list/") }}' + '/' + id + '?all=1');
        }

        // $('.dob').val(" ");
        $(function () {

            $('.dob').daterangepicker({
                showDropdowns: !0,
                // autoUpdateInput: false,
                minYear: 1960,
                maxYear: 2001,
                singleDatePicker: !0,
                locale: {
                    format: "YYYY-MM-DD",
                }
            });

            $('.datepicker').daterangepicker({
                singleDatePicker: !0,
                locale: {format: "YYYY-MM-DD"}

            });
            // $('.dropify').dropify();
            $('.select2').select2();
            $(".form-control-min-search").select2({
                minimumResultsForSearch: Infinity
            });
            autoFillSelect('calltronix_department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');
            autoFillSelect('line_of_business_id', '{{ url('admin/settings/config/lineofbusiness/list?all=1') }}');
            autoFillSelect('position_id', '{{ url('admin/settings/hrm/positions/list?all=1') }}');
        })

        function addRow() {
            let num = Math.round(111 + Math.random() * (99999));
            console.log("Random no: " + num)
            let row_id = 'row_' + num;

            $("#nok_details_div").append('<div id="' + row_id + '" class="row col-md-12">\n' +
                '                    <div class="col-md-12 row">\n' +
                '                    <div class="col-md-11 row" style="padding-left: 9px; padding-right: 2px;">\n' +
                '                       <div class=" form-group col-md-4" style="padding-left: 2px; padding-right: 2px;">\n' +
                '                           <label for="nok_name">Name</label>\n' +
                '                           <input type="text" placeholder="Name" class="form-control" name="nok_name[]" id="nok_name">\n' +
                '                       </div> \n' +
                '                       <div class=" form-group  col-md-4" style="padding-left: 2px; padding-right: 2px;"> \n' +
                '                           <label for="Nok_phone">Contact</label>\n' +
                '                           <input type="text" placeholder="7xxxxxxxx" class="form-control"  name="nok_phone[]" id="Nok_phone"> \n' +
                '                       </div> \n' +
                '                       <div class="form-group  col-md-4" style="padding-left: 0; padding-right: 0"> \n' +
                '                           <label for="relationship">Relationship</label> \n' +
                '                           <input type="text" placeholder="Enter Relationship" class="form-control"  name="nok_relationship[]" id="relationship"> \n' +
                '                       </div> \n' +

                '                    </div> \n' +
                '                    <div class="col-md-1">\n' +
                '                    <div class=" float-right mt-4">\n' +
                '                        <div class="form-group">\n' +
                '                    <button type="button" onclick="deleteRow(`' + row_id + '`)" class="btn-sm btn btn-danger "><i class="fa fa-times"></i></button>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    </div>\n' +
                '                    </div>\n' +
                '\n' +
                '                </div>');


        }

        function addSpouseRow() {

            let num = Math.round(111 + Math.random() * (99999));
            console.log("Random no: " + num)
            let row_id = 'row_' + num;

            $("#spouse_children_details_div").append('<div id="' + row_id + '" class="row col-md-12">\n' +
                '                    <div class="col-md-12 row " >\n' +
                '                    <div class=" row col-md-11" style="padding-left: 9px; padding-right: 2px;">\n' +
                '                       <div class="form-group col-md-4" style="padding-left: 2px; padding-right: 2px;">\n' +
                '                           <label for="s_or_c_name">Name</label>\n' +
                '                           <input type="text" placeholder="Name" class="form-control" name="s_or_c_name[]" id="s_or_c_name">\n' +
                '                       </div> \n' +
                '                       <div class="form-group col-md-4" style="padding-left: 2px; padding-right: 2px;"> \n' +
                '                           <label for="s_or_c_phone">Contact</label>\n' +
                '                           <input type="text" placeholder="7xxxxxxxx" class="form-control"  name="s_or_c_phone[]" id="s_or_c_phone"> \n' +
                '                       </div> \n' +
                '                       <div class="form-group col-md-4" style="padding-left: 2px; padding-right: 2px;"> \n' +
                '                           <label for="relationship">Relationship</label> \n' +
                '                             <select style="width: 100%;" name="s_or_c_relationship[]" id="s_or_c_relationship" class="form-control"> \n' +
                '                              <option value="">Please select ...</option> \n' +
                '                            <option value="Spouse">Spouse</option> \n' +
                '                            <option value="Child">Child</option> \n' +
                '                            </select> \n' +
                '                       </div> \n' +

                '                    </div> \n' +
                '                    <div class="col-md-1">\n' +
                '                    <div class=" float-right mt-4">\n' +
                '                        <div class="form-group">\n' +
                '                    <button type="button" onclick="deleteRow(`' + row_id + '`)" class="btn-sm btn btn-danger "><i class="fa fa-times"></i></button>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    </div>\n' +
                '                    </div>\n' +
                '\n' +
                '                </div>');

        }

        function addBeneficiaryRow() {

            let num = Math.round(111 + Math.random() * (99999));
            console.log("Random no: " + num)
            let row_id = 'row_' + num;

            $("#beneficiary_details_div").append('<div id="' + row_id + '" class="row col-md-12">\n' +
                '                    <div class="col-md-12 row">\n' +
                '                    <div class=" col-md-11 row"  style="padding-left: 9px; padding-right: 2px;">\n' +
                '                       <div class="form-group col-md-4" style="padding-left: 2px; padding-right: 2px;">\n' +
                '                           <label for="nok_name">Name</label>\n' +
                '                           <input type="text" placeholder="Name" class="form-control" name="beneficiary_name[]" id="beneficiary_name">\n' +
                '                       </div> \n' +
                '                       <div class=" form-group col-md-4"  style="padding-left: 2px; padding-right: 2px;"> \n' +
                '                           <label for="Nok_phone">Contact</label>\n' +
                '                           <input type="text" placeholder="7xxxxxxxx" class="form-control"  name="beneficiary_phone[]" id="beneficiary_phone"> \n' +
                '                       </div> \n' +
                '                       <div class="form-group col-md-4"  style="padding-left: 2px; padding-right: 2px;"> \n' +
                '                           <label for="relationship">Relationship</label> \n' +
                '                           <input type="text" placeholder="Enter Relationship" class="form-control"  name="beneficiary_relationship[]" id="beneficiary_relationship"> \n' +
                '                       </div> \n' +

                '                    </div> \n' +
                '                    <div class="col-md-1"  style="padding-left: 2px; padding-right: 2px;">\n' +
                '                    <div class=" float-right mt-4">\n' +
                '                        <div class="form-group">\n' +
                '                    <button type="button" onclick="deleteRow(`' + row_id + '`)" class="btn-sm btn btn-danger "><i class="fa fa-times"></i></button>\n' +
                '                        </div>\n' +
                '                    </div>\n' +
                '                    </div>\n' +
                '                    </div>\n' +
                '\n' +
                '                </div>');


        }


        function deleteRow(row_id) {
            $("#" + row_id).remove()
        }

    </script>
@endsection


