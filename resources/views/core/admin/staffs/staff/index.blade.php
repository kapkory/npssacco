@extends('layouts.admin')

@section('bread_crumb')

    <a href="{{ url('admin/staffs') }}" class="breadcrumb-item load-page"> Staff</a>
    <span class="breadcrumb-item active">Staff #{{$staff->id}}</span>
@endsection

@section('title'){{$staff->name}} @endsection

@section('content')

    <div class="col-md-12 col-lg-12">
        <div class="card ">
            <div class="card-body">
                <?php
                $tabs = ['staff_details'];
                if(userCan('update_staff_profile'))
                    $tabs = ['staff_details','contact_information','statutory_documents', 'beneficiaries', 'documents','leaves_history'];

                    ?>
{{--                <a href="{{url('admin/staffs/create')}}"--}}
{{--                   class="btn btn-info btn-sm clear-form float-right load-page"><i--}}
{{--                        class="fa fa-plus"></i> NEW STAFF</a>--}}

                <div class="mt-1">
                    @include('common.auto_tabs',[
         'tabs'=>@$tabs,
         'tabs_folder'=>'core.admin.staffs.staff.tabs',
         'base_url'=>'admin/staffs/staff/'.$staff->id
         ])
                </div>

            </div><!--end card-body-->
        </div><!--end card-->
    </div>


@endsection


