@if(Request::ajax())
    <script type="text/javascript">
        $(".select2").select2();
        $(".update-select2").select2();
    </script>

@endif
{{--{{ dd($staff) }}--}}
<div class="col-md-12 row ">
    <form action="{{url('admin/staffs/staff/update-profile/'.$staff->id)}}" class="mt-2 ajax-post col-md-12">
        @csrf
        <fieldset>
            <input type="hidden" name="id" value="{{@$staff->id}}">
            <input type="hidden" name="user_id" value="{{@$staff->user_id}}">
            <input type="hidden" name="form_model" value="{{\App\Models\Core\Staff::class}}">

            <div class="row">
                <div class="form-group col-4">
                    <label> First Name: <span class="text-danger">*</span></label>
                    <input type="text" name="firstname" class="form-control" value="{{ @$staff->firstname }}">
                </div>
                <div class="form-group col-4">
                    <label> Middle Name: </label>
                    <input type="text" name="middlename" class="form-control" value="{{ @$staff->middlename }}">
                </div>
                <div class="form-group col-4">
                    <label> Last Name: <span class="text-danger">*</span></label>
                    <input type="text" name="lastname" class="form-control" value="{{ @$staff->lastname }}">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <label> National Id No.: <span class="text-danger">*</span></label>
                    <input type="text" name="id_number" class="form-control" value="{{ @$staff->id_number }}">
                </div>
                <div class="form-group col-4">
                    <label> Employment Date: <span class="text-danger">*</span></label>
                    <input type="text" name="employment_date" class="form-control date_field"
                           value="{{ @$staff->employment_date }}">
                </div>
                <div class="form-group col-4">
                    <label> Date of Birth: <span class="text-danger">*</span></label>
                    <input type="text" name="dob" class="form-control dob" value="{{ @$staff->dob }}">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-4">
                    <label> Position: <span class="text-danger">*</span></label>
                    <select name="position_id" class="form-control select2">
                        <option selected>Please Select..</option>
                    </select>
                </div>
                <div class="form-group col-4">
                    <label for="line_of_business_id">Line of Business</label>
                    <select class="select2 form-control custom-select" name="line_of_business_id"
                            style="width: 100%;">
                    </select>
                </div>
                <div class="form-group col-4">
                    <label> Department: <span class="text-danger">*</span></label>
                    <select name="calltronix_department_id" class="form-control select2">
                        <option value="" selected>Please Select..</option>
                    </select>
                </div>

            </div>


            {{--            <div class="row">--}}
            {{--                <div class="form-group col-4">--}}
            {{--                    <label> Email Address: <span class="text-danger">*</span></label>--}}
            {{--                    <input type="text" name="email" class="form-control" value="{{ @$staff->email }}">--}}
            {{--                </div>--}}
            {{--                <div class="form-group col-4">--}}
            {{--                    <label> National Id No.: <span class="text-danger">*</span></label>--}}
            {{--                    <input type="text" name="id_number" class="form-control" value="{{ @$staff->id_number }}">--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="row">--}}
            {{--                <div class="form-group col-4">--}}
            {{--                    <label> Phone Number: <span class="text-danger">*</span></label>--}}
            {{--                    <input type="text" name="phone" class="form-control" value="{{ @$staff->phone }}">--}}
            {{--                </div>--}}
            {{--                <div class="form-group col-4">--}}
            {{--                    <label> Alternate Phone number: <span class="text-danger">*</span></label>--}}
            {{--                    <input type="text" name="alternate_phone" class="form-control" value="{{ @$staff->alternate_phone }}">--}}
            {{--                </div>--}}
            {{--            </div>--}}

            <div class="row">
                <div class="form-group col-4">
                    <label> Role: </label>
                    <select name="staff_role_id" class="select2 form-control">
                        <option value="" selected>Please Select..</option>
                    </select>
                </div>
                <div class="form-group col-4">
                    <label> Gender: <span class="text-danger">*</span></label>
                    <select disabled name="gender" class="form-control select2">
                        <option value="" selected>Please Select..</option>
                        <option {{ ($staff->gender == 1) ? "selected" : " " }} value="1">Male</option>
                        <option {{ ($staff->gender == 2) ? "selected" : " " }} value="2">Female</option>
                    </select>
                </div>
                <div class="form-group col-4">
                    <label> Marital Status: </label>
                    <select name="marital_status" class="form-control select2">
                        <option value="" selected>Please Select..</option>
                        <option {{ ($staff->marital_status == 1) ? "selected" : " " }} value="1">Married</option>
                        <option {{ ($staff->marital_status == 2) ? "selected" : " " }} value="2">Single</option>
                    </select>
                </div>

            </div>

        </fieldset>

        <div class="text-right">
            <button type="submit" class="btn btn-md btn-info submit-btn"><i class="fa fa-save "></i> Submit</button>
        </div>
    </form>
</div>
@include('common.auto_modal_lg_static',[
'modal_id'=>'more_ticket_info_modal',
'modal_title'=>'Ticket Details',
'modal_content'=>'<div class="ticket_details row " ></div>'
])

<script type="text/javascript">
    $("#toggle_files_section").click(function () {
        $("#upload_files_section").toggle('slow', function () {
        });
    });
    window.url = "{{ url('/') }}";

    function getMoreDetails(id) {
        $(".modal-dialog").addClass('modal-lg')
        ajaxLoad('{{ url("admin/tickets/ticket/details") }}/' + id, 'ticket_details')
    }

    var call_history = $("#call-history");
    $(function () {
        $(".update-select2").select2({
            dropdownParent: $("#more_ticket_info_modal")
        });

        $('.select2').select2();
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
        autoFillSelect('calltronix_department_id', '{{ url("admin/settings/config/departments/list?all=1") }}', 'setSelectedDepartment');
        autoFillSelect('line_of_business_id', '{{ url('admin/settings/config/lineofbusiness/list?all=1') }}', 'setSelectedLineOfBusiness');
        autoFillSelect('position_id', '{{ url('admin/settings/hrm/positions/list?all=1') }}', 'setSelectedPosition');
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
        $('.dob').daterangepicker({
            showDropdowns: !0,
            // autoUpdateInput: false,
            minYear: 1960,
            maxYear: 2001,
            singleDatePicker: !0,
            locale: {
                format: "YYYY-MM-DD",
            }
        });
        $('.date_field').daterangepicker({
            showDropdowns: !0,
            // autoUpdateInput: false,
            minYear: 1960,
            maxYear: {{ date('Y') }},
            singleDatePicker: !0,
            locale: {
                format: "YYYY-MM-DD",
            }
        });

        $("select[name='calltronix_department_id']").on('change', function (e) {
            $("select[name='line_of_business_id']").val(null).trigger('change');
            department_id = $(this).val();
            getDepartmentRoles(department_id)
        });
    })

    function setSelectedDepartment() {
        $("select[name='calltronix_department_id']").val('{{@$staff->calltronix_department_id }}').trigger('change');
    }

    function setSelectedLineOfBusiness() {
        $("select[name='line_of_business_id']").val('{{@$staff->line_of_business_id }}').trigger('change');
    }

    function setSelectedPosition() {
        $("select[name='position_id']").val('{{@$staff->position_id }}').trigger('change');
    }

    function getDepartmentRoles(id) {
        autoFillSelect('staff_role_id', '{{ url("admin/settings/config/departments/roles/list/") }}' + '/' + id + '?all=1', 'setSelectedRole');
    }

    function setSelectedRole() {
        $("select[name='staff_role_id']").val('{{@$staff->staff_role_id }}').trigger('change');
    }
</script>
