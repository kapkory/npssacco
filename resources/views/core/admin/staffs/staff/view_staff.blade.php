@extends('layouts.admin')

@section('bread_crumb')

    <a href="{{ url('admin/staffs') }}" class="breadcrumb-item load-page"> Staff</a>
    <span class="breadcrumb-item active">Staff #{{$staff->id}}</span>
@endsection

@section('title'){{$staff->name}} @endsection

@section('content')
    <?php
    $tabs = ['staff_details'];
    if(userCan('update_staff_profile'))
        $tabs = ['staff_details','contact_information','statutory_documents', 'beneficiaries', 'documents','leaves_history'];

    $tabs_folder ='core.admin.staffs.staff.tabs';

    $base_url='admin/staffs/staff/'.$staff->id;
    if($tab == ''){
        $tab = $tabs[0];
    }
    ?>

    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-lg-3">
            <div class="card user-card user-card-1">
                <div class="card-body pb-0">
                    <div class="float-right">
                        <span class="badge badge-light-danger">Pro</span>
                    </div>
                    <div class="media user-about-block align-items-center mt-0 mb-3">
                        <div class="position-relative d-inline-block">
                            <img class="img-radius img-fluid wid-80" src="{{ url('storage/profile-pics/user.jpg') }}" alt="User image">
                            <div class="certificated-badge">
                                <i class="fas fa-certificate text-primary bg-icon"></i>
                                <i class="fas fa-check front-icon text-white"></i>
                            </div>
                        </div>
                        <div class="media-body ml-3">
                            <h6 class="mb-1">Suzen</h6>
                            <p class="mb-0 text-muted">UI/UX Designer</p>
                        </div>
                    </div>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <span class="f-w-500"><i class="feather icon-mail m-r-10"></i>Email</span>
                        <a href="mailto:demo@sample" class="float-right text-body">demo@sample.com</a>
                    </li>
                    <li class="list-group-item">
                        <span class="f-w-500"><i class="feather icon-phone-call m-r-10"></i>Phone</span>
                        <a href="#" class="float-right text-body">(+99) 9999 999 999</a>
                    </li>
                    <li class="list-group-item border-bottom-0">
                        <span class="f-w-500"><i class="feather icon-map-pin m-r-10"></i>Location</span>
                        <span class="float-right">Melbourne</span>
                    </li>
                </ul>
                <div class="card-body">
                    <div class="row text-center">
                        <div class="col">
                            <h6 class="mb-1">37</h6>
                            <p class="mb-0">Mails</p>
                        </div>
                        <div class="col border-left">
                            <h6 class="mb-1">2749</h6>
                            <p class="mb-0">Followers</p>
                        </div>
                        <div class="col border-left">
                            <h6 class="mb-1">678</h6>
                            <p class="mb-0">Following</p>
                        </div>
                    </div>
                </div>

                <div class="nav flex-column nav-pills list-group list-group-flush list-pills" id="user-set-tab" role="tablist" aria-orientation="vertical">
                    <div class="card-header">
                        <h5 class="text-muted" >User Menu</h5>
                    </div>
                    @foreach($tabs as $single_tab)
                        <a class="nav-link list-group-item list-group-item-action auto-tab {{ $tab==$single_tab ? 'active':'' }} tab_{{ $single_tab }}" href="{{ url($base_url."?tab=".$single_tab) }}"  onclick="return ajaxLoad('{{ url($base_url."?tab=".$single_tab) }}&t_optimized=1','ajax_tab_content','tab_{{ $single_tab }}')">
                            <span class="f-w-500"><i class="feather icon-user m-r-10 h5 "></i>{{ ucwords(str_replace('_',' ',$single_tab)) }} <span class="{{ 'tab_badge_'.$single_tab }}"></span></span>
                            <span class="float-right"><i class="feather icon-chevron-right"></i></span>
                        </a>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="tab-content" id="user-set-tabContent">
                <div class="tab-pane fade show active" id="user-set-profile" role="tabpanel" aria-labelledby="user-set-profile-tab">

                    <div class="card">
                        <div class="card-header">
                            <h5><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user icon-svg-primary wid-20"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg><span class="p-l-5">
                                    {{ ucwords(str_replace('_',' ',$tab)) }}
                                </span></h5>
                        </div>
                        <div class="card-body">
                            @if(!isset($_GET['t_optimized']) || Request::ajax() == false)
                                @include($tabs_folder.".".$tab)
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- [ sample-page ] end -->
    </div>



@endsection


