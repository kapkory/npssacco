<div class="card-body staff_details1 row">
    <table class="table col-md-12">
        <tbody>

        <a href="#" onclick="return editStaffStatutoryDocs()" data-toggle="modal">
            <button type="submit" style="margin-left: .635rem" class="btn btn-outline btn-outline-primary btn-sm submit-btn"><i
                    class="fa fa-edit"></i> Update
            </button>
        </a>
        <tr>
            <th class="font-weight-bold">Bank Name:</th>
            <td id="type">{{ $staff->bank_name}}</td>
            <th class="font-weight-bold">Bank Branch:</th>
            <td id="type">{{ $staff->bank_branch }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Account Number:</th>
            <td id="type">{{ $staff->account_number}}</td>
            <th class="font-weight-bold">KRA PIN:</th>
            <td id="type">{{ $staff->kra_pin }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> NHIF Number:</th>
            <td id="type">{{ $staff->nhif_number}}</td>
            <th class="font-weight-bold">NSSF Number:</th>
            <td id="type">{{ $staff->nssf_number }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Employee Number:</th>
            <td id="type">{{ $staff->employee_number}}</td>
            <th class="font-weight-bold">Payroll Number:</th>
            <td id="type">{{ $staff->pay_roll_number }}</td>
        </tr>
        </tbody>
    </table>
</div>
@include('common.auto_modal_static',[
    'modal_id'=>'edit_staff_statutory_docs_info_modal',
    'modal_title'=>'UPDATE STAFF DETAILS',
    'modal_content'=>autoForm(['bank_name','bank_branch','account_number','kra_pin','nhif_number','nssf_number','hidden_user_id'=>$staff->user_id,'hidden_id'=>$staff->id,'form_model'=>\App\Models\Core\Staff::class],'admin/staffs/staff/update-statutorydocs/'.$staff->id)
    ])

<script>
    function editStaffStatutoryDocs(){
        $("input[name='bank_name']").val('{{ $staff->bank_name }}')
        $("input[name='bank_branch']").val('{{ $staff->bank_branch }}')
        $("input[name='account_number']").val('{{ $staff->account_number }}')
        $("input[name='kra_pin']").val('{{ $staff->kra_pin }}')
        $("input[name='nhif_number']").val('{{ $staff->nhif_number }}')
        $("input[name='nssf_number']").val('{{ $staff->nssf_number }}')
        $("#edit_staff_statutory_docs_info_modal").modal('show')
    }
</script>
