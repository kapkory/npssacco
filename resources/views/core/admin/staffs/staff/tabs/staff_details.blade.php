{{--Originally Answered: What is the meaning of " first name " and " last name " ?--}}
{{--In conventional usage in English and in nearly all Western countries:—--}}

{{--“first name” = the 1st forename = the 1st given name--}}
{{--“middle name” = the 2nd forename = the 2nd given name--}}
{{--“last name” = surname = the hereditary family name--}}
<div class="card-body staff_details1 row">
    @if(userCan('update_staff_profile'))
        <div class="col-12 row">
            <div class="col-md-3 float-left">
                <div class="card-img-actions d-inline-block">
                    @php
                        $path = 'storage/profile-pics';
                        $has_image = false;
                        if (@getimagesize($path . "/" . $staff->avatar)){
                            $image = $staff->avatar;
                            $has_image = true;
                        }
                        else
                            $image = "user.jpg";
                    @endphp
                    <img style="width: 150px; height: 150px;" class="img-fluid rounded-circle"
                         src="{{ asset($path."/".$image) }}" alt="Profile image">
                    <div class="col-md-12">
                        <button href="#update_profile_picture" data-toggle="modal"
                                class="btn btn-info btn-sm">
                            <i class="fa fa-edit"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <table class="table col-md-12">
        <tbody>
        @if(userCan('update_staff_profile'))
            <a href="#edit_staff_info_modal" onclick="return updatestaffProfile(`{{ $staff->id }}`)"
               data-toggle="modal">
                <button type="submit" style="margin-left: .635rem"
                        class="btn btn-outline btn-outline-primary btn-sm submit-btn"><i
                        class="fa fa-edit"></i> Update
                </button>
            </a>
            <a href="javascript:void(0)" onclick="return updatestaff(`{{$staff->id}}`)" data-toggle="modal">
                <button type="submit" style="margin-left: .635rem"
                        class="btn btn-outline-secondary btn waves-effect waves-light btn-sm "><i
                        class="fa fa-envelope "></i> Send Email
                </button>
            </a>
        @endif
        @if(getStaffEmploymentStatus($staff->employment_status) !== "dismissed" )
            @if(userCan('discharge_staff'))
                <a href="javascript:void(0)" onclick="return dischargeStaff(`{{$staff->id}}`)" data-toggle="modal">
                    <button type="submit" style="margin-left: .635rem"
                            class="btn btn-outline-info btn waves-effect waves-light btn-sm "><i
                            class="fa fa-lock"></i> Discharge
                    </button>
                </a>
            @endif
        @endif

        <tr>
            <th class="font-weight-bold">First Name:</th>
            <td>{{ $staff->firstname}}</td>
            <th class="font-weight-bold">Middle Name:</th>
            <td>@if(strlen($staff->middlename) == 1 && $staff->middlename !== 0){{ $staff->middlename }}@endif</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Employee Number:</th>
            <td id="employee_number" class="font-weight-bold">{{ @$staff->employee_number}}</td>
            <th class="font-weight-bold">Last Name:</th>
            <td>{{ $staff->lastname }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold ">Marital Status:</th>
            <td>{{ @\App\Repositories\StatusRepository::getMaritalStatus($staff->marital_status)}}</td>
            <th class="font-weight-bold">Gender:</th>
            <td>{{ \App\Repositories\StatusRepository::getGender($staff->gender) }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Date of Employment</th>
            <td>@if($staff->employment_date){!! \Carbon\Carbon::parse($staff->employment_date)->format('d-m-Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($staff->employment_date)->diffForHumans().")</b></small>" !!}@endif</td>
            <th class="font-weight-bold"> Date of Birth</th>
            <?php $age = \Carbon\Carbon::parse($staff->dob)->diffForHumans(); ?>
            <td>@if($staff->dob){!! \Carbon\Carbon::parse($staff->dob)->format('d-m-Y')."  &nbsp;&nbsp;<small><b>(".str_replace('ago','',$age).")</b></small>" !!}@endif</td>

        </tr>
        <tr>
            <th class="font-weight-bold ">Position of Employment:</th>
            <td>{{$staff->position}}</td>
            <th class="font-weight-bold">Department</th>
            <td>{{ $staff->department }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold ">Role:</th>
            <td>{{ $staff->role }}</td>
            <th class="font-weight-bold">Line of Business</th>
            <td>{{ $staff->lob }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold ">Staff Status:</th>
            <td>{!! getStaffEmploymentStatusButton($staff->employment_status) !!}</td>
            {{--            <th class="font-weight-bold">Date discharged</th>--}}
            {{--            <td>{{ @$staff->discharged_at }}</td>--}}
        </tr>

        <tr>
            <th class="font-weight-bold"> Created By</th>
            <td>{{ \App\User::find($staff->created_by)->name }}</td>
            <th class="font-weight-bold ">Created At:</th>
            <td id="created_at">  {!! \Carbon\Carbon::parse($staff->created_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($staff->created_at)->format('Y-m-d h:i:s A').")</b></small>" !!}</td>
        </tr>
        </tbody>
    </table>

</div>
@include('common.auto_modal_lg_static',[
    'modal_id'=>'edit_staff_info_modal',
    'modal_title'=>'UPDATE STAFF DETAILS',
    'modal_content'=>"<div class='staff_details_form'></div>"
    ])
@include('common.auto_modal_static',[
    'modal_id'=>'discharge_staff_modal',
    'modal_title'=>'DISCHARGE STAFF',
    'modal_content'=>autoForm(['date_discharged','reason','hidden_staff_id'],'admin/staffs/staff/discharge/'.$staff->id)
    ])
@include('common.auto_modal',[
'modal_id'=>'update_profile_picture',
'modal_title'=>'Profile Picture',
'modal_content'=>autoForm(['avatar', 'hidden_user_id'],'user/profile-picture'),
])
<script type="text/javascript">
    $(function () {
        $("input[name='date_discharged']").datetimepicker({
            timepicker: false,
            format: 'Y-m-d'
        });
        $("input[name='user_id']").val(`{{@$staff->user_id}}`)
    })

    function updatestaffProfile(staff_id) {
        ajaxLoad('{{ url("admin/staffs/staff/update-profile") }}/' + staff_id, 'staff_details_form')
    }

    function dischargeStaff(staff_id) {
        $("input[name='staff_id']").val(staff_id)
        $("#discharge_staff_modal").modal("show")
    }
</script>
