<div class="card-body staff_details1 row">
    <table class="table col-md-12">
        <tbody>

        <a href="#more_staff_info_modal" onclick="return editStaffContactsDetails()" data-toggle="modal">
            <button type="submit" style="margin-left: .635rem" class="btn btn-outline btn-outline-primary btn-sm submit-btn"><i
                    class="fa fa-edit"></i> Update
            </button>
        </a>
        <tr>
            <th class="font-weight-bold">Email Address:</th>
            <td id="type">{{ $staff->email}}</td>
            <th class="font-weight-bold">Alternate Email:</th>
            <td id="type">{{ $staff->alternate_email }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Phone:</th>
            <td id="type">{{ $staff->phone}}</td>
            <th class="font-weight-bold">Alternate Phone:</th>
            <td id="type">{{ $staff->alternate_phone }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Postal Address:</th>
            <td id="type">{{ $staff->postal_address}}</td>
            <th class="font-weight-bold">Residential Physical Address:</th>
            <td id="type">{{ $staff->residential_physical_address }}</td>
        </tr>
        </tbody>
    </table>
</div>
@include('common.auto_modal_static',[
    'modal_id'=>'edit_staff_contact_info_modal',
    'modal_title'=>'UPDATE STAFF DETAILS',
    'modal_content'=>autoForm(['email','alternate_email','phone','alternate_phone','postal_address','residential_physical_address','hidden_user_id'=>$staff->user_id,'hidden_id'=>$staff->id],'admin/staffs/staff/update-contacts/'.$staff->id)
    ])

<script>
    function editStaffContactsDetails(){
        $("input[name='email']").val('{{ $staff->email }}')
        $("input[name='alternate_email']").val('{{ $staff->alternate_email }}')
        $("textarea[name='postal_address']").val('{{ $staff->postal_address }}')
        $("input[name='phone']").val('{{ $staff->phone }}')
        $("input[name='alternate_phone']").val('{{ $staff->alternate_phone }}')
        $("input[name='residential_physical_address']").val('{{ $staff->residential_physical_address }}')
        $("#edit_staff_contact_info_modal").modal('show')
    }
</script>
