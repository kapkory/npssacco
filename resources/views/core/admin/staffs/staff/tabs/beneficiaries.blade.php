
    <div class="card-body"><h4 class="mt-0 header-title">Next of Kin</h4>
        <button onclick="addNOKModal()" class="btn btn-sm btn-info float-right badge"><i class="fa fa-plus"></i> Add Next Of Kin</button>

        <div class="table-responsive">
            <table class="table mb-0">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Relationship</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                @if(count($staff->nextOfKins) > 0)
                @foreach($staff->nextOfKins as $key=>$nok)
                    <tr>
                        <th scope="row">{{++$key}}</th>
                        <td>{{$nok->nok_name}}</td>
                        <td>{{$nok->nok_phone}}</td>
                        <td>{{$nok->nok_relationship}}</td>
                        <td>
                            <a href="javascript:void(0)" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>
                            <a href="javascript:void(0)"><i class="fas fa-trash-alt text-danger font-16"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @endif

                </tbody>
            </table>
            <!--end /table-->
        </div>
        <!--end /tableresponsive-->
    </div>


    <div class="card-body"><h4 class="mt-0 header-title">Spouse/Children</h4>
        <div class="table-responsive">
            <button onclick="addFamilyModal()" class="btn btn-sm btn-info float-right badge"><i class="fa fa-plus"></i> Add Spouse/Children</button>

            <table class="table mb-0">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Relationship</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($staff->staffSpouses) > 0)
                @foreach($staff->staffSpouses as $key=>$spouse)
                    <tr>
                        <th scope="row">{{++$key}}</th>
                        <td>{{$spouse->name}}</td>
                        <td>{{$spouse->phone}}</td>
                        <td>{{$spouse->relationship}}</td>
                        <td>
                            <a href="javascript:void(0)" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>
                            <a href="javascript:void(0)"><i class="fas fa-trash-alt text-danger font-16"></i></a>
                        </td>
                    </tr>
                @endforeach
                    @endif
                </tbody>
            </table>
            <!--end /table-->
        </div>
        <!--end /tableresponsive-->
    </div>


    <div class="card-body"><h4 class="mt-0 header-title">Beneficiary</h4>
        <div class="table-responsive">
            <button onclick="addBeneficiaryModal()" class="btn btn-sm btn-info float-right badge"><i class="fa fa-plus"></i> Add beneficiary</button>
            <table class="table mb-0">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Contact</th>
                    <th>Relationship</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(count($staff->beneficiaries) > 0)
                @foreach($staff->beneficiaries as $key=>$beneficiary)
                    <tr>
                        <th scope="row">{{++$key}}</th>
                        <td>{{$beneficiary->name}}</td>
                        <td>{{$beneficiary->phone}}</td>
                        <td>{{$beneficiary->relationship}}</td>
                        <td>
                            <a href="javascript:void(0)" class="mr-2"><i class="fas fa-edit text-info font-16"></i></a>
                            <a href="javascript:void(0)"><i class="fas fa-trash-alt text-danger font-16"></i></a>
                        </td>
                    </tr>
                @endforeach
                    @endif
                </tbody>
            </table>
            <!--end /table-->
        </div>
        <!--end /tableresponsive-->
    </div>
    @include('common.auto_modal_static',[
    'modal_id'=>'staff_beneficiary_info_modal',
    'modal_title'=>'BENEFICIARY DETAILS',
    'modal_content'=>autoForm(['name','phone','relationship','hidden_staff_id'=>$staff->id],'admin/staffs/staff/beneficiaries/'.$staff->id)
    ])
    @include('common.auto_modal_static',[
    'modal_id'=>'nok_info_modal',
    'modal_title'=>'NEXT OF KIN DETAILS',
    'modal_content'=>autoForm(['name','phone','relationship','hidden_staff_id'=>$staff->id],'admin/staffs/staff/beneficiaries/'.$staff->id)
    ])
    @include('common.auto_modal_static',[
    'modal_id'=>'spouse_info_modal',
    'modal_title'=>'SPOUSE/CHILD DETAILS',
    'modal_content'=>autoForm(['name','phone','relationship','hidden_staff_id'=>$staff->id],'admin/staffs/staff/beneficiaries/'.$staff->id)
    ])
    <script type="text/javascript">
        function addBeneficiaryModal(){
            $("#staff_beneficiary_info_modal").modal('show')
        }
        function addFamilyModal() {
            $("#spouse_info_modal").modal('show')
        }
        function addNOKModal(){
            $("#nok_info_modal").modal('show')
        }
    </script>
