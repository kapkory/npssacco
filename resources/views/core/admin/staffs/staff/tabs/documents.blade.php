
<fieldset class="">
    <span class="float-right">
               <a href="#upload_file" data-toggle="modal" class="btn btn-info  "><i class="fa fa-upload"></i> Upload File</a>
        <span>MAX: {{ ini_get('upload_max_filesize') }} </span>
        </span>

</fieldset>
@if($staff->files()->count() > 1)
    <a href="{{url('admin/staffs/staff/files/downloadAll', [$staff->id])}}" class="btn btn-info"><i
            class="fa fa-download"></i> Download All</a>
@endif
@include('common.bootstrap_table_ajax',[
'table_headers'=>["name","description","uploaded_by","uploaded_at", "action"],
'table_class'=>["table table-md"],
'data_url'=>'admin/staffs/staff/files/list/?staff_id='.$staff->id,
'base_tbl'=>'files'
])


<div id="upload_file" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="upload_file_label"
     style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="upload_file_label">UPLOAD DIAGNOSIS REPORT</h3>
                <button class="btn btn-danger btn-sm legitRipple" data-dismiss="modal">×
                    <div class="legitRipple-ripple"
                         style="left: 67.5989%; top: 40.625%; transform: translate3d(-50%, -50%, 0px); width: 281.076%; opacity: 0;"></div>
                </button>
            </div>

            <div class="modal-body">
                <div class="section">
                    <form enctype="multipart/form-data" class="model_form_id file-form" method="post"
                          action="{{url('admin/staffs/staff/files/upload')}}">
                        @csrf
                        <input type="hidden" name="staff_id" value="{{ $staff->id }}">
                        <div class="form-group ">
                            <label for="type">File Type </label>
                            <select name="type" class=" form-control">
                                <option value="" selected>Please Select..</option>
                                <option value="1">Letter of Appointment</option>
                                <option value="2">CV / Resume</option>
                                <option value="3">Non-Disclosure Agreement</option>
                                <option value="4">University Degree / School certificates</option>
                                <option value="5">ID Copy</option>
                                <option value="6">Medical Copy</option>
                                <option value="7">Bank Account Details</option>
                                <option value="8">Photo</option>
                                <option value="9">Exit Interview</option>
                                <option value="10">Others</option>
                            </select>
                        </div>
                        <div class="form-group file">
                            <div class="form-group">
                                <label class="fg-label control-label label_file">File</label>
                                <input type="file" name="file" class="form-control">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="fg-line">
                                <label class="fg-label control-label label_file_type">Description</label>
                                <div class="form-group ">
                                        <textarea rows="3" cols="3" name="description" class="form-control"
                                                  placeholder="Brief description of the file"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">

                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary btn-raised submit-btn legitRipple"><i
                                        class="zmdi zmdi-save"></i> Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">

    $(function () {
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
    $('.select2').select2();
    });
</script>

