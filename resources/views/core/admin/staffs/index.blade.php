@extends('layouts.admin')

@section('bread_crumb')
    <a href="#">Staff</a>
@endsection

@section('title') Staff @endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{url('admin/staffs/create')}}"
                       class="btn btn-info btn-sm clear-form float-right load-page"><i
                            class="fa fa-plus"></i> ADD STAFF</a>
                    <div class="table-responsive">
{{--                        'table_headers'=>["employee_number"=>"Employee_no","users.name"=>"name","positions.name"=>"position","departments.name"=>"department","staff_roles.name"=>"role","users.phone"=>"phone","users.phone"=>"email","employment_date"],--}}

                        @include('common.bootstrap_table_ajax',[
                        'table_headers'=>["users.name"=>"name","positions.name"=>"position","departments.name"=>"department","staff_roles.name"=>"role","users.phone"=>"phone","users.phone"=>"email","staffs.employment_status"=>"Status","employment_date","action"],
                        'data_url'=>'admin/staffs/list',
                          'table_class'=>["small"],
                        'base_tbl'=>'staffs'
                        ])
                    </div>

                    @include('common.auto_modal',[
                        'modal_id'=>'staff_modal',
                        'modal_title'=>'STAFF FORM',
                        'modal_content'=>autoForm(\App\Models\Core\Staff::class,"admin/staffs")
                    ])
                </div>
            </div>
        </div>
    </div>
@endsection


