@extends('layouts.admin')

@section('title')
    {{ ucfirst($calltronix_department->name) }} Off Duty Requests
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Off-duty requests</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
    'tabs'=>['pending' , 'approved', 'rejected','all_requests'],
    'tabs_folder'=>'core.admin.hodoffdutyrequests.tabs',
    'base_url'=>'admin/hodoffdutyrequests/'
    ])
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->

        </div><!--end col-->
    </div>
    @include('common.auto_modal_lg_static',[
     'modal_id'=>'more_leave_request_info_modal',
     'modal_title'=>'Off-Duty Request Details',
     'modal_content'=>'<div class="offduty_request_details row " ></div>'
  ])
    @include('common.auto_modal_static',[
   'modal_id'=>'approve_offduty_request_modal',
   'modal_title'=>'Approve Off-duty Request',
   'modal_content'=>autoForm(['hidden_id','approve_comment'],'admin/hodoffdutyrequests/offdutyrequest/approve/')
])
    @include('common.auto_modal_static',[
       'modal_id'=>'reject_offduty_request_modal',
       'modal_title'=>'Reject Off-duty Request',
       'modal_content'=>autoForm(['hidden_id','reject_reason'],'admin/hodoffdutyrequests/offdutyrequest/reject/')
    ])
    <script type="text/javascript">

        function getMoreDetails(offduty_request_id){
            // $(".modal-dialog").addClass('modal-lg')
            ajaxLoad('{{ url("admin/hodoffdutyrequests/offdutyrequest/details") }}/' + offduty_request_id, 'offduty_request_details')
        }

        function approveRequest(offduty_request_id){
            $("input[name='id']").val(offduty_request_id)
            $("textarea[name='approve_comment']").val('')
            $("#approve_offduty_request_modal").modal('show')
        }
        function rejectRequest(offduty_request_id){
            $("input[name='id']").val(offduty_request_id)
            $("textarea[name='reject_reason']").val('')
            $("#reject_offduty_request_modal").modal('show')
        }

        $(function () {
            getTabCounts('{{ url('admin/hodoffdutyrequests/list?count=1') }}');
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
        })
    </script>
@endsection

