<div class=" col-md-12">
    <table class="table table-striped table-condensed table-sm table-fit table-hover">
        <tbody>
        <tr>
            <th class="font-weight-bold"> Request No:</th>
            <td >#{{ $offduty_request->id }}</td>
            <th class="font-weight-bold ">Created At:</th>
            <td >  {!! \Carbon\Carbon::parse($offduty_request->created_at)->toDayDateTimeString()."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($offduty_request->created_at)->format('Y-m-d').")</b></small>" !!}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Staff</th>
            <td >{{ $staff->name }}</td>
            <th class="font-weight-bold ">Email:</th>
            <td > {{ @$staff->email }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Days:</th>
            <td >{{ $offduty_request->days }}</td>
            <th class="font-weight-bold">Status:</th>
            <td >{!! getOffdutyRequestStatusButton($offduty_request->status) !!}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Reason </th>
            <td colspan="3" >{{ $offduty_request->description }}</td>
        </tr>

        <tr>
            <th class="font-weight-bold"> Dates: </th>
            <td colspan="3" >
                @foreach($offduty_request->offdutyRequestDates as $date)
                    <ul class="row text-justify col-12">
                        <li class="col-12">{!! \Carbon\Carbon::parse($date->date)->isoFormat('Do MMM Y')." <small><b>(".$date->date.")</small></b>" !!}</li>
                    </ul>
                @endforeach
            </td>
        </tr>


        <?php $statuses = ['approved','rejected']; ?>
        @if(in_array(getOffdutyRequestStatus($offduty_request->status),$statuses))
            <?php $action_date = ($offduty_request->approved_at) ? $offduty_request->approved_at : $offduty_request->rejected_at;  ?>
            <tr>
                <th class="font-weight-bold"> H.O.D Action</th>
                <td >@if($offduty_request->hod_approve_comment)<span class="text-success-800">Approved</span> @elseif($offduty_request->hod_reject_reason)<span class="text-danger-800">Rejected</span> @else<span class="text-info-800"> Pending</span>@endif</td>
                <th class="font-weight-bold ">Date of Action:</th>
                <td>  @if($action_date) {!! \Carbon\Carbon::parse($action_date)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($action_date)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
            </tr>
            <?php
            $hod_comment_title = ($offduty_request->hod_approve_comment) ? "H.O.D approve comment" : null;
            if(!$hod_comment_title)
                $hod_comment_title = ($offduty_request->hod_reject_reason) ? "H.O.D reject reason" : null;
            $comment = ($offduty_request->hod_approve_comment) ? $offduty_request->hod_approve_comment : $offduty_request->hod_reject_reason;
            ?>
            @if($hod_comment_title)
                <tr>
                    <th class="font-weight-bold"> {{ $hod_comment_title }}</th>
                    <td colspan="3" >{{ @$comment }}</td>
                </tr>
            @endif

        @endif

        </tbody>
    </table>
</div>
