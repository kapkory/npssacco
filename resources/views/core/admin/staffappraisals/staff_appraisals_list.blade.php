<?php
$path = 'storage/profile-pics';
$image = (@getimagesize($path . "/" .$staff->avatar)) ? $staff->avatar : "user.jpg";
$department_name = @$staff->getCalltronixDepartment()->name;
if (!$department_name)
    $department_name = "N/A";
$employment_status = getStaffEmploymentStatus($staff->employment_status);
?>

{{--<div class="mt-3 card col-md-12">--}}
{{--    <div class="media-body chat-user-box">--}}
{{--        <div class="col-md-2 float-left">--}}
{{--            <img class="d-flex  m-2 rounded-circle" src="{{ asset($path."/".$image) }}" alt="image" height="76" width="76">--}}
{{--            <span class="font-weight-bold small col-md-12 text-center">{{ $staff->name }}</span>--}}
{{--        </div>--}}
{{--        <div class="col-md-10 float-right">--}}
{{--            <table class="table">--}}
{{--                <tbody>--}}
{{--                <tr>--}}
{{--                    <th>Employment status:</th>--}}
{{--                    <td class="user-title font-weight-bold {{ ($employment_status == "active") ? "text-success" : "text-danger" }}" id="staff_employment_status">{{ ucfirst($employment_status) }}</td>--}}
{{--                    <th>Department:</th>--}}
{{--                    <td id="staff_department">{{ $department_name }}</td>--}}
{{--                </tr>--}}
{{--                <tr>--}}
{{--                    <th>Employment date:</th>--}}
{{--                    <td class="user-title small font-weight-bold" >{{ \Carbon\Carbon::parse($staff->employment_date)->toDayDateTimeString() }}</td>--}}
{{--                    <th>Dismissal date:</th>--}}
{{--                    <td class="user-title small font-weight-bold" >{{ ($staff->dismissal_date) ? \Carbon\Carbon::parse($staff->dismissal_date)->toDayDateTimeString() : '' }}</td>--}}
{{--                </tr>--}}
{{--                </tbody>--}}
{{--            </table>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
<div class="card col-md-12">
    <div class="card-header col-md-12"> @if($employment_status == "active")<a href="#staffappraisal_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD STAFF APPRAISAL</a>@endif</div>
    <div class="card-body">

        @include('common.bootstrap_table_ajax',[
        'table_headers'=>["id","staff_appraisals.review_period_from"=>"Period_from","staff_appraisals.review_period_to"=>"Period_to","Status","Created_at","action"],
        'data_url'=>'admin/staffappraisals/staff/list/'.$staff->id,
        'table_class' => ['table','condensed','small'],
        'base_tbl'=>'staff_appraisals'
        ])
    </div>
</div>
@include('common.auto_modal',[
    'modal_id'=>'staffappraisal_modal',
    'modal_title'=>'STAFF APPRAISAL FORM',
    'modal_content'=>autoForm(['period_from','period_to','hidden_staff_id'=>$staff->id,'form_model'=>\App\Models\Core\StaffAppraisal::class],"admin/staffappraisals")
                    ])
@include('common.auto_modal_lg_static',[
    'modal_id'=>'assign_checklistitem_modal',
    'modal_title'=>'Staff Appraisal Details',
    'modal_content'=>'<div class="staff_appraisal_details container" ></div>'
    ])

<script>
    $(function(){
        $('.select2').select2();
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });

        $('input[name="period_from"]').datetimepicker({
            timepicker: false,
            format:'Y-m-d'
        });
        $('input[name="period_to"]').datetimepicker({
            timepicker: false,
            format:'Y-m-d'
        });
    })

    function loadStaffAppraisalResults(appraisal_id){
        let url = '{{ url('admin/staffappraisals/staffappraisal/get-details/') }}'+'/'+appraisal_id;
        ajaxLoad(url,'staff_appraisal_details')
    }

</script>
