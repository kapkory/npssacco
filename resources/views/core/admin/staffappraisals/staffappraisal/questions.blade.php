@extends('layouts.admin')

@section('title') Questions @endsection
@section('bread_crumb')
    <a href="{{ url('admin/staffappraisals') }}" class="breadcrumb-item load-page"> Staff Appraisals</a>
    <li class="breadcrumb-item active">Questions</li>
@endsection

@section('content')
    <?php
    $path = 'storage/profile-pics';
    $image = (@getimagesize($path . "/" .$staff->avatar)) ? $staff->avatar : "user.jpg";
    $department_name = @$staff->getCalltronixDepartment()->name;
    if (!$department_name)
        $department_name = "N/A";
    $employment_status = getStaffEmploymentStatus($staff->employment_status);
    ?>
    <div class="row">
        <div class="col-md-4 float-left">
            <div class="card">
                <div class="card-body">
                    {{--                                <div class="col-md-2 float-left">--}}
                    {{--                                    <img class="d-flex  m-2 rounded-circle" src="{{ asset($path."/".$image) }}" alt="image" height="76" width="76">--}}
                    {{--                                    <span class="font-weight-bold small col-md-12 text-center">{{ $staff->name }}</span>--}}
                    {{--                                </div>--}}
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Name:</th>
                            <td class="user-title font-weight-bold">{{ ucfirst($staff->name) }}</td>
                        </tr>
                        <tr>
                            <th>Job Title:</th>
                            <td class="user-title">{{ ucfirst($staff->role) }}</td>
                        </tr>
                        <tr>
                            <th>Employment status:</th>
                            <td class="user-title">{!! getStaffEmploymentStatusButton($staff->employment_status) !!}</td>
                        </tr>
                        <tr>
                            <th>Department:</th>
                            <td id="staff_department">{{ $department_name }}</td>
                        </tr>
                        <tr>
                            <th>Employment date:</th>
                            <td class="user-title small font-weight-bold" >{{ \Carbon\Carbon::parse($staff->employment_date)->toDayDateTimeString() }}</td>
                        </tr>
                        <tr>
                            <th>Dismissal date:</th>
                            <td class="user-title small font-weight-bold" >{{ ($staff->dismissal_date) ? \Carbon\Carbon::parse($staff->dismissal_date)->toDayDateTimeString() : '' }}</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="col-md-8 float-right">

            <div class="col-md-12">
                <div class="card-body">
                    <div class="">
                        <div class="card col-md-12">
                            <div class="card-header col-12"> {{ ucfirst($question->category) }} <span class="float-right"><b>({{ $question->position."/".$questions_total_count }})</b></span></div>
                            <div class="card-body">
                                <p><b>{{ $question->position }}</b>.&nbsp;{{ $question->question }}</p>
                                <form class="ajax-post" method="post" action="{{ url('admin/staffappraisals/staffappraisal/question') }}">
                                    @csrf
                                    <input name="question_id" type="hidden" value="{{ $question->id }}">
                                    <input name="staff_appraisal_id" type="hidden" value="{{ $staff_appraisal->id }}">
                                    <input name="next_question_position" type="hidden" value="{{ $next_position }}">
                                    <div class="col-12">
                                        <div class="my-2f">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" value="5" name="question_option" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio1">Outstanding</label>
                                            </div>
                                        </div>

                                        <div class="my-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" value="4" name="question_option" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio2">Exceed Expectation</label>
                                            </div>
                                        </div>

                                        <div class="my-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio3" value="3" name="question_option" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio3">Meets Expectations</label>
                                            </div>
                                        </div>

                                        <div class="my-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio4" value="2" name="question_option" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio4">Needs Improvement</label>
                                            </div>
                                        </div>

                                        <div class="my-2">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio5" value="1" name="question_option" class="custom-control-input">
                                                <label class="custom-control-label" for="customRadio5">Unacceptable</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-right">
                                        <button style="background-color:#ffb833; color:#FFFFFF" type="submit" class=" btn submit-btn"><b>Next >> </b></button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        $(function () {
            // loadNextQuestions(1)
        })
        function loadNextQuestions(questions_order){
            var url = '{{ url('admin/staffappraisals/staffappraisal/get-next-question') }}'+'/'+questions_order;
            setTimeout(function () {
                ajaxLoad(url,'questions_details')
            });
        }
    </script>
@endsection


