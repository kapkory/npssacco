@extends('layouts.admin')

@section('title') Staff Appraisals @endsection
@section('bread_crumb')
    <li class="breadcrumb-item active">Staff Appraisals</li>
@endsection

@section('content')
{{--<div class="row">--}}
{{--        <div class="col-md-12">--}}
{{--            <div class="card">--}}
{{--                <div class="card-body">--}}
{{--                <a href="#staffappraisal_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD STAFFAPPRAISAL</a>--}}
{{--                    @include('common.bootstrap_table_ajax',[--}}
{{--                    'table_headers'=>["id","users.name"=>"staff","date_from","date_to","Created_at","action"],--}}
{{--                    'data_url'=>'admin/staffappraisals/list',--}}
{{--                    'base_tbl'=>'staffappraisals'--}}
{{--                    ])--}}

{{--                    @include('common.auto_modal',[--}}
{{--                        'modal_id'=>'staffappraisal_modal',--}}
{{--                        'modal_title'=>'STAFF APPRAISAL FORM',--}}
{{--                        'modal_content'=>autoForm(\App\Models\Core\StaffAppraisal::class,"")--}}
{{--                    ])--}}
{{--                 </div>--}}

{{--               </div>--}}

{{--            </div>--}}
{{--        </div>--}}




<style>
    .staff_target_link.active{
        background-color: #edeeef;
        border-radius: 5px;
    }
    .staff_target_link:hover {
        background-color: #edeeef;
        border-radius: 5px;
    }
</style>
<div class="row">
    <div class="col-12">
        <!-- Left sidebar -->
        <div class="email-leftbar">
            <div class="card mt-1">
                <div class="card-body">
                    <h5 class="my-1">Staff&nbsp;&nbsp;<button class="btn btn-dark badge btn-sm float-right">{{ count($staffs) }}</button></h5>
                    <div class="">
                        <div class="chat-search mt-2">
                            <div class="form-group">
                                <div class="input-group">
                                    <input style="border-radius: 5px !important; height: 2.0rem !important;" onkeyup="searchText()" type="text" id="search-input" name="staff-search" class="form-control" placeholder="Search Staff name">
                                </div>
                            </div>
                        </div>
                        <?php
                        $path = 'storage/profile-pics';
                        ?>
                        <div class="staff-list" id="myUl">
                            @foreach($staffs as $staff)
                                <?php $image = (@getimagesize($path . "/" .$staff->avatar)) ? $staff->avatar : "user.jpg";
                                $first_user_id = null;
                                ?>
                                <div class="staff_target" id="div_{{ $staff->id }}" >
                                    <a id="tab_{{ $staff->id }}" href="#" onclick="loadStaffTab('{{ $staff->id }}')" class="media staff_target_link tab_{{ $staff->id }}" >
                                        <img class="d-flex mr-3 rounded-circle" src="{{ asset($path."/".$image) }}" alt="image" height="40" width="40">
                                        <div id="chat_body_{{ $staff->id }}" class="media-body chat-user-box ">
                                            <p class="user-title m-0 staff_name">{{ $staff->name }}</p>
                                            <p class="text-muted">
                                                <?php $department_name = @$staff->getCalltronixDepartment()->name;
                                                if (!$department_name)
                                                    $department_name = "N/A";
                                                if($first_user_id == null)
                                                    $first_user_id = $staff->id;
                                                ?>
                                                {{ $department_name }}
                                            </p>
                                        </div>
                                        <i style="display: none; margin-top:1.5em;  margin-bottom:1.5em " id="check_icon_{{ $staff->id }}" class="fa fa-check-circle text-success media-tab-check "></i>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- end card-body -->
            </div><!-- end card -->
        </div><!-- End Left sidebar -->
        <!-- Right Sidebar -->
        <div class="email-rightbar">
            <div class="staff_details_tab">

            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        let user_id = window.localStorage.getItem('staffappraisal_user_id');
        if(!user_id)
            user_id = '{{ @$first_user_id }}';
        if(user_id)
            $( "#tab_"+user_id).trigger( "click" );

    })
    function loadStaffTab(user_id,state=0){
        $(".staff_target_link").removeClass('active');
        $('#tab_'+user_id).addClass('active');
        $(".media-tab-check").hide();
        $('#check_icon_'+user_id).show();
        window.localStorage.setItem('staffappraisal_user_id',user_id)
        let url = '{{ url('admin/staffappraisals/staff/get-data') }}'+'/'+user_id;
        setTimeout(function () {
            ajaxLoad(url,'staff_details_tab')
        }, state);
    }

    function searchText(){
        input = document.getElementById("search-input");
        filter = input.value.toUpperCase();
        var length = document.getElementsByClassName('staff_target').length;

        for (i=0; i<length; i++){
            if(document.getElementsByClassName('staff_name')[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                document.getElementsByClassName("staff_target")[i].style.display = "block";
            }
            else{
                document.getElementsByClassName("staff_target")[i].style.display = "none";
            }
        }
    }
</script>
@endsection

