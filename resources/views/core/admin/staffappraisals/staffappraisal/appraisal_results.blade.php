@extends('layouts.admin')

@section('title') Appraisal Results @endsection
@section('bread_crumb')
    <a href="{{ url('admin/staffappraisals') }}" class="breadcrumb-item load-page"> Staff Appraisals</a>
    <li class="breadcrumb-item active">Appraisal Results</li>
@endsection

@section('content')
    <?php
    $path = 'storage/profile-pics';
    $image = (@getimagesize($path . "/" .$staff->avatar)) ? $staff->avatar : "user.jpg";
    $department_name = @$staff->getCalltronixDepartment()->name;
    if (!$department_name)
        $department_name = "N/A";
    $employment_status = getStaffEmploymentStatus($staff->employment_status);

    $results_map = [];
    $results_map[1]= 'Unacceptable';
    $results_map[2]= 'Needs Improvement';
    $results_map[3]= 'Meets Expectations';
    $results_map[4]= 'Exceed Expectations';
    $results_map[5]= 'Outstanding';
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{--                                <div class="col-md-2 float-left">--}}
                    {{--                                    <img class="d-flex  m-2 rounded-circle" src="{{ asset($path."/".$image) }}" alt="image" height="76" width="76">--}}
                    {{--                                    <span class="font-weight-bold small col-md-12 text-center">{{ $staff->name }}</span>--}}
                    {{--                                </div>--}}
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Name:</th>
                            <td class="user-title font-weight-bold">{{ ucfirst($staff->name) }}</td>
                            <th>Job Title:</th>
                            <td class="user-title">{{ ucfirst($staff->role) }}</td>
                        </tr>
                        <tr>
                            <th>Employment status:</th>
                            <td class="user-title" >{!! getStaffEmploymentStatusButton($staff->employment_status) !!}</td>

                            <th>Department:</th>
                            <td id="staff_department">{{ $department_name }}</td>
                        </tr>
                        <tr>
                            <th>Appraisal Period From:</th>
                            <td class="user-title small font-weight-bold" >{{ \Carbon\Carbon::parse($staff_appraisal->review_period_from)->isoFormat('ddd, Do MMM Y') }}</td>

                            <th>Appraisal Period To:</th>
                            <td class="user-title small font-weight-bold" >{{ \Carbon\Carbon::parse($staff_appraisal->review_period_to)->isoFormat('ddd, Do MMM Y') }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header"> {{ ucfirst($staff->name) }}</div>
                <div class="card-body" style="max-height:440px; overflow: auto">
                    <table class="table table-sm table-stripped table-condensed">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Question</th>
                            <th>Score</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($results as $res)
                            <tr>
                                <td>{{ $res->position }}</td>
                                <td>{{ $res->category }}</td>
                                <td><p>{{ limit_string_words($res->question ,15)}}</p></td>
                                <td><small><b>{!! @$results_map[$res->score] !!}</b></small></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>

    </script>
@endsection


