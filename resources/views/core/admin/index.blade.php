@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Dashboard</li>
@endsection
@section('content')
    <style>
        .table .thead-custom th {
            color: #FFFFFF !important;
            background-color: #ff943d;
            border-color: #000000;
            font-weight: bolder;
        }
    </style>
    @if(userCan('view_admin_dashboard_tickets_reports'))
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6 col-xl-3">
                            <div class="card card-body">
                                <a href="{{ url('admin/tickets?tab=open') }}" class="load-page">
                                    <div class="media">
                                        <div class="mr-3 align-self-center">
                                            <i class="fa fa-phone-alt fa-3x text-success-400"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h3 class="font-weight-semibold mb-0">{{ number_format($tickets_count['open']) }}</h3>
                                            <span
                                                class="text-uppercase font-size-sm text-muted">All Open Tickets</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3">
                            <div class="card card-body">
                                <a href="{{ url('admin/tickets?tab=in_progress') }}" class="load-page">
                                    <div class="media">
                                        <div class="mr-3 align-self-center">
                                            <i class="fa fa-spinner fa-2x text-blue-400"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h3 class="font-weight-semibold mb-0">{{ number_format($tickets_count['in_progress']) }}</h3>
                                            <span
                                                class="text-uppercase font-size-sm text-muted">In progress Tickets</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3">
                            <div class="card card-body">
                                <a href="{{ url('admin/tickets?tab=closed') }}" class="load-page">
                                    <div class="media">
                                        <div class="mr-3 align-self-center">
                                            <i class="fa fa-envelope fa-3x text-indigo-400"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h3 class="font-weight-semibold mb-0">{{ number_format($tickets_count['closed']) }}</h3>
                                            <span
                                                class="text-uppercase font-size-sm text-muted">Closed Tickets</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-6 col-xl-3">
                            <div class="card card-body">
                                <a href="{{ url('admin/tickets?tab=all_tickets') }}" class="load-page">
                                    <div class="media">
                                        <div class="mr-3 align-self-center">
                                            <i class="fa fa-phone-square fa-3x text-success-400"></i>
                                        </div>
                                        <div class="media-body text-right">
                                            <h3 class="font-weight-semibold mb-0">{{ number_format($tickets_count['all']) }}</h3>
                                            <span
                                                class="text-uppercase font-size-sm text-muted">All Tickets</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body"><span style="font-size: 16px !important;" class="header-title row  justify-content-around font-weight-bolder"><b><u>Tickets Created By Departments Count:</u></b></span>
                    <div class="table-responsive">
                        <table class="table table-striped ">
                            <thead class="thead-custom">
                            <tr>
                                <th scope="col"><b>Department</b></th>
                                <th scope="col">Open</th>
                                <th scope="col">In Progress</th>
                                <th scope="col">Resolved</th>
                                <th scope="col">Closed</th>
                                <th scope="col">Cancelled</th>
                                <th scope="col">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $open_count = $in_progress_count = $resolved_count = $closed_count = $cancelled_count = $total_count = 0;
                            ?>
                            @foreach($tickets_created as $department_data)
                                <?php
                                $dept_url = url('admin/departmentstickets/created/'.$department_data['id']);
                                $open_count += $department_data['open'];
                                $in_progress_count += $department_data['in_progress'];
                                $resolved_count += $department_data['resolved'];
                                $closed_count += $department_data['closed'];
                                $cancelled_count += $department_data['cancelled'];

                                $total = $department_data['closed'] +  $department_data['resolved'] +  $department_data['open'] +  $department_data['in_progress'] +  $department_data['cancelled'];
                                $total_count += $total;
                                ?>
                            <tr>
                                <th scope="row"><a class="load-page" href="{{ $dept_url }}">{{ $department_data['name'] }} <i class="fas fa-link"></i></a></th>
                                <th scope="col" class="text-danger">{{ $department_data['open'] }}</th>
                                <th scope="col" class="text-info">{{ $department_data['in_progress'] }}</th>
                                <th scope="col" class="text-primary">{{ $department_data['resolved'] }}</th>
                                <th scope="col" class="text-success">{{ $department_data['closed'] }}</th>
                                <th scope="col" class="text-success">{{ $department_data['cancelled'] }}</th>
                                <th scope="col">{{ $total }}</th>
                            </tr>
                            @endforeach
                            <tr style="font-size: 18px !important;">
                                <th scope="row">Total</th>
                                <th scope="col" class="text-danger">{{ $open_count }}</th>
                                <th scope="col" class="text-info">{{ $in_progress_count }}</th>
                                <th scope="col" class="text-primary">{{ $resolved_count }}</th>
                                <th scope="col" class="text-success">{{ $closed_count }}</th>
                                <th scope="col" class="text-success">{{ $cancelled_count }}</th>
                                <th scope="col" class="text-blue">{{ $total_count }}</th>
                            </tr>
                        </table>
                    </div>
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body"><span style="font-size: 16px !important;" class="header-title row  justify-content-around font-weight-bolder"><b><u>Tickets Assigned To Departments-Count:</u></b></span>
                    <div class="table-responsive">
                        <table class="table table-striped ">
                            <thead class="thead-custom">
                            <tr>
                                <th scope="col"><b>Department</b></th>
                                <th scope="col">Open</th>
                                <th scope="col">In Progress</th>
                                <th scope="col">Resolved</th>
                                <th scope="col">Closed</th>
                                <th scope="col">Cancelled</th>
                                <th scope="col">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $open_count = $in_progress_count = $resolved_count = $closed_count = $cancelled_count = $total_count = 0;
                            ?>
                            @foreach($tickets_assigned as $department_data)
                                <?php
                                $dept_url = url('admin/permissiongroup/tickets/assigned/'.$department_data['id']);
                                $open_count += $department_data['open'];
                                $in_progress_count += $department_data['in_progress'];
                                $resolved_count += $department_data['resolved'];
                                $closed_count += $department_data['closed'];
                                $cancelled_count += $department_data['cancelled'];

                                $total = $department_data['closed'] +  $department_data['resolved'] +  $department_data['open'] +  $department_data['in_progress']+  $department_data['cancelled'];
                                $total_count += $total;
                                ?>
                            <tr>
                                <th scope="row"><a class="load-page" href="{{ $dept_url }}">{{ $department_data['name'] }} <i class="fas fa-link"></i></a></th>
                                <th scope="col" class="text-danger">{{ $department_data['open'] }}</th>
                                <th scope="col" class="text-info">{{ $department_data['in_progress'] }}</th>
                                <th scope="col" class="text-primary">{{ $department_data['resolved'] }}</th>
                                <th scope="col" class="text-success">{{ $department_data['closed'] }}</th>
                                <th scope="col" class="text-success">{{ $department_data['cancelled'] }}</th>
                                <th scope="col">{{ $total }}</th>
                            </tr>
                            @endforeach
                            <tr style="font-size: 18px !important;">
                                <th scope="row">Total</th>
                                <th scope="col" class="text-danger">{{ $open_count }}</th>
                                <th scope="col" class="text-info">{{ $in_progress_count }}</th>
                                <th scope="col" class="text-primary">{{ $resolved_count }}</th>
                                <th scope="col" class="text-success">{{ $closed_count }}</th>
                                <th scope="col" class="text-blue">{{ $cancelled_count }}</th>
                                <th scope="col" class="text-blue">{{ $total_count }}</th>
                            </tr>
                        </table>
                    </div>
                </div><!--end card-body-->
            </div><!--end card-->
        </div><!--end col-->
    </div>
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body"><h4 class="header-title mt-0"></h4>
                        <div class="">
                            <div class="alert icon-custom-alert alert-outline-success alert-success-shadow"
                                 role="alert">
                                <i class="mdi mdi-check-all alert-icon"></i>
                                <div class="alert-text">
                                    <strong>Welcome Admin</strong> Dashboard and reports module coming soon.</div>
                            </div>
                        </div>
                    </div><!--end card-body-->
                </div><!--end card-->

            </div><!--end col-->
        </div>

        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="card feed-card">
                    <div class="card-body p-t-0 p-b-0">
                        <div class="row">
                            <div class="col-4 bg-primary border-feed">
                                <i class="fas fa-user-tie f-40"></i>
                            </div>
                            <div class="col-8">
                                <div class="p-t-25 p-b-25">
                                    <h2 class="f-w-400 m-b-10">Ksh 232,672</h2>
                                    <p class="text-muted m-0">Total Deposit Contribution <span class="text-primary f-w-400"></span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6">
                <div class="card feed-card">
                    <div class="card-body p-t-0 p-b-0">
                        <div class="row">
                            <div class="col-4 bg-success border-feed">
                                <i class="fas fa-wallet f-40"></i>
                            </div>
                            <div class="col-8">
                                <div class="p-t-25 p-b-25">
                                    <h2 class="f-w-400 m-b-10">Ksh 126,391</h2>
                                    <p class="text-muted m-0">Total Share Capital</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-md-6">
                <div class="card feed-card">
                    <div class="card-body p-t-0 p-b-0">
                        <div class="row">
                            <div class="col-4 bg-danger border-feed">
                                <i class="fas fa-sitemap f-40"></i>
                            </div>
                            <div class="col-8">
                                <div class="p-t-25 p-b-25">
                                    <h2 class="f-w-400 m-b-10">Ksh 23,619</h2>
                                    <p class="text-muted m-0">Dividend Earned </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-xl-3 col-md-6">
                <div class="card feed-card">
                    <div class="card-body p-t-0 p-b-0">
                        <div class="row">
                            <div class="col-4 bg-warning border-feed">
                                <i class="fas fa-users f-40"></i>
                            </div>
                            <div class="col-8">
                                <div class="p-t-25 p-b-25">
                                    <h2 class="f-w-400 m-b-10">34</h2>
                                    <p class="text-muted m-0">Total Members </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
