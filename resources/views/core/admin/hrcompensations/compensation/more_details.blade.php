<div class=" col-md-12">
    <table class="table table-striped table-condensed table-sm table-fit table-hover">
        <tbody>
        <tr>
            <th class="font-weight-bold"> Request No:</th>
            <td >#{{ $compensation_request->id }}</td>
            <th class="font-weight-bold ">Created At:</th>
            <td >  {!! \Carbon\Carbon::parse($compensation_request->created_at)->toDayDateTimeString()."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($compensation_request->created_at)->format('Y-m-d').")</b></small>" !!}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Staff</th>
            <td >{{ $staff->name }}</td>
            <th class="font-weight-bold ">Email:</th>
            <td > {{ @$staff->email }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Days:</th>
            <td >{{ $compensation_request->days }}</td>
            <th class="font-weight-bold">Status:</th>
            <td >{!! getOffdutyRequestStatusButton($compensation_request->status) !!}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Reason </th>
            <td colspan="3" >{{ $compensation_request->description }}</td>
        </tr>

        <tr>
            <th class="font-weight-bold"> Dates: </th>
            <td colspan="3" >
                @foreach($compensation_request->requestDates as $date)
                    <ul class="row text-justify col-12">
                        <li class="col-12">{!! \Carbon\Carbon::parse($date->date)->isoFormat('Do MMM Y')." <small><b>(".$date->date.")</small></b>" !!}</li>
                    </ul>
                @endforeach
            </td>
        </tr>


        <?php $statuses = ['approved','rejected']; ?>
        @if(in_array(getCompensationStatus($compensation_request->status),$statuses))
            <?php $action_date = ($compensation_request->approved_at) ? $compensation_request->approved_at : $compensation_request->rejected_at;  ?>
            <tr>
                <th class="font-weight-bold"> H.R Action</th>
                <td >@if($compensation_request->hr_approve_comment)<span class="text-success-800">Approved</span> @elseif($compensation_request->hr_reject_reason)<span class="text-danger-800">Rejected</span> @else<span class="text-info-800"> Pending</span>@endif</td>
                <th class="font-weight-bold ">Date of Action:</th>
                <td>  @if($action_date) {!! \Carbon\Carbon::parse($action_date)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($action_date)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
            </tr>
            <?php

            $hr_comment_title = ($compensation_request->hr_approve_comment) ? "H.R approve comment" : null;
            if(!$hr_comment_title)
                $hr_comment_title = ($compensation_request->hr_reject_reason) ? "H.R reject reason" : null;

            $comment = ($compensation_request->hr_approve_comment) ? $compensation_request->hr_approve_comment : $compensation_request->hr_reject_reason;
            ?>
            @if($hr_comment_title)
                <tr>
                    <th class="font-weight-bold"> {{ @$hr_comment_title }}</th>
                    <td colspan="3" >{{ @$comment }}</td>
                </tr>
            @endif

        @endif

        </tbody>
    </table>
</div>
