@extends('layouts.admin')

@section('title')
    Compensation Requests
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Compensation requests</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
    'tabs'=>['pending' , 'approved', 'rejected','all_requests'],
    'tabs_folder'=>'core.admin.hrcompensations.tabs',
    'base_url'=>'admin/hrcompensations/'
    ])
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->

        </div><!--end col-->
    </div>
    @include('common.auto_modal_lg_static',[
     'modal_id'=>'more_compensation_request_info_modal',
     'modal_title'=>'Compensation Request Details',
     'modal_content'=>'<div class="compensation_request_details row " ></div>'
  ])
    @include('common.auto_modal_static',[
   'modal_id'=>'approve_compensation_request_modal',
   'modal_title'=>'Approve Compensation Request',
   'modal_content'=>autoForm(['hidden_id','approve_comment'],'admin/hrcompensations/compensation/approve/')
])
    @include('common.auto_modal_static',[
       'modal_id'=>'reject_compensation_request_modal',
       'modal_title'=>'Reject Compensation Request',
       'modal_content'=>autoForm(['hidden_id','reject_reason'],'admin/hrcompensations/compensation/reject/')
    ])
    <script type="text/javascript">

        function getMoreDetails(compensation_request_id){
            // $(".modal-dialog").addClass('modal-lg')
            ajaxLoad('{{ url("admin/hrcompensations/compensation/details") }}/' + compensation_request_id, 'compensation_request_details')
        }

        function approveRequest(compensation_request_id){
            $("input[name='id']").val(compensation_request_id)
            $("textarea[name='approve_comment']").val('')
            $("#approve_compensation_request_modal").modal('show')
        }
        function rejectRequest(compensation_request_id){
            $("input[name='id']").val(compensation_request_id)
            $("textarea[name='reject_reason']").val('')
            $("#reject_compensation_request_modal").modal('show')
        }

        $(function () {
            getTabCounts('{{ url('admin/hrcompensations/list?count=1') }}');
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
        })
    </script>
@endsection

