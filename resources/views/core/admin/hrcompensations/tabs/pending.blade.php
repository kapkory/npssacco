<div class=" table-responsive">
    <div style="    /* position: relative; */
    padding: .25rem 1.25rem;
    /* margin-bottom: 1rem; */
    background-color: #edf0f5;
    color:#fd7e14;
    border: 1px solid transparent;
    border-radius: .25rem;" class="alert alert-styled-custom col-md-4 float-right font-weight-bolder small">
        <p><strong>Approved compensation days are added to Staff's Annual Leave Days.</strong></p>
    </div>
    @include('common.bootstrap_table_ajax',[
     'table_headers'=>["description","users.name"=>"staff","Status","days","Created_at","action"],
     'data_url'=>'admin/hrcompensations/list/pending',
     'base_tbl'=>'compensation_requests'
     ])
</div>
