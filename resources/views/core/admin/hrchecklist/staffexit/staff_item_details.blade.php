<?php
$path = 'storage/profile-pics';
$image = (@getimagesize($path . "/" .$staff->avatar)) ? $staff->avatar : "user.jpg";
$department_name = @$staff->getCalltronixDepartment()->name;
if (!$department_name)
    $department_name = "N/A";
$employment_status = getStaffEmploymentStatus($staff->employment_status);
?>

<div class="mt-3 card col-md-12">
    <div class="media-body chat-user-box">
        <div class="col-md-2 float-left mb-2">
            <img id="staff_avatar" class="d-flex  m-2 rounded-circle" src="{{ asset($path."/".$image) }}" alt="image" height="76" width="76">
            <span class="font-weight-bold small col-md-12 text-center">{{ $staff->name }}</span>
        </div>
        <div class="col-md-10 float-right">
            <table class="table">
                <tbody>
                <tr>
                    <th>Employment status:</th>
                    <td class="user-title font-weight-bold " >{!! getStaffEmploymentStatusButton($staff->employment_status) !!}</td>
                    <th>Department:</th>
                    <td id="staff_department">{{ $department_name }}</td>
                </tr>
                <tr>
                    <th>Employment date:</th>
                    <td class="user-title small font-weight-bold" >{{ \Carbon\Carbon::parse($staff->employment_date)->toDayDateTimeString() }}</td>
                    <th>Dismissal date:</th>
                    <td class="user-title small font-weight-bold" >{{ ($staff->dismissal_date) ? \Carbon\Carbon::parse($staff->dismissal_date)->toDayDateTimeString() : '' }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="card col-md-12">
    <div class="card-header col-md-12">Assigned Returnable Items </div>
    <div class="card-body table-responsive">
        @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["Name","Is_returnable","employee_onboarding_checklists.created_at"=>"Assigned_at","Returned_at","users.name"=>"Assigned_by","action"],
                    'data_url'=>'admin/hrchecklist/staffexit/staff-items-details-list/'.$staff->id,
                    'base_tbl'=>'onboarding_checklist_items',
                    'table_class' => ['table','condensed','small'],
                    'search_form'=>false
                    ])
    </div>
</div>

@include('common.auto_modal',[
                        'modal_id'=>'assign_checklistitem_modal',
                        'modal_title'=>'ASSIGN CHECK-LIST ITEM',
                        'modal_content'=>autoForm(['checklist_item_id','form_model'=>\App\Models\Core\EmployeeOnboardingChecklist::class],"admin/hrchecklist/onboarding/staff-items-details-add/".$staff->id)
                    ])

@include('common.auto_modal',[
                        'modal_id'=>'return_assigned_checklistitem_modal',
                        'modal_title'=>'Return Assigned Item',
                        'modal_content'=>autoForm(['date_returned','item_condition'],"admin/hrchecklist/staffexit/staff-items-details-return/".$staff->id)
                    ])

<script>
    $(function(){
        autoFillSelect('checklist_item_id','{{ url('admin/hrchecklist/onboarding/staff-items-details-list-unassigned/'.$staff->id.'?all=1') }}')

        // $('input[name="assigned_at"]').daterangepicker({
        //     singleDatePicker: !0,
        //     locale: {format: "YYYY-MM-DD HH:mm:ss"}
        // });
        $('input[name="date_returned"]').datetimepicker({
            timepicker: false,
            format:'Y-m-d'
        });

        // $('.dropify').dropify();
        $('.select2').select2();
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
    })
    function showAssignedChecklistItem(item_id){
        $("input[name='id']").val(item_id)
        $("#return_assigned_checklistitem_modal").modal('show')
    }
</script>
