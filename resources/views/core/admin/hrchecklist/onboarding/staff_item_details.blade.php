<?php
$path = 'storage/profile-pics';
$image = (@getimagesize($path . "/" .$staff->avatar)) ? $staff->avatar : "user.jpg";
$department_name = @$staff->getCalltronixDepartment()->name;
if (!$department_name)
    $department_name = "N/A";
$employment_status = getStaffEmploymentStatus($staff->employment_status);
?>

<div class="mt-3 card col-md-12">
    <div class="media-body chat-user-box">
        <div class="col-md-2 float-left">
            <img class="d-flex  m-2 rounded-circle" src="{{ asset($path."/".$image) }}" alt="image" height="76" width="76">
            <span class="font-weight-bold small col-md-12 text-center">{{ $staff->name }}</span>
        </div>
        <div class="col-md-10 float-right">
            <table class="table">
                <tbody>
                <tr>
                    <th>Employment status:</th>
                    <td class="user-title font-weight-bold">{!! getStaffEmploymentStatusButton($staff->employment_status) !!}</td>
                    <th>Department:</th>
                    <td id="staff_department">{{ $department_name }}</td>
                </tr>
                <tr>
                    <th>Employment date:</th>
                    <td class="user-title small font-weight-bold" >{{ \Carbon\Carbon::parse($staff->employment_date)->toDayDateTimeString() }}</td>
                    <th>Dismissal date:</th>
                    <td class="user-title small font-weight-bold" >{{ ($staff->dismissal_date) ? \Carbon\Carbon::parse($staff->dismissal_date)->toDayDateTimeString() : '' }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="card col-md-12">
    <div class="card-header col-md-12">Assigned Items @if($employment_status == "active") <a href="#assign_checklistitem_modal" onclick="loadChecklistItems()" class="btn btn-sm btn-success float-right" data-toggle="modal"><i class="fa fa-check"></i> Checklist</a> @endif</div>
    <div class="card-body">
        @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["Name","Is_returnable","employee_onboarding_checklists.created_at"=>"Assigned_at","users.name"=>"Assigned_by","action"],
                    'data_url'=>'admin/hrchecklist/onboarding/staff-items-details-list/'.$staff->id,
                    'base_tbl'=>'onboarding_checklist_items',
                    'table_class' => ['table','condensed','small'],
                    'search_form'=>false
                    ])
    </div>
</div>

@include('common.auto_modal_lg_static',[
    'modal_id'=>'assign_checklistitem_modal',
    'modal_title'=>'ASSIGN CHECK-LIST ITEMS',
    'modal_content'=>'<div class="checklist_items_details container" ></div>'
    ])

<script>
    $(function(){
        autoFillSelect('checklist_item_id','{{ url('admin/hrchecklist/onboarding/staff-items-details-list-unassigned/'.$staff->id.'?all=1') }}')
        $('input[name="assigned_at"]').daterangepicker({
            singleDatePicker: !0,
            locale: {format: "YYYY-MM-DD"}
        });
        // $('.dropify').dropify();
        $('.select2').select2();
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
    })

    function loadChecklistItems(){
        let url = '{{ url('admin/hrchecklist/itemcategories/assign-staff-items/'.$staff->id) }}';
        ajaxLoad(url,'checklist_items_details')
    }

</script>
