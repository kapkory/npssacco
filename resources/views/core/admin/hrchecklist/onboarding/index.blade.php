@extends('layouts.admin')
@section('bread_crumb')
    {{--    <a href="{{ url('admin/staffs') }}" class="breadcrumb-item load-page"> Staffs</a>--}}
    <span class="breadcrumb-item active">Onboarding Checklist</span>
@endsection
@section('title') Onboarding Checklist @endsection

@section('content')
    <style>
        .staff_target_link.active{
            background-color: #edeeef;
            border-radius: 5px;
        }
        .staff_target_link:hover {
            background-color: #edeeef;
            border-radius: 5px;
        }
        /*.slimscroll{*/
        /*    background: rgba(162, 177, 208, 0.13);*/
        /*    width: 7px;*/
        /*    !*position: static;*!*/
        /*    top: 0px;*/
        /*    opacity: 1;*/
        /*    display: block;*/
        /*    border-radius: 7px;*/
        /*    z-index: 99;*/
        /*    !*right: 1px;*!*/
        /*    !*height: 323.232px;*!*/
        /*}*/
    </style>
    <div class="row">
        <div class="col-12">
            <!-- Left sidebar -->
            <div class="email-leftbar">
                <div class="card mt-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <a href="{{ url('admin/hrchecklist/itemcategories') }}" class="btn btn-link btn-round btn-custom btn-block waves-effect waves-light  load-page p-1" style="text-align: left"><i class="fas fa-check-square mr-2"></i>Edit Checklist Items</a>

                                <h5 class="my-1">Staff&nbsp;&nbsp;<button class="btn btn-dark badge btn-sm float-right">{{ count($staffs) }}</button></h5>
                                <div class="">
                                    <div class="chat-search mt-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input style="border-radius: 5px !important; height: 2.0rem !important;" onkeyup="searchText()" type="text" id="search-input" name="staff-search" class="form-control" placeholder="Search Staff name">
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $path = 'storage/profile-pics';
                                    ?>
                                    <div class="staff-list slimscroll" id="myUl" style="max-height: 400px; overflow: scroll">
                                        @foreach($staffs as $staff)
                                            <?php $image = (@getimagesize($path . "/" .$staff->avatar)) ? $staff->avatar : "user.jpg";
                                            $first_user_id = null;
                                            ?>
                                            <div class="staff_target" id="div_{{ $staff->id }}" >
                                                <a id="tab_{{ $staff->id }}" href="#" onclick="loadStaffTab('{{ $staff->id }}')" class="media staff_target_link tab_{{ $staff->id }}" >
                                                    <img class="d-flex mr-3 rounded-circle" src="{{ asset($path."/".$image) }}" alt="image" height="40" width="40">
                                                    <div id="chat_body_{{ $staff->id }}" class="media-body chat-user-box ">
                                                        <p class="user-title m-0 staff_name">{{ $staff->name }}</p>
                                                        <p class="text-muted">
                                                            <?php $department_name = @$staff->getCalltronixDepartment()->name;
                                                            if (!$department_name)
                                                                $department_name = "N/A";
                                                            if($first_user_id == null)
                                                                $first_user_id = $staff->id;
                                                            ?>
                                                            {{ $department_name }}
                                                        </p>
                                                    </div>
                                                    <i style="display: none; margin-top:1.5em;  margin-bottom:1.5em " id="check_icon_{{ $staff->id }}" class="fa fa-check-circle text-success media-tab-check "></i>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-9">
                                <div class="email-rightbar">
                                    <div class=" staff_details_tab">

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- end card-body -->
                </div><!-- end card -->
            </div><!-- End Left sidebar -->
            <!-- Right Sidebar -->
        </div>
    </div>
    <script>
        $(function(){
            let user_id = window.localStorage.getItem('staff_user_id');
            if(!user_id)
                user_id = '{{ @$first_user_id }}';

            if(user_id){
                $( "#tab_"+user_id).trigger( "click" );
            }
        })
        function loadStaffTab(user_id,state=0){
            $(".staff_target_link").removeClass('active');
            $('#tab_'+user_id).addClass('active');
            $(".media-tab-check").hide();
            $('#check_icon_'+user_id).show();
            window.localStorage.setItem('staff_user_id',user_id)
            let url = '{{ url('admin/hrchecklist/onboarding/staff-items-details') }}'+'/'+user_id;
            setTimeout(function () {
                ajaxLoad(url,'staff_details_tab')
            }, state);
        }

        function searchText(){
            input = document.getElementById("search-input");
            filter = input.value.toUpperCase();
            var length = document.getElementsByClassName('staff_target').length;

            for (i=0; i<length; i++){
                if(document.getElementsByClassName('staff_name')[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
                    document.getElementsByClassName("staff_target")[i].style.display = "block";
                }
                else{
                    document.getElementsByClassName("staff_target")[i].style.display = "none";
                }
            }
        }
    </script>
@endsection

