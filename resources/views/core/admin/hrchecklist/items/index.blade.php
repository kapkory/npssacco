@extends('layouts.admin')
@section('bread_crumb')
    <a href="{{ url('admin/hrchecklist/onboarding') }}" class="breadcrumb-item load-page"> Onboarding Checklist</a>
    <span class="breadcrumb-item active">Checklist Items</span>
@endsection
@section('title') Checklist Items  @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="#onboardingchecklistitem_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD ONBOARDINGCHECKLISTITEM</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["name","description","Is_returnable","Created_at","users.name"=>"created_by","action"],
                    'data_url'=>'admin/hrchecklist/items/list',
                    'base_tbl'=>'onboarding_checklist_items'
                    ])

                    @include('common.auto_modal',[
                        'modal_id'=>'onboardingchecklistitem_modal',
                        'modal_title'=>'CHECK-LIST ITEM FORM',
                        'modal_content'=>autoForm(['name','description','is_returnable','form_model'=>\App\Models\Core\OnboardingChecklistItem::class],"admin/hrchecklist/items")
                    ])
                </div>

            </div>

        </div>
    </div>
@endsection

