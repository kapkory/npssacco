@extends('layouts.admin')
@section('bread_crumb')
    <a href="{{ url('admin/hrchecklist/onboarding') }}" class="breadcrumb-item load-page"> Onboarding Checklist</a>
    <a href="{{ url('admin/hrchecklist/itemcategories') }}" class="breadcrumb-item load-page"> Item Categories</a>
    <span class="breadcrumb-item active">{{ $itemcategory->name }} Items</span>
@endsection
@section('title') Item Category: {{ $itemcategory->name }}  @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="#" onclick="newChecklistItemModal()" class="btn btn-info btn-sm float-right"><i class="fa fa-plus"></i> ADD CHECKLIST ITEM</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["name","description","Is_returnable","Created_at","users.name"=>"created_by","action"],
                    'data_url'=>'admin/hrchecklist/itemcategories/items/list/'.$itemcategory->id,
                    'base_tbl'=>'onboarding_checklist_items'
                    ])

                    {{--                    @include('common.auto_modal',[--}}
                    {{--                        'modal_id'=>'onboardingchecklistitem_modal',--}}
                    {{--                        'modal_title'=>'CHECK-LIST ITEM FORM',--}}
                    {{--                        'modal_content'=>autoForm(['name','description','is_returnable','hidden_item_category_id'=>$itemcategory->id,'form_model'=>\App\Models\Core\OnboardingChecklistItem::class],"admin/hrchecklist/items")--}}
                    {{--                    ])--}}
                </div>

            </div>

        </div>
    </div>


    <div id="onboardingchecklistitem_modal" class="modal fade " data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="onboardingchecklistitem_modal_label" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="onboardingchecklistitem_modal_label"> <b>{{ strtoupper($itemcategory->name) }} :</b> CHECK-LIST ITEM FORM</h3>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></button>
                </div>

                <div class="modal-body ">
                    <div class="section">
                        <form id="checklist_item_form" class="ajax-post" method="post" action="{{ url('admin/hrchecklist/items') }}">
                            @csrf
                            <input type="hidden" name="id">
                            <input type="hidden" name="form_model" value="{{ \App\Models\Core\OnboardingChecklistItem::class }}">
                            <input type="hidden" name="item_category_id" value="{{ $itemcategory->id }}">
                            <div class="form-group name">
                                <div class="fg-line">
                                    <label class="fg-label control-label label_name">Name</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                            </div>
                            <div class="form-group description">
                                <div class="fg-line">
                                    <label class="fg-label control-label label_description">Description</label>
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="row col-md-12">
                                <div class="form-group col-md-6">
                                    <label class="fg-label control-label label_is_returnable">Is Returnable</label>
                                    <div class="select">
                                        <select name="is_returnable" class="form-control select2">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="fg-label control-label label_is_notifiable">Notify department</label>
                                    <div class="select">
                                        <select name="is_notifiable" class="form-control select2">
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div style="display: none" id="variable_fields" class="row col-md-12">
                                <div class="form-group col-md-6">
                                    <label class="fg-label control-label">Department</label>
                                    <div class="select">
                                        <select name="calltronix_department_id" class="form-control select2"></select>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label class="fg-label control-label label_disposition">Disposition</label>
                                    <div class="select">
                                        <select name="disposition_id" class="form-control select2"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row col-md-12">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-raised submit-btn"><i class="fa fa-save"></i> Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        window.disposition_id = null;
        $(function () {
            $('.select2').select2();
            $(".form-control-min-search").select2({
                minimumResultsForSearch: Infinity
            });
            $("select[name='is_notifiable']").val(0).trigger('change');
            autoFillSelect('calltronix_department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');

        })

        $("select[name='calltronix_department_id']").on('change', function (e) {
            $("select[name='disposition_id']").val(null).trigger('change');
            changeDispositions($(this).val())
        })
        $("select[name='is_notifiable']").on('change', function (e) {
            let is_notifiable = $(this).val();
            if(is_notifiable == 1)
                $("#variable_fields").show()
            else
                $("#variable_fields").hide()
            $("select[name='calltronix_department_id']").val(null).trigger('change');
        })

        function changeDispositions(id){
            autoFillSelect('disposition_id', '{{ url('admin/settings/config/issuecategories/list/dispositions?calltronix_department_id=') }}' + id,'setSelectedDisposition' )
        }

        function setSelectedDisposition(){
            $("select[name='disposition_id']").val(window.disposition_id).trigger('change')
        }
        function newChecklistItemModal(){
            $("select[name='disposition_id']").val('').trigger('change');
            $("select[name='is_notifiable']").val('').trigger('change')
            $("input[name='id']").val('')
            $("input[name='name']").val('')
            $("textarea[name='description']").val('')
            $("select[name='is_returnable']").val('').trigger('change')
            $("select[name='calltronix_department_id']").val('').trigger('change')
            $("#onboardingchecklistitem_modal").modal('show')
        }

        function editChecklistItem(element){
            var items_data = $(element).data('model');
            window.disposition_id = items_data.disposition_id;

            $("select[name='is_notifiable']").val(items_data.is_notifiable).trigger('change')
            $("input[name='id']").val(items_data.id)
            $("input[name='name']").val(items_data.name)
            $("textarea[name='description']").val(items_data.description)
            $("select[name='is_returnable']").val(items_data.is_returnable).trigger('change')
            $("select[name='calltronix_department_id']").val(items_data.calltronix_department_id).trigger('change')
            $("#onboardingchecklistitem_modal").modal('show')
            setSelectedDisposition()
        }
    </script>
@endsection

