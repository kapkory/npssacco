@extends('layouts.admin')
@section('bread_crumb')
    <a href="{{ url('admin/hrchecklist/onboarding') }}" class="breadcrumb-item load-page"> Onboarding Checklist</a>
    <span class="breadcrumb-item active">Item categories</span>
@endsection
@section('title') Item categories  @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="#itemcategory_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD ITEM CATEGORY</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["name","description","Created_at","users.name"=>"created_by","action"],
                    'data_url'=>'admin/hrchecklist/itemcategories/list',
                    'base_tbl'=>'item_categories'
                    ])

                    @include('common.auto_modal',[
                        'modal_id'=>'itemcategory_modal',
                        'modal_title'=>'ITEM CATEGORY FORM',
                        'modal_content'=>autoForm(['name','description','form_model'=>\App\Models\Core\ItemCategory::class],"admin/hrchecklist/itemcategories")
                    ])
                </div>

            </div>

        </div>
    </div>
@endsection

