<form class="ajax-post" method="post" action="{{ url("admin/hrchecklist/onboarding/staff-items-details-add/".$staff->id) }}">
    @csrf
    <input type="hidden" name="form_model" value="{{ \App\Models\Core\OnboardingChecklistItem::class }}">
    <input type="hidden" name="staff_id" value="{{ $staff->id }}">
    <input class="form-control" type="hidden" name="checklist_item_ids" >


    @foreach($item_categories as $category)
        <label for="item_category" class="ml-2"><h5>{{ $category->name }}</h5></label>

        @foreach($category->onboardingChecklistItems->chunk(4) as $chunk)
            <div class="row text-justify">
                @foreach($chunk as $item)
                    <div class="form-check-inline checkbox col-sm" >
                        <div class="custom-control custom-checkbox form-group">
                            <input type="checkbox" name="checklist_item_ids[]" value="{{ $item->id }}"  @if(in_array($item->id,$existing))checked @endif  class="custom-control-input form-control" id="{{$item->id}}"
                                   data-parsley-multiple="groups"
                                   data-parsley-mincheck="2">
                            <label class="custom-control-label" for="{{ $item->id }}">{{ $item->name }}</label>
                        </div>
                    </div>

                @endforeach
            </div>
        @endforeach

    @endforeach


    <div class="form-group row col-md-12 text-right">
        <div class="col-md-12">
            <button style="color: #fff;background-color: #ff943d; border-color: #ff943d;" type="submit" class="btn btn-primary btn-raised submit-btn"><i class="fa fa-save"></i> Submit</button>
        </div>
    </div>
</form>

<script type="text/javascript">
    $("#assign_checklistitem_modal_label").html("<h4>{!! "H.R Checklist for <b>".$staff->user->name !!}</b></h4>")
</script>

