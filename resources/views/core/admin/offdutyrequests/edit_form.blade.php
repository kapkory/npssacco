
<form id="edit_offdutyrequest" class="model_form_id" method="post" action="{{ url('admin/offdutyrequests') }}">
    <input type="hidden" name="id" value="{{ $offduty->id }}">
    @csrf
    <div class="row">
        <div class=" col-md-12">
            <div class="fg-line">
                <label class="fg-label control-label label_dates">Dates:</label>
                <div class="row col-md-12 form-group">
                    <input value="{{ @$f_date->date }}"  type="text" name="dates[{{ @$f_date->id }}]" class="dates_field form-control col-md-10">&nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="button" onclick="addDatesFieldRow('edit_additional_dates')" class="btn btn-md btn-info "><i class="fa fa-plus"></i></button>
                </div>
                <div id="edit_additional_dates" class="mt-3">
                    @foreach($dates as $date)
                        <div id="{{ "row_".$date->id  }}" class="row">
                            <div class="col-md-12 row">
                                <div class="col-md-11 " style="padding-left: 9px; padding-right: 2px;">
                                    <div class=" form-group col-md-12" style="padding-left: 2px; padding-right: 2px;">
                                        <input type="text" value="{{ $date->date }}" placeholder="date" class="form-control dates_field" name="dates[{{ $date->id }}]" >
                                    </div>
                                </div>
                                <div class="">
                                    <div class=" float-right">
                                        &nbsp;<button type="button" onclick="deleteRow(`{{ "row_".$date->id }}`)" class="btn-md btn btn-danger "><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="form-group description">
        <div class="fg-line">
            <label class="fg-label control-label label_description">Description</label>
            <textarea name="description" class="form-control">{{ $offduty->description }}</textarea>
        </div>
    </div>
    <div class="form-group row col-md-12">
        <div class="col-md-12">
            <button onclick="submitFormData('edit_offdutyrequest','edit-submit-btn')" style="color: #fff;background-color: #ff943d; border-color: #ff943d;" type="button" class="btn btn-info btn-raised edit-submit-btn"><i class="fa fa-save"></i> <b>Submit</b></button>
        </div>
    </div>
</form>
<script type="text/javascript">
    initiateDatePicker()
</script>
