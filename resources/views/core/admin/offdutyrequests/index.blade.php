@extends('layouts.admin')

@section('title') Off Duty Requests @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="#offdutyrequest_modal" class="btn btn-info btn-sm clear-form float-right"
                       data-toggle="modal"><i class="fa fa-plus"></i> REQUEST OFF DUTY</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["id","description","Status","days","Created_at","action"],
                    'data_url'=>'admin/offdutyrequests/list',
                    'base_tbl'=>'offduty_requests'
                    ])
                </div>

            </div>

        </div>
    </div>

    <div id="offdutyrequest_modal" class="modal fade" data-backdrop="static" role="dialog"
         aria-labelledby="offdutyrequest_modal_label">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="offdutyrequest_modal_label">OFF DUTY REQUEST FORM</h3>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div style="    /* position: relative; */
    padding: .25rem 1.25rem;
    /* margin-bottom: 1rem; */
    background-color: #edf0f5;
    color:#fd7e14 !important;
    border: 1px solid transparent;
    border-radius: .25rem;" class="alert alert-styled-custom col-md-12 font-weight-semibold small">Request to work on an
                        Off Day ie weekend or holiday..
                    </div>
                    <div class="section">
                        <form id="offdutyrequest" class="model_form_id" method="post"
                              action="{{ url('admin/offdutyrequests') }}">
                            <input type="hidden" name="id">
                            @csrf
                            <div class="row">
                                <div class=" col-md-12">
                                    <div class="fg-line">
                                        <label class="fg-label control-label label_dates">Dates:</label>
                                        <div class="row col-md-12 form-group">
                                            <input type="text" name="dates[0]"
                                                   class="dates_field form-control col-md-10">&nbsp;&nbsp;&nbsp;&nbsp;
                                            <button type="button" onclick="addDatesFieldRow('additional_dates')"
                                                    class="btn btn-md btn-info "><i class="fa fa-plus"></i></button>
                                        </div>
                                        <div id="additional_dates" class="mt-3">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group description">
                                <div class="fg-line">
                                    <label class="fg-label control-label label_description">Description</label>
                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group row col-md-12">
                                <div class="col-md-12">
                                    <button onclick="submitFormData('offdutyrequest')"
                                            style="color: #fff;background-color: #ff943d; border-color: #ff943d;"
                                            type="button" class="btn btn-info btn-raised submit-btn"><i
                                            class="fa fa-save"></i> <b>Submit</b></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div id="edit_offdutyrequest_modal" class="modal fade" data-backdrop="static" role="dialog"
         aria-labelledby="offdutyrequest_modal_label">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="edit_offdutyrequest_modal_label">OFF DUTY REQUEST FORM</h3>
                    <button class="btn btn-danger btn-sm" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <div class="edit_offduty_section">

                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        function loadEditOffdutyForm(offduty_id) {
            ajaxLoad('{{ url('admin/offdutyrequests/offdutyrequest/edit') }}' + '/' + offduty_id, 'edit_offduty_section')
        }

        $(function () {
            initiateDatePicker()

        })

        function addDatesFieldRow(div_id) {
            let num = Math.round(111 + Math.random() * (99999));
            let row_id = 'row_' + num;
            var field_str = $('<div id="' + row_id + '" class="row">\n' +
                '    <div class="col-md-12 row">\n' +
                '        <div class="col-md-11 " style="padding-left: 9px; padding-right: 2px;">\n' +
                '            <div class=" form-group col-md-12" style="padding-left: 2px; padding-right: 2px;">\n' +
                '                <input type="text" placeholder="date" class="form-control dates_field" name="dates[' + num + ']" >\n' +
                '            </div>\n' +
                '        </div>\n' +
                '        <div class="">\n' +
                '            <div class=" float-right">\n' +
                '                &nbsp;<button type="button" onclick="deleteRow(`' + row_id + '`)" class="btn-md btn btn-danger "><i class="fa fa-times"></i></button>\n' +
                '                </div>\n' +
                '        </div>\n' +
                '    </div>\n' +
                '</div>').hide();
            $("#" + div_id).append(field_str);
            field_str.show(500);
            initiateDatePicker()
        }

        function deleteRow(row_id) {
            $("#" + row_id).hide(300, function () {
                $(this).remove()
            })
        }

        function initiateDatePicker() {
            $('.dates_field').datetimepicker({
                timepicker: false,
                format: 'Y-m-d'
            });
        }

        function submitFormData(form_id, submit_btn_class = 'submit-btn') {
            event.preventDefault();
            var form = $("#" + form_id);
            var btn = form.find("." + submit_btn_class);
            btn.prepend('<img class="processing-submit-image" style="height: 50px;margin:-10px !important;" src="{{ url("img/Ripple.gif") }}">');
            btn.attr('disabled', true);
            this.form = form;
            $(".fg-line").removeClass('has-error');
            var url = $(form).attr('action');
            var data = $(form).serialize();

            $.post(url, data).done(function (response) {
                var btn = form.find("." + submit_btn_class);
                btn.find('img').remove();
                btn.attr('disabled', false);
                sanitizeModalCss(form_id + "_modal")
                // $("#offdutyrequest_modal").modal('hide')
                endLoading(response);
                removeError();
                ajaxLoad(response.redirect)
                return false;

            })
                .fail(function (xhr, status, error) {
                    var btn = form.find(".submit-btn");
                    btn.find('img').remove();
                    btn.attr('disabled', false);
                    if (xhr.status == 422) {
                        form.find('.alert_status').remove();
                        var response = JSON.parse(xhr.responseText).errors;
                        for (field in response) {

                            var str = field.split('.');
                            var str2 = field.split('dates.');
                            var new_field = field;
                            var error_mess = response[field];
                            if (str.length > 1) {
                                new_field = str[0] + '[' + str[1] + ']';
                                error_mess = response[field];
                                if (error_mess) {
                                    error_mess = String(error_mess).replace(str2[1], "");
                                }
                            }

                            form.find("input[name='" + new_field + "']").addClass('is-invalid');
                            form.find("input[name='" + new_field + "']").closest(".form-group").find('.help-block').remove();
                            // form.find("input[name='" + new_field + "']").closest(".form-group").append('<small class="help-block invalid-feedback">' + response[field] + '</small>');
                            form.find("input[name='" + new_field + "']").closest(".form-group").append('<small class="help-block invalid-feedback">' + error_mess + '</small>');

                            form.find("select[name='" + new_field + "']").addClass('is-invalid');
                            form.find("select[name='" + new_field + "']").closest(".form-group").find('.help-block').remove();
                            form.find("select[name='" + new_field + "']").closest(".form-group").append('<small class="help-block invalid-feedback">' + response[field] + '</small>');

                            form.find("textarea[name='" + new_field + "']").addClass('is-invalid');
                            form.find("textarea[name='" + new_field + "']").closest(".form-group").find('.help-block').remove();
                            form.find("textarea[name='" + new_field + "']").closest(".form-group").append('<small class="help-block invalid-feedback">' + response[field] + '</small>');

                        }

                        jQuery(".invalid-feedback").css('display', 'block');
                    } else if (xhr.status == 406) {
                        form.find('#form-exception').remove();
                        form.find('.alert_status').remove();
                        form.prepend('<div id="form-exception" class="alert alert-warning"><strong>' + xhr.status + '</strong> ' + error + '<br/>' + xhr.responseText + '</div>');
                    } else {
                        form.find('#form-exception').remove();
                        form.find('.alert_status').remove();
                        form.prepend('<div id="form-exception" class="alert alert-danger"><strong>' + xhr.status + '</strong> ' + error + '<br/>(' + url + ')</div>');
                    }

                });
        }

        function sanitizeModalCss(modal_id) {
            $('#' + modal_id).modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }
    </script>
@endsection

