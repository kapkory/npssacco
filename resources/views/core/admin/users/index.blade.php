@extends('layouts.admin')
@section('title')
    System Users
@endsection
@section('bread_crumb')
    <span class="breadcrumb-item active">Users</span>
@endsection
@section('content')

    @if(Request::ajax())
        <script type="text/javascript">
            $(".form-control-select2").select2();
            $("select[name='department_id']").select2();
        </script>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="#add_user" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i
                            class="fa fa-plus"></i> ADD USER</a>
{{--                    &nbsp;<a href="#import_users" data-toggle="modal"--}}
{{--                       class="btn btn-light btn-sm buttons-csv buttons-html5"><i--}}
{{--                            class="fa fa-file-excel"></i> Import Users</a>--}}
                    <div class="table-responsive">
                        @include('common.bootstrap_table_ajax',[
                      'table_headers'=>["name","email","permission_groups.name"=>"permission_group","is_staff","action"],
                      'data_url'=>'admin/users/list',
                      'table_class'=>['table','small','condensed'],
                      'base_tbl'=>'users'
                      ])
                    </div>
                </div>

            </div>


        </div>
    </div>
    @include('common.auto_modal',[
          'modal_id'=>'add_user',
          'modal_title'=>'CREATE USER',
          'modal_content'=>autoForm(['first_name','email','password','department_id','last_name','phone','password_confirmation','permission_group_id','form_model'=>\App\User::class],'admin/users'),
      ])
    @include('common.auto_modal',[
        'modal_id'=>'import_users',
        'modal_title'=>'IMPORT USERS FORM',
        'modal_content'=>autoForm(['file', 'form_model'=>\App\User::class],"admin/users/import")
    ])
    <script type="text/javascript">

        $(function () {
            autoFillSelect('department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');
            autoFillSelect('permission_group_id', '{{ url("admin/permissiongroup/list?all=1") }}');
            $("select[name='department_id']").select2({
                dropdownParent: $("#add_user")
            });
            $("select[name='permission_group_id']").select2({
                dropdownParent: $("#add_user")
            });
            if ($('body').hasClass('sidebar-xs')) {
                $('.sidebar-main-toggle').trigger('click');
            }
            $('#secondary-sidebar').html('');
            $('#secondary-sidebar').removeClass('sidebar sidebar-light sidebar-secondary sidebar-expand-md');
        })
    </script>

@endsection
