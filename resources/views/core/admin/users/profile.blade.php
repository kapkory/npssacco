@extends('layouts.admin')

@section('title')
    System User
@endsection
@section('bread_crumb')

    <a href="{{ url('admin/users') }}" class="breadcrumb-item load-page"> Users</a>
    <span class="breadcrumb-item active">User profile: {{$user->name}}</span>
@endsection


@section('page_title')
    <span class="font-weight-semibold">Home</span> -
    User Profile
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            @include('common.model_display',[
                                'keys'=>['name','email','phone','department','role','created_at'],
                                'model'=>$user
                            ])
                        </div>
                        <div class="col-md-4">
                            <h3>Main Actions</h3>
                            <a onclick="editUser()" data-toggle="modal" class="btn btn-outline-info">
                                <img style="max-height: 50px;"
                                     src="{{ url(env('CDN_HOST','')."/img/edit-profile.png") }}">
                                <br/>
                                Edit Details
                            </a>
                            <a onclick="updatePassword()" data-toggle="modal" class="btn btn-outline-info">
                                <img style="max-height: 50px;"
                                     src="{{ url(env('CDN_HOST','')."/img/edit-password.png") }}">
                                <br/>
                                Change Password
                            </a>

                        </div>
                    </div>

                    @include('common.auto_modal',[
                        'modal_id'=>'edit_profile_details',
                        'modal_title'=>'Edit User Profile ',
                        'modal_content'=>autoForm(['name','email','phone','department_id', 'hidden_user_id'],"admin/users/user/update")
                    ])
                    @include('common.auto_modal',[
                           'modal_id'=>'update_password',
                           'modal_title'=>'New Password',
                           'modal_content'=>autoForm(['password','password_confirmation','hidden_user_id'],'admin/users/user/update-password'),
                       ])
                </div>

            </div>


        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            autoFillSelect('department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');
            $("select[name='department_id']").select2({
                dropdownParent: $("#edit_profile_details")
            });
        })
        function editUser() {
            $("input[name='name']").val('{{ $user->name }}');
            $("input[name='email']").val('{{ $user->email }}');
            $("input[name='phone']").val('{{ $user->phone }}');
            $("select[name='department_id']").val('{{ $user->calltronix_department_id }}').change();
            $("input[name='user_id']").val('{{ $user->id }}');

            $("#edit_profile_details").modal('show');
        }

        function updatePassword() {
            $("input[name='user_id']").val('{{ $user->id }}');
            $("#update_password").modal('show');
        }

        $('#secondary-sidebar').html('');
        $('#secondary-sidebar').removeClass('sidebar sidebar-light sidebar-secondary sidebar-expand-md');

        if ($('body').hasClass('sidebar-xs')) {
            $('.sidebar-main-toggle').trigger('click');
        }

    </script>

@endsection

