@extends('layouts.admin')

@section('title')
    Create vacancy
@endsection

@section('bread_crumb')
    <a href="{{ url('admin/vacancies') }}" class="breadcrumb-item load-page"> Vacancies</a>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                    <form method="POST" class="ajax-post" action="{{url('admin/vacancies')}}">
                        @csrf
                        <input type="hidden" name="form_model" value="{{\App\Models\Core\Vacancy::class}}">

                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="department_id">Department</label>
                                <select name="department_id" class=" select2 custom-select form-control"  style="width: 100%;">

                                </select>

                            </div>
                            <div class="form-group  col-md-6">
                                <label for="line_of_business_id">Vacancy Position</label>
                                <select class="select2 form-control custom-select" name="position_id"  style="width: 100%;">

                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label class="control-label" for="priority">Priority </label>
                                <select class="form-control-min-search custom-select form-control"    style="width: 100%;"
                                        name="priority">
                                    <option value="">Please select</option>
                                    <option value="1">Low</option>
                                    <option value="2">Normal</option>
                                    <option value="3">High</option>
                                    <option value="4">urgent</option>
                                </select>
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="assigned_to">Assigned to</label>
                                <select class=" select2 custom-select form-control" id="assigned_to" name="assigned_to"    style="width: 100%;">

                                </select>
                            </div>

                        </div>
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <label for="description">Description </label>
                                <textarea id="description" class="form-control" name="description" rows="5"
                                          placeholder="Brief description of your case"
                                          spellcheck="false"></textarea>
                            </div>


                        </div>


                        <button type="submit" class="btn btn-primary submit-btn"><i class="fa fa-save"></i> Submit
                        </button>

                    </form>
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>
    <script type="text/javascript">
        var department_id;
        $("select[name='department_id']").on('change', function (e) {
            $("select[name='position_id']").val(null).trigger('change');
            department_id = $(this).val();

        })
        function changeDispositions(id){
            autoFillSelect('disposition_id', '{{ url('admin/settings/config/issuecategories/list/dispositions?issue_category_id=') }}' + id + '&department_id=' + department_id )

        }
        $(function () {
            $('.select2').select2();
            $(".form-control-min-search").select2({
                minimumResultsForSearch: Infinity
            });
            autoFillSelect('department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');
        });


    </script>
@endsection
