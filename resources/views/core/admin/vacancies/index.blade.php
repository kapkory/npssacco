@extends('layouts.admin')

@section('title') Vacancies @endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{ url('admin/vacancies/create') }}" class="btn btn-info btn-sm load-page float-right" ><i class="fa fa-plus"></i> ADD VACANCY</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["name","departments.name"=>"department","positions.name"=>"position_id","date_created","application_deadline","status","action"],
                    'data_url'=>'admin/vacancies/list',
                    'base_tbl'=>'vacancies'
                    ])

                    @include('common.auto_modal',[
                        'modal_id'=>'vacancy_modal',
                        'modal_title'=>'VACANCY FORM',
                        'modal_content'=>autoForm(['title','description','application_deadline','application_deadline','department_id','position_id'],"admin/vacancies")
                    ])
                </div>

            </div>

        </div>
    </div>
@endsection

