@extends('layouts.admin')
@section('page-title')
    Admin
@endsection
@section('title')
    My leave requests
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Leave requests</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class=" table-responsive">
                        <a class="btn btn-sm btn-info load-page float-right" href="{{ url('admin/leaverequests/create') }}" ><i class="fa fa-plus"></i> <b>Request leave</b></a>

                        @include('common.bootstrap_table_ajax',[
                  'table_headers'=>["leave_types.name"=>"leave_type","date_from"=>"Request_date","date_to"=>"Return_date","Status","leave_requests.reporting_comments"=>"Comments","leave_requests.id"=>"Days", "action"],
                  'data_url'=>'admin/leaverequests/list',
                  'base_tbl'=>'leave_requests'
                  ])
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->

        </div><!--end col-->
    </div>
    @include('common.auto_modal_lg_static',[
   'modal_id'=>'more_leave_request_info_modal',
   'modal_title'=>'Leave Request Details',
   'modal_content'=>'<div class="leave_request_details row " ></div>'
])
    <script type="text/javascript">
        function getMoreDetails(id) {
            // $(".modal-dialog").addClass('modal-lg')
            ajaxLoad('{{ url("admin/leaverequests/leaverequest/details") }}/' + id, 'leave_request_details')
        }


        function updateTicket(id) {
            $(".modal-dialog").removeClass('modal-lg')
            ajaxLoad('{{ url("admin/tickets/ticket/update") }}/' + id, 'leave_request_details')
        }

        $(function () {
            autoFillSelect('duties_to_be_assumed_by','{{ url('admin/staffs/my-department/list') }}')
            autoFillSelect('leave_type_id','{{ url('admin/settings/config/leavetypes/list?all=1') }}')
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
        })
    </script>
@endsection
