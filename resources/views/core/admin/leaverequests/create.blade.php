@extends('layouts.admin')

@section('bread_crumb')

    <a href="{{ url('admin/leaverequests') }}" class="breadcrumb-item load-page"> My Leave requests</a>
    <span class="breadcrumb-item active">Create</span>
@endsection

@section('title') New leave request @endsection

@section('content')
    <div class="row justify-content-around">
        <div class="card col-md-10">
            <div class="card-body">
                <div class="">
                    <form class="ajax-post" action="{{ url('admin/leaverequests') }}" method="post">
                        @csrf
                        <div class="form-group leave_type_id">
                            <div class="fg-line">
                                <label class="fg-label control-label">Leave Type</label>
                                <div class="select">
                                    <select name="leave_type_id" class="form-control select2">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class=" row">
                            <?php $today = \Carbon\Carbon::yesterday(); ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="fg-label control-label ">Date from</label>
                                    <input type="text" name="date_from" class="form-control" value="{{ $today->addDay()->format('Y-m-d H:i') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="fg-label control-label ">Date to</label>
                                    <input type="text" name="date_to" class="form-control" value="{{ $today->addDays(2)->format('Y-m-d H:i') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="fg-label control-label ">Duties to be assumed by?</label>
                                    <div class="select">
                                        <select name="assuming_staff_id" class="select2 form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="fg-label control-label ">Contact while away</label>
                                    <input type="text" name="contact" class="form-control" value="{{ @Auth::user()->phone }}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group reason">
                            <div class="fg-line">
                                <label class="fg-label control-label label_reason">Reason</label>
                                <textarea type="text" name="reason" class="form-control" ></textarea>
                            </div>
                        </div>

                        <div class="form-group row col-md-12">
                            <div class="col-md-12">
                                <button style="color: #ffffff;background-color: #ff943d; border-color: #ff943d;" type="submit" class="btn btn-primary btn-raised submit-btn"><i class="fa fa-save"></i> <b>Submit</b></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('.select2').select2();
            $('input[name="date_from"]').datetimepicker({
                timepicker: true,
                format:'Y-m-d H:i'
            });
            $('input[name="date_to"]').datetimepicker({
                timepicker: true,
                format:'Y-m-d H:i'
            });

            $(".form-control-min-search").select2({
                minimumResultsForSearch: Infinity
            });
            autoFillSelect('assuming_staff_id','{{ url('admin/staffs/my-department/list') }}')
            autoFillSelect('leave_type_id','{{ url('admin/settings/config/leavetypes/list?all=1') }}')
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
        })
    </script>
@endsection
