@extends('layouts.admin')

@section('bread_crumb')

    <a href="{{ url('admin/tickets') }}" class="breadcrumb-item load-page"> Tickets</a>
    <span class="breadcrumb-item active">Ticket #{{$ticket->id}}</span>
@endsection

@section('title') Ticket #{{$ticket->id}} @endsection

@section('content')


    <div class="col-md-12 col-lg-12">
        <div class="card ">
            <div class="card-body">
                <a href="{{url('admin/tickets/create')}}"
                   class="btn btn-info btn-sm clear-form float-right load-page"><i
                        class="fa fa-plus"></i> NEW TICKET</a>

                <div class="mt-1">
                    @include('common.auto_tabs',[
         'tabs'=>['ticket_details' , 'update_history'],
         'tabs_folder'=>'core.admin.tickets.ticket.tabs',
         'base_url'=>'admin/tickets/ticket/'.$ticket->id
         ])
                </div>

            </div><!--end card-body-->
        </div><!--end card-->
    </div>
    <script type="text/javascript">
        $(function () {
            getTabCounts('{{ url('admin/tickets/ticket/'.$ticket->id.'?tabs=1') }}');
        });

    </script>

@endsection


