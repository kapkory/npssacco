<div class="col-sm-12">

    <!-- Right aligned -->
    <div class="card card-body border-top-teal">
        <div class=" activity-scroll1">
            <div class="activity">

                @foreach($ticket->TicketUpdates as $ticket_update)
                    <div class="activity-info">
                        <div class="icon-info-activity"><i
                                class="mdi mdi-checkbox-marked-circle-outline bg-soft-success"></i>
                        </div>
                        <div class="activity-info-text">
                            <div class="d-flex justify-content-between align-items-center">
                                <h6 class="m-0 w-70">{{@$ticket_update->user->name}}</h6>
                                <span
                                    class="text-muted d-block">{{\Illuminate\Support\Carbon::parse($ticket_update->created_at)->toDayDateTimeString()}}</span>
                            </div>
                            <table class="table table-condensed table-sm col-md-12 mt-3">
                                <tbody>
                                <tr>
                                    <th class="font-weight-bold">Disposition:</th>
                                    <td id="material_cost">{{ @$ticket_update->disposition->name }}</td>
                                    <th class="font-weight-bold"> Assigned to</th>
                                    <td id="material_location">
                                        {{ @$ticket_update->Assigned->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-weight-bold ">Updated At:</th>
                                    <td id="created_at">  {!! \Carbon\Carbon::parse($ticket_update->created_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket_update->created_at)->format('Y-m-d h:i:s A').")</b></small>" !!}</td>

                                    <th class="font-weight-bold ">Status:</th>
                                    <td id="status">{{$ticket_update->TicketStatus->name}}</td>

                                </tr>
                                <tr>
                                    <th class="font-weight-bold ">Comment:</th>

                                    <td colspan="3" class="text-left" id="status">{!! $ticket_update->comment !!}</td>

                                </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div><!--end activity-->
        </div>
    </div>
    <!-- /right aligned -->
</div>
