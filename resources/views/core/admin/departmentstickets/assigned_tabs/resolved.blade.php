<?php
/**
 * Created by PhpStorm.
 * User: kemboi
 * Date: 09/08/2020
 * Time: 13:18
 */
?>
<div class=" table-responsive fixed-solution  ">
    <?php
//    $removed_cols = ["ticket_statuses.name"=>"status","department_from.name"=>"department_from" ];
    ?>
@include('common.bootstrap_table_ajax',[
'table_headers'=>["tickets.id"=>"ticket_no","department_from.name"=>"department_from","issue_categories.name"=>"issue_category","dispositions.name"=>"disposition","users.name"=>"created_by","assigned_user.name"=>"assigned_to","Created_at","ticket_statuses.name"=>"status"],
'data_url'=>'admin/departmentstickets/assigned/list/'.$department->id.'?status=resolved&',
'base_tbl'=>'tickets',
'table_class'=>['small'],
])
</div>
