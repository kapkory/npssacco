<?php
/**
 * Created by PhpStorm.
 * User: kemboi
 * Date: 09/08/2020
 * Time: 13:18
 */
?>
<div class=" table-responsive fixed-solution  ">
@include('common.bootstrap_table_ajax',[
'table_headers'=>["tickets.id"=>"ticket_no","department_from.name"=>"department_from","department_to.name"=>"department_to","issue_categories.name"=>"issue_category","dispositions.name"=>"disposition","users.name"=>"created_by","assigned_user.name"=>"assigned_to","Created_at","ticket_statuses.name"=>"status"],
'data_url'=>'admin/departmentstickets/assigned/list/'.$department->id.'?status=all&',
'table_class'=>['small'],
'base_tbl'=>'tickets'
])
</div>
