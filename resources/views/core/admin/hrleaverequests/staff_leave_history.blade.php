@extends('layouts.admin')

@section('bread_crumb')

    <a href="{{ url('admin/hrleaverequests/viewleavetracker') }}" class="breadcrumb-item load-page"> Staff</a>
    <span class="breadcrumb-item active">Staff #{{$staff->id}}</span>
@endsection

@section('title'){{$staff->name}} @endsection

@section('content')

    <div class="col-md-12 col-lg-12">
        <div class="card ">
            <div class="card-body">
                <div class="mt-1">
                    <p> - covered {{@$myLeaveBalance[0]->days_covered}} - prev
                        bal {{@$myLeaveBalance[0]->prev_year_bal}}</p>
                    <table class="table" style="">
                        <tr>
                            <th>Rem days</th>
                            <td>{{@$myLeaveBalance[0]->days_remaining}}</td>
                            <th>Covered leave days</th>
                            <td>{{@$myLeaveBalance[0]->days_covered}}</td>
                            <th>Prev years Bal</th>
                            <td>{{@$myLeaveBalance[0]->prev_year_bal}}</td>
                            <td>
                                <a href="#update_leave_days" class="btn btn-info btn-sm clear-form float-right"
                                   data-toggle="modal"><i
                                        class="fa fa-plus"></i> Update Leave Days
                                </a>
                                <a href="#" class="btn btn-info btn-sm clear-form float-right mr-2"
                                   data-toggle="modal" onclick="return updateLeaveRecords(`{{$staff->id}}`)"><i
                                        class="fa fa-plus"></i> Update Leave Records
                                </a>
                            </td>
                        </tr>
                    </table>
                    @include('common.auto_modal',[
                          'modal_id'=>'update_leave_days',
                          'modal_title'=>'UPDATE LEAVE DAYS',
                          'modal_content'=>autoForm(['prev_year_bal','days_covered','form_model'=>\App\Models\Core\YearlyStaffLeavesTracker::class],'admin/hrleaverequests/update-leaves/'. $myLeaveBalance[0]->id),
                      ])
                    <div class=" table-responsive">
                        @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["leave_types.name"=>"leave_type","Date_from","Date_to","leave_types.is_paid"=>"Is_paid","Status","Created_at",],
                    'data_url'=>'admin/hrleaverequests/staff/list/'.$staff->id,
                    'base_tbl'=>'leave_requests'
                    ])
                    </div>

                </div><!--end card-body-->
            </div><!--end card-->
        </div>
    </div>

    @include('common.auto_modal_lg_static',[
'modal_id'=>'leave_request_record_modal',
'modal_title'=>'Leave Request Details',
'modal_content'=>'<div class="leave_request_record_modal " ></div>'
])

    <script>
        function updateLeaveRecords(id) {
            console.log(id)
            ajaxLoad('{{ url("admin/hrleaverequests/leaverequest/record") }}/' + id, 'leave_request_record_modal')
            $("#leave_request_record_modal").modal('show')
        }
    </script>
@endsection
