@extends('layouts.admin')
@section('page-title')
    Admin
@endsection
@section('title')
    Yearly Leaves Tracker Dashboard
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Leaves Tracker</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class=" table-responsive">
                        <div style="color: #ffffff; background-color: #b0cbec; border-radius: 7px; border-color: #FF8EB9F1;" class="alert col-sm-4 float-right alert-styled-right alert-rounded text-justify alert-dismissible">
                            Yearly Leaves Tracker Dashboard.
                        </div>
                        @include('common.bootstrap_table_ajax',[
                        'table_headers'=>['users.name'=>'staff_name','leave_types.name'=>'leave_type_name','year','prev_year_bal','max_days_allowed','days_remaining','action'],
                        'data_url'=>'admin/hrleaverequests/hrleavetracker',
                        'base_tbl'=>'yearly_staff_leaves_trackers'
                    ])
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->

        </div><!--end col-->
    </div>


@endsection
