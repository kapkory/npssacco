@extends('layouts.admin')
@section('page-title')
    Admin
@endsection
@section('title')
    Returning Leaves Dashboard
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Returning Leaves</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
                        'tabs'=>['today' , 'all'],
                        'tabs_folder'=>'core.admin.hrleaverequests.dashboard_tabs',
                        'base_url'=>'admin/hrleaverequests/dashboard'
                    ])
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->

        </div><!--end col-->
    </div>
    @include('common.auto_modal_lg_static',[
     'modal_id'=>'more_leave_request_info_modal',
     'modal_title'=>'Leave Request Details',
     'modal_content'=>'<div class="leave_request_details row " ></div>'
  ])
    @include('common.auto_modal_static',[
   'modal_id'=>'set_returned_modal',
   'modal_title'=>'Mark Staff Leave as Returned',
   'modal_content'=>autoForm(['hidden_id','date_returned','days_out','comment'],'admin/hrleaverequests/leaverequest/returned')
])

    <script type="text/javascript">

        function getMoreDetails(leave_request_id) {
            ajaxLoad('{{ url("admin/hrleaverequests/leaverequest/details") }}/' + leave_request_id, 'leave_request_details')
        }

        var request_leave_id = '';
        $('input[name="date_returned"]').change(function () {
            let date_returned = $('input[name="date_returned"]').val()
            let url = "{{url('admin/hrleaverequests/leaverequest/days-covered')}}/" + request_leave_id + '?date_returned=' + date_returned
            console.log(url)
            $.get(url).done(function (response) {
                $('input[name="days_out"]').val(response)
            });
        })

        function setAsReturned(leave_request_id) {
            request_leave_id = leave_request_id
            $("input[name='id']").val(leave_request_id)
            $("input[name='date_returned']").val()
            $("textarea[name='comment']").val('')
            $("#set_returned_modal").modal('show')
        }

        $(function () {
            $('input[name="date_returned"]').datetimepicker({
                timepicker: false,
                format: 'Y-m-d'
            });
            getTabCounts('{{ url('admin/hrleaverequests/dashboard/list/count') }}');
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
        })
    </script>
@endsection
