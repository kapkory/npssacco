<?php

$status_str = getLeaveRequestStatus($leave_request->status);
$status = $status_str;
if ($status_str == "hod_approved")
    $status = "<span class='text-primary'><i class='fa fa-spinner'></i> <b>Pending</b></span>";
else if ($status_str == "hr_approved")
    $status = "<span class='text-success'><i class='fa fa-thumbs-up'></i> <b>Approved</b></span>";
else if ($status_str == "hr_rejected")
    $status = "<span class='text-danger'><i class='fa fa-thumbs-down'></i> <b>Rejected</b></span>";
?>
<div class="card-body col-md-12">
    @if(getLeaveRequestStatus($leave_request->status) == "hod_approved")
        <a href="#" onclick="return approveRequest(`{{$leave_request->id}}`)">
            <button type="submit" style="margin-left: .635rem" class="btn btn-outline-success btn-sm"><i
                    class="fa fa-thumbs-up"></i> Approve Request
            </button>
        </a>
        <a href="javascript:void(0)" onclick="return rejectRequest(`{{$leave_request->id}}`)" data-toggle="modal">
            <button type="submit" style="margin-left: .635rem"
                    class="btn btn-outline-danger btn waves-effect waves-light btn-sm "><i
                    class="fa fa-thumbs-down"></i> Reject Request
            </button>
        </a>
    @endif
    @if(getLeaveRequestStatus($leave_request->status) == "hr_approved" )
        @if(\Carbon\Carbon::parse($leave_request->date_from) < \Carbon\Carbon::today() && $leave_request->date_returned == null)
            <a href="#" onclick="return setAsReturned(`{{$leave_request->id}}`)">
                <button type="submit" style="margin-left: .635rem" class="btn btn-outline-primary btn-sm"><i
                        class="fa fa-thumbs-up"></i> Mark As Returned
                </button>
            </a>
        @endif
    @endif
    <table class="table table-striped table-fit table-hover">
        <tbody>

        <tr>
            <th class="font-weight-bold"> Request No:</th>
            <td>#{{ $leave_request->id }}</td>
            <th class="font-weight-bold ">Created At:</th>
            <td>  {!! \Carbon\Carbon::parse($leave_request->created_at)->toDayDateTimeString()."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->created_at)->format('Y-m-d').")</b></small>" !!}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Staff</th>
            <td>{{ $staff->name }}</td>
            <th class="font-weight-bold ">Duties assumed by:</th>
            <td> {{ @$assuming_staff->name }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Date Requested:</th>
            <td class="font-weight-bold">{{ \Carbon\Carbon::parse($leave_request->date_from)->isoFormat('Do MMM Y') }}</td>
            <th class="font-weight-bold">Date Expected:</th>
            <td class="font-weight-bold">{!!  formatDeadline1($leave_request->date_to)  !!}</td>
        </tr>
        @if(\Carbon\Carbon::parse($leave_request->date_from) < \Carbon\Carbon::today())
            <tr>
                <th class="font-weight-bold">Date Returned:</th>
                <td class="font-weight-bold">{{ ($leave_request->date_returned) ? \Carbon\Carbon::parse($leave_request->date_returned)->isoFormat('Do MMM Y') : 'Not Yet' }}</td>
                <th class="font-weight-bold">Days Exceeded:</th>
                <td class="font-weight-bold">{!! getDaysDifference($leave_request->date_to,$leave_request->date_returned,true) !!}</td>
            </tr>
        @endif

        <tr>
            <th class="font-weight-bold">Leave Type:</th>
            <td>{{ $leave_request->leaveType->name }}</td>
            <th class="font-weight-bold">Status:</th>
            <td>{!! getLeaveRequestStatusButton($leave_request->status) !!}</td>
        </tr>

        <tr>
            <th class="font-weight-bold"> Reason</th>
            <td colspan="3">{{ $leave_request->reason }}</td>
        </tr>

        <!--A.M action details-->
        @if($leave_request->is_cse == 1 || isset($leave_request->am_action_at))
            <tr>
                <th class="font-weight-bold"> A.M. Action</th>
                <td>@if($leave_request->am_approve_comment)<span
                        class="text-success-800">Approved</span> @elseif($leave_request->am_reject_reason)<span
                        class="text-danger-800">Declined</span> @else<span class="text-info-800"> Pending</span>@endif
                </td>
                <th class="font-weight-bold ">Date of Action:</th>
                <td>  @if($leave_request->am_action_at) {!! \Carbon\Carbon::parse($leave_request->am_action_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->am_action_at)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
            </tr>
            <?php
            $am_comment_title = ($leave_request->am_approve_comment) ? "A.M approve comment" : null;
            if (!$am_comment_title)
                $am_comment_title = ($leave_request->am_reject_reason) ? "A.M decline reason" : null;
            $comment = ($leave_request->am_approve_comment) ? $leave_request->am_approve_comment : $leave_request->am_reject_reason;
            ?>
            @if($am_comment_title)
                <tr>
                    <th class="font-weight-bold"> {{ $am_comment_title }}</th>
                    <td colspan="3">{{ @$comment }}</td>
                </tr>
            @endif
        @endif
        <?php $statuses = ['am_approved', 'hod_approved', 'hr_approved', 'am_rejected', 'am_declined', 'hod_rejected', 'hod_declined', 'hr_rejected', 'hr_declined']; ?>
        <!--H.O.D. action details-->
        @if(in_array(getLeaveRequestStatus($leave_request->status),$statuses))
            <tr>
                <th class="font-weight-bold"> H.O.D Action</th>
                <td>@if($leave_request->hod_approve_comment)<span
                        class="text-success-800">Approved</span> @elseif($leave_request->hod_reject_reason)<span
                        class="text-danger-800">Declined</span> @else<span class="text-info-800"> Pending</span>@endif
                </td>
                <th class="font-weight-bold ">Date of Action:</th>
                <td>  @if($leave_request->hod_action_at) {!! \Carbon\Carbon::parse($leave_request->hod_action_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->hod_action_at)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
            </tr>
            <?php
            $hod_comment_title = ($leave_request->hod_approve_comment) ? "H.O.D approve comment" : null;
            if (!$hod_comment_title)
                $hod_comment_title = ($leave_request->hod_reject_reason) ? "H.O.D decline reason" : null;
            $comment = ($leave_request->hod_approve_comment) ? $leave_request->hod_approve_comment : $leave_request->hod_reject_reason;
            ?>
            @if($hod_comment_title)
                <tr>
                    <th class="font-weight-bold"> {{ $hod_comment_title }}</th>
                    <td colspan="3">{{ @$comment }}</td>
                </tr>
            @endif

            <!--HR. action details-->
            <?php $hrstatuses = ['hr_approved', 'hr_rejected', 'hr_declined', 'hod_approved']; ?>
            @if(in_array(getLeaveRequestStatus($leave_request->status),$hrstatuses))
                <tr>
                    <th class="font-weight-bold"> H.R Action</th>
                    <td>@if($leave_request->hr_approve_comment)<span
                            class="text-success-800">Approved</span> @elseif($leave_request->hr_reject_reason)<span
                            class="text-danger-800">Declined</span> @else<span
                            class="text-info-800"> Pending</span>@endif</td>
                    <th class="font-weight-bold ">Date of Action:</th>
                    <td>  @if($leave_request->hr_action_at) {!! \Carbon\Carbon::parse($leave_request->hr_action_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->hr_action_at)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
                </tr>

                <?php
                $hr_comment_title = ($leave_request->hr_approve_comment) ? "H.R approve comment" : null;
                if (!$hr_comment_title)
                    $hr_comment_title = ($leave_request->hr_reject_reason) ? "H.R. decline reason" : null;
                $hr_comment = ($leave_request->hr_approve_comment) ? $leave_request->hr_approve_comment : $leave_request->hr_reject_reason;
                ?>
                @if($hr_comment_title)
                    <tr>
                        <th class="font-weight-bold"> {{ $hr_comment_title }}</th>
                        <td colspan="3">{{ @$hr_comment }}</td>
                    </tr>
                @endif
            @endif
        @endif

        </tbody>
    </table>
</div>

@include('common.auto_modal_static',[
   'modal_id'=>'approve_leave_request_modal',
   'modal_title'=>'Approve Leave Request',
   'modal_content'=>autoForm(['hidden_id','approve_comment'],'admin/hrleaverequests/leaverequest/approve/')
])
@include('common.auto_modal_static',[
   'modal_id'=>'reject_leave_request_modal',
   'modal_title'=>'Reject Leave Request',
   'modal_content'=>autoForm(['hidden_id','reject_reason'],'admin/hrleaverequests/leaverequest/reject/')
])
@include('common.auto_modal_static',[
'modal_id'=>'set_returned_modal',
'modal_title'=>'Mark Staff Leave as Returned',
'modal_content'=>autoForm(['hidden_id','date_returned','days_out','comment'],'admin/hrleaverequests/leaverequest/returned')
])
<script type="text/javascript">
    $(function () {
        $('input[name="date_returned"]').datetimepicker({
            timepicker: false,
            format: 'Y-m-d'
        });
    })
    $('input[name="date_returned"]').change(function () {
        let date_returned = $('input[name="date_returned"]').val()
        let url = "{{url('admin/hrleaverequests/leaverequest/days-covered/'.$leave_request->id.'?date_returned=')}}"+date_returned
        $.get(url).done(function (response) {
            $('input[name="days_out"]').val(response)
        });
    })

    function setAsReturned(leave_request_id) {
        $("input[name='id']").val(leave_request_id)
        $("input[name='date_returned']").val()
        $("textarea[name='comment']").val('')
        $("#set_returned_modal").modal('show')
    }

    function approveRequest(leave_request_id) {
        $("input[name='id']").val(leave_request_id)
        $("textarea[name='approve_comment']").val('')
        $("#approve_leave_request_modal").modal('show')
    }

    function rejectRequest(leave_request_id) {
        $("input[name='id']").val(leave_request_id)
        $("textarea[name='reject_reason']").val('')
        $("#reject_leave_request_modal").modal('show')
    }
</script>
