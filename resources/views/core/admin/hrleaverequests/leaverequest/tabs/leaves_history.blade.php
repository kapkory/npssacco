<div class=" table-responsive">
    @include('common.bootstrap_table_ajax',[
'table_headers'=>["Date_from","Date_to","leave_types.is_paid"=>"Is_paid","Status","Created_at", "action"],
'data_url'=>'admin/hrleaverequests/staff/list/'.$staff->id,
'base_tbl'=>'leave_requests'
])
</div>
@include('common.auto_modal_lg_static',[
   'modal_id'=>'more_leave_request_info_modal',
   'modal_title'=>'Leave Request Details',
   'modal_content'=>'<div class="leave_request_details row " ></div>'
])
<script>
    function getMoreDetails(leave_request_id){
        // $(".modal-dialog").addClass('modal-lg')
        ajaxLoad('{{ url("admin/hrleaverequests/leaverequest/details") }}/' + leave_request_id, 'leave_request_details')
    }
</script>
