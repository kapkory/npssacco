<div class=" table-responsive">
    <div style="color: #ffffff; background-color: #b0cbec; border-radius: 7px; border-color: #FF8EB9F1;" class="alert col-sm-4 float-right alert-styled-right alert-rounded text-justify alert-dismissible">
        All staffs on leave and have not yet returned.
    </div>
    @include('common.bootstrap_table_ajax',[

    'table_headers'=>["leave_types.name"=>"leave_type","users.name"=>"staff","date_from"=>"Request_date","date_to"=>"Expected_date","leave_types.id"=>"Days_exceeded","leave_requests.id"=>"Days", "Status","action"],
    'data_url'=>'admin/hrleaverequests/dashboard/list/notreturned',
    'base_tbl'=>'leave_requests'
])
</div>
