<div class=" table-responsive">
    @include('common.bootstrap_table_ajax',[

    'table_headers'=>["leave_types.name"=>"leave_type","users.name"=>"staff","date_from"=>"Request_date","date_to"=>"Return_date","leave_requests.reporting_comments"=>"Comments","leave_requests.id"=>"Days","Status", "action"],
    'data_url'=>'admin/hrleaverequests/list',
    'base_tbl'=>'leave_requests'
])
</div>
