@extends('layouts.admin')

@section('title') Staff Exits @endsection

@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
{{--                <a href="#staffexit_modal" class="btn btn-info btn-sm clear-form float-right" data-toggle="modal"><i class="fa fa-plus"></i> ADD STAFFEXIT</a>--}}
{{--                    @include('common.bootstrap_table_ajax',[--}}
{{--                    'table_headers'=>["users.name"=>"staff","staffs.employment_status"=>"Staff_status","discharged_at","users1.name"=>"discharged_by","Discharge_reason"],--}}
{{--                    'data_url'=>'admin/staffexits/list',--}}
{{--                    'base_tbl'=>'staff_exits',--}}
{{--                    'table_class' => ['small']--}}

{{--                    ])--}}
                    @include('common.bootstrap_table_ajax',[
                        'table_headers'=>["users.name"=>"name","positions.name"=>"position","departments.name"=>"department","staff_roles.name"=>"role","users.phone"=>"phone","users.phone"=>"email","staffs.employment_status"=>"Status","employment_date","dismissal_date"],
                        'data_url'=>'admin/staffexits/list',
                          'table_class'=>["small"],
                        'base_tbl'=>'staffs'
                        ])

                    @include('common.auto_modal',[
                        'modal_id'=>'staffexit_modal',
                        'modal_title'=>'STAFFEXIT FORM',
                        'modal_content'=>autoForm(\App\Models\Core\StaffExit::class,"admin/staffexits")
                    ])
                 </div>

               </div>

            </div>
        </div>
@endsection

