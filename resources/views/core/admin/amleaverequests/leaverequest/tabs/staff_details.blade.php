<div class="staff_details1 row">
    <table class="table col-md-12">
        <tbody>
        <tr>
            <th class="font-weight-bold">First Name:</th>
            <td id="type">{{ $staff->firstname}}</td>
            <th class="font-weight-bold">Middle Name:</th>
            <td id="type">{{ $staff->middlename }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Last Name:</th>
            <td id="material_location">{{ $staff->lastname }}</td>
            <th class="font-weight-bold">Gender:</th>
            <td id="material_cost">{{ \App\Repositories\StatusRepository::getGender($staff->gender) }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Date of Birth</th>
            <td id="material_location">{!! \Carbon\Carbon::parse($staff->dob)->format('d-m-Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($staff->dob)->diffForHumans().")</b></small>" !!}</td>
            <th class="font-weight-bold ">Marital Status:</th>
            <td>{{ \App\Repositories\StatusRepository::getMaritalStatus($staff->marital_status)}}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Date of Employment</th>
            <td id="material_location">{!! \Carbon\Carbon::parse($staff->employment_date)->format('d-m-Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($staff->employment_date)->diffForHumans().")</b></small>" !!}</td>
            <th class="font-weight-bold ">Position of Employement:</th>
            <td>{{$staff->position}}</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Department</th>
            <td id="material_location">{{ $staff->department }}</td>
            <th class="font-weight-bold ">Role:</th>
            <td>{{$staff->role}}</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Line of Business</th>
            <td id="material_location">{{ $staff->lob }}</td>
            <th class="font-weight-bold ">Email:</th>
            <td>{{$staff->email}}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Created By</th>
            <td id="material_location">{{ \App\User::find($staff->created_by)->name }}</td>
            <th class="font-weight-bold ">Created At:</th>
            <td id="created_at">  {!! \Carbon\Carbon::parse($staff->created_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($staff->created_at)->format('Y-m-d h:i:s A').")</b></small>" !!}</td>
        </tr>
        </tbody>
    </table>
</div>
