@extends('layouts.admin')

@section('title')
    Create Ticket
@endsection

@section('bread_crumb')
    <a href="{{ url('admin/tickets') }}" class="breadcrumb-item load-page"> Tickets</a>
    <li class="breadcrumb-item active">Create</li>
@endsection
@section('content')
    <?php
    $count = count($resolved_tickets);
    ?>
    @if( $count > 0)
        <div style="border-radius: 18px"
             class="alert alert-outline-danger col-md-12 alert-styled-right text-justify alert-dismissible text-white">
            <button type="button" class="close" data-dismiss="alert"><i class="fa fa-times text-danger"></i></button>
            <span class="font-weight-semibold text-w">Warning!</span> You have <b>{{ $count }} Resolved Tickets!</b>
            Please
            <b>Close</b> or <b>Reopen</b> them to proceed with ticket creation! Thanks.
        </div>
        <div class=" row justify-content-center pb-2">
            <a data-toggle="tooltip" title="View Resolved Tickets" class="btn btn-success load-page "
               href="{{ url('admin/tickets?tab=resolved') }}">Resolved Tickets</a>
        </div>
        <script>
            // $(this).closest('form').find('input').prop('disabled', true);
            // $("#new_ticket_form :select").prop("disabled", true);
            $('form *').prop('disabled', true);
        </script>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body"><h4 class="header-title mt-0"></h4>

                    <form id="new_ticket_form" method="POST" class="ajax-post" action="{{url('admin/tickets')}}">
                        @csrf
                        <input type="hidden" name="form_model" value="{{\App\Models\Core\Ticket::class}}">
                        <div class="row">
                            <div class="form-group  col-md-6">
                                <label for="issue_source_id">Issue Source: <span class="text-danger">*</span> </label>
                                <select class="form-control-min-search form-control custom-select"
                                        name="issue_source_id">
                                </select>

                            </div>
                            <div class="form-group  col-md-6">
                                <label for="line_of_business_id">Ticket Subject:  <span class="text-danger">*</span></label>
                                <input type="text" autocomplete="off" name="subject" class="form-control">
                            </div>

                            <div class="form-group  col-md-6">
                                <label for="branches">Branch:  <span class="text-danger">*</span></label>
                                <select class="select2 form-control custom-select" name="branch_id">
                                </select>
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="department_id">Department: <span class="text-danger">*</span></label>
                                <select name="department_id" onchange="return department($(this).val())"
                                        class=" select2 custom-select form-control">
                                </select>
                            </div>

                            <div class="form-group  col-md-6">
                                <label for="issue_category_id">Issue Category:  <span class="text-danger">*</span></label>
                                <select onchange="return changeSubCategory($(this).val())"
                                        class="select2 form-control custom-select" name="issue_category_id">

                                </select>
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="issue_sub_category_id">Issue SubCategory </label>
                                <select onchange="return changeDispositions($(this).val())"
                                        class="select2 form-control custom-select" name="issue_sub_category_id">
                                </select>

                            </div>
                            <div class="form-group  col-md-6">
                                <label for="disposition_id">Disposition:  <span class="text-danger">*</span> </label>
                                <select class="select2 form-control custom-select" name="disposition_id">
                                </select>

                            </div>
                            <div class="form-group  col-md-6">
                                <label for="ticket_status_id">Ticket Status:  <span class="text-danger">*</span> </label>
                                <select class="select2 form-control custom-select" name="ticket_status_id">
                                </select>
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="priority">Priority:  <span class="text-danger">*</span> </label>
                                <select class="form-control-min-search form-control custom-select" name="priority">
                                    <option value="1"> Very Low</option>
                                    <option value="2"> Low</option>
                                    <option value="3" selected> Normal</option>
                                    <option value="4"> High</option>
                                    <option value="5"> Very High</option>
                                </select>
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="priority">Business Impact:  <span class="text-danger">*</span> </label>
                                <select class="form-control-min-search form-control custom-select" name="impact">
                                    <option value="1"> Low</option>
                                    <option value="2" selected> Medium</option>
                                    <option value="3"> High</option>
                                </select>
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="requester_id">Requester:  <span class="text-danger">*</span></label>
                                <select class=" select2 custom-select form-control" id="requester_id"
                                        name="requester_id">
                                </select>
                            </div>
                            <div class="form-group  col-md-6">
                                <label for="assigned_to">Assigned to :  <span class="text-danger">*</span></label>
                                <select class=" select2 custom-select form-control" id="assigned_to" name="assigned_to">

                                </select>
                            </div>

                        </div>
                        <div class="form-group pb-2">
                            <button id="toggle_files_section" type="button" class="btn btn-dark badge btn-sm"><i
                                    class="fa fa-plus toggle_files_section_icon"></i> Attach files:
                            </button>
                        </div>
                        <div id="upload_files_section" class="form-group mt-2 col-md-12 " style="display:none">
                            <label> Attach Files: <small>(optional)</small> </label>
                            <input type="file" class="file-input-ajax" name="files[]" id="files" multiple="multiple">
                        </div>
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <label for="description">Description </label>
                                <textarea id="description" class="form-control" name="description" rows="5"
                                          placeholder="Brief description of your case"
                                          spellcheck="false"></textarea>
                            </div>
                        </div>



                        <button type="submit" class="btn btn-primary submit-btn"><i class="fa fa-save"></i> Submit
                        </button>

                    </form>
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>
    @if($count > 0)

        <script>
            // $(this).closest('form').find('input').prop('disabled', true);
            // $("#new_ticket_form :select").prop("disabled", true);
            $('form *').prop('disabled', true);

        </script>
    @endif
    <script type="text/javascript">
        $(function () {
            loadDropZone('files', "{{ url("admin/tickets/tmp") }}?_token={{ csrf_token() }}", true, false, true)
            $("#toggle_files_section").click(function () {
                var icon_class = ".toggle_files_section_icon";
                if ($(icon_class).hasClass('fa-minus')) {
                    $(icon_class).removeClass('fa-minus')
                    $(icon_class).addClass('fa-plus')
                } else {
                    $(icon_class).removeClass('fa-plus')
                    $(icon_class).addClass('fa-minus')
                }
                $("#upload_files_section").toggle('slow', function () {
                });
            });
            loadSummerNote('description', 150, 'Brief description of your case');
            $('.select2').select2();
            $(".form-control-min-search").select2({
                minimumResultsForSearch: Infinity
            });
            autoFillSelect('department_id', '{{ url("admin/settings/config/departments/list?all=1") }}');
            autoFillSelect('issue_category_id', '{{ url('admin/settings/tickets/issuecategories/list?all=1') }}');
            autoFillSelect('issue_source_id', '{{ url('admin/settings/tickets/issuesources/list?all=1') }}');
            autoFillSelect('branch_id', '{{ url('admin/settings/tickets/branches/list?all=1') }}');
            {{--autoFillSelect('issue_sub_category_id', '{{ url('admin/settings/tickets/issuesubcategories/list?all=1') }}');--}}
            {{--autoFillSelect('disposition_id', '{{ url('admin/settings/tickets/dispositions/list?all=1') }}');--}}
            autoFillSelect('ticket_status_id', '{{ url('admin/settings/tickets/statuses/list?all=1') }}', 'setSelectedStatus');
            autoFillSelect('requester_id', '{{ url('admin/users/list?requester=1') }}', 'setSelectedRequester');

        });
        var department_id;
        function department(val) {
            department_id = val;
            loadAssignedToList(department_id)
        }
        function changeSubCategory(val) {
            autoFillSelect('issue_sub_category_id', '{{ url('admin/settings/tickets/issuesubcategories/list?issue_category_id=') }}' + val)
        }
        function changeDispositions(id) {
            var issue_category_id= $("select[name='issue_category_id']").val();
            autoFillSelect('disposition_id', '{{ url('admin/settings/tickets/dispositions/list?issue_sub_category_id=') }}' + id + '&issue_category_id=' + issue_category_id)
        }

        function loadAssignedToList(department_id) {
            autoFillSelect('assigned_to', '{{ url('admin/users/list?department_id=') }}' + department_id);
        }
        function setSelectedRequester(){
            $("select[name='requester_id']").val('{{ auth()->id() }}').change();
        }
        function setSelectedStatus(){
            $("select[name='ticket_status_id']").val(1).change();
        }
        $(document).ready(function () {
            runSilentAction('{{ url("admin/tickets/tmp?clear=1") }}');
        });

    </script>
@endsection
