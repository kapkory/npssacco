<div class="col-md-12 row ">
    <form action="{{url('admin/tickets')}}" class="mt-2 ajax-post col-md-12">
        @csrf
        <fieldset>
            <input type="hidden" name="id" value="{{@$ticket->id}}">
            <input type="hidden" name="assigned_to" value="{{@$ticket->assigned_id}}">
            <input type="hidden" name="requester_id" value="{{@$ticket->requester_id}}">
            <input type="hidden" name="requester_id" value="{{@$ticket->requester_id}}">
            <input type="hidden" name="department_id" value="{{@$ticket->department_id}}">
            <input type="hidden" name="issue_category_id" value="{{@$ticket->issue_category_id}}">
            <input type="hidden" name="issue_sub_category_id" value="{{@$ticket->issue_sub_category_id}}">
            <input type="hidden" name="priority" value="{{@$ticket->priority}}">
            <input type="hidden" name="impact" value="{{@$ticket->impact}}">
            <input type="hidden" name="disposition_id" value="{{@$ticket->disposition_id}}">
            <input type="hidden" name="issue_source_id" value="{{@$ticket->issue_source_id }}">
            <input type="hidden" name="subject" value="{{@$ticket->subject }}">
            <input type="hidden" name="branch_id" value="{{@$ticket->branch_id}}">
            <input type="hidden" name="ticket_status_id" value="{{$ticket->ticket_status_id}}">
            <input type="hidden" name="acknowledged" value="1">
            <input type="hidden" name="form_model" value="{{\App\Models\Core\Ticket::class}}">

            <div class="row">
                <div class="form-group col-md-12">
                    <label>TAT: <span class="text-danger">*</span></label>
                    <div class="row">
                        <div class="col-md-8" style="padding-right: 0;">
                            <div class="form-group">
                                <input type="number" min="1" name="tat" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-4" style="padding-left: 4px;">
                            <div class="form-group">
                                <select id="time" name="time" class="form-control-min-search form-control"
                                        data-fouc>
                                    <option value="days">Days</option>
                                    <option value="hours">Hours</option>
                                    <option value="minutes">Minutes</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-md-12">
                <div class="form-group ">
                    <label>Expected start date: &nbsp; &nbsp;<i data-toggle="tooltip"
                                                                title="Expected time you'll begin this task."
                                                                class="fa fa-question-circle"></i></label>
                    <div class="row">
                        <div class="col-md-12" style="padding-right: 0;">
                            <div class="form-group">
                                <input type="datetime-local" name="expected_start_date"
                                       value="{{ \Carbon\Carbon::now()->addMinutes(15)->roundMinutes(15)->format('m/d/Y') }}"
                                       class="form-control expected_start_date">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Message:</label>
                <textarea rows="5" cols="5" id="messsage" name="description" class="form-control message"
                          placeholder="Enter your message here"></textarea>
            </div>
        </fieldset>

        <div class="text-left">
            <button type="submit" class="btn btn-sm btn-primary submit-btn"><i
                    class="fa fa-save "></i> Submit Ticket
            </button>
            <a class="load-page" href="{{url('admin/tickets/ticket/'.$ticket->id)}}">
                <button type="button" class="btn btn-sm btn-danger">
                    Cancel <i
                        class="fa fa-backward ml-2"></i>
                </button>
            </a>

        </div>
    </form>
</div>
<script>
    $('input[name="expected_start_date"]').val(`{{ \Carbon\Carbon::now()->addMinutes(15)->roundMinutes(15)->format('m/d/Y')}}`)
    $(".form-control-min-search").select2({
        minimumResultsForSearch: Infinity
    });
    loadSummerNote('messsage', 120, 'Enter message');

</script>
