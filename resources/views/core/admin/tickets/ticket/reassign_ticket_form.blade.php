<div class="col-md-12 row ">
    <form action="{{url('admin/tickets')}}" class="mt-2 ajax-post col-md-12">
        @csrf
        <fieldset>
            <input type="hidden" name="id" value="{{@$ticket->id}}">
            <input type="hidden" name="department_id"
                   value="{{@$ticket->calltronix_department_id}}">
            <input type="hidden" name="line_of_business_id"
                   value="{{@$ticket->line_of_business_id}}">
            <input type="hidden" name="issue_category_id"
                   value="{{@$ticket->issue_category_id}}">
            <input type="hidden" name="priority" value="{{@$ticket->priority}}">
            <input type="hidden" name="disposition_id" value="{{@$ticket->disposition_id}}">
            <input type="hidden" name="ticket_status_id" value="{{@$ticket->ticket_status_id}}">
            <input type="hidden" name="re_assigned" value="1">
            <input type="hidden" name="form_model" value="{{\App\Models\Core\Ticket::class}}">

            <div class="row">
                <div class="form-group col-md-12">
                    <label> Assigned to: <span class="text-danger">*</span></label>
                    <select id="assigned_to" name="assigned_to"
                            data-placeholder="Select Assigned User"
                            class="form-control  update-select2 " data-fouc>
                    </select>
                </div>

            </div>
            <div class="row">

            </div>
            <div class="form-group">
                <label>Message:</label>
                <textarea rows="5" cols="5" name="description" id="assign_description" class="form-control message"
                          placeholder="Enter your message here"></textarea>
            </div>
        </fieldset>

        <div class="text-left">
            <button type="submit" class="btn btn-sm btn-primary submit-btn"><i
                    class="fa fa-save "></i> Submit Ticket
            </button>
            <a class="load-page" href="{{url('admin/tickets/ticket/'.$ticket->id)}}">
                <button type="button" class="btn btn-sm btn-danger">
                    Cancel <i
                        class="fa fa-backward ml-2"></i>
                </button>
            </a>
        </div>
    </form>
</div>
<script type="text/javascript">
    var ticket_department_id= `{{$ticket->department_id}}`
   $(function (){
       loadSummerNote('assign_description', 120, 'Enter message');
       $(".update-select2").select2();
       autoFillSelect('assigned_to', '{{ url('admin/users/list?department_id=') }}' + ticket_department_id, 'setAssigned');
   })
    function setAssigned() {
        $("select[name='assigned_to']").val('{{@$ticket->assigned_id }}')
    }
</script>
