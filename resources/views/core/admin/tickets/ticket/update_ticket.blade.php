<?php //1-Open, 2 - in_progress, 3-wait_for_response, 4-Resolved, 5-Closed
$allowed_statuses = [];
if (auth()->id() == $ticket->user_id) {
    $allowed_statuses = [1, 5];
    if ($ticket->ticket_status_id == 5)
        $allowed_statuses = [1];
} else if (auth()->id() == $ticket->assigned_id) {
    if ($ticket->ticket_status_id == 1)
        $allowed_statuses = [2, 3, 4];
    else if ($ticket->ticket_status_id == 2)
        $allowed_statuses = [3, 4];
    else if ($ticket->ticket_status_id == 3)
        $allowed_statuses = [4];
    else
        $allowed_statuses = [];
}
?>

@if(Request::ajax())
    <script type="text/javascript">
        $(".form-control-select2").select2();
        $(".update-select2").select2();
    </script>

@endif
<style>


    /* Important part */
    .modal-dialog {
        overflow-y: initial !important
    }

    .modal-body {
        overflow-y: auto;
    }
</style>
<div class="col-md-12 ">
    <form action="{{url('admin/tickets')}}" class="mt-2 ajax-post col-md-12">
        @csrf
        <fieldset>
            <input type="hidden" name="id" value="{{@$ticket->id}}">
            <input type="hidden" name="assigned_to" value="{{@$ticket->assigned_id}}">
            <input type="hidden" name="department_id" value="{{@$ticket->calltronix_department_id}}">
            <input type="hidden" name="issue_category_id" value="{{@$ticket->issue_category_id}}">
            <input type="hidden" name="priority" value="{{@$ticket->priority}}">
            <input type="hidden" name="form_model" value="{{\App\Models\Core\Ticket::class}}">
            <div class="row">
                <div class="form-group  col-md-6">
                    <label for="issue_source_id">Issue Source: <span class="text-danger">*</span> </label>
                    <select class="form-control-min-search form-control custom-select"
                            name="issue_source_id">
                    </select>

                </div>
                <div class="form-group  col-md-6">
                    <label for="line_of_business_id">Ticket Subject: <span class="text-danger">*</span></label>
                    <input type="text" name="subject" value="{{$ticket->subject}}" class="form-control">
                </div>

                <div class="form-group  col-md-6">
                    <label for="branches">Branch: <span class="text-danger">*</span></label>
                    <select class="update-select2 form-control custom-select" name="branch_id">
                    </select>
                </div>
                <div class="form-group  col-md-6">
                    <label for="department_id">Department: <span class="text-danger">*</span></label>
                    <select name="department_id" onchange="return department($(this).val())"
                            class=" update-select2 custom-select form-control">
                    </select>
                </div>

                <div class="form-group  col-md-6">
                    <label for="issue_category_id">Issue Category: <span class="text-danger">*</span></label>
                    <select onchange="return changeSubCategory($(this).val())"
                            class="update-select2 form-control custom-select" name="issue_category_id">

                    </select>
                </div>
                <div class="form-group  col-md-6">
                    <label for="issue_sub_category_id">Issue SubCategory </label>
                    <select onchange="return changeDispositions($(this).val())"
                            class="update-select2 form-control custom-select" name="issue_sub_category_id">
                    </select>

                </div>
                <div class="form-group  col-md-6">
                    <label for="disposition_id">Disposition: <span class="text-danger">*</span> </label>
                    <select class="update-select2 form-control custom-select" name="disposition_id">
                    </select>

                </div>
                <div class="form-group col-md-6">
                    <label> Ticket Status: <span class="text-danger">*</span></label>
                    <select id="ticket_status_id" name="ticket_status_id" data-placeholder="Select Ticket Status"
                            class="form-control update-select2 " data-fouc>
                        @foreach($statuses as $status)
                            <option
                                {{ (in_array($status['id'],$allowed_statuses )) ? "" : "disabled" }} {{ ($ticket->ticket_status_id == $status['id']) ? "selected" : "" }} value="{{ $status['id'] }}">{{ $status['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group  col-md-6">
                    <label for="priority">Priority: <span class="text-danger">*</span> </label>
                    <select class="form-control-min-search form-control custom-select" name="priority">
                        <option value="1"> Very Low</option>
                        <option value="2"> Low</option>
                        <option value="3" selected> Normal</option>
                        <option value="4"> High</option>
                        <option value="5"> Very High</option>
                    </select>
                </div>
                <div class="form-group  col-md-6">
                    <label for="priority">Business Impact: <span class="text-danger">*</span> </label>
                    <select class="form-control-min-search form-control custom-select" name="impact">
                        <option value="1"> Low</option>
                        <option value="2" selected> Medium</option>
                        <option value="3"> High</option>
                    </select>
                </div>

                <div class="form-group  col-md-6">
                    <label for="requester_id">Requester: <span class="text-danger">*</span></label>
                    <select class=" update-select2 custom-select form-control" id="requester_id"
                            name="requester_id">
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label> Assigned to: <span class="text-danger">*</span></label>
                    <select id="assigned_to" name="assigned_to" data-placeholder="Select Assigned User"
                            class="form-control update-select2 " data-fouc>
                    </select>
                </div>

            </div>
            <div class="form-group">
                <label>Comments:</label>
                <textarea rows="5" cols="5" name="description" id="description" class="form-control message"
                          placeholder="Enter your comments here"></textarea>
            </div>
            <div class="form-group pb-2">
                <button id="toggle_files_section" type="button" class="btn btn-dark badge btn-sm"><i
                        class="fa fa-plus toggle_files_section_icon "></i> Attach files:
                </button>
            </div>
            <div id="upload_files_section" class="form-group mt-2 col-md-12 " style="display:none">
                <label> Attach Files: <small>(optional)</small> </label>
                <input type="file" class="file-input-ajax" name="files[]" id="files" multiple="multiple">
            </div>
        </fieldset>

        <div class="text-left">
            <button type="submit" class="btn btn-sm btn-primary submit-btn"><i class="fa fa-save "></i> Submit Ticket
            </button>
           <a class="load-page" href="{{url('admin/tickets/ticket/'.$ticket->id)}}">
               <button type="button" class="btn btn-sm btn-danger" >
                   Cancel <i
                       class="fa fa-backward ml-2"></i></button>
           </a>
        </div>
    </form>
</div>
@include('common.auto_modal_lg_static',[
'modal_id'=>'more_ticket_info_modal',
'modal_title'=>'Ticket Details',
'modal_content'=>'<div class="ticket_details row " ></div>'
])

<script type="text/javascript">
    $("#toggle_files_section").click(function () {
        var icon_class = ".toggle_files_section_icon";
        if ($(icon_class).hasClass('fa-minus')) {
            $(icon_class).removeClass('fa-minus')
            $(icon_class).addClass('fa-plus')
        } else {
            $(icon_class).removeClass('fa-plus')
            $(icon_class).addClass('fa-minus')
        }
        $("#upload_files_section").toggle('slow', function () {
        });
    });
    window.url = "{{ url('/') }}";

    function getMoreDetails(id) {
        $(".modal-dialog").addClass('modal-lg')
        ajaxLoad('{{ url("admin/tickets/ticket/details") }}/' + id, 'ticket_details')
    }

    var call_history = $("#call-history");
    $(function () {
        $(".update-select2").select2();
        loadDropZone('files', "{{ url("admin/tickets/tmp") }}?_token={{ csrf_token() }}", true, false)

        let department_id = '{{ @$ticket->department_id }}';
        autoFillSelect('issue_category_id', '{{ url('admin/settings/config/issuecategories/list?all=1') }}', 'setSelectedIssueCategory')
        autoFillSelect('issue_source_id', '{{ url('admin/settings/tickets/issuesources/list?all=1') }}', 'setSelectedSource');
        autoFillSelect('line_of_business_id', '{{ url('admin/settings/config/lineofbusiness/list?all=1') }}');
        autoFillSelect('assigned_to', '{{ url('admin/users/list?department_id=') }}' + department_id, 'setAssigned');
        autoFillSelect('branch_id', '{{ url('admin/settings/tickets/branches/list?all=1') }}', 'setSelectedBranch');
        autoFillSelect('department_id', '{{ url("admin/settings/config/departments/list?all=1") }}', 'setSelectedDepartment');
        autoFillSelect('issue_category_id', '{{ url('admin/settings/tickets/issuecategories/list?all=1') }}', 'setSelectedIssueCategory');
        autoFillSelect('issue_sub_category_id', '{{ url('admin/settings/tickets/issuesubcategories/list?all=1') }}', 'setSelectedIssueSubCategory');
        autoFillSelect('disposition_id', '{{ url('admin/settings/tickets/dispositions/list?all=1') }}','setSelectedDisposition');
        autoFillSelect('requester_id', '{{ url('admin/users/list?requester=1') }}', 'setSelectedRequester');

        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
        loadSummerNote('description', 120, 'Enter comments');
    })

    function setSelectedIssueSubCategory() {
        $("select[name='issue_sub_category_id']").val('{{@$ticket->issue_sub_category_id }}')
    }

    function setSelectedSource() {
        $("select[name='issue_source_id']").val('{{@$ticket->issue_source_id }}')
    }

    function setSelectedIssueCategory() {
        $("select[name='issue_category_id']").val('{{@$ticket->issue_category_id }}')
    }

    function setSelectedDepartment() {
        $("select[name='department_id']").val('{{@$ticket->department_id }}')
    }

    function setSelectedBranch() {
        $("select[name='branch_id']").val('{{@$ticket->branch_id }}')
    }

    function setSelectedStatus() {
        $("select[name='ticket_status_id']").val('{{@$ticket->ticket_status_id }}')
    }

    function setSelectedDisposition() {
        $("select[name='disposition_id']").val('{{@$ticket->disposition_id }}')
    }

    function setSelectedIssueCategory() {
        $("select[name='issue_category_id']").val('{{@$ticket->issue_category_id }}')
    }

    function setAssigned() {
        $("select[name='assigned_to']").val('{{@$ticket->assigned_id }}')
    }

    function changeSubCategory(val) {
        autoFillSelect('issue_sub_category_id', '{{ url('admin/settings/tickets/issuesubcategories/list?issue_category_id=') }}' + val)
    }
    function setSelectedRequester(){
        $("select[name='requester_id']").val('{{ $ticket->requester_id }}').change();
    }
    $(document).ready(function () {
        runSilentAction('{{ url("admin/tickets/tmp?clear=1") }}');
    });
</script>
