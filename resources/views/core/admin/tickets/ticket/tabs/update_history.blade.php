<style>
    .excerpt {
        padding: 15px;
        background: #f0f2f8;
        border: 1px solid #f1f1f1;
    }
    .closed {
        border-color: #5AC17F!important;
    }
    .ticket-block .ticket-body {
        border-left: 3px solid #ccc;
    }
</style>
<?php
$path = 'storage/profile-pics';
$image = "user.jpg";
if (@getimagesize($path . "/" . Auth::user()->avatar))
    $image = Auth::user()->avatar;
?>
<div class="col-sm-12">
    @foreach($ticket->TicketUpdates as $ticket_update)
        <div class="ticket-block ">
            <div class="row">
                <div class="col-auto">
                    <img class="media-object wid-60 img-radius" src="{{ asset($path."/".$image) }}" alt="avator ">
                </div>
                <div class="col">
                    <div class="card ticket-body closed">
                        <div class="row align-items-center">
                            <div class="col border-right">
                                <div class="card-body">
                                    <div class="ticket-customer font-weight-bold">{{@$ticket_update->user->name}}</div>
                                    <ul class="list-inline mt-2 mb-0">
                                        <li class="list-inline-item">Assigned
                                            to {{ @$ticket_update->Assigned->name }}</li>
                                        <li class="list-inline-item"><i
                                                class="feather icon-calendar mr-1 f-14"></i>{{\Illuminate\Support\Carbon::parse($ticket_update->created_at)->toDayDateTimeString()}}
                                        </li>
                                    </ul>
                                    <div class="table-responsive">
                                        <table class="table small col-md-12 mt-3">
                                            <tbody>
                                            <tr>
                                                <th class="font-weight-bold">Disposition:</th>
                                                <td id="material_cost">{{ @$ticket_update->disposition->name }}</td>
                                                <th class="font-weight-bold ">Status:</th>
                                                <td id="status">{{$ticket_update->TicketStatus->name}}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="excerpt mt-2">
                                        <h6>Comment </h6>
                                        <p>{!! $ticket_update->comment !!}</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endforeach
</div>
