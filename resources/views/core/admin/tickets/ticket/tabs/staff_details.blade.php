<div class="row justify-content-center">
    <div class=" col-md-10">
        <div class="staff_details1 card">
            <div class="card-header">
                Basic staff details:
                <span style="color:rgb(255, 148, 61)" class="float-right font-weight-bold">(Staff Assigned Checklist Items )</span>
            </div>
            <div class="card-body ">
                <table class="table table-condensed  table-striped small">
                    <tbody>
                    <tr>
                        <th class="font-weight-bold">Name:</th>
                        <td >{{ $staff->name}}</td>
                        <th class="font-weight-bold">Gender:</th>
                        <td id="material_cost">{{ \App\Repositories\StatusRepository::getGender($staff->gender) }}</td>
                    </tr>
                    <tr>
                        <th class="font-weight-bold ">Position of Employement:</th>
                        <td id="status">{{$staff->position}}</td>
                        <th class="font-weight-bold">Employee Number:</th>
                        <td id="employee_number" class="font-weight-bold">{{ $staff->employee_number}}</td>
                    </tr>
                    <tr>
                        <th class="font-weight-bold">Department</th>
                        <td id="material_location">{{ $staff->department }}</td>
                        <th class="font-weight-bold ">Role:</th>
                        <td id="status">{{$staff->role}}</td>
                    </tr>
                    <tr>
                        <th class="font-weight-bold">Line of Business</th>
                        <td id="material_location">{{ $staff->lob }}</td>
                        <th class="font-weight-bold ">Email:</th>
                        <td id="status">{{$staff->email}}</td>
                    </tr>
                    <tr>
                        <th class="font-weight-bold"> Created By</th>
                        <td id="material_location">{{ \App\User::find($staff->created_by)->name }}</td>
                        <th class="font-weight-bold ">Created At:</th>
                        <td id="created_at">  {!! \Carbon\Carbon::parse($staff->created_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($staff->created_at)->format('Y-m-d h:i:s A').")</b></small>" !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
