<?php //1-Open, 2 - in_progress, 3-wait_for_response, 4-Resolved, 5-Closed
$status_str = getTicketStatus($ticket->ticket_status_id);
$show_update_button = false;

if ($ticket->assigned_id == auth()->id()) {
    if ($ticket->ticket_status_id < 5)
        $show_update_button = true;
}
if ($ticket->user_id == auth()->id())
    if ($ticket->ticket_status_id < 5)
        $show_update_button = true;
?>
<div class="ticket_details1 row">
    <div class="table-responsive ticket_table ticket_details_div">
        <table class="table col-md-12 table-condensed small">
            <tbody>
            @if($ticket->status=="Closed")
            @else
                @if($show_update_button && $ticket->tat !== null)
                    <a href="javascript:void(0)" onclick="return updateTicket(`{{$ticket->id}}`)"
                       class="btn btn-primary btn-sm clear-form float-left text-right align-right"
                       style="margin-left: .635rem"><i class="fa fa-edit"></i>Update</a>
                @endif
                @if($ticket->status=="Open"  && $ticket->tat == null)
                    @if($ticket->assigned_id == auth()->id())
                        <a href="javascript:void(0)" onclick="return acknowledgeTicket(`{{$ticket->id}}`) ">
                            <button type="submit" style="margin-left: .635rem"
                                    class="btn btn-success btn-sm submit-btn"><i
                                    class="fas fa-handshake"></i> Acknowledge
                            </button>
                        </a>
                    @endif
                    @if($ticket->user_id == auth()->id())
                        <a href="#cancel_ticket_modal" data-toggle="modal">
                            <button type="submit" style="margin-left: .635rem"
                                    class="btn btn-warning btn-sm submit-btn"><i
                                    class="fas fa-times-circle"></i> Cancel Ticket
                            </button>
                        </a>
                    @endif
                @endif
                @if($ticket->assigned_id == auth()->id() && $ticket->status!="Closed" && $ticket->status!="Cancelled")
                    <a href="javascript:void(0)" onclick="return reassignTicket(`{{$ticket->id}}`) ">
                        <button type="submit" style="margin-left: .635rem" class="btn btn-dark btn-sm submit-btn">
                            <i
                                class="fas fa-redo-alt"></i>&nbsp;Re-Assign
                        </button>
                    </a>
                @endif
            @endif
            <tr>
                <th class="font-weight-bold">Ticket Number:</th>
                <td id="type">#{{str_pad($ticket->id,6,0, STR_PAD_LEFT)}}</td>
                <th class="font-weight-bold">Department:</th>
                <td id="type">{{ $ticket->department_name }}</td>
            </tr>
            @if($ticket->tat)
                <tr>
                    <th class="font-weight-bold">Acknowledged At:</th>
                    <td>@if($ticket->acknowledged_at){!! \Carbon\Carbon::parse($ticket->acknowledged_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->acknowledged_at)->format('h:i:s A').")</b></small>" !!}@endif</td>
                    <th class="font-weight-bold">T.A.T:</th>
                    <td>{{ \Carbon\CarbonInterval::minute($ticket->tat)->cascade()->forHumans() }}</td>
                </tr>
            @endif

            <tr>
                <th class="font-weight-bold">Ticket Age:</th>
                <td>{!! getTicketAge($ticket->id) !!}</td>
                <th class="font-weight-bold">Closed At:</th>
                <td>@if($ticket->closed_at){!! \Carbon\Carbon::parse($ticket->closed_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->closed_at)->format('h:i:s A').")</b></small>" !!}@else
                        Not yet. @endif</td>
            </tr>


            @if($ticket->resolved_at && $ticket->acknowledged_at)
                <tr>
                    <th class="font-weight-bold">Resolved At:</th>
                    <td>{!! \Carbon\Carbon::parse($ticket->resolved_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->resolved_at)->format('h:i:s A').")</b></small>" !!}</td>
                    <th class="font-weight-bold">Actual Resolution Time:</th>
                    <td>{!! getTimeDifferenceInDaysAndHours($ticket->acknowledged_at,$ticket->resolved_at) !!}</td>
                </tr>
            @endif
            <tr>

                <th class="font-weight-bold">Issue Category:</th>
                <td id="material_location">{{ $ticket->issue_category }}</td>
                <th class="font-weight-bold">Disposition:</th>
                <td id="material_cost">{{ $ticket->disposition }}</td>
            </tr>
            <tr>
                <th class="font-weight-bold ">Status:</th>
                <td>{{$ticket->status}}</td>
                <th class="font-weight-bold"> Created By</th>
                <td>{!! ($ticket->is_system == 0) ? $ticket->created_by : "<b>System</b>" !!}</td>
            </tr>
            <tr>
                <th class="font-weight-bold ">Assigned To:</th>
                <td>{{$ticket->assigned_user}}</td>
                <th class="font-weight-bold ">Created At:</th>
                <td>  {!! \Carbon\Carbon::parse($ticket->created_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->created_at)->format('Y-m-d h:i:s A').")</b></small>" !!}</td>

            </tr>
            <tr>
                <th class="font-weight-bold ">Expected start date:</th>
                <td>@if($ticket->expected_start_date){!! \Carbon\Carbon::parse($ticket->expected_start_date)->isoFormat('Do MMM Y H:mm A') !!}
                    &nbsp;&nbsp;@if(!\Carbon\Carbon::parse($ticket->expected_start_date)->isPast())
                        <small><b>({{ \Carbon\Carbon::parse($ticket->expected_start_date)->diffForHumans() }}
                                )</b></small>@endif @endif</td>

            </tr>

            <tr>
                <th class="font-weight-bold" colspan="1">Comments:</th>

                <td colspan="4">

                    <ul class="list-unstyled task-list">
                        @foreach($ticket->TicketUpdates as $ticket_update)
                            <li>
                                <i class="feather icon-check f-w-600 task-icon bg-success text-white"></i>
                                <p class="m-b-5"><span style="font-size: 1.2em;">{{@$ticket_update->user->name}}</span>
                                    <span class="text-muted ml-2">
                                    {{\Illuminate\Support\Carbon::parse($ticket_update->created_at)->toDayDateTimeString()}}
                                </span></p>
                                <h6>    {!! $ticket_update->comment !!}</h6>
                            </li>
                        @endforeach
                    </ul>
                </td>


            </tr>
            </tbody>
        </table>
    </div>
</div>
@include('common.auto_modal_lg_static',[
'modal_id'=>'more_ticket_info_modal',
'modal_title'=>'Ticket Details',
'modal_content'=>'<div class="ticket_details row " ></div>'
])

<div class="modal modal-info" id="cancel_ticket_modal" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="more_ticket_info_modal_label" style="display: none;" aria-hidden="true">
    <div class="modal-dialog animationSandbox zoomIn animated animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="more_ticket_info_modal_label">Cancel Ticket</h3>

                <button class="btn btn-danger  btn-sm" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="section">
                    <div class="ticket_details row">

                        <div class="col-md-12 row ">
                            <form action="{{url('admin/tickets/ticket/cancel/' . $ticket->id)}}"
                                  class="mt-2 ajax-post col-md-12">
                                @csrf
                                <fieldset>
                                    <input type="hidden" name="id" value="{{@$ticket->id}}">

                                    <div class="form-group">
                                        <label for="reason">Reason For Cancellation:</label>
                                        <textarea id="reason" rows="5" cols="5" name="reason"
                                                  class="form-control message"
                                                  placeholder="Enter your reason here"></textarea>
                                    </div>
                                </fieldset>

                                <div class="text-left">
                                    <button type="submit" class="btn btn-sm btn-primary submit-btn"><i
                                            class="fa fa-save "></i> Submit Cancellation
                                    </button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function updateTicket(id) {
        ajaxLoad('{{ url("admin/tickets/ticket/update") }}/' + id, 'ticket_details_div')
    }

    function acknowledgeTicket(id) {
        ajaxLoad('{{ url("admin/tickets/ticket/acknowledge") }}/' + id, 'ticket_details_div')
    }

    function reassignTicket(id) {
        ajaxLoad('{{ url("admin/tickets/ticket/reassign") }}/' + id, 'ticket_details_div')
    }

    $(function () {
        $('.note-editable').css('font-size', '18px');
        $(".select2").select2();
        $(".ack-select2").select2({
            dropdownParent: $("#re_assign_ticket_modal")
        });
        $(".form-control-min-search").select2({
            minimumResultsForSearch: Infinity
        });
        autoFillSelect('assigned_to', '{{ url('admin/users/list?all=1') }}' + '&assigned_to=1', 'setSelectedAssigned');
    })

    function setSelectedAssigned() {
        $("select[name='assigned_to']").val(`{{$ticket->assigned_id }}`)
    }

</script>
