<?php //1-Open, 2 - in_progress, 3-wait_for_response, 4-Resolved, 5-Closed
$show_update_button = false;
if ($ticket->assigned_id == auth()->id())
    if ($ticket->ticket_status_id < 5)
        $show_update_button = true;
if ($ticket->user_id == auth()->id())
    if ($ticket->ticket_status_id < 5)
        $show_update_button = true;
?>
<div class="table-responsive">
<table class="table col-md-12 table-condensed small">
    <tbody>
    @if($ticket->status=="Closed")
    @else
        @if($show_update_button && $ticket->tat !== null)
            <a href="#more_ticket_info_modal" onclick="return updateTicket(`{{$ticket->id}}`)">
                <button type="submit" style="margin-left: .635rem" class=" btn btn-primary btn-sm submit-btn"><i
                        class="fa fa-edit"></i> Update
                </button>
            </a>
            {{--        <a href="#more_ticket_info_modal" data-toggle="modal" onclick="return updateTicket(`{{$ticket->id}}`)"  class="btn btn-info btn-sm clear-form float-right text-right align-right" style="margin-left: .635rem" ><i class="icon icon-pencil5"></i>Update</a>--}}
        @endif
        {{--        @if($ticket->assigned_id == auth()->id() && $ticket->status=="Open"  && $ticket->tat == null)--}}
        {{--            <a href="#acknowledge_ticket_modal" data-toggle="modal">--}}
        {{--                <button type="submit" style="margin-left: .635rem" class="btn btn-success btn-sm submit-btn"><i--}}
        {{--                        class="fas fa-handshake"></i> Acknowledge--}}
        {{--                </button>--}}
        {{--            </a>--}}
        {{--        @endif--}}
    @endif
    <a href="{{url('admin/tickets/ticket/'.$ticket->id)}}"
       class="load-page btn btn-success btn-sm clear-form float-right text-right align-right"
       style="margin-left: .635rem"><i
            class="fa fa-expand "></i> More details </a>
    <tr>
        <th class="font-weight-bold">Ticket Number:</th>
        <td>{{ 'CC'.$ticket->id }}</td>
        <th class="font-weight-bold">Department:</th>
        <td>{{ $ticket->department }}</td>
    </tr>
    @if($ticket->tat)
        <tr>
            <th class="font-weight-bold">Acknowledged At:</th>
            <td>@if($ticket->acknowledged_at){!! \Carbon\Carbon::parse($ticket->acknowledged_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->acknowledged_at)->format('h:i:s A').")</b></small>" !!}@endif</td>
            <th class="font-weight-bold">T.A.T:</th>
            <td>{{ \Carbon\CarbonInterval::minute($ticket->tat)->cascade()->forHumans() }}</td>
        </tr>
    @endif
{{--    @if($ticket->closed_at)--}}
        <tr>
            <th class="font-weight-bold">Ticket Age:</th>
            <td>{!! getTicketAge($ticket->id) !!}</td>
            <th class="font-weight-bold">Closed At:</th>
            <td>@if($ticket->closed_at){!! \Carbon\Carbon::parse($ticket->closed_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->closed_at)->format('h:i:s A').")</b></small>" !!}@else
                    Not yet. @endif</td>
        </tr>
{{--    @endif--}}

    @if($ticket->resolved_at && $ticket->acknowledged_at)
        <tr>
            <th class="font-weight-bold">Resolved At:</th>
            <td>{!! \Carbon\Carbon::parse($ticket->resolved_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->resolved_at)->format('h:i:s A').")</b></small>" !!}</td>
            <th class="font-weight-bold">Actual Resolution Time:</th>
            <td>{!! getTimeDifferenceInDaysAndHours($ticket->acknowledged_at,$ticket->resolved_at) !!}</td>
        </tr>
    @endif

    <tr>
        <th class="font-weight-bold">Issue Category:</th>
        <td id="material_location">{{ $ticket->issue_category }}</td>
        <th class="font-weight-bold">Disposition:</th>
        <td id="material_cost">{{ $ticket->disposition }}</td>
    </tr>
    <tr>

        <th class="font-weight-bold"> Line of Business</th>
        <td id="material_location">{{ $ticket->LOB }}</td>
        <th class="font-weight-bold ">Status:</th>
        <td id="status">{{$ticket->status}}</td>
    </tr>
    <tr>

        <th class="font-weight-bold"> Created By</th>
        <td id="material_location">{{ ($ticket->created_by) ? $ticket->created_by : "System" }}</td>
        <th class="font-weight-bold ">Assigned To:</th>
        <td id="status">{{ $ticket->assigned_to }}</td>
    </tr>
    <tr>
        <th class="font-weight-bold ">Created At:</th>
        <td id="created_at">  {!! \Carbon\Carbon::parse($ticket->created_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->created_at)->format('Y-m-d h:i:s A').")</b></small>" !!}</td>
        <th class="font-weight-bold ">Expected start date:</th>
        <td>@if($ticket->expected_start_date){!! \Carbon\Carbon::parse($ticket->expected_start_date)->isoFormat('Do MMM Y H:mm A') !!}&nbsp;&nbsp;@if(!\Carbon\Carbon::parse($ticket->expected_start_date)->isPast())<small><b>({{ \Carbon\Carbon::parse($ticket->expected_start_date)->diffForHumans() }})</b></small>@endif @endif</td>

    </tr>

    <tr>
        <th class="font-weight-bold" colspan="1">Last Five Comment:</th>


        <td colspan="4">
            <div class="activity">
                @foreach($ticket->TicketUpdates as $ticket_update)
                    <div class="activity-info">
                        <div class="icon-info-activity"><i
                                class="mdi mdi-checkbox-marked-circle-outline bg-soft-success"></i>
                        </div>
                        <div class="activity-info-text">
                            <div class="d-flex justify-content-between align-items-center">
                                <h6 class="m-0 w-70">{{@$ticket_update->user->name}}</h6>
                                <span
                                    class="text-muted d-block">{{\Illuminate\Support\Carbon::parse($ticket_update->created_at)->toDayDateTimeString()}}</span>
                            </div>
                            <p class="text-muted mt-3">
                                {!! $ticket_update->comment !!}
                            </p>
                        </div>
                    </div>
                @endforeach
            </div>
        </td>


    </tr>
    </tbody>
</table>
</div>

<div class="modal modal-info" id="acknowledge_ticket_modal" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="more_ticket_info_modal_label" style="display: none;" aria-hidden="true">
    <div class="modal-dialog animationSandbox zoomIn animated animated-3x" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="more_ticket_info_modal_label">Ticket Acknowledgement</h3>

                <button class="btn btn-danger  btn-sm" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <div class="section">
                    <div class="ticket_details row">

                        <div class="col-md-12 row ">
                            <form action="{{url('admin/tickets')}}" class="mt-2 ajax-post col-md-12">
                                @csrf
                                <fieldset>
                                    <input type="hidden" name="id" value="{{@$ticket->id}}">
                                    <input type="hidden" name="department_id"
                                           value="{{@$ticket->calltronix_department_id}}">
                                    <input type="hidden" name="line_of_business_id"
                                           value="{{@$ticket->line_of_business_id}}">
                                    <input type="hidden" name="issue_category_id"
                                           value="{{@$ticket->issue_category_id}}">
                                    <input type="hidden" name="priority" value="{{@$ticket->priority}}">
                                    <input type="hidden" name="disposition_id" value="{{@$ticket->disposition_id}}">
                                    <input type="hidden" name="ticket_status_id" value="{{@$ticket->ticket_status_id}}">
                                    <input type="hidden" name="acknowledged" value="1">
                                    <input type="hidden" name="form_model" value="{{\App\Models\Core\Ticket::class}}">

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label> Assigned to: <span class="text-danger">*</span></label>
                                            <select id="assigned_to" name="assigned_to"
                                                    data-placeholder="Select Assigned User"
                                                    class="form-control ack-select2" data-fouc>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>TAT: <span class="text-danger">*</span></label>
                                            <div class="row">
                                                <div class="col-md-8" style="padding-right: 0;">
                                                    <div class="form-group">
                                                        <input type="number" min="1" name="tat" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-4" style="padding-left: 0;">
                                                    <div class="form-group">
                                                        <select id="time" name="time"
                                                                data-placeholder="Please select..."
                                                                class="form-control form-control-min-search " data-fouc>
                                                            <option value="hours">Hours</option>
                                                            <option value="minutes">Minutes</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                    </div>
                                    <div class="form-group">
                                        <label>Message:</label>
                                        <textarea rows="5" cols="5" name="description" class="form-control"
                                                  placeholder="Enter your message here"></textarea>
                                    </div>
                                </fieldset>

                                <div class="text-left">
                                    <button type="submit" class="btn btn-sm btn-primary submit-btn"><i
                                            class="fa fa-save "></i> Submit Ticket
                                    </button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function updateTicket(id) {
        // $(".modal-dialog").removeClass('modal-lg')
        ajaxLoad('{{ url("admin/tickets/ticket/update") }}/' + id, 'ticket_details')
    }

    $(function () {
        $(".ack-select2").select2({
            dropdownParent: $("#acknowledge_ticket_modal")
        });
        autoFillSelect('assigned_to', '{{ url('admin/users/list?all=1') }}' + '&assigned_to=1', 'setSelectedAssigned');
    })

    function setSelectedAssigned() {
        $("select[name='assigned_to']").val(`{{$ticket->assigned_id }}`)
    }

</script>
