@extends('layouts.admin')
@section('page-title')
    Admin
@endsection
@section('title')
    Tickets
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Tickets</li>
@endsection
@section('content')

{{--    <div class="row">--}}
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="mt-1">
                        @include('common.auto_tabs',[
             'tabs'=>['all_tickets' ,'open' , 'in_progress','resolved','closed', 'cancelled'],
             'tabs_folder'=>'core.admin.tickets.tabs',
             'base_url'=>'admin/tickets'
             ])
                    </div>
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->

        </div><!--end col-->
{{--    </div>--}}
    @include('common.auto_modal_lg_static',[
   'modal_id'=>'more_ticket_info_modal',
   'modal_title'=>'Ticket Details',
   'modal_content'=>'<div class="ticket_details row " ></div>'
])
    <script type="text/javascript">
        $(function () {
            getTabCounts('{{ url('admin/tickets/list/?tabs=1') }}' + '&getall=1');

        })


        function getMoreDetails(id) {
            $(".modal-dialog").addClass('modal-lg')
            ajaxLoad('{{ url("admin/tickets/ticket/details") }}/' + id, 'ticket_details')
        }

        function filterByDate() {
            var from_date = $("input[name='from_date']").val();
            var to_date = $("input[name='to_date']").val();
            var tickets_data = '{{ url('admin/tickets/list') }}';
            if (from_date) {
                tickets_data = tickets_data + '?from_date=' + from_date + '&to_date=' + to_date;
                getTabCounts('{{ url('admin/tickets/list/?tabs=1') }}' + '&from_date=' + from_date + '&to_date=' + to_date);
                loadAjaxTableData(tickets_data)
                $(".search-form").attr('action', tickets_data)
            } else {
                $("input[name='from_date']").addClass('is-invalid');
                $("input[name='to_date']").addClass('is-invalid');
                // $(".error_field").find('.help-block').remove();
                $("#error_field").html('<small class="help-block invalid-feedback">Please pick a date..</small>');
            }
        }

        function exportToExcel() {
            var from_date = $("input[name='from_date']").val();
            var to_date = $("input[name='to_date']").val();
            var url = '{{url('admin/tickets/export')}}' + '?from_date=' + from_date + '&to_date=' + to_date
            window.location.href = url;
        }

        function updateTicket(id) {
            // $(".modal-dialog").removeClass('modal-lg')
            ajaxLoad('{{ url("admin/tickets/ticket/update") }}/' + id, 'ticket_details')
        }

        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
            if ($('body').hasClass('sidebar-xs')) {
                $('.sidebar-main-toggle').trigger('click');

            }
            $('#secondary-sidebar').html('');
            $('#secondary-sidebar').removeClass('sidebar sidebar-light sidebar-secondary sidebar-expand-md');
        })
    </script>
@endsection
