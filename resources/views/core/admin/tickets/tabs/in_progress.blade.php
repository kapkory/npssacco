<div class=" table-responsive fixed-solution  ">
    <a href="{{ url('admin/tickets/create') }}" class="btn btn-info btn-sm font-weight-bolder clear-form float-right load-page"><i class="fa fa-plus"></i> NEW TICKET</a>
    @include('common.bootstrap_table_ajax',[
'table_headers'=>["tickets.id"=>"ticket_no","departments.name"=>"department","issue_categories.name"=>"issue_category","dispositions.name"=>"disposition","users.name"=>"created_by","assigned_user.name"=>"assigned_to","Created_at","ticket_statuses.name"=>"status", "action"],
'data_url'=>'admin/tickets/list/status/2?getall=1&',
'base_tbl'=>'tickets'
])
</div>
