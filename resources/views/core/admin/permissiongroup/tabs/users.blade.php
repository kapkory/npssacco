<div class="col-md-12">
    <a onclick="setDepartmentId('{{$department->id}}')" href="#add_user_modal" data-toggle="modal"
       class="btn btn-info btn-sm float-right"><i
            class="fa fa-plus"></i> ADD USER TO TO DEPARTMENT</a>
    <div class="table-responsive">
        @include('common.bootstrap_table_ajax',[
     'table_headers'=>["name","email","departments.name"=>"permission_group","phone","action"],
     'data_url'=>'admin/permissiongroup/list/users?department_id='.$department->id,
     'base_tbl'=>'users'
     ])
    </div>
</div>
