<form class="form-horizontal" method="post" action="{{ request()->url() }}">
    {{ csrf_field() }}
    <div class="form-group">
        <label class="control-label">Permissions</label>
        <div>
            @foreach($permissions->admin as $permission)
                <input value="{{ $permission->slug }}" {{ @in_array($permission->slug,$existing) ? 'checked':'' }} type="checkbox" name="permissions[]">&nbsp;{{ ucwords(str_replace('_',' ',$permission->slug)) }}<br/>
            @endforeach
        </div>
    </div>
    <div class="form-group">
        <button style="color: #fff;background-color: #ff943d; border-color: #ff943d;" type="submit" class="btn btn-info"><i class="fa fa-save"></i> Update</button>
    </div>
</form>
