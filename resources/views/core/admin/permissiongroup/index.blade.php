@extends('layouts.admin')

@section('title')
    Departments
@endsection
@section('bread_crumb')

    <span class="breadcrumb-item active">Departments</span>
@endsection
@section('content')
    <div class="card">
        <div class="card-body">
            <a href="#department_modal" class="btn btn-info" data-toggle="modal"><i class="fa fa-plus"></i> ADD DEPARTMENT</a>
            <hr/>
            <div class="table-responsive">
            @include('common.bootstrap_table_ajax',[
            'table_headers'=>["name","description","action"],
            'data_url'=>'admin/permissiongroup/list',
            'base_tbl'=>'departments'
            ])
            </div>
            @include('common.auto_modal',[
                'modal_id'=>'department_modal',
                'modal_title'=>'DEPARTMENT FORM',
                'modal_content'=>autoForm(['name','description', 'form_model'=>\App\Models\Core\Department::class],"admin/permissiongroup")
            ])
            @include('common.auto_modal',[
                'modal_id'=>'permissions_modal',
                'modal_title'=>'Edit Department Permissions',
                'modal_content'=>'<div class="permissions_section"></div>'
            ])
            @include('common.auto_modal',[
                'modal_id'=>'add_user_modal',
                'modal_title'=>'Add User to Group',
                'modal_content'=>autoForm(['hidden_department_id','user_id'],"admin/permissiongroup/user")
            ])
            <script type="text/javascript">
                function getDepartmentPermission(id){
                    ajaxLoad('{{ url("admin/permissiongroup/permissions") }}/'+id,'permissions_section');
                }

                function setDepartmentId(id){
                    $("input[name='department_id']").val(id);
                }
                $(document).ready(function(){
                    autoFillSelect('user_id','{{ url("admin/permissiongroup/list/users") }}');
                });
            </script>
        </div>
    </div>

@endsection
