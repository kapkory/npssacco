@extends('layouts.admin')

@section('bread_crumb')
    <a href="{{ url('admin/permissiongroup') }}" class="breadcrumb-item load-page"> Departments</a>
    <span class="breadcrumb-item active">{{$department->name}} Department</span>
@endsection
@section('title'){{$department->name}} @endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body"><h4 class="header-title mt-0"></h4>

                    @include('common.auto_tabs',[
                 'tabs'=>['permissions','users'],
                 'tabs_folder'=>'core.admin.permissiongroup.tabs',
                 'base_url'=>'admin/permissiongroup/view/'.$department->id
                 ])
                    @include('common.auto_modal',[
                           'modal_id'=>'add_user_modal',
                           'modal_title'=>'Add User to Group',
                           'modal_content'=>autoForm(['hidden_department_id','user_id'],"admin/permissiongroup/user")
                       ])
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>

    <script type="text/javascript">
        function getDepartmentPermission(id) {
            ajaxLoad('{{ url("admin/permissiongroup/permissions") }}/' + id, 'permissions_section');
        }

        function setDepartmentId(id) {
            $("input[name='department_id']").val(id);
        }

        $(document).ready(function () {
            autoFillSelect('user_id', '{{ url("admin/permissiongroup/get/users") }}');
        });
    </script>
    <script type="text/javascript">
        $(function () {
            if ($('body').hasClass('sidebar-xs')) {
                $('.sidebar-main-toggle').trigger('click');

            }
            $('#secondary-sidebar').html('');
            $('#secondary-sidebar').removeClass('sidebar sidebar-light sidebar-secondary sidebar-expand-md');
        })
    </script>

@endsection




