<div class=" col-md-12">
    <table class="table table-striped table-condensed table-sm table-fit table-hover">
        <tbody>
        <tr>
            <th class="font-weight-bold"> Request No:</th>
            <td >#{{ $leave_request->id }}</td>
            <th class="font-weight-bold ">Date requested:</th>
            <td >  {!! \Carbon\Carbon::parse($leave_request->created_at)->toDayDateTimeString()."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->created_at)->format('Y-m-d').")</b></small>" !!}</td>
        </tr>
        <tr>
            <th class="font-weight-bold"> Staff</th>
            <td >{{ $staff->name }}</td>
            <th class="font-weight-bold ">Duties assumed by:</th>
            <td > {{ @$assuming_staff->name }}</td>
        </tr>
        <tr>
            <th class="font-weight-bold">Date Requested:</th>
            <td class="font-weight-bold">{{ \Carbon\Carbon::parse($leave_request->date_from)->isoFormat('Do MMM Y') }}</td>
            <th class="font-weight-bold">Date Expected:</th>
            <td class="font-weight-bold">{!!  formatDeadline1($leave_request->date_to)  !!}</td>
        </tr>
        @if(\Carbon\Carbon::parse($leave_request->date_from) < \Carbon\Carbon::today())
            <tr>
                <th class="font-weight-bold">Date Returned:</th>
                <td class="font-weight-bold">{{ ($leave_request->date_returned) ? \Carbon\Carbon::parse($leave_request->date_returned)->isoFormat('Do MMM Y') : 'Not Yet' }}</td>
                <th class="font-weight-bold">Days Exceeded:</th>
                <td class="font-weight-bold">{!! getDaysDifference($leave_request->date_to,$leave_request->date_returned,true) !!}</td>
            </tr>
        @endif

        <tr>
            <th class="font-weight-bold">Leave Type:</th>
            <td >{{ $leave_request->leaveType->name }}</td>
            <th class="font-weight-bold">Status:</th>
            <td >{!! getLeaveRequestStatusButton($leave_request->status) !!}</td>
        </tr>

        <tr>
            <th class="font-weight-bold"> Reason </th>
            <td colspan="3" >{{ $leave_request->reason }}</td>
        </tr>

        <!--A.M action details-->
        @if($leave_request->is_cse == 1 || isset($leave_request->am_action_at))
            <tr>
                <th class="font-weight-bold"> A.M. Action</th>
                <td >@if($leave_request->am_approve_comment)<span class="text-success-800">Approved</span> @elseif($leave_request->am_reject_reason)<span class="text-danger-800">Declined</span> @else<span class="text-info-800"> Pending</span>@endif</td>
                <th class="font-weight-bold ">Date of Action:</th>
                <td>  @if($leave_request->am_action_at) {!! \Carbon\Carbon::parse($leave_request->am_action_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->am_action_at)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
            </tr>
            <?php
            $am_comment_title = ($leave_request->am_approve_comment) ? "A.M approve comment" : null;
            if(!$am_comment_title)
                $am_comment_title = ($leave_request->am_reject_reason) ? "A.M decline reason" : null;
            $comment = ($leave_request->am_approve_comment) ? $leave_request->am_approve_comment : $leave_request->am_reject_reason;
            ?>
            @if($am_comment_title)
                <tr>
                    <th class="font-weight-bold"> {{ $am_comment_title }}</th>
                    <td colspan="3" >{{ @$comment }}</td>
                </tr>
            @endif
        @endif
        <?php $statuses = ['am_approved','hod_approved', 'hr_approved', 'am_rejected', 'am_declined', 'hod_rejected', 'hod_declined', 'hr_rejected', 'hr_declined']; ?>
        <!--H.O.D. action details-->
        @if(in_array(getLeaveRequestStatus($leave_request->status),$statuses))
            <tr>
                <th class="font-weight-bold"> H.O.D Action</th>
                <td >@if($leave_request->hod_approve_comment)<span class="text-success-800">Approved</span> @elseif($leave_request->hod_reject_reason)<span class="text-danger-800">Declined</span> @else<span class="text-info-800"> Pending</span>@endif</td>
                <th class="font-weight-bold ">Date of Action:</th>
                <td>  @if($leave_request->hod_action_at) {!! \Carbon\Carbon::parse($leave_request->hod_action_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->hod_action_at)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
            </tr>
            <?php
            $hod_comment_title = ($leave_request->hod_approve_comment) ? "H.O.D approve comment" : null;
            if(!$hod_comment_title)
                $hod_comment_title = ($leave_request->hod_reject_reason) ? "H.O.D decline reason" : null;
            $comment = ($leave_request->hod_approve_comment) ? $leave_request->hod_approve_comment : $leave_request->hod_reject_reason;
            ?>
            @if($hod_comment_title)
                <tr>
                    <th class="font-weight-bold"> {{ $hod_comment_title }}</th>
                    <td colspan="3" >{{ @$comment }}</td>
                </tr>
            @endif

            <!--HR. action details-->
            <?php $hrstatuses = ['hr_approved', 'hr_rejected','hr_declined','hod_approved']; ?>
            @if(in_array(getLeaveRequestStatus($leave_request->status),$hrstatuses))
                <tr>
                    <th class="font-weight-bold"> H.R Action</th>
                    <td >@if($leave_request->hr_approve_comment)<span class="text-success-800">Approved</span> @elseif($leave_request->hr_reject_reason)<span class="text-danger-800">Declined</span> @else<span class="text-info-800"> Pending</span>@endif</td>
                    <th class="font-weight-bold ">Date of Action:</th>
                    <td >  @if($leave_request->hr_action_at) {!! \Carbon\Carbon::parse($leave_request->hr_action_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($leave_request->hr_action_at)->format('Y-m-d h:i:s A').")</b></small>" !!} @endif</td>
                </tr>

                <?php
                $hr_comment_title = ($leave_request->hr_approve_comment) ? "H.R approve comment" : null;
                if(!$hr_comment_title)
                    $hr_comment_title = ($leave_request->hr_reject_reason) ? "H.R. decline reason" : null;
                $hr_comment = ($leave_request->hr_approve_comment) ? $leave_request->hr_approve_comment : $leave_request->hr_reject_reason;
                ?>
                @if($hr_comment_title)
                    <tr>
                        <th class="font-weight-bold"> {{ $hr_comment_title }}</th>
                        <td colspan="3" >{{ @$hr_comment }}</td>
                    </tr>
                @endif
            @endif
        @endif

        </tbody>
    </table>
</div>
