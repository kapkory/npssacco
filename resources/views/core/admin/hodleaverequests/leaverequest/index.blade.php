@extends('layouts.admin')

@section('bread_crumb')

    <a href="{{ url('admin/hodleaverequests') }}" class="breadcrumb-item load-page"> Leave requests</a>
    <span class="breadcrumb-item active">Leave request #{{$leave_request->id}}</span>
@endsection

@section('title') Leave request #{{$leave_request->id}} @endsection

@section('content')
    <div class="col-md-12 col-lg-12">
        <div class="card ">
            <div class="card-body">
                <div class="mt-1">
                    @include('common.auto_tabs',[
         'tabs'=>['request_details' , 'staff_details', 'staff_requests_history'],
         'tabs_folder'=>'core.admin.hodleaverequests.leaverequest.tabs',
         'base_url'=>'admin/hodleaverequests/leaverequest/'.$leave_request->id
         ])
                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            {{--getTabCounts('{{ url('admin/tickets/ticket/'.$ticket->id.'?tabs=1') }}');--}}
        });

    </script>

@endsection


