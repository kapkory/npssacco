@extends('layouts.admin')
@section('page-title')
    Admin
@endsection
@section('title')
    {{ ucfirst($calltronix_department->name) }} leave requests
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Leave requests</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    @include('common.auto_tabs',[
        'tabs'=>['pending' , 'approved', 'rejected','all_requests'],
        'tabs_folder'=>'core.admin.hodleaverequests.tabs',
        'base_url'=>'admin/hodleaverequests/'
        ])
                </div>
                <!--end card-body-->
            </div>
            <!--end card-->

        </div><!--end col-->
    </div>
    @include('common.auto_modal_lg_static',[
     'modal_id'=>'more_leave_request_info_modal',
     'modal_title'=>'Leave Request Details',
     'modal_content'=>'<div class="leave_request_details row " ></div>'
  ])
    @include('common.auto_modal_static',[
   'modal_id'=>'approve_leave_request_modal',
   'modal_title'=>'Approve Leave Request',
   'modal_content'=>autoForm(['hidden_id','approve_comment'],'admin/hodleaverequests/leaverequest/approve/')
])
    @include('common.auto_modal_static',[
       'modal_id'=>'reject_leave_request_modal',
       'modal_title'=>'Reject Leave Request',
       'modal_content'=>autoForm(['hidden_id','reject_reason'],'admin/hodleaverequests/leaverequest/reject/')
    ])
    <script type="text/javascript">

        function getMoreDetails(leave_request_id){
            // $(".modal-dialog").addClass('modal-lg')
            ajaxLoad('{{ url("admin/hodleaverequests/leaverequest/details") }}/' + leave_request_id, 'leave_request_details')
        }

        function approveRequest(leave_request_id){
            $("input[name='id']").val(leave_request_id)
            $("textarea[name='approve_comment']").val('')
            $("#approve_leave_request_modal").modal('show')
        }
        function rejectRequest(leave_request_id){
            $("input[name='id']").val(leave_request_id)
            $("textarea[name='reject_reason']").val('')
            $("#reject_leave_request_modal").modal('show')
        }

        $(function () {
            getTabCounts('{{ url('admin/hodleaverequests/list?count=1') }}');
            $('[data-toggle="tooltip"]').tooltip();
            $(".tooltip").tooltip("hide");
        })
    </script>
@endsection
