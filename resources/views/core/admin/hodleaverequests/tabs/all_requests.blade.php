<div class=" table-responsive">
    @include('common.bootstrap_table_ajax',[
    'table_headers'=>["leave_types.name"=>"leave_type","users.name"=>"staff","date_from"=>"Request_date","date_to"=>"Return_date","Status","leave_requests.reporting_comments"=>"Comments","leave_requests.id"=>"Days", "action"],
    'data_url'=>'admin/hodleaverequests/list',
    'base_tbl'=>'leave_requests'
])
</div>
