
<form id="compensation_request" class="model_form_id ajax-post" method="post" action="{{ url('admin/compensations') }}">
    <input type="hidden" name="id" value="{{ @$compensation->id }}">
    @csrf

    <div class="row col-12"><label for="item_category" class="ml-2"><span class="font-14">Available Dates:</span>&nbsp;&nbsp;&nbsp;<small class="">(Approved Off-duty dates)</small></label></div>
    <div class="form-group dates_field">
        <div class="fg-line">
            <input  class="form-control" type="hidden" name="dates_field">
        </div>
    </div>
    @if(count($available_dates) == 0)
        <div class="alert alert-outline-warning">
            You have no Available dates for compensation!
        </div>
        @else
        @foreach($available_dates->chunk(4) as $dates)
            <div class="row text-justify">
                @foreach($dates as $date)
                    <div class="form-check-inline checkbox col-sm" >
                        <div class="custom-control custom-checkbox form-group">
                            <input type="checkbox" name="dates[]" value="{{ $date->id }}"  @if(in_array($date->id,$existing))checked @endif  class="custom-control-input form-control" id="{{ $date->id }}"
                                   data-parsley-multiple="groups"
                                   data-parsley-mincheck="2">
                            <label data-toggle="tooltip" title="{{ $date->description }}" class="custom-control-label" for="{{ $date->id }}"><small>{{ $date->date }}</small></label>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
@endif


    <div class="form-group description">
        <div class="fg-line">
            <label class="fg-label control-label label_description">Description</label>
            <textarea name="description" class="form-control">{{ @$compensation->description }}</textarea>
        </div>
    </div>
    <div class="form-group row col-md-12">
        <div class="col-md-12">
            <button style="color: #fff; background-color: #ff943d; border-color: #ff943d;"  class="btn btn-info btn-raised submit-btn"><i class="fa fa-save"></i> <b>Submit</b></button>
{{--            <button onclick="submitFormData('compensation_request','submit-btn')" style="color: #fff; background-color: #ff943d; border-color: #ff943d;" type="button" class="btn btn-info btn-raised submit-btn"><i class="fa fa-save"></i> <b>Submit</b></button>--}}
        </div>
    </div>
</form>
