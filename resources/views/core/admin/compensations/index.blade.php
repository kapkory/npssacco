@extends('layouts.admin')

@section('title') Compensation Requests @endsection

@section('content')
<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                <a href="#compensationrequest_modal" onclick="return loadCompensationForm()" class="btn btn-info btn-sm float-right" data-toggle="modal"><i class="fa fa-plus"></i> REQUEST COMPENSATION</a>
                    @include('common.bootstrap_table_ajax',[
                    'table_headers'=>["id","description","Status","days","Created_at","action"],
                    'data_url'=>'admin/compensations/list',
                    'base_tbl'=>'compensation_requests'
                    ])

                    @include('common.auto_modal_static',[
                        'modal_id'=>'compensationrequest_modal',
                        'modal_title'=>'COMPENSATION REQUEST FORM',
                        'modal_content'=>"<div class='compensation_request_section'></div>"
                    ])

                 </div>

               </div>

            </div>
        </div>

{{--<div id="edit_offdutyrequest_modal" class="modal fade" data-backdrop="static" role="dialog" aria-labelledby="offdutyrequest_modal_label">--}}
{{--    <div class="modal-dialog">--}}
{{--        <div class="modal-content">--}}
{{--            <div class="modal-header">--}}
{{--                <h3 class="modal-title" id="edit_offdutyrequest_modal_label">OFF DUTY REQUEST FORM</h3>--}}
{{--                <button class="btn btn-danger btn-sm" data-dismiss="modal">×</button>--}}
{{--            </div>--}}

{{--            <div class="modal-body">--}}
{{--                <div class="compensation_request_section"></div>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}


<script type="text/javascript">

    function loadCompensationForm(id=null){
        ajaxLoad('{{ url('admin/compensations/get-compensation-form?compensation_id=') }}'+id,'compensation_request_section')
    }
</script>
@endsection

