@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Member Dashboard</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><h4 class="header-title mt-0"></h4>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Make Loan Payment
                    </button>
                    <div class="alert alert-info mt-2">
                        This is Page is Currently under construction, it shall have a list Members Loan
                    </div>
                    <!-- Button trigger modal -->


                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Loan Payment</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="{{ url('member/loans') }}">
                                        <div class="form-group">
                                            <label>Enter Amount</label>
                                            <input type="text" class="form-control" name="amount" value="1">
                                        </div>
                                        @csrf
                                        <div class="form-group">
                                            <input type="submit" value="Make Repayment"  name="submit" class="btn btn-outline-primary">
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>


@endsection
