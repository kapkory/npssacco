@extends('layouts.member')

@section('title')
    Dashboard
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Create Tickets</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><h4 class="header-title mt-0"></h4>
                    <form action="{{url('member/tickets/create')}}" method="POST">
                        @csrf
                    <div class="input-group">
                        <input type="text" value="{{old('phone')}}"  id="example-input2-group2" name="phone"
                                                    class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }} " placeholder="Enter Customer phone as 7xxxxxxxx">


                        <span
                            class="input-group-append">
                            <button type="submit"    class="btn btn-primary">Submit</button></span>
                        @if ($errors->has('phone'))
                            <small class="help-block invalid-feedback"
                                   style="display: block;">{{ $errors->first('phone') }}</small>

                        @endif
                    </div>
                    </form>

                    @if($customer!=null)
                        <div class="row justify-content-around ">
                            <div class="col-md-12">
                                <div class=" card float-left border-left-3 border-left-success-300 col-md-12" style="padding-top: 20px;">
                                    <div class="card-header font-weight-bold text-uppercase" style="padding-bottom: 0.9rem"><u>Customer
                                            Bio Data: {{ $customer->name }}</u></div>
                                    <div class="card-body">
                                        <table class="table table-sm">
                                            <tbody>
                                            <tr>
                                                <th>Phone number:</th>
                                                <td>{{ $customer->phone }}</td>
                                                <th>Depot:</th>
                                                <td>{{ $customer->depot->name }}</td>

                                            </tr>
                                            <tr>
                                                <th>Route Name:</th>
                                                <td>{{ $customer->route->name }}</td>
                                                <th>Shop Number:</th>
                                                <td>{{ $customer->shop_number }}</td>
                                            </tr>
                                            <tr>
                                                <th>Shop Name:</th>
                                                <td>{{$customer->shop_name}}</td>
                                                <th>Created at:</th>
                                                <td><b>{{ $customer->created_at->isoFormat('Do MMMM, YYYY') }}</b></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @endif
                    @if($customer==null&& $phone)
                        <div class="alert alert-warning alert-styled-left alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <span class="font-weight-semibold">Opps!</span> Customer with phone <a class="alert-link">{{$phone}}</a> not
                            found.
                        </div>
                    @endif
                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>
@endsection
