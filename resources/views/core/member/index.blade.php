@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('bread_crumb')
    <li class="breadcrumb-item active">Member Dashboard</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body"><h4 class="header-title mt-0"></h4>
                    <div class="alert alert-success">
                        Welcome back {{ auth()->user()->name }}
                    </div>

                </div><!--end card-body-->
            </div><!--end card-->

        </div><!--end col-->
    </div>

    <div class="row">
        <div class="col-xl-3 col-md-6">
            <div class="card feed-card">
                <div class="card-body p-t-0 p-b-0">
                    <div class="row">
                        <div class="col-4 bg-primary border-feed">
                            <i class="fas fa-user-tie f-40"></i>
                        </div>
                        <div class="col-8">
                            <div class="p-t-25 p-b-25">
                                <h2 class="f-w-400 m-b-10">Ksh 2,672</h2>
                                <p class="text-muted m-0">Total Deposit Contribution <span class="text-primary f-w-400"></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card feed-card">
                <div class="card-body p-t-0 p-b-0">
                    <div class="row">
                        <div class="col-4 bg-success border-feed">
                            <i class="fas fa-wallet f-40"></i>
                        </div>
                        <div class="col-8">
                            <div class="p-t-25 p-b-25">
                                <h2 class="f-w-400 m-b-10">Ksh 6,391</h2>
                                <p class="text-muted m-0">Total Share Capital</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6">
            <div class="card feed-card">
                <div class="card-body p-t-0 p-b-0">
                    <div class="row">
                        <div class="col-4 bg-danger border-feed">
                            <i class="fas fa-sitemap f-40"></i>
                        </div>
                        <div class="col-8">
                            <div class="p-t-25 p-b-25">
                                <h2 class="f-w-400 m-b-10">Ksh 619</h2>
                                <p class="text-muted m-0">Dividend Earned </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-3 col-md-6">
            <div class="card feed-card">
                <div class="card-body p-t-0 p-b-0">
                    <div class="row">
                        <div class="col-4 bg-warning border-feed">
                            <i class="fas fa-users f-40"></i>
                        </div>
                        <div class="col-8">
                            <div class="p-t-25 p-b-25">
                                <h2 class="f-w-400 m-b-10">1</h2>
                                <p class="text-muted m-0">Guaranteed Loans </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
