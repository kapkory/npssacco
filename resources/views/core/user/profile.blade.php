<?php
/**
 * Created by PhpStorm.
 * User: kemboi
 * Date: 3/1/19
 * Time: 2:30 PM
 */
?>
@extends('layouts.admin')
@section('title')
    My Profile
@endsection
@section('bread_crumb')
    <li class="breadcrumb-item active">Profile</li>
@endsection
{{--@section('bread_crumb')--}}
{{--    <span class="breadcrumb-item active">profile</span>--}}
{{--@endsection--}}
@section('content')
    {{--Originally Answered: What is the meaning of " first name " and " last name " ?--}}
    {{--In conventional usage in English and in nearly all Western countries:—--}}

    {{--“first name” = the 1st forename = the 1st given name--}}
    {{--“middle name” = the 2nd forename = the 2nd given name--}}
    {{--“last name” = surname = the hereditary family name--}}
    <style>
        .met-profile .met-profile-main .met-profile-main-pic .fro-profile_main-pic-change i {
            -webkit-transition: all 0.3s;
            transition: all 0.3s;
            color: #fff;
            font-size: 16px;
        }
    </style>

    <div class="row">
        <div class="col-md-9 float-left">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">

                        <div class="col-sm-12">
                            <h6 class="card-title float-left">Background Info</h6>
{{--                            <a href="#details_modal_2" data-toggle="modal" class="btn btn-sm btn-outline-success float-right"><i class="fa fa-edit"></i>&nbsp;Edit profile</a>--}}
                        </div>

                        <div class="card-body">
                            <div class="col-12 row">
                                <div class="col-md-3 float-left">
                                    <div class="card-img-actions d-inline-block">
                                        <?php
                                        $path = 'storage/profile-pics';
                                        $has_image = false;
                                        if (@getimagesize($path . "/" . Auth::user()->avatar)){
                                            $image = Auth::user()->avatar;
                                            $has_image = true;
                                        }
                                        else
                                            $image = "user.jpg";
                                        ?>
                                        <img style="width: 150px; height: 150px;" class="img-fluid rounded-circle" src="{{ asset($path."/".$image) }}" alt="Profile image" >
                                        <div class="col-md-12">
                                            <button {{ ($has_image) ? "disabled" : "" }}  href="#update_profile_picture" data-toggle="modal"
                                               class="btn btn-info btn-sm">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="float-right col-md-9">
                                    <div class="table-responsive col-sm-12">
                                        <table class="table table-striped table-condensed table-hover">
                                            <tbody>
                                            @if(!$staff)
                                            <tr>
                                                <th> Name</th>
                                                <td>{{ $user->name }}</td>
                                            </tr>

                                            <tr>
                                                <th>Phone Number</th>
                                                <td>{{ $user->phone }}</td>
                                            </tr>

                                            <tr>
                                                <th>Email</th>
                                                <td>{{ $user->email }}</td>
                                            </tr>
                                            @endif
                                            <tr>
                                                <th>Password</th>
                                                <td>********
                                                    <a href="#update_password" data-toggle="modal" class="btn btn-sm btn-outline-dark float-right "><i class="fa fa-lock"></i>&nbsp;Edit Password</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @if($staff)
                            <div class="row">
                                <div class="card-body">
                                    <h6>Other details:</h6>

                                        <table class="table col-md-12 table-condensed table-striped table-hover small">
                                            <tbody>
                                            <tr>
                                                <th class="font-weight-bold">First Name:</th>
                                                <td >{{ $staff->firstname}}</td>
                                                <th class="font-weight-bold">Middle Name:</th>
                                                <td >@if(strlen($staff->middlename) == 1 && $staff->middlename !== 0){{ $staff->middlename }}@endif</td>
                                            </tr>
                                            <tr>
                                                <th class="font-weight-bold">Last Name:</th>
                                                <td>{{ $staff->lastname }}</td>
                                                <th class="font-weight-bold">Employee Number:</th>
                                                <td id="employee_number" class="font-weight-bold">{{ @$staff->employee_number}}</td>
                                            </tr>
                                            <tr>
                                                <th class="font-weight-bold">Phone Number:</th>
                                                <td class="font-weight-bold">{{ @$staff->phone}}</td>
                                                <th class="font-weight-bold">Email Address:</th>
                                                <td>{{ $staff->email }}</td>
                                            </tr>
                                            <tr>
                                                <th class="font-weight-bold ">Marital Status:</th>
                                                <td>{{ @\App\Repositories\StatusRepository::getMaritalStatus($staff->marital_status)}}</td>
                                                <th class="font-weight-bold">Gender:</th>
                                                <td>{{ \App\Repositories\StatusRepository::getGender($staff->gender) }}</td>
                                            </tr>
                                            <tr>
                                                <th class="font-weight-bold"> Date of Employment</th>
                                                <td>@if($staff->employment_date){!! \Carbon\Carbon::parse($staff->employment_date)->format('d-m-Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($staff->employment_date)->diffForHumans().")</b></small>" !!}@endif</td>
                                                <th class="font-weight-bold"> Date of Birth</th>
                                                <?php $age = \Carbon\Carbon::parse($staff->dob)->diffForHumans(); ?>
                                                <td>@if($staff->dob){!! \Carbon\Carbon::parse($staff->dob)->format('d-m-Y')."  &nbsp;&nbsp;<small><b>(".str_replace('ago','',$age).")</b></small>" !!}@endif</td>

                                            </tr>
                                            <tr>
                                                <th class="font-weight-bold ">Position of Employment:</th>
                                                <td>{{$staff->position}}</td>
                                                <th class="font-weight-bold">Department</th>
                                                <td>{{ $staff->department }}</td>
                                            </tr>
                                            <tr>
                                                <th class="font-weight-bold ">Role:</th>
                                                <td>{{ $staff->role }}</td>
                                                <th class="font-weight-bold">Line of Business</th>
                                                <td>{{ $staff->lob }}</td>
                                            </tr>
                                            </tbody>
                                        </table>




                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->

            </div><!--end col-->
        </div>
        @if($user->is_staff == 1)
            <div class="col-md-3 float-right">
                <div class="card">
                    <div class="card-header">
                        <h6>My Leave Balance: (Annual)</h6>
                    </div>
                    <div class="card-body">
                        <?php $leave_balance = $user->getMyLeaveBalanceData(); ?>
                        <table class="table">
                            <tbody>
                            <tr>
                                <th>Days Covered:</th>
                                <td>{{ @$leave_balance['days_covered'] }}</td>
                            </tr>
                            <tr>
                                <th>Days Remaining: &nbsp;<i data-toggle="tooltip" title="Total days including available compensation days" class="fa fa-question-circle"></i></th>
                                <td>{{ @$leave_balance['days_remaining'] }}</td>
                            </tr>
                            <tr>
                                <th>Total Entitled Days:</th>
                                <td>{{ @$leave_balance['days_entitled']}}</td>
                            </tr>
                            <tr>
                                <th>Compensation: &nbsp;<i data-toggle="tooltip" title="Days compensated from your Off-duty requests" class="fa fa-question-circle"></i></th>
                                <td>{{ @$leave_balance['compensation_days']}}</td>
                            </tr>
                            </tbody>
                        </table>
                            <a class="float-right load-page text-orange font-weight-bolder" href="{{ url('user/leaves') }}"><u>more</u> </a>
                    </div>
                </div>
            </div>
        @endif
    </div>


    @include('common.auto_modal',[
        'modal_id'=>'details_modal_2',
        'modal_title'=>'Personal Details',
        'modal_content'=>autoForm(['name', 'phone'],'user/profile',null,Auth::user()),
    ])
    @include('common.auto_modal',[
        'modal_id'=>'update_password',
        'modal_title'=>'New Password',
        'modal_content'=>autoForm(['password','password_confirmation'],'user/password'),
    ])
    @include('common.auto_modal',[
    'modal_id'=>'update_profile_picture',
    'modal_title'=>'Profile Picture',
    'modal_content'=>autoForm(['avatar'],'user/profile-picture'),
    ])
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".tooltip").tooltip("hide");
    </script>

@endsection
