<?php
/**
 * Created by PhpStorm.
 * User: kemboi
 * Date: 3/1/19
 * Time: 2:30 PM
 */
?>
@extends('layouts.admin')
@section('title')
    My Profile
@endsection
@section('bread_crumb')
    <a href="{{ url('user/profile') }}" class="breadcrumb-item load-page"> Profile</a>
    <span class="breadcrumb-item active">Leaves Details</span>
@endsection

@section('content')
<?php $leave_balance = $user->getMyLeaveBalanceData(); ?>
    <div class="row justify-content-center">
        <div class="col-md-12 ">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="col-sm-12">

                            <h5 class="card-title float-left">My Leaves Details:&nbsp; <b>{{ date('Y') }}</b> </h5>
                            <h6 class="card-title float-right">Compensation Days:
                                <span style="padding: .15rem .75rem;" class="btn btn-info">{{ @$leave_balance['compensation_days']}}</span>
                            </h6>
                        </div>

                        <div class="card-body">
                            <div class="col-12">
                                <div class=" table-responsive">
                                    @include('common.bootstrap_table_ajax',[
                                    'table_headers'=>["leave_types.name"=>"leave_type","Is_paid","leave_types.entitlement"=>"Entitled_days","yearly_staff_leaves_trackers.days_covered"=>"Days_covered","yearly_staff_leaves_trackers.days_remaining"=>"Days_remaining"],
                                    'data_url'=>'user/leaves-details/list',
                                    'base_tbl'=>'leave_types',
                                    'table_class'=>['small'],
                                    'search_form'=>false
                                ])
                                    <small class="text-orange font-weight-bolder float-right">NB: Compensation days are only added to your Annual leave days.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end card-body-->
                </div>
                <!--end card-->

            </div><!--end col-->
        </div>

    </div>

    <script>
        $('[data-toggle="tooltip"]').tooltip();
        $(".tooltip").tooltip("hide");
    </script>

@endsection
