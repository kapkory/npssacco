
@if(Request::ajax())
    @if(isset($_GET['t_optimized']))
        @yield('t_optimized')
    @elseif(isset($_GET['ta_optimized']))
        @yield('ta_optimized')
    @else
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <div class="page-header-title">
                                <h5>
                                    @yield('title')
                                </h5>
                            </div>
                            <ul class="breadcrumb">
                                @yield('bread_crumb')
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
                <!-- [ Main Content ] start -->

                @yield('content')
            <!-- [ Main Content ] end -->



            <div class="q-view">
                <div class="overlay"></div>
                <div class="content">
                    <div class="card-body">
                        @yield('sidebar')
                    </div>

                </div>

            </div>
    @endif
    @include('common.essential_js')

@else

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <title>{{ config('app.name', 'Cassavahub HR Solution') }} </title>
            <!-- HTML5 Shim and Respond.js IE11 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 11]>
                <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
            <!-- Meta -->
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="description" content="" />
            <meta name="keywords" content="">
            <meta name="author" content="Phoenixcoded" />
            <!-- Favicon icon -->
            <link rel="icon" href="{{ url('backend/assets/images/favicon.svg') }}" type="image/x-icon">
            <!-- font css -->
            <link rel="stylesheet" href="{{ url('backend/assets/fonts/font-awsome-pro/css/pro.min.css') }}">
            <link rel="stylesheet" href="{{ url('backend/assets/fonts/feather.css') }}">
            <link rel="stylesheet" href="{{ url('backend/assets/fonts/fontawesome.css') }}">
            <!-- vendor css -->
            <link rel="stylesheet" href="{{ url('backend/assets/css/style.css') }}">
            <link rel="stylesheet" href="{{ url('backend/assets/css/customizer.css') }}">
            <link href="{{ url('backend/assets/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css">
            <link href="{{ url('backend/assets/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet">
            <link href="{{ url('backend/assets/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
            <link href="{{ url('backend/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
            <link href="{{ url('plugins/sweetalert/dist/sweetalert.css') }}" type="text/css" rel="stylesheet">
            <link rel="stylesheet" href="{{ url('backend/assets/css/layout-advance.css') }}">
            <script src="{{ url('backend/assets/js/jquery.min.js') }}"></script>
            <link href="{{url('plugins/fileinput/css/fileinput.min.css')}}" media="all" rel="stylesheet"
                  type="text/css"/>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
            <link rel="stylesheet" href="{{ url('plugins/dropzone/dropzone.css') }}">
            @stack('header-scripts')
            @yield('header')
            <style>
                .select2-container--default .select2-selection--single .select2-selection__arrow{
                    height: calc(1.5em + 0.75rem + 7px) !important;
                }

                .select2-container .select2-selection--single {
                    height: calc(1.5em + 0.75rem + 7px) !important;
                }
                .select2-container--default .select2-selection--single .select2-selection__rendered {
                    color: #444;
                    line-height: calc(1.5em + 0.75rem + 2px) !important;
                }
                .breadcrumb-item,.breadcrumb-item.active {
                    color: white;
                }
            </style>

        </head>
        <body class="advance-layout ">
        <!-- [ Pre-loader ] start -->
        <div class="loader-bg">
            <div class="loader-track">
                <div class="loader-fill"></div>
            </div>
        </div>
        <!-- [ Pre-loader ] End -->
        <!-- [ Mobile header ] start -->
        <div class="pc-mob-header pc-header">
            <div class="pcm-logo">
                <img src="{{ url('backend') }}/assets/images/logo.svg" alt="" class="logo logo-lg">
            </div>
            <div class="pcm-toolbar">
                <a href="#" class="pc-head-link" id="mobile-collapse">
                    <div class="hamburger hamburger--arrowturn">
                        <div class="hamburger-box">
                            <div class="hamburger-inner"></div>
                        </div>
                    </div>
                </a>
                <a href="#" class="pc-head-link" id="headerdrp-collapse">
                    <i data-feather="align-right"></i>
                </a>
                <a href="#" class="pc-head-link" id="header-collapse">
                    <i data-feather="more-vertical"></i>
                </a>
            </div>
        </div>
        <!-- [ Mobile header ] End -->

        <!-- [ navigation menu ] start -->
        <nav class="pc-sidebar light-sidebar">
            <div class="navbar-wrapper">
                <div class="navbar-content">
                    <ul class="pc-navbar">
                        <li class="pc-item pc-caption">
                            <label>Navigation</label>

                        </li>

                        @if(isset($real_menus))
                            @foreach($real_menus as $menu)

                                @if($menu->type=='single' && @$menu->menus)
                                    <li class="pc-item">
                                        <a href="{{ url($menu->menus->url) }}" class="pc-link load-page">
                                            <span class="pc-micon"><i data-feather="{{ $menu->menus->icon }}"></i></span>
                                            <span class="pc-mtext">{{ $menu->menus->label }}</span>
                                        </a>
                                    </li>
                                @endif
                                    @if($menu->type=='many')
                                    <li class="pc-item pc-hasmenu">
                                        <a href="javascript:void(0);" class="pc-link ">
                                            <span class="pc-micon"><i data-feather="{{ $menu->icon }}"></i></span>
                                            <span class="pc-mtext">{{ $menu->label }}</span>
                                            <span class="pc-arrow"><i data-feather="chevron-right"></i></span>
                                        </a>
                                        <ul class="pc-submenu">
                                            @foreach($menu->children as $drop)
                                                @if($drop->label)
                                                    <li class="pc-item">
                                                        <a class="pc-link load-page" href="{{ url($drop->url) }}">
                                                            {{ $drop->label }}
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                               @endif


                            @endforeach
                        @endif

                    </ul>
                </div>
            </div>
        </nav>
        <!-- [ navigation menu ] end -->
        <!-- [ Header ] start -->
        <header class="pc-header bg-primary ">
            <div class="header-wrapper">
                <div class="m-header d-flex align-items-center mr-2">
                    <a href="{{ url('/') }}" class="b-brand">
                        <!-- ========   change your logo hear   ============ -->
                        <img src="{{ url('backend') }}/assets/images/logo.svg" alt="" class="logo logo-lg">
                    </a>
                </div>

                <div class="ml-auto">
                    <ul class="list-unstyled">
                        <li class="dropdown pc-h-item">
                            <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="layout-advance.html#" role="button" aria-haspopup="false" aria-expanded="false">
                                <i data-feather="search"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pc-h-dropdown drp-search">
                                <form class="px-3">
                                    <div class="form-group mb-0 d-flex align-items-center">
                                        <i data-feather="search"></i>
                                        <input type="search" class="form-control border-0 shadow-none" placeholder="Search here. . .">
                                    </div>
                                </form>
                            </div>
                        </li>
                        <li class="pc-h-item">
                            <a class="pc-head-link mr-0" href="#" data-toggle="modal" data-target="#notification-modal">
                                <i data-feather="bell"></i>
                                <span class="badge badge-danger pc-h-badge dots"><span class="sr-only"></span></span>
                            </a>
                        </li>
                        <?php
                        $path = 'storage/profile-pics';
                        $image = "user.jpg";
                        if (@getimagesize($path . "/" . Auth::user()->avatar))
                            $image = Auth::user()->avatar;
                        ?>
                        <li class="dropdown pc-h-item">
                            <a class="pc-head-link dropdown-toggle arrow-none mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="{{ asset($path."/".$image) }}" alt="user-image" class="user-avtar">
                                <span>
                                        <span class="user-name">{{ Auth::user()->name }}</span>
                                        <span class="user-desc">Administrator</span>
                                    </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pc-h-dropdown">
                                <div class=" dropdown-header">
                                    <h6 class="text-overflow m-0">Welcome !</h6>
                                </div>
                                <a href="{{ url('user/profile') }}" class="dropdown-item">
                                    <i data-feather="user"></i>
                                    <span>My Account</span>
                                </a>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();  document.getElementById('logout-form').submit();"class="dropdown-item">
                                    <i data-feather="power"></i>
                                    <span>Logout</span>
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>
        </header>

        <!-- Modal -->
        <div class="modal notification-modal fade" id="notification-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <ul class="nav nav-pill tabs-light mb-3" id="pc-noti-tab" role="tablist">

                            <li class="nav-item">
                                <a class="nav-link active" id="pc-noti-news-tab" data-toggle="pill" href="#pc-noti-news" role="tab" aria-controls="pc-noti-news" aria-selected="false">News<span class="badge badge-danger ml-2 d-none d-sm-inline-block">4</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pc-noti-settings-tab" data-toggle="pill" href="#pc-noti-settings" role="tab" aria-controls="pc-noti-settings" aria-selected="false">Setting<span class="badge badge-success ml-2 d-none d-sm-inline-block">Update</span></a>
                            </li>
                        </ul>
                        <div class="tab-content pt-4" id="pc-noti-tabContent">
                            <div class="tab-pane active" id="pc-noti-news" role="tabpanel" aria-labelledby="pc-noti-news-tab">
                                <div class="pb-3 border-bottom mb-3 media">
                                    <a href="#!"><img src="{{ url('backend') }}/assets/images/news/img-news-2.jpg" class="wid-90 rounded" alt="..."></a>
                                    <div class="media-body ml-3">
                                        <p class="float-right mb-0 text-success"><small>now</small></p>
                                        <a href="#!"><h6>This is a news image</h6></a>
                                        <p class="mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</p>
                                    </div>
                                </div>

                                <div class="pb-3 border-bottom mb-3 media">
                                    <a href="#!"><img src="{{ url('backend') }}/assets/images/news/img-news-2.jpg" class="wid-90 rounded" alt="..."></a>
                                    <div class="media-body ml-3">
                                        <p class="float-right mb-0 text-muted"><small>5 mins ago</small></p>
                                        <a href="#!"><h6>Ipsum has been the industry's</h6></a>
                                        <p class="mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
                                        <a href="#" class="badge badge-light">JavaScript</a>
                                        <a href="#" class="badge badge-light">Scss</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pc-noti-settings" role="tabpanel" aria-labelledby="pc-noti-settings-tab">
                                <h6 class="mt-2"><i data-feather="monitor" class="mr-2"></i>Desktop settings</h6>
                                <hr>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="pcsetting1" checked>
                                    <label class="custom-control-label f-w-600 pl-1" for="pcsetting1">Allow desktop notification</label>
                                </div>
                                <p class="text-muted ml-5">you get lettest content at a time when data will updated</p>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="pcsetting2">
                                    <label class="custom-control-label f-w-600 pl-1" for="pcsetting2">Store Cookie</label>
                                </div>
                                <h6 class="mb-0 mt-5"><i data-feather="save" class="mr-2"></i>Application settings</h6>
                                <hr>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="pcsetting3">
                                    <label class="custom-control-label f-w-600 pl-1" for="pcsetting3">Backup Storage</label>
                                </div>
                                <p class="text-muted mb-4 ml-5">Automaticaly take backup as par schedule</p>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="pcsetting4">
                                    <label class="custom-control-label f-w-600 pl-1" for="pcsetting4">Allow guest to print file</label>
                                </div>
                                <h6 class="mb-0 mt-5"><i data-feather="cpu" class="mr-2"></i>System settings</h6>
                                <hr>
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="pcsetting5" checked>
                                    <label class="custom-control-label f-w-600 pl-1" for="pcsetting5">View other user chat</label>
                                </div>
                                <p class="text-muted ml-5">Allow to show public user message</p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-danger btn-sm" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-light-primary btn-sm">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ Header ] end -->

        <!-- [ Main Content ] start -->
        <div class="pc-container">
            <!-- [ breadcrumb ] start -->
            <div class="page-header">
                <div class="page-block">
                    <div class="row align-items-center">
                        <div class="col-md-8">
                            <div class="page-header-title">
                                <h5 class="system-title">@yield('title')</h5>
                            </div>
                            <ul class="breadcrumb">
                                @yield('bread_crumb')
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- [ breadcrumb ] end -->
            <div class="pcoded-content system-container">
                    @yield('content')


                <!-- [ Main Content ] start -->
        </div>


            <div class="q-view">
                <div class="overlay"></div>
                <div class="content">
                    <div class="card-body">
                        @yield('sidebar')
                    </div>

                </div>

            </div>
        <!-- [ Main Content ] end -->
        </div>
        <!-- Warning Section start -->
        <!-- Older IE warning message -->
        <!--[if lt IE 11]>
            <div class="ie-warning">
                <h1>Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade
                   <br/>to any of the following web browsers to access this website.
                </p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="{{ url('backend') }}/assets/images/browser/chrome.png" alt="Chrome">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="{{ url('backend') }}/assets/images/browser/firefox.png" alt="Firefox">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="{{ url('backend') }}/assets/images/browser/opera.png" alt="Opera">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="{{ url('backend') }}/assets/images/browser/safari.png" alt="Safari">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="{{ url('backend') }}/assets/images/browser/ie.png" alt="">
                                <div>IE (11 & above)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>


        <![endif]-->
        <!-- Warning Section Ends -->
        <!-- Required Js -->
        <script src="{{ url('backend/assets/js/jquery-ui.min.js') }}"></script>
        <script src="{{ url('backend/assets/js/vendor-all.min.js') }}"></script>
        <script src="{{ url('backend/assets/js/plugins/bootstrap.min.js') }}"></script>
        <script src="{{ url('backend/assets/js/plugins/feather.min.js') }}"></script>
        <script src="{{ url('backend/assets/js/pcoded.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
        <script src="{{ url('backend/assets/js/plugins/clipboard.min.js') }}"></script>
        <script src="{{ url('backend/assets/js/uikit.min.js') }}"></script>

        <script src="{{ url("plugins/dropzone/dropzone.js") }}"></script>
        <script src="{{ url("plugins/fileinput/js/plugins/sortable.min.js") }}"></script>
        <script src="{{ url("plugins/fileinput/js/fileinput.min.js") }}"></script>
        <script src="{{ url("plugins/fileinput/themes/fas/theme.js") }}"></script>
        <script src="{{ asset('js/jquery.history.js') }}"></script>
        <script src="{{ url('js/jquery.datetimepicker.js') }}"></script>
        <script src="{{ url('js/jquery.form.js') }}"></script>

        <script src="{{ url('backend/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ url('backend/assets/plugins/select2/select2.min.js') }}"></script>
        <script src="{{ url('backend/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ url('backend/assets/plugins/summernote/summernote-bs4.js') }}"></script>

        <script>
            $('.chk-indeterminate').prop('indeterminate', true);
        </script>
        <script>
            $('#exampleModalVarying').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('.modal-body #recipient-name').text(recipient)
                modal.find('.modal-body #message-text').text('hello ' + recipient)
                    ``

            })
        </script>
        <div class="pct-customizer">
            <div href="#!" class="pct-c-btn">
                <button class="btn btn-light-danger" id="pct-toggler">
                    <i data-feather="settings"></i>
                </button>
                <button class="btn btn-light-primary" data-toggle="tooltip" title="Document" data-placement="left">
                    <i data-feather="book"></i>
                </button>
                <button class="btn btn-light-success" data-toggle="tooltip" title="Buy Now" data-placement="left">
                    <i data-feather="shopping-bag"></i>
                </button>
                <button class="btn btn-light-info" data-toggle="tooltip" title="Support" data-placement="left">
                    <i data-feather="headphones"></i>
                </button>
            </div>
            <div class="pct-c-content ">
                <div class="pct-header bg-primary">
                    <h5 class="mb-0 text-white f-w-500">Nextro Customizer</h5>
                </div>
                <div class="pct-body">
                    <h6 class="mt-2"><i data-feather="credit-card" class="mr-2"></i>Header settings</h6>
                    <hr class="my-2">
                    <div class="theme-color header-color">
                        <a href="#!" class="" data-value="bg-default"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-primary"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-danger"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-warning"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-info"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-success"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-dark"><span></span><span></span></a>
                    </div>
                    <h6 class="mt-4"><i data-feather="layout" class="mr-2"></i>Sidebar settings</h6>
                    <hr class="my-2">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" id="cust-sidebar">
                        <label class="custom-control-label f-w-600 pl-1" for="cust-sidebar">Light Sidebar</label>
                    </div>
                    <div class="custom-control custom-switch mt-2">
                        <input type="checkbox" class="custom-control-input" id="cust-sidebrand">
                        <label class="custom-control-label f-w-600 pl-1" for="cust-sidebrand">Color Brand</label>
                    </div>
                    <div class="theme-color brand-color d-none">
                        <a href="#!" class="active" data-value="bg-primary"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-danger"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-warning"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-info"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-success"><span></span><span></span></a>
                        <a href="#!" class="" data-value="bg-dark"><span></span><span></span></a>
                    </div>
                </div>
            </div>
        </div>
        <script>

            $(document).ready(function() {
                $(".hd-body").click(function() {
                    $(".q-view").addClass("active");
                });
                $(".overlay").click(function() {
                    $(".q-view").removeClass("active");
                });
            });

            // header option
            $('#pct-toggler').on('click', function() {
                $('.pct-customizer').toggleClass('active');

            });
            // header option
            $('#cust-sidebrand').change(function() {
                if ($(this).is(":checked")) {
                    $('.theme-color.brand-color').removeClass('d-none');
                    $('.m-header').addClass('bg-dark');
                } else {
                    $('.m-header').removeClassPrefix('bg-');
                    $('.m-header > .b-brand > .logo-lg').attr('src', 'assets/images/logo-dark.svg');
                    $('.theme-color.brand-color').addClass('d-none');
                }
            });
            // Header Color
            $('.brand-color > a').on('click', function() {
                var temp = $(this).attr('data-value');
                // $('.header-color > a').removeClass('active');
                // $('.pcoded-header').removeClassPrefix('brand-');
                // $(this).addClass('active');
                if (temp == "bg-default") {
                    $('.m-header').removeClassPrefix('bg-');
                } else {
                    $('.m-header').removeClassPrefix('bg-');
                    $('.m-header > .b-brand > .logo-lg').attr('src', 'assets/images/logo.svg');
                    $('.m-header').addClass(temp);
                }
            });
            // Header Color
            $('.header-color > a').on('click', function() {
                var temp = $(this).attr('data-value');
                // $('.header-color > a').removeClass('active');
                // $('.pcoded-header').removeClassPrefix('brand-');
                // $(this).addClass('active');
                if (temp == "bg-default") {
                    $('.pc-header').removeClassPrefix('bg-');
                } else {
                    $('.pc-header').removeClassPrefix('bg-');
                    $('.pc-header').addClass(temp);
                }
            });
            // sidebar option
            $('#cust-sidebar').change(function() {
                if ($(this).is(":checked")) {
                    $('.pc-sidebar').addClass('light-sidebar');
                    $('.pc-horizontal .topbar').addClass('light-sidebar');
                    // $('.m-header > .b-brand > .logo-lg').attr('src', 'assets/images/logo-dark.svg');
                } else {
                    $('.pc-sidebar').removeClass('light-sidebar');
                    $('.pc-horizontal .topbar').removeClass('light-sidebar');
                    // $('.m-header > .b-brand > .logo-lg').attr('src', 'assets/images/logo.svg');
                }
            });
            $.fn.removeClassPrefix = function(prefix) {
                this.each(function(i, it) {
                    var classes = it.className.split(" ").map(function(item) {
                        return item.indexOf(prefix) === 0 ? "" : item;
                    });
                    it.className = classes.join(" ");
                });
                return this;
            };
        </script>

        <script>
            Dropzone.autoDiscover = false;
        </script>
        @include('common.javascript')
        @yield('footer_js')
        @stack('footer-scripts')
        <!-- Custom main Js -->
        <input type="hidden" name="material_page_loaded" value="1">
        <script src="{{ url('plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
        </body>

        </html>

    @endif
