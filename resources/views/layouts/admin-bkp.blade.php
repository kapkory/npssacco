@if(Request::ajax())
    @if(isset($_GET['t_optimized']))
        @yield('t_optimized')
    @elseif(isset($_GET['ta_optimized']))
        @yield('ta_optimized')
    @else

        <!-- Page header -->
        <div class="page-header">
{{--            <div class="page-header-content header-elements-md-inline">--}}
{{--                <div class="page-title d-flex" style="padding: 0.9rem">--}}
{{--                    <h4><i class="icon-arrow-left52 mr-2"></i>--}}
{{--                        @yield('page_title')--}}
{{--                    </h4>--}}
{{--                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
{{--                </div>--}}


{{--            </div>--}}

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="{{ url(Auth::user()->role) }}" class="breadcrumb-item load-page"><i
                                class="icon-home2 mr-2"></i> Home</a>
                        @yield('bread_crumb')
                    </div>
                    <a data-toggle="tooltip" title="Click to Reload Page"
                       href="{{ url(\Illuminate\Support\Facades\Request::fullUrl()) }}"
                       class="header-elements-toggle text-default d-md-none load-page" data-action="reload"></a>
                    {{--                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
                </div>

                <a data-toggle="tooltip" title="Click to Reload Page"
                   href="{{ url(\Illuminate\Support\Facades\Request::fullUrl()) }}"
                   class=" text-default d-none d-md-block load-page" data-action="reload"></a>

            </div>

        </div>

        <!-- /page header -->
        <!-- Content area -->
        <div class="content pt-0 ">
            <!-- /page header -->
            <div class="card">
                <div class="card-header header-elements-inline system-title">
                    @yield('title')
                    @yield('header-elements')

                </div>
                <div class="card-body">
                    @yield('content')
                </div>
            </div>

        </div>

        <!-- Footer -->
        <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Cassavahub  Kenya Limited
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
                        &copy; {{ Date('Y') }}  A product of <a href="http://cassavahub.co.ke/" target="_blank">CassavaHub Kenya Limited</a>
					</span>


            </div>
        </div>
        <!-- /footer -->
        <!-- /footer -->
    @endif
    @include('common.essential_js')
@else
    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{url('/')}}/img/favicon.ico">
        <title>{{ config('app.name', 'Truckmart Workshop') }}</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
              type="text/css">
        <link href="{{ url(env('CDN_HOST').'css/fontawesome-all.css') }}" rel="stylesheet">
        <link href="{{ url('limitless/global_assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('limitless/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('limitless/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('limitless/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('limitless/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('limitless/assets/css/colors.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('css/jquery.datetimepicker.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ url('sweetalert/dist/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ url('printjs/src/css/print.css') }}" rel="stylesheet">
    {{--        <link href="{{ url('css/form.css') }}" rel="stylesheet">--}}


    <!-- /global stylesheets -->
        <!-- Core JS files -->
        <script src="{{ url('limitless/global_assets/js/main/jquery.min.js') }}"></script>
        <script src="{{ url('limitless/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ url('limitless/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
        <script src="{{ url('limitless/global_assets/js/plugins/ui/ripple.min.js') }}"></script>
        {{--        <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>--}}


        <script src="{{ url('printjs/dist/print.min.js') }}"></script>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <!-- /core JS files -->
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/all.fine-uploader/fine-uploader-gallery.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/file-uploader/5.16.2/fine-uploader.min.js"></script>
        <style>
            .sidebar {
                width: 13.875rem;
            }

            @media (min-width: 768px) {
                .navbar-expand-md .navbar-brand {
                    min-width: 12.625rem;
                }
            }

            /*li.nav-item.active a {*/
            /*    margin-left: 20px;*/
            /*}*/
            .nav-link {
                display: block !important;
                padding: .625rem 1.25rem;
            }

            @media (min-width: 768px) {
                .sidebar-xs .sidebar-main .nav-sidebar > li.nav-item.active a {
                    /*margin-left: 20px;*/
                }
            }
        </style>
        @stack('header-scripts')
        @yield('header')
    </head>
    <!-- /head -->

    <body>

    <!-- Main navbar -->
    <div class="navbar navbar-expand-md bg-blue-custom navbar-light">

        <!-- Header with logos -->
        <div class="navbar-header navbar-light d-none d-md-flex align-items-md-center">
            <div class="navbar-brand navbar-brand-md "
                 style="background-image: url( {{asset('/img/ashokLeyland_logo.png')}})">
                <a href="{{ url(request()->user()->role) }}" class="d-inline-block load-page">
{{--                    <img src="{{ url('img/ashokLeyland_logo.png') }}" alt=""--}}
{{--                         style=" height: 25px; max-height:100%; max-width: 100%; margin-left: 20px">--}}
                </a>

            </div>

            <div class="navbar-brand navbar-brand-xs">
                <a href="{{ url(request()->user()->role) }}" class="d-inline-block">
                    <img src="{{ url('img/favicon.ico') }}" alt="">
                </a>
            </div>
        </div>
        <!-- /header with logos -->


        <!-- Mobile controls -->
        <div class="d-flex flex-1 d-md-none">
            <div class="navbar-brand mr-auto">
                <a href="{{ url(request()->user()->role) }}" class="d-inline-block">
                    <img src="{{ url('img/ashokLeyland_logo.png') }}" alt=""
                         style=" height: 25px; max-height:100%; max-width: 100%; margin-left: 20px">
                </a>
            </div>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>

            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>
        <!-- /mobile controls -->


        <!-- Navbar content -->
        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="javascript:void(0)"
                       class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>

            </ul>
            <span class="navbar-text ml-md-3 mr-md-auto">
				<span class="badge bg-pink-400 badge-pill"></span>
			</span>
            {{--            <span class="badge bg-pink-400 badge-pill ml-md-3 mr-md-auto">16 orders</span>--}}

            <ul class="navbar-nav">

                <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">
                        <i class="icon-bell3"></i>
                        <span class="d-md-none ml-2">Notifications</span>
                        <span class="badge badge-mark border-pink-400 ml-auto ml-md-0"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
                        <div class="dropdown-content-header">
                            <span class="font-weight-semibold">Notifications</span>
                            <a href="javascript:void(0)" class="text-default"><i class="icon-compose"></i></a>
                        </div>

                        <div class="dropdown-content-body dropdown-scrollable">
                            <ul class="media-list">
                                <li class="media">
                                    <div class="mr-3 position-relative">
                                        <img
                                            src="{{ url('limitless/global_assets/images/placeholders/placeholder.jpg') }}"
                                            width="36" height="36" class="rounded-circle" alt="">
                                    </div>

                                    <div class="media-body">
                                        <div class="media-title">
                                            <a href="javascript:void(0)">
                                                <span class="font-weight-semibold">James Alexander</span>
                                                <span class="text-muted float-right font-size-sm">04:58</span>
                                            </a>
                                        </div>

                                        <span
                                            class="text-muted">who knows, maybe that would be the best thing for me...</span>
                                    </div>
                                </li>

                                <li class="media">
                                    <div class="mr-3 position-relative">
                                        <img
                                            src="{{ url('limitless/global_assets/images/placeholders/placeholder.jpg') }}"
                                            width="36" height="36" class="rounded-circle" alt="">
                                    </div>

                                    <div class="media-body">
                                        <div class="media-title">
                                            <a href="javascript:void(0)">
                                                <span class="font-weight-semibold">Margo Baker</span>
                                                <span class="text-muted float-right font-size-sm">12:16</span>
                                            </a>
                                        </div>

                                        <span
                                            class="text-muted">That was something he was unable to do because...</span>
                                    </div>
                                </li>


                            </ul>
                        </div>

                        <div class="dropdown-content-footer bg-light">
                            <a href="javascript:void(0)" class="text-grey mr-auto">All Notifications</a>
                            <div>
                                <a href="javascript:void(0)" class="text-grey" data-popup="tooltip"
                                   title="Mark all as read"><i class="icon-radio-unchecked"></i></a>
                                <a href="javascript:void(0)" class="text-grey ml-2" data-popup="tooltip"
                                   title="Settings"><i class="icon-cog3"></i></a>
                            </div>
                        </div>
                    </div>
                </li>

                <li class="nav-item dropdown dropdown-user">
                    <a href="javascript:void(0)" class="navbar-nav-link d-flex align-items-center dropdown-toggle"
                       data-toggle="dropdown">
                        <img src="{{ url('limitless/global_assets/images/placeholders/user.png') }}"
                             class="rounded-circle mr-2" height="34" alt="">
                        <span>{{ Auth::user()->name }}</span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="{{ url('user/profile') }}" class="dropdown-item load-page"><i
                                class="icon-user-plus"></i> My profile</a>
                        {{--                        <a href="index.html#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>--}}
                        {{--                        <a href="index.html#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-indigo-400 ml-auto">58</span></a>--}}

{{--                        <a href="javascript:void(0)" class="dropdown-item"><i class="icon-cog5"></i> Account--}}
{{--                            settings</a>--}}
                        <div class="dropdown-divider"></div>
                        <a href="javascript:void(0)" class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="icon-switch2"></i> {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        <!-- /navbar content -->

    </div>
    <!-- /main navbar -->

    <!-- Page content -->
    <div class="page-content">

        <!-- Aside -->
        <!-- Main sidebar -->
        <div class="sidebar sidebar-light   sidebar-main sidebar-expand-md">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="javascript:void(0)" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                Navigation
                <a href="javascript:void(0)" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->


            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <!-- Main -->
                        <li class="nav-item-header">
                            <div class="text-uppercase font-size-xs line-height-xs">Main</div>
                            <i class="icon-menu" title="Main"></i>
                        </li>

                        @if(isset($real_menus))
                            @foreach($real_menus as $menu)

                                @if($menu->type=='single' && @$menu->menus)
                                    <li class="nav-item">
                                        <a href="{{ url($menu->menus->url)  }}" class="nav-link load-page">
                                            <i class="icon-{{ $menu->menus->icon }}"></i>
                                            <span>{{ $menu->menus->label }}</span>
                                        </a>
                                    </li>

                                @endif

                                @if($menu->type=='many')
                                    <li class="nav-item nav-item-submenu">
                                        <a href="#" class="nav-link"><i class="icon-{{ $menu->icon }}"></i>
                                            <span>{{ $menu->label }}</span></a>

                                        <ul class="nav nav-group-sub" data-submenu-title="{{ $menu->label }}">
                                            @foreach($menu->children as $drop)
                                                @if($drop->label)
                                                    <li class="nav-item ">
                                                        <a href="{{ url($drop->url) }}"
                                                           class="nav-link load-page">{{ $drop->label }}</a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                @endif

                            @endforeach
                        @endif


                    </ul>
                </div>
                <!-- /main navigation -->

            </div>
            <!-- /sidebar content -->

        </div>
        <!-- /main sidebar -->
        <!-- /aside -->

        <!-- Secondary sidebar -->
        <div class=" " id="secondary-sidebar">

        </div>
        <!-- /secondary sidebar -->

        <!-- Main content -->
        <div class="content-wrapper system-container">

            <!-- Page header -->
            <div class="page-header">
{{--                <div class="page-header-content header-elements-md-inline">--}}
{{--                    <div class="page-title d-flex" style="padding: 0.9rem">--}}
{{--                        <h4><i class="icon-arrow-left52 mr-2"></i>--}}
{{--                            @yield('page_title')--}}
{{--                        </h4>--}}
{{--                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
{{--                    </div>--}}


{{--                </div>--}}

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        <div class="breadcrumb">
                            <a href="{{ url(Auth::user()->role) }}" class="breadcrumb-item load-page"><i
                                    class="icon-home2 mr-2"></i> Home</a>
                            @yield('bread_crumb')
                        </div>
                        <a data-toggle="tooltip" title="Click to Reload Page"
                           href="{{ url(\Illuminate\Support\Facades\Request::fullUrl()) }}"
                           class="header-elements-toggle text-default d-md-none load-page" data-action="reload"></a>
                        {{--                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
                    </div>

                    <a data-toggle="tooltip" title="Click to Reload Page"
                       href="{{ url(\Illuminate\Support\Facades\Request::fullUrl()) }}"
                       class=" text-default d-none d-md-block load-page" data-action="reload"></a>

                    {{--                    <div class="header-elements d-none">--}}
                    {{--                        <div class="breadcrumb justify-content-center">--}}
                    {{--                            <a href="#" class="breadcrumb-elements-item">--}}
                    {{--                                <i class="icon-comment-discussion mr-2"></i>--}}
                    {{--                                Support--}}
                    {{--                            </a>--}}

                    {{--                            <div class="breadcrumb-elements-item dropdown p-0">--}}
                    {{--                                <a href="#" class="breadcrumb-elements-item dropdown-toggle" data-toggle="dropdown">--}}
                    {{--                                    <i class="icon-gear mr-2"></i>--}}
                    {{--                                    Settings--}}
                    {{--                                </a>--}}

                    {{--                                <div class="dropdown-menu dropdown-menu-right">--}}
                    {{--                                    <a href="#" class="dropdown-item"><i class="icon-user-lock"></i> Account--}}
                    {{--                                        security</a>--}}
                    {{--                                    <a href="#" class="dropdown-item"><i class="icon-statistics"></i> Analytics</a>--}}
                    {{--                                    <a href="#" class="dropdown-item"><i class="icon-accessibility"></i>--}}
                    {{--                                        Accessibility</a>--}}
                    {{--                                    <div class="dropdown-divider"></div>--}}
                    {{--                                    <a href="#" class="dropdown-item"><i class="icon-gear"></i> All settings</a>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>

            </div>
            <!-- /page header -->

            <div class="content pt-0 ">

                <div class="card">
                    <div class="card-header header-elements-inline system-title">
                        @yield('title')
                        @yield('header-elements')
                    </div>

                    <div class="card-body">
                        @yield('content')
                    </div>
                </div>

            </div>

            <!-- Footer -->
            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="text-center d-lg-none w-100">
                    <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                            data-target="#navbar-footer">
                        <i class="icon-unfold mr-2"></i>
                     CassavaHub Kenya Limited
                    </button>
                </div>

                <div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
                        &copy; {{ Date('Y') }}  A product of <a href="http://cassavahub.co.ke/" target="_blank"> CassavaHub Kenya Limited</a>
					</span>


                </div>
            </div>
            <!-- /footer -->
            <!-- /footer -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

    <!-- Theme JS files -->
    <script src="{{ url('limitless/global_assets/js/plugins/visualization/d3/d3.min.js') }}"></script>
    <script src="{{ url('limitless/global_assets/js/plugins/visualization/d3/d3_tooltip.js') }}"></script>
    <script src="{{ url('limitless/global_assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    {{--<script src="{{ url('limitless/global_assets/js/plugins/forms/styling/switch.min.js') }}"></script>--}}
    <script src="{{ url('limitless/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js') }}"></script>
    <script src="{{ url('limitless/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    {{--    <script src="{{ url('limitless/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>--}}

    <script src="{{ url('limitless/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script src="{{ url('limitless/assets/js/app.js') }}"></script>
    {{--    <script src="{{ url('limitless/global_assets/js/demo_pages//dashboard.js') }}"></script>--}}
    <script src="{{ url('limitless/global_assets/js/demo_pages/form_layouts.js') }}"></script>

    <script src="{{ url('limitless/global_assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    {{--    <script src="{{ url('limitless/global_assets/js/demo_pages/dashboard.js') }}"></script>--}}
    <!-- /theme JS files -->

    {{--<script src="{{ url(env('CDN_HOST').'js/fontawesome.min.js') }}"></script>--}}
    <script src="{{ asset('js/jquery.history.js') }}"></script>
    <script src="{{ url('js/jquery.datetimepicker.js') }}"></script>
    <script src="{{ url('js/jquery.form.js') }}"></script>

    @include('common.javascript')
    @yield('footer_js')
    @stack('footer-scripts')
    <script type="text/javascript">
        window.url = "{{url('/')}}";
        if (window.url + '/admin/settings/config' !== window.location.href) {
            $('#secondary-sidebar').html('');
        }

    </script>

    <!-- Custom main Js -->
    <input type="hidden" name="material_page_loaded" value="1">
    <script src="{{ url('sweetalert/dist/sweetalert.min.js') }}"></script>
    </body>
    </html>
@endif
