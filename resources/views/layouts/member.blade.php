<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Calltronix CRM | @yield('title')</title>
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <meta content="Mannatthemes" name="author"><!-- App favicon -->
    <link rel="shortcut icon" href="{{url('theme')}}/assets/images/favicon.ico"><!-- App css -->
    <link href="{{url('theme')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('theme')}}/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="{{url('theme')}}/assets/css/metisMenu.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('theme')}}/assets/css/style.css" rel="stylesheet" type="text/css">
</head>
<body><!-- Top Bar Start -->
<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <a href="{{ url(Auth::user()->role) }}" class="logo">
            <span>
                <img src="{{url('/')}}/img/logo-small.svg" style="height: 40px;" alt="logo-small" class="logo-sm">
            </span>

        </a>
    </div>
    <!--end logo-->
    <!-- Navbar -->
    <nav class="navbar-custom">
        <ul class="list-unstyled topbar-nav float-right mb-0">

            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"
                   href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false"><i
                        class="dripicons-bell noti-icon"></i>
                    <span class="badge badge-danger badge-pill noti-icon-badge">2</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-lg"><!-- item-->
                    <h6 class="dropdown-item-text"> Notifications (2)</h6>
                    <div class="slimscroll notification-list"><!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success"><i class="mdi mdi-bell-ring"></i></div>
                            <p class="notify-details">Notification Item<small class="text-muted">This module is comming
                                    soon.</small></p>
                        </a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success"><i class="mdi mdi-bell-ring"></i></div>
                            <p class="notify-details">Notification Item<small class="text-muted">This module is comming
                                    soon.</small></p>
                        </a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success"><i class="mdi mdi-bell-ring"></i></div>
                            <p class="notify-details">Notification Item<small class="text-muted">This module is comming
                                    soon.</small></p>
                        </a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success"><i class="mdi mdi-bell-ring"></i></div>
                            <p class="notify-details">Notification Item<small class="text-muted">This module is comming
                                    soon.</small></p>
                        </a>
                        <a href="javascript:void(0);" class="dropdown-item notify-item active">
                            <div class="notify-icon bg-success"><i class="mdi mdi-bell-ring"></i></div>
                            <p class="notify-details">Notification Item<small class="text-muted">This module is comming
                                    soon.</small></p>
                        </a>

                    </div><!-- All-->
                    <a href="javascript:void(0);" class="dropdown-item text-center text-primary">View all <i
                            class="fi-arrow-right"></i></a></div>
            </li>
            <li class="dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-light nav-user"
                   data-toggle="dropdown" href="javascript:void(0);" role="button"
                   aria-haspopup="false" aria-expanded="false"><img
                        src="{{url('theme')}}/assets/images/users/user.png" alt="profile-user" class="rounded-circle">
                    <span class="ml-1 nav-user-name hidden-sm">{{Auth::user()->name}} <i
                            class="mdi mdi-chevron-down"></i></span></a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="javascript:void(0);">
                        <i class="dripicons-user text-muted mr-2"></i> Profile</a>

                    <a class="dropdown-item" href="javascript:void(0);">
                        <i class="dripicons-gear text-muted mr-2"></i> Settings</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                            class="dripicons-exit text-muted mr-2"></i>
                        Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        <!--end topbar-nav-->
        <ul class="list-unstyled topbar-nav mb-0">
            <li>
                <button class="button-menu-mobile nav-link waves-effect waves-light"><i
                        class="dripicons-menu nav-icon"></i></button>
            </li>
            <li class="hide-phone app-search">
                <form role="search" class=""><input type="text" placeholder="Search..." class="form-control"> <a
                        href="{{url('')}}"><i class="fas fa-search"></i></a></form>
            </li>
        </ul>
    </nav>
    <!-- end navbar-->
</div>
<!-- Top Bar End -->
<div class="page-wrapper">
    <!-- Left Sidenav -->
    <div class="left-sidenav">
        <div class="main-icon-menu">
            <nav class="nav">
                <a href="#MetricaAnalytic" class="nav-link "
                   data-toggle="tooltip-custom" data-placement="top" title=""
                   data-original-title="Home">
                    <i style="font-size: 25px;" class="ti ti-home"></i>
                </a>
                <!--end MetricaAnalytic-->
                <a href="#tickets" class="nav-link"
                   data-toggle="tooltip-custom" data-placement="top" title=""
                   data-original-title="Tickets">
                    <i style="font-size: 25px" class="ti ti-headphone"></i>
                </a>
                <!--end MetricaCrypto-->
            </nav>
            <!--end nav-->
        </div>
        <!--end main-icon-menu-->
        <div class="main-menu-inner display-hidden">
            <div class="menu-body slimscroll">
                <div id="MetricaAnalytic" class="main-icon-menu-pane ">
                    <div class="title-box"><h6 class="menu-title">Home</h6></div>
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" style="color: #7c92a7" href="{{url('member')}}">
                                <i class="dripicons-meter"></i>Dashboard
                            </a>
                        </li>

                    </ul>
                </div>
                <!-- end Analytic -->
                <div id="tickets" class="main-icon-menu-pane">
                    <div class="title-box"><h6 class="menu-title">Tickets</h6></div>
                    <ul class="nav">

                        <li class="nav-item"><a class="nav-link" href="{{url('member/tickets/create')}}"><i
                                    class="dripicons-plus"></i>Create</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{url('/')}}"><i
                                    class="dripicons-calendar"></i>Todays Tickets</a></li>

                    </ul>
                </div>
                <!-- end Crypto -->
            </div>
            <!--end menu-body-->
        </div>
        <!-- end main-menu-inner-->
    </div>
    <!-- end left-sidenav-->
    <!-- Page Content-->
    <div class="page-content">

        <div class="container-fluid"><!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <div class="float-right">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url(Auth::user()->role)}}">Home</a></li>
                                @yield('bread_crumb')
                            </ol>
                        </div>
                        <h4 class="page-title">@yield('title')</h4>
                    </div>
                    <!--end page-title-box-->
                </div><!--end col-->
            </div>
            <!-- end page title end breadcrumb -->

            <!--end row-->
        </div>
    @yield('content')
    <!-- container -->
        <footer class="footer text-center text-sm-left">&copy; 2019 Calltronix CRM <span
                class="text-muted d-none d-sm-inline-block float-right">Crafted with <i
                    class="mdi mdi-heart text-danger"></i> by Calltronix Kenya Limited</span>
        </footer>
        <!--end footer-->
    </div>
    <!-- end page content --></div><!-- end page-wrapper --><!-- jQuery  -->
<script src="{{url('theme')}}/assets/js/jquery.min.js"></script>
<script src="{{url('theme')}}/assets/js/bootstrap.bundle.min.js"></script>
<script src="{{url('theme')}}/assets/js/metisMenu.min.js"></script>
<script src="{{url('theme')}}/assets/js/waves.min.js"></script>
<script src="{{url('theme')}}/assets/js/jquery.slimscroll.min.js"></script>
<script src="{{url('theme')}}/assets/plugins/apexcharts/apexcharts.min.js"></script>
<script src="{{url('theme')}}/assets/pages/jquery.analytics_dashboard.init.js"></script><!-- App js -->
<script src="{{url('theme')}}/assets/js/app.js"></script>
</body>
</html>
