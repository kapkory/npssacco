@if(Request::ajax())
    @if(isset($_GET['t_optimized']))
        @yield('t_optimized')
    @elseif(isset($_GET['ta_optimized']))
        @yield('ta_optimized')
    @else
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="float-right">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a class="load-page" href="{{ url(Auth::user()->role) }}"><i class="fa fa-home"></i> Home</a></li>
                            @yield('bread_crumb')
                        </ol>
                    </div>
                    <h4 class="page-title system-title">@yield('title')</h4>
                </div>
            </div>
        </div>

        @yield('content')




    @endif
    @include('common.essential_js')
@else
    <!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <title>{{ config('app.name', 'Calltronix Internal CRM') }}</title>
        <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content="" name="author">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- App favicon -->
        <link rel="shortcut icon" href="{{ url('theme') }}/assets/images/icon.png">
        <link href="{{ url('theme') }}/assets/plugins/select2/select2.min.css" rel="stylesheet" type="text/css">

        <link href="{{ url('theme') }}/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet">
        <link href="{{ url('theme') }}/plugins/lightpick/lightpick.css" rel="stylesheet"><!-- App css -->
        <link href="{{ url('theme') }}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url('theme') }}/assets/css/jquery-ui.min.css" rel="stylesheet">
        <link href="{{ url('theme') }}/assets/css/icons.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url('theme') }}/assets/css/metisMenu.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url('theme') }}/assets/css/app.min.css" rel="stylesheet" type="text/css">
        <link href="{{ url('theme') }}/plugins/daterangepicker/daterangepicker.css" rel="stylesheet">
        <link href="{{ url('theme') }}/plugins/summernote/summernote-bs4.css" rel="stylesheet">
{{--        <link href="{{ url('theme') }}/plugins/summernote/summernote.min.css" rel="stylesheet">--}}
        <link href="{{ url('sweetalert/dist/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ url('css/jquery.datetimepicker.css') }}" rel="stylesheet">

        <!-- jQuery  -->
        <script src="{{ url('theme') }}/assets/js/jquery.min.js"></script>
        <!-- Custom CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        <link href="{{ url('css/style.css') }}" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ url('dropzone/dropzone.css') }}">
        @stack('header-scripts')
        @yield('header')

        <style>
            .left-sidenav-menu li ul li > a {
                padding: 10px 10px 10px 20px !important;
                color: #ffffff;
                border-left: none;
            }

            .left-sidenav-menu li > a {
                display: block;
                padding: 12px 0px 12px 18px !important;
                color: #ffffff;
                font-size: 13px;
                font-weight: 600;
                -webkit-transition: all 0.3s ease-out;
                transition: all 0.3s ease-out;
            }

        </style>
    </head>
    <body class="">
    <!-- Left Sidenav -->
    <div class="left-sidenav">
        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{ url(Auth::user()->role) }}" class="logo">
            <span>
                 <img src="{{ url('/') }}/theme/assets/images/Calltronix-Animation .gif"
                      style="height: 55px;margin-left: -36px;" alt="logo-small" class="logo-sm">
            </span>
                {{--                        <span>--}}
                {{--                            <img src="{{ url('theme') }}/assets/images/logo.png" alt="logo-large" class="logo-lg logo-light">--}}
                {{--                            <img src="{{ url('theme') }}/assets/images/logo-dark.png" alt="logo-large" class="logo-lg logo-dark">--}}
                {{--                        </span>--}}
            </a>
        </div>
        <!--end logo-->
        <div class="leftbar-profile p-1 w-100">

        </div>
        <ul class="metismenu left-sidenav-menu slimscroll" style="padding-top: 0;padding-bottom: 2em; max-height: 560px !important; overflow: scroll">
            <li class="menu-label">Main</li>
            @if(isset($real_menus))
                @foreach($real_menus as $menu)

                    @if($menu->type=='single' && @$menu->menus)
                        <li class="leftbar-menu-item">
                            <a href="{{ url($menu->menus->url) }}" class="menu-link load-page">
                                <i data-feather="{{ $menu->menus->icon }}"
                                   class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                                <span>{{ $menu->menus->label }}</span>
                            </a>
                        </li>
                    @endif

                    @if($menu->type=='many')


                        <li class="leftbar-menu-item">
                            <a href="javascript: void(0);" class="menu-link">
                                <i data-feather="{{ $menu->icon }}"
                                   class="align-self-center vertical-menu-icon icon-dual-vertical"></i>
                                <span>{{ $menu->label }}</span>
                                <span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @foreach($menu->children as $drop)
                                    @if($drop->label)
                                        <li class="nav-item">
                                            <a class="nav-link load-page" href="{{ url($drop->url) }}">
                                                <i class="ti-control-record"></i>
                                                {{ $drop->label }}</a>
                                        </li>
                                    @endif
                                @endforeach

                            </ul>
                        </li>
                    @endif

                @endforeach
            @endif


        </ul>
    </div>
    <!-- end left-sidenav-->
    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- Navbar -->
        {{--        style="background-color: #aed581 ;"--}}
        <nav class="navbar-custom">
            <ul class="list-unstyled topbar-nav float-right mb-0">

                {{--                <li class="dropdown notification-list">--}}
                {{--                    <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown"--}}
                {{--                       href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false">--}}
                {{--                        <i class="ti-bell noti-icon"></i>--}}
                {{--                        <span class="badge badge-danger badge-pill noti-icon-badge">2</span>--}}
                {{--                    </a>--}}
                {{--                    <div class="dropdown-menu dropdown-menu-right dropdown-lg pt-0">--}}
                {{--                        <h6 class="dropdown-item-text font-15 m-0 py-3 bg-primary text-white d-flex justify-content-between align-items-center">--}}
                {{--                            Notifications <span class="badge badge-light badge-pill">2</span>--}}
                {{--                        </h6>--}}
                {{--                        <div class="slimscroll notification-list">--}}
                {{--                            <!-- item-->--}}
                {{--                            <a href="javascript:void(0)" class="dropdown-item py-3">--}}
                {{--                                <small class="float-right text-muted pl-2">2 min ago</small>--}}
                {{--                                <div class="media">--}}
                {{--                                    <div class="avatar-md bg-primary">--}}
                {{--                                        <i class="la la-cart-arrow-down text-white"></i>--}}
                {{--                                    </div>--}}
                {{--                                    <div class="media-body align-self-center ml-2 text-truncate">--}}
                {{--                                        <h6 class="my-0 font-weight-normal text-dark">Coming Soon</h6>--}}
                {{--                                        <small class="text-muted mb-0">Notification module will be rolled soon</small>--}}
                {{--                                    </div>--}}
                {{--                                    <!--end media-body-->--}}
                {{--                                </div>--}}
                {{--                                <!--end media-->--}}
                {{--                            </a><!--end-item-->--}}
                {{--                            <!-- item-->--}}
                {{--                            <a href="javascript:void(0)" class="dropdown-item py-3">--}}
                {{--                                <small class="float-right text-muted pl-2">2 min ago</small>--}}
                {{--                                <div class="media">--}}
                {{--                                    <div class="avatar-md bg-primary">--}}
                {{--                                        <i class="la la-cart-arrow-down text-white"></i>--}}
                {{--                                    </div>--}}
                {{--                                    <div class="media-body align-self-center ml-2 text-truncate">--}}
                {{--                                        <h6 class="my-0 font-weight-normal text-dark">Coming Soon</h6>--}}
                {{--                                        <small class="text-muted mb-0">Notification module will be rolled soon</small>--}}
                {{--                                    </div>--}}
                {{--                                    <!--end media-body-->--}}
                {{--                                </div>--}}
                {{--                                <!--end media-->--}}
                {{--                            </a><!--end-item-->--}}
                {{--                            <!-- item-->--}}
                {{--                            <a href="javascript:void(0)" class="dropdown-item py-3">--}}
                {{--                                <small class="float-right text-muted pl-2">2 min ago</small>--}}
                {{--                                <div class="media">--}}
                {{--                                    <div class="avatar-md bg-primary">--}}
                {{--                                        <i class="la la-cart-arrow-down text-white"></i>--}}
                {{--                                    </div>--}}
                {{--                                    <div class="media-body align-self-center ml-2 text-truncate">--}}
                {{--                                        <h6 class="my-0 font-weight-normal text-dark">Coming Soon</h6>--}}
                {{--                                        <small class="text-muted mb-0">Notification module will be rolled soon</small>--}}
                {{--                                    </div>--}}
                {{--                                    <!--end media-body-->--}}
                {{--                                </div>--}}
                {{--                                <!--end media-->--}}
                {{--                            </a><!--end-item-->--}}
                {{--                        </div>--}}
                {{--                        <!-- All-->--}}
                {{--                        <a href="javascript:void(0);" class="dropdown-item text-center text-primary">--}}
                {{--                            View all<i class="fi-arrow-right"></i>--}}
                {{--                        </a>--}}
                {{--                    </div>--}}
                {{--                </li>--}}
                <li class="dropdown">
                    <?php
                    $path = 'storage/profile-pics';
                    if (@getimagesize($path . "/" . Auth::user()->avatar))
                        $image = Auth::user()->avatar;
                    else
                        $image = "user.jpg";
                    ?>
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user"
                       data-toggle="dropdown" href="javascript:void(0);" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        {{--                        src="{{ url('theme') }}/assets/images/users/user.png"--}}
                        <img src="{{ asset($path."/".$image) }}" alt="profile-user" class="rounded-circle">
                        <span class="ml-1 nav-user-name hidden-sm">{{ Auth::user()->name }} <i
                                class="mdi mdi-chevron-down"></i></span></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item load-page" href="{{ url('user/profile') }}">
                            <i class="dripicons-user text-muted mr-2"></i> Profile
                        </a>

                        <a class="dropdown-item" href="javascript:void(0);">
                            <i class="dripicons-gear text-muted mr-2"></i> Settings</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i
                                class="dripicons-exit text-muted mr-2"></i>
                            Logout</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                {{--                <li class="mr-2">--}}
                {{--                    <a href="javascript:void(0)" class="nav-link" data-toggle="modal" data-animation="fade"--}}
                {{--                       data-target=".modal-rightbar">--}}
                {{--                        <i data-feather="align-right" class="align-self-center"></i>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
            </ul>
            <!--end topbar-nav-->
            <ul class="list-unstyled topbar-nav mb-0">
                <li>
                <span class="responsive-logo">
                    <img src="{{ url('theme') }}/assets/images/logo-sm.png" alt="logo-small"
                         class="logo-sm align-self-center" height="34"></span>
                </li>
                <li>
                    <button class="button-menu-mobile nav-link waves-effect waves-light"><i
                            class="dripicons-menu nav-icon"></i></button>
                </li>
                <li class="hide-phone app-search">
                    <form role="search" class="">
                        <input type="text" id="AllCompo" placeholder="Search..."
                               class="form-control">
                        <a href="#"><i class="fas fa-search"></i></a>
                    </form>
                </li>
            </ul>
        </nav>
        <!-- end navbar-->
    </div>
    <!-- Top Bar End -->
    <div class="page-wrapper">
        <!-- Page Content-->
        <div class="page-content-tab">
            <div class="container-fluid system-container" style="width: 100%; height: 100%">
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-title-box">
                            <div class="float-right">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a class="load-page" href="{{ url(Auth::user()->role) }}"><i class="fa fa-home"></i>  Home</a></li>
                                    @yield('bread_crumb')
                                </ol>
                            </div>
                            <h4 class="page-title system-title">@yield('title')</h4>
                        </div>
                    </div>
                </div>
                <!--end row-->
                <!-- end page title end breadcrumb -->
                <!-- Wrap this in a row and card always-->
                @yield('content')
            </div>

            <footer class="footer text-center text-sm-center">&copy; {{ date('Y') }} Calltronix CRM | <span style="background-color: rgb(255, 148, 61)" class="badge badge-info">Version 1.0</span></footer>
        </div>
        <!-- end page content -->
    </div>
    <!-- end page-wrapper -->

    <script src="{{ url('theme') }}/assets/js/jquery-ui.min.js"></script>
    <script src="{{ url('theme') }}/assets/js/bootstrap.bundle.min.js"></script>
    <script src="{{ url('theme') }}/assets/js/metismenu.min.js"></script>
    <script src="{{ url('theme') }}/assets/js/waves.js"></script>
    <script src="{{ url('theme') }}/assets/js/feather.min.js"></script>
    <script src="{{ url('theme') }}/assets/js/jquery.slimscroll.min.js"></script>
    <script src="{{ url('theme') }}/plugins/chartjs/chart.min.js"></script>
    <script src="{{ url('theme') }}/plugins/chartjs/roundedBar.min.js"></script>
    <script src="{{ url('theme') }}/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="{{ url('theme') }}/plugins/jvectormap/jquery-jvectormap-us-aea-en.js"></script>

    <!-- App js -->
    <script src="{{ url('theme') }}/assets/js/app.js"></script>
    <script src="{{ url('theme') }}/assets/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('theme') }}/assets/plugins/select2/select2.min.js"></script>
    <script src="{{ url("dropzone/dropzone.js") }}"></script>
    <script src="{{ asset('js/jquery.history.js') }}"></script>
    <script src="{{ url('js/jquery.datetimepicker.js') }}"></script>
    <script src="{{ url('js/jquery.form.js') }}"></script>
    <script src="{{ url('limitless/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ url('theme') }}/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="{{ url('theme') }}/plugins/summernote/summernote-bs4.js"></script>
{{--    <script src="{{ url('theme') }}/plugins/summernote/summernote.js"></script>--}}
    <script>
        Dropzone.autoDiscover = false;
    </script>
    @include('common.javascript')
    @yield('footer_js')
    @stack('footer-scripts')
    <!-- Custom main Js -->
    <input type="hidden" name="material_page_loaded" value="1">
    <script src="{{ url('sweetalert/dist/sweetalert.min.js') }}"></script>
    </body>
    </html>
@endif
