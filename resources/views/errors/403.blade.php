<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>403 Calltronix Internal CRM</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{url('limitless/')}}/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('limitless/')}}/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('limitless/')}}/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('limitless/')}}/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('limitless/')}}/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="{{url('limitless/')}}/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="{{url('limitless/')}}/global_assets/js/main/jquery.min.js"></script>
    <script src="{{url('limitless/')}}/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="{{url('limitless/')}}/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{url('limitless/')}}/assets/js/app.js"></script>
    <!-- /theme JS files -->

</head>

<body>
<style>
    .sidebar {
        width: 13.875rem;
    }

    @media (min-width: 768px) {
        .navbar-expand-md .navbar-brand {
            min-width: 12.625rem;
        }
    }

    /*li.nav-item.active a {*/
    /*    margin-left: 20px;*/
    /*}*/
    .nav-link {
        display: block !important;
        padding: .625rem 1.25rem;
    }

    .navbar-light .navbar-nav-link {
        color: #fff;
    }

    .navbar-light .navbar-nav-link:hover {
        color: #fff;
    }

    .navbar-light .show > .navbar-nav-link {
        color: #fff;
        background-color: rgba(0, 0, 0, .04);
    }

    .navbar-light .navbar-toggler:focus, .navbar-light .navbar-toggler:hover, .navbar-light .navbar-toggler[aria-expanded=true] {
        color: #fff;
    }

    .navbar-light .navbar-toggler {
        color: #fff;
    }

    @media (min-width: 768px) {
        .sidebar-xs .sidebar-main .nav-sidebar > li.nav-item.active a {
            /*margin-left: 20px;*/
        }
    }
</style>

<!-- Main navbar -->
<div class="navbar navbar-expand-md navbar-light" style="background-color:#5e5c5c !important;">
    <div class="">
        <a href="{{ url('/') }}" class="d-inline-block load-page">
            <img src="{{ url('/') }}/theme/assets/images/Calltronix-Animation .gif" alt="" style="height: 72px" >
        </a>
    </div>



    <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                {{--                <a href="error_500.html#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">--}}
                {{--                    <i class="icon-git-compare"></i>--}}
                {{--                    <span class="d-md-none ml-2">Git updates</span>--}}
                {{--                    <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0">9</span>--}}
                {{--                </a>--}}

                <div class="dropdown-menu dropdown-content wmin-md-350">


                    <div class="dropdown-content-body dropdown-scrollable">
                        <ul class="media-list">
                            <li class="media">
                                <div class="mr-3">
                                    <a href="error_500.html#" class="btn bg-transparent border-primary text-primary rounded-round border-2 btn-icon"><i class="icon-git-pull-request"></i></a>
                                </div>

                                <div class="media-body">
                                    Drop the IE <a href="error_500.html#">specific hacks</a> for temporal inputs
                                    <div class="text-muted font-size-sm">4 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="error_500.html#" class="btn bg-transparent border-warning text-warning rounded-round border-2 btn-icon"><i class="icon-git-commit"></i></a>
                                </div>

                                <div class="media-body">
                                    Add full font overrides for popovers and tooltips
                                    <div class="text-muted font-size-sm">36 minutes ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="error_500.html#" class="btn bg-transparent border-info text-info rounded-round border-2 btn-icon"><i class="icon-git-branch"></i></a>
                                </div>

                                <div class="media-body">
                                    <a href="error_500.html#">Chris Arney</a> created a new <span class="font-weight-semibold">Design</span> branch
                                    <div class="text-muted font-size-sm">2 hours ago</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="error_500.html#" class="btn bg-transparent border-success text-success rounded-round border-2 btn-icon"><i class="icon-git-merge"></i></a>
                                </div>

                                <div class="media-body">
                                    <a href="error_500.html#">Eugene Kopyov</a> merged <span class="font-weight-semibold">Master</span> and <span class="font-weight-semibold">Dev</span> branches
                                    <div class="text-muted font-size-sm">Dec 18, 18:36</div>
                                </div>
                            </li>

                            <li class="media">
                                <div class="mr-3">
                                    <a href="error_500.html#" class="btn bg-transparent border-primary text-primary rounded-round border-2 btn-icon"><i class="icon-git-pull-request"></i></a>
                                </div>

                                <div class="media-body">
                                    Have Carousel ignore keyboard events
                                    <div class="text-muted font-size-sm">Dec 12, 05:46</div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="dropdown-content-footer bg-light">
                        <a href="error_500.html#" class="text-grey mr-auto">All updates</a>
                        <div>
                            <a href="error_500.html#" class="text-grey" data-popup="tooltip" title="Mark all as read"><i class="icon-radio-unchecked"></i></a>
                            <a href="error_500.html#" class="text-grey ml-2" data-popup="tooltip" title="Bug tracker"><i class="icon-bug2"></i></a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>

        {{--        <span class="badge bg-success ml-md-3 mr-md-auto">Online</span>--}}

        {{--        <ul class="navbar-nav">--}}
        {{--            <li class="nav-item dropdown">--}}
        {{--                <a href="error_500.html#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">--}}
        {{--                    <i class="icon-people"></i>--}}
        {{--                    <span class="d-md-none ml-2">Users</span>--}}
        {{--                </a>--}}

        {{--                <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-300">--}}
        {{--                    <div class="dropdown-content-header">--}}
        {{--                        <span class="font-weight-semibold">Users online</span>--}}
        {{--                        <a href="error_500.html#" class="text-default"><i class="icon-search4 font-size-base"></i></a>--}}
        {{--                    </div>--}}

        {{--                    <div class="dropdown-content-body dropdown-scrollable">--}}
        {{--                        <ul class="media-list">--}}
        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face18.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <a href="error_500.html#" class="media-title font-weight-semibold">Jordana Ansley</a>--}}
        {{--                                    <span class="d-block text-muted font-size-sm">Lead web developer</span>--}}
        {{--                                </div>--}}
        {{--                                <div class="ml-3 align-self-center"><span class="badge badge-mark border-success"></span></div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face24.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <a href="error_500.html#" class="media-title font-weight-semibold">Will Brason</a>--}}
        {{--                                    <span class="d-block text-muted font-size-sm">Marketing manager</span>--}}
        {{--                                </div>--}}
        {{--                                <div class="ml-3 align-self-center"><span class="badge badge-mark border-danger"></span></div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face17.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <a href="error_500.html#" class="media-title font-weight-semibold">Hanna Walden</a>--}}
        {{--                                    <span class="d-block text-muted font-size-sm">Project manager</span>--}}
        {{--                                </div>--}}
        {{--                                <div class="ml-3 align-self-center"><span class="badge badge-mark border-success"></span></div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face19.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <a href="error_500.html#" class="media-title font-weight-semibold">Dori Laperriere</a>--}}
        {{--                                    <span class="d-block text-muted font-size-sm">Business developer</span>--}}
        {{--                                </div>--}}
        {{--                                <div class="ml-3 align-self-center"><span class="badge badge-mark border-warning-300"></span></div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face16.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <a href="error_500.html#" class="media-title font-weight-semibold">Vanessa Aurelius</a>--}}
        {{--                                    <span class="d-block text-muted font-size-sm">UX expert</span>--}}
        {{--                                </div>--}}
        {{--                                <div class="ml-3 align-self-center"><span class="badge badge-mark border-grey-400"></span></div>--}}
        {{--                            </li>--}}
        {{--                        </ul>--}}
        {{--                    </div>--}}

        {{--                    <div class="dropdown-content-footer bg-light">--}}
        {{--                        <a href="error_500.html#" class="text-grey mr-auto">All users</a>--}}
        {{--                        <a href="error_500.html#" class="text-grey"><i class="icon-gear"></i></a>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </li>--}}

        {{--            <li class="nav-item dropdown">--}}
        {{--                <a href="error_500.html#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown">--}}
        {{--                    <i class="icon-bubbles4"></i>--}}
        {{--                    <span class="d-md-none ml-2">Messages</span>--}}
        {{--                    <span class="badge badge-pill bg-warning-400 ml-auto ml-md-0">2</span>--}}
        {{--                </a>--}}

        {{--                <div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">--}}
        {{--                    <div class="dropdown-content-header">--}}
        {{--                        <span class="font-weight-semibold">Messages</span>--}}
        {{--                        <a href="error_500.html#" class="text-default"><i class="icon-compose"></i></a>--}}
        {{--                    </div>--}}

        {{--                    <div class="dropdown-content-body dropdown-scrollable">--}}
        {{--                        <ul class="media-list">--}}
        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3 position-relative">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face10.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}

        {{--                                <div class="media-body">--}}
        {{--                                    <div class="media-title">--}}
        {{--                                        <a href="error_500.html#">--}}
        {{--                                            <span class="font-weight-semibold">James Alexander</span>--}}
        {{--                                            <span class="text-muted float-right font-size-sm">04:58</span>--}}
        {{--                                        </a>--}}
        {{--                                    </div>--}}

        {{--                                    <span class="text-muted">who knows, maybe that would be the best thing for me...</span>--}}
        {{--                                </div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3 position-relative">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face3.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}

        {{--                                <div class="media-body">--}}
        {{--                                    <div class="media-title">--}}
        {{--                                        <a href="error_500.html#">--}}
        {{--                                            <span class="font-weight-semibold">Margo Baker</span>--}}
        {{--                                            <span class="text-muted float-right font-size-sm">12:16</span>--}}
        {{--                                        </a>--}}
        {{--                                    </div>--}}

        {{--                                    <span class="text-muted">That was something he was unable to do because...</span>--}}
        {{--                                </div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face24.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <div class="media-title">--}}
        {{--                                        <a href="error_500.html#">--}}
        {{--                                            <span class="font-weight-semibold">Jeremy Victorino</span>--}}
        {{--                                            <span class="text-muted float-right font-size-sm">22:48</span>--}}
        {{--                                        </a>--}}
        {{--                                    </div>--}}

        {{--                                    <span class="text-muted">But that would be extremely strained and suspicious...</span>--}}
        {{--                                </div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face4.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <div class="media-title">--}}
        {{--                                        <a href="error_500.html#">--}}
        {{--                                            <span class="font-weight-semibold">Beatrix Diaz</span>--}}
        {{--                                            <span class="text-muted float-right font-size-sm">Tue</span>--}}
        {{--                                        </a>--}}
        {{--                                    </div>--}}

        {{--                                    <span class="text-muted">What a strenuous career it is that I've chosen...</span>--}}
        {{--                                </div>--}}
        {{--                            </li>--}}

        {{--                            <li class="media">--}}
        {{--                                <div class="mr-3">--}}
        {{--                                    <img src="../../../../global_assets/images/demo/users/face25.jpg" width="36" height="36" class="rounded-circle" alt="">--}}
        {{--                                </div>--}}
        {{--                                <div class="media-body">--}}
        {{--                                    <div class="media-title">--}}
        {{--                                        <a href="error_500.html#">--}}
        {{--                                            <span class="font-weight-semibold">Richard Vango</span>--}}
        {{--                                            <span class="text-muted float-right font-size-sm">Mon</span>--}}
        {{--                                        </a>--}}
        {{--                                    </div>--}}

        {{--                                    <span class="text-muted">Other travelling salesmen live a life of luxury...</span>--}}
        {{--                                </div>--}}
        {{--                            </li>--}}
        {{--                        </ul>--}}
        {{--                    </div>--}}

        {{--                    <div class="dropdown-content-footer justify-content-center p-0">--}}
        {{--                        <a href="error_500.html#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="Load more"><i class="icon-menu7 d-block top-0"></i></a>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </li>--}}

        {{--            <li class="nav-item dropdown dropdown-user">--}}
        {{--                <a href="error_500.html#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">--}}
        {{--                    <img src="../../../../global_assets/images/demo/users/face11.jpg" class="rounded-circle mr-2" height="34" alt="">--}}
        {{--                    <span>Victoria</span>--}}
        {{--                </a>--}}

        {{--                <div class="dropdown-menu dropdown-menu-right">--}}
        {{--                    <a href="error_500.html#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>--}}
        {{--                    <a href="error_500.html#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>--}}
        {{--                    <a href="error_500.html#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>--}}
        {{--                    <div class="dropdown-divider"></div>--}}
        {{--                    <a href="error_500.html#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>--}}
        {{--                    <a href="error_500.html#" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>--}}
        {{--                </div>--}}
        {{--            </li>--}}
        {{--        </ul>--}}
    </div>
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Content area -->
        <div class="content d-flex justify-content-center align-items-center">

            <!-- Container -->
            <div class="flex-fill">

                <!-- Error title -->
                <div class="text-center mb-3">
                    <h1 class="error-title">403</h1>
                    <h5>Oops, You are not allowed to view this page. Permission Denied!</h5>
                    <p> You accessed  the wrong side of the app. <br> If you feel you need this permission please contact support.</p>

                </div>
                <!-- /error title -->


                <!-- Error content -->
                <div class="row">
                    <div class="col-xl-4 offset-xl-4 col-md-8 offset-md-2">

                        <!-- Search -->
                        <form action="javascript:void(0)">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control form-control-lg" placeholder="Paste the url here">

                                <div class="input-group-append">
                                    <button style="background-color: rgb(255, 148, 61)" type="submit" class="btn bg-slate-600 btn-icon btn-lg"><i class="icon-search4"></i></button>
                                </div>
                            </div>
                        </form>
                        <!-- /search -->


                        <!-- Buttons -->
                        <div class="row">
                            <div class="col-sm-6">
                                <a style="background-color: rgb(255, 148, 61)" href="{{ url('login')}}" class="btn btn-primary btn-block"><i class="icon-home4 mr-2"></i> Go To Dashboard</a>
                            </div>

                            <div class="col-sm-6">
                                <a href="mailto:daniel.tarus@calltronix.co.ke" class="btn btn-light btn-block mt-3 mt-sm-0"><i class="icon-mail5 mr-2"></i> EMAIL IT SUPPORT</a>
                            </div>
                        </div>
                        <!-- /buttons -->

                    </div>
                </div>
                <!-- /error wrapper -->

            </div>
            <!-- /container -->

        </div>
        <!-- /content area -->


        <div class="navbar navbar-expand-lg navbar-light">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                        data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Calltronix Internal CRM
                </button>
            </div>

        </div>

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->
<footer class="footer text-center text-sm-center">&copy; {{date('Y')}} Calltronix CRM | <span style="background-color: rgb(255, 148, 61)" class="badge badge-info">Version 1.0</span>
    {{--                <span class="text-muted d-none d-sm-inline-block float-right">Crafted with--}}
    {{--                    <i class="mdi mdi-heart text-danger"></i> by Calltronix Kenya Limited</span>--}}
</footer>
</body>
</html>
