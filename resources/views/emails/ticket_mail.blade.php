@component('mail::ticket',['ticket'=>$ticket,'data'=>$data])

    Kind Regards, <br>
    {{ config('app.name') }}
    <hr>
    <div style="width:100%">
        <div style="align-content: center">
            <small>Note: This is a system generated mail. Please <b>DO NOT</b> reply to it.</small>
        </div>
    </div>
@endcomponent
