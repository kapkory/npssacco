@extends('beautymail::templates.minty')
{{--@extends('beautymail::templates.widgets')--}}

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title" style="width: 100%">
            Dear <b>{{  $data['name'] }}</b>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            {!! $data['message'] !!}

        </td>

    </tr>

    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'View', 'link' => $data['link']])
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop
