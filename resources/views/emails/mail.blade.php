@component('mail::message')
    # Dear {{$content['user_name']}}

    {{ wordwrap($content['message'],60,"\n") }}
    {{"\n"}}
    {{ wordwrap($content['password'],60,"\n") }}
    {{"\n"}}
    {{ wordwrap($content['instruction'],60,"\n") }}

   Kind Regards, <br>
    {{ config('app.name') }}
    <hr>
    <div style="width:100%">
        <div style="align-content: center">
            <small>Note: This is a system generated mail. Please <b>DO NOT</b> reply to it.</small>
        </div>
    </div>
@endcomponent
