@extends('beautymail::templates.minty')
{{--@extends('beautymail::templates.widgets')--}}

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title" style="width: 100%">
            Dear <b>{{ ($data['name'] == "System") ? $data['assigned_to'] : $data['name'] }}</b>
            <strong style="color:#116fe2; float:right !important;">({{ $data['title_type'] }})</strong>
        </td>
    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    <tr>
        <td class="paragraph">
            {!! $data['message'] !!}

        </td>

    </tr>
    <tr>
        <td width="100%" height="10"></td>
    </tr>
    @if(!$data['is_acknowledgement'])
        @if($ticket->user_id == request()->user()->id)
            <tr>
                <td class="paragraph">
                    Please update the status of the pending ticket within the CRM to avoid further escalation.
                    <hr>
                </td>

            </tr>
        @endif
    @endif


    <tr>
        <td width="100%" style="text-align: center;"><h4>Ticket Information</h4></td>

    </tr>
    <tr>
        <td width="100%">

            <table style="width:100%; border: 1px solid black;border-collapse: collapse;">
                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Ticket Number:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">
                        CC{{$ticket->id}}</td>
                </tr>
                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Ticket Status:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{$ticket->status}}</td>
                </tr>
{{--                <tr>--}}
{{--                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">--}}
{{--                        Ticket Priority:--}}
{{--                    </th>--}}
{{--                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{\App\Repositories\StatusRepository::getTicketPriority($ticket->priority)}}</td>--}}
{{--                </tr>--}}
                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Department:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{$ticket->department}}</td>
                </tr>

                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">Issue
                        Category:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{$ticket->issue_category}}</td>
                </tr>

                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Disposition:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{$ticket->disposition}}</td>
                </tr>
                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">Line
                        Of Business:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{$ticket->LOB}}</td>
                </tr>
                @if($ticket->tat)
                    <tr>
                        <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                            Turn Around Time:
                        </th>
                        <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{!! $data['time'] !!}</td>
                    </tr>
                @endif

                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Description:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">   {!! $data['description'] !!}</td>
                </tr>
                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Ticket Assigned To :
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{$data['assigned_to']}}</td>
                </tr>
                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Ticket Created By:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;">{{ (@$ticket->created_by) ? $ticket->created_by : "System" }}</td>
                </tr>
                <tr>
                    <th style=" padding: 5px;text-align: right;border: 1px solid black;border-collapse: collapse;">
                        Ticket Created At:
                    </th>
                    <td style=" padding: 5px;text-align: left;border: 1px solid black;border-collapse: collapse;"> {!! \Carbon\Carbon::parse($ticket->created_at)->isoFormat('Do MMM Y')."  &nbsp;&nbsp;<small><b>(".\Carbon\Carbon::parse($ticket->created_at)->format('Y-m-d h:i:s A').")</b></small>" !!}</td>
                </tr>
            </table>
            <hr>
            <br>
        </td>

    </tr>
    <tr>
        <td>
            @include('beautymail::templates.widgets.newfeatureStart')

            <h4 class="secondary"><strong>Last Comments</strong></h4>
            @foreach($ticket->TicketUpdates->take(10) as $key=> $ticket_update)
                {{--               @if($key>0)--}}
                <ul>
                    <li>
                        {!! wordwrap($ticket_update->comment,70,"<br>") !!}
                    </li>
                </ul>
                {{--               @endif--}}
            @endforeach

            @include('beautymail::templates.widgets.newfeatureEnd')
        </td>
    </tr>



    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'View Ticket', 'link' => $data['link']])
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop
